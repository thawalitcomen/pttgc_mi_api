<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Device_model extends CI_Model {
	
	const TABLE_NAME = 'device' ;
	
	function findByPk($id){
		$this->db->where('device_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	function insert($data){
	
		$this->db->insert(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();
		return $this->findByPk($insert_id);
	}
	
	function findByUuid($uuid , $room_id = null , $device_name=null , $is_active=0 , $type_id=null , $floor_id = null){
	$this->db->where('uuid', $uuid);
		$query = $this->db->get(self::TABLE_NAME) ;
		$deviceModel = $query->row() ;
		if(!empty($deviceModel)){
			$data = array(
				'room_id' => !empty($room_id)?$room_id:$deviceModel->room_id ,
				'device_name' => !empty($device_name)?$room_id:$deviceModel->device_name  ,
				'is_active' => !empty($is_active)?$room_id:$deviceModel->is_active  ,
				'update_time' => date('Y-m-d H:i:s') ,
				'type_id' => !empty($type_id)?$room_id:$deviceModel->type_id  ,
				'floor_id' => !empty($floor_id)?$room_id:$deviceModel->floor_id  
			
			) ;
			
			$this->db->where('device_id',$deviceModel->device_id) ;
			$this->db->update(self::TABLE_NAME ,$data);
			return $deviceModel ;
		}else{
			$data = array(
					'room_id' => $room_id ,
					'device_name' => $device_name ,
					'uuid' => $uuid ,
					'is_active' => $is_active ,
					'create_time' => date('Y-m-d H:i:s') ,
					'update_time' => date('Y-m-d H:i:s') ,
					'type_id' => $type_id ,
					'floor_id' => $floor_id 
		
			) ;
			$this->db->insert(self::TABLE_NAME ,$data) ;
			$insert_id = $this->db->insert_id();
			return $this->findByPk($insert_id);
		}
	}
	
}