<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signage_model extends CI_Model {
    const TABLE_NAME = 'signage' ;
	
	
    public function findByPk($id){
        $this->db->where('signage_id', $id);
        $query = $this->db->get(self::TABLE_NAME) ;
        return $query->row();
    }
    
    public function findByDeviceId($deviceId=null) {
        
        $param = null ;
        
        $sql = "select * from signage where is_active = 1 " ;
        $sql .= " and now() > publish_up  " ;
        $sql .= " and (publish_down is null OR now() < publish_down ) " ;
        $sql .= " and (device_require = 0  " ;
        if(!is_null($deviceId)){
            $sql .= " OR signage_id in ( select signage_id from signage_device where device_id = ? )  " ;
            $param = array($deviceId) ;
        }
        $sql .= " ) order by publish_up desc " ;
       
        $query = $this->db->query($sql,$param) ;
        return $query->row();
    }
    
}