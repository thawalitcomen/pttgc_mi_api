<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Event_attendee_model extends CI_Model
{

    const TABLE_NAME = 'event_attendee';


    public function __construct() {
        parent::__construct();
    }

	public function findByEventId($event_id) {
		$cache_key = 'Event_attendee_model-findByEventId_'. $event_id ;
		
		$result = $this->cache->memcached->get($event_id) ;
		if(empty($result)){
			
			$this->db->where( "event_id",$event_id ) ;
			$query = $this->db->get(self::TABLE_NAME) ;

			$tmp_result = NULL ;
			if ($query->num_rows() > 0){
				$tmp_result = $query->result();
			}
				
			if(!empty($tmp_result)){
				$this->cache->memcached->save($cache_key ,$tmp_result, 36000 ) ; // cache 360 secs
				$result = $tmp_result ;
			}
			
		}
		return $result ;
	}
}