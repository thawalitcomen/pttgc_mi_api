<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Event_queue_model extends CI_Model {
	const TABLE_NAME = 'event_queue' ;
	
	 public function findByPk($id){
		$this->db->where('event_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	public function findAllByUserId ($userId , $start = "" , $end = ""){
		
		if(empty($start)){
			$start = date ( 'Y-m-d H:i:s' );
		}
		
		if(empty($end)){
			$end = date ( 'Y-m-d H:i:s', strtotime ( $start . "+1 years" ) );
		}
		
		$sql = "select * from event where status_id in ( 1,2 ) " ;
		$sql .= " and ( (? between start_time and end_time) OR (? between start_time and end_time)  or  (? <= start_time and ? >= end_time) )  " ;
		$sql .= " and ( create_by = ( select contact_id from user where user_id = ? limit 1) or event_id in ( " ;
		$sql .= "        select event_id from event_attendee where 0=0 " ;
		$sql .= "        and contact_id = ? ) ) " ;		
		$sql .= " order by start_time " ;
		
		$param = array($start,$end,$start,$end,$userId,$userId);
		
		$query = $this->db->query($sql,$param) ;
		return $query->result();
	}
	
	public function update($data){
		$this->db->where('event_id', $data->event_id);
		$this->db->update(self::TABLE_NAME, $data);
	}
	
	public function findByRoom($roomId , $start = "" , $end = "" ){
		if(empty($start)){
			$start = date ( 'Y-m-d H:i:s' );
		}
		
		if(empty($end)){
			$end = date ( 'Y-m-d H:i:s', strtotime ( $start . "+1 years" ) );
		}
		
		
		$sql = "select * from event where 0=0 " ;
		$sql .= "and ( (? between start_time and end_time) OR (? between start_time and end_time)  or  (? <= start_time and ? >= end_time) )  " ;
		$sql .= "and ( room_id = ? ) " ;		
		$sql .= " order by start_time " ;
// 		echo $sql ; exit;
		$param = array($start,$end,$start,$end,$roomId);
		
		$query = $this->db->query($sql,$param) ;
		return $query->result();
	}
	
	function insert($data ){

		$this->db->insert(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();

		
		return $this->findByPk($insert_id);
	}
	
	public function findNextEventByRoom($room_id, $start_time=null ){
		
		if(empty($start_time)){
			$start_time = date('Y-m-d H:i:s') ;
		}
		$param = array($room_id,$start_time) ;
		$sql = " select * from event where room_id = ? " ;
		$sql .= " AND start_time > ? " ;
		$sql .= " 	order by start_time limit 2" ;
		
		$query = $this->db->query($sql,$param) ;
		return $query->result();
	}
	
	
	
	


}
 