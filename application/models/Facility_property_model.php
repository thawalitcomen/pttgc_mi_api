<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facility_property_model extends CI_Model {
	
	const TABLE_NAME = 'facility_property' ;

	function findByPk($id,$property){
		$query = $this->db->get_where(self::TABLE_NAME, array('facility_id'=>$id,"property_name"=>$property)) ;
		return $query->row() ;
	}
	

}
