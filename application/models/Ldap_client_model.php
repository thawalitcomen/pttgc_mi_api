<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ldap_client_model extends CI_Model {

    /**
     * Perform LDAP Authentication
     *
     * @param  string $username The username to validate
     * @param  string $password The password to validate
     * @return boolean true for valid user otherwise return false
     */
    function perform_ldap_auth($username = '', $password = NULL, $domain = NULL) {

        $time_start = microtime(true);

        try {
            if (empty($username)) {
                log_message('debug', 'LDAP Auth: failure, empty username');
                return FALSE;
            }

            log_message('debug', 'LDAP Auth: Loading configuration');

            $this->config->load('ldap.php', TRUE);

            $ldap = [
                'timeout' => $this->config->item('timeout', 'ldap'),
                'host' => $this->config->item('server', 'ldap'),
                'port' => $this->config->item('port', 'ldap'),
                'rdn' => $this->config->item('binduser', 'ldap'),
                'pass' => $this->config->item('bindpw', 'ldap'),
                'basedn' => $this->config->item('basedn', 'ldap'),
                'defaultdn' => $this->config->item('defaultdn', 'ldap')
            ];

// 	        var_dump($ldap); exit;
// 	        echo("debug Login:[". $username . "|" . $password ."|". $domain) ; 

            log_message('debug', 'LDAP Auth: Connect to ' . (isset($ldaphost) ? $ldaphost : '[ldap not configured]'));

            // Connect to the ldap server
            $ldapconn = ldap_connect($ldap['host'], $ldap['port']);
            if ($ldapconn) {
                //log_message('debug', 'Setting timeout to ' . $ldap['timeout'] . ' seconds');
                log_message('debug', "Ldap connection=" . json_encode($ldapconn));

                ldap_set_option($ldapconn, LDAP_OPT_NETWORK_TIMEOUT, $ldap['timeout']);
                ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);
                ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);


                if (empty($domain)) {
                    $domain = $this->config->item('domain_default', 'ldap');
                }

                $admin = $domain . "\\" . $ldap['rdn'];
                log_message('debug', 'LDAP Auth: Binding to ' . $ldap['host'] . ' with dn ' . $admin);
                // Binding to the ldap server
                $ldapbind = ldap_bind($ldapconn, $admin, $ldap['pass']);
// 				var_dump($ldapbind); exit;
                // Verify the binding
                if ($ldapbind === FALSE) {
                    log_message('error', 'LDAP Auth: bind was unsuccessful');
                    return FALSE;
                }

                log_message('debug', 'LDAP Auth: bind successful');
            }

            log_message('debug', 'domain=' . $domain . ',username=' . $username);
            if (!empty($domain)) {
// 	        	$newBaseDn = 'CN=Users,DC='. $domain . ",DC=" . $ldap['basedn'] ; 
                $newBaseDn = 'DC=' . $domain . ",DC=" . $ldap['basedn'];

// 	        	$dcList = explode('.',$domain) ;
// 	        	foreach ($dcList as $dc){
// 	        		$newBaseDn .= (",DC=".$dc) ;
// 	        	}
// 	        	$username = explode( "\\", $username ) [1];
                $ldap['basedn'] = $newBaseDn;
            } else {
// 	        	$newBaseDn = 'CN=Users,DC='. $ldap['defaultdn'] . ",DC=" . $ldap['basedn'] ;
                $newBaseDn = 'DC=' . $ldap['defaultdn'] . ",DC=" . $ldap['basedn'];
                // 	        	$dcList = explode('.',$domain) ;
                // 	        	foreach ($dcList as $dc){
                // 	        		$newBaseDn .= (",DC=".$dc) ;
                // 	        	}

                $ldap['basedn'] = $newBaseDn;
            }
            log_message('debug', 'newBaseDn=' . $newBaseDn);

// 	        var_dump($ldapconn) ; exit;
// 	        echo("debug Login:[". $username . "| newBaseDn " . $newBaseDn."|". $domain .", userName" . $username) ; exit;
// 	        $command = "(&(objectCategory=person)(objectClass=user)(!(displayName=CN*))(!(displayName=NUL))(sAMAccountName=miuser))" ;
            $command = ("(&(objectCategory=person)(objectClass=user)(!(displayName=CN*))(!(displayName=NUL))(sAMAccountName=$username))");
// 	        $aaa = ldap_search($ldapconn, $newBaseDn ,$command) ;
// 	        var_dump($aaa) ; exit;
            log_message("debug", "Command : " . $command);
            // Search for user
// 	        if (($res_id = ldap_search($ldapconn, $newBaseDn , "sAMAccountName=$username")) === FALSE)
            if (($res_id = ldap_search($ldapconn, $newBaseDn, $command)) === FALSE) {
                log_message('error', 'LDAP Auth: User ' . $username . ' not found in search');
                return FALSE;
            }
            log_message("debug", "res_id : " . $res_id);
            if (ldap_count_entries($ldapconn, $res_id) !== 1) {
                log_message('error', 'LDAP Auth: Failure, username ' . $username . 'found more than once');
                return FALSE;
            }

            if (($entry_id = ldap_first_entry($ldapconn, $res_id)) === FALSE) {
                log_message('error', 'LDAP Auth: Failure, entry of search result could not be fetched');
                return FALSE;
            }

            if (($user_dn = ldap_get_dn($ldapconn, $entry_id)) === FALSE) {
                log_message('error', 'LDAP Auth: Failure, user-dn could not be fetched');
                return FALSE;
            }

            log_message("debug", "user:" . $username . "|" . $password . "|" . $ldapconn);
            $link_id = ldap_bind($ldapconn, $domain . "\\" . $username, $password);
            log_message('debug', 'LDAP Auth: user_dn: ' . $username . ', password:' . $password . ', link_id=' . json_encode($link_id));
// 	      	var_dump($link_id) ; exit;
// 	       	echo "Link::".json_encode($link_id) ; exit;
            // User found, could not authenticate as user
            if (!$link_id || empty($link_id) || $link_id === FALSE) {
                log_message('error', 'LDAP Auth: Failure, username/password did not match: ' . $user_dn);
                return FALSE;
            }

            log_message('debug', 'LDAP Auth: Success ' . $user_dn . ' authenticated successfully');

            $this->_user_ldap_dn = $user_dn;

            ldap_close($ldapconn);


            $time_end = microtime(true);
            $time = $time_end - $time_start;
            log_message('debug', "LDAP Authen Time: $time_start - $time_end  usage_time is $time seconds");

            return $user_dn;
        } catch (Exception $e) {
//     		echo "Error:".json_encode($e)  ;
            log_message("error", json_encode($e));
        }
        return false;
    }

    /**
     * Perform LDAP Get user
     *
     * @param int $uid
     * @return mix return false for no data otherwise return user data
     */
    function get_user_by_id($uid = '') {
        if (empty($uid)) {
            log_message('debug', 'LDAP Get user: failure, empty uid');
            return FALSE;
        }

        log_message('debug', 'LDAP Get user: Loading configuration');

        $this->config->load('ldap.php', TRUE);

        $ldap = [
            'timeout' => $this->config->item('timeout', 'ldap'),
            'host' => $this->config->item('server', 'ldap'),
            'port' => $this->config->item('port', 'ldap'),
            'rdn' => $this->config->item('binduser', 'ldap'),
            'pass' => $this->config->item('bindpw', 'ldap'),
            'basedn' => $this->config->item('basedn', 'ldap'),
        ];

        log_message('debug', 'LDAP Get user: Connect to ' . (isset($ldaphost) ? $ldaphost : '[ldap not configured]'));

        // Connect to the ldap server
        $ldapconn = ldap_connect($ldap['host'], $ldap['port']);
        if ($ldapconn) {
            log_message('debug', 'Setting timeout to ' . $ldap['timeout'] . ' seconds');

            ldap_set_option($ldapconn, LDAP_OPT_NETWORK_TIMEOUT, $ldap['timeout']);

            log_message('debug', 'LDAP Get user: Binding to ' . $ldap['host'] . ' with dn ' . $ldap['rdn']);

            // Binding to the ldap server
            $ldapbind = ldap_bind($ldapconn, $ldap['rdn'], $ldap['pass']);

            // Verify the binding
            if ($ldapbind === FALSE) {
                log_message('error', 'LDAP Get user: bind was unsuccessful');
                return FALSE;
            }

            log_message('debug', 'LDAP Get user: bind successful');
        }

        // Search for user
        if (($res_id = ldap_search($ldapconn, $ldap['basedn'], "uid=$uid")) === FALSE) {
            log_message('error', 'LDAP Get user: User ' . $uid . ' not found in search');
            return FALSE;
        }

        if (ldap_count_entries($ldapconn, $res_id) !== 1) {
            log_message('error', 'LDAP Get user: Failure, uid ' . $uid . 'found more than once');
            return FALSE;
        }

        if (($entry_id = ldap_first_entry($ldapconn, $res_id)) === FALSE) {
            log_message('error', 'LDAP Get user: Failure, entry of search result could not be fetched');
            return FALSE;
        }

        if (($user_attrs = ldap_get_attributes($ldapconn, $entry_id)) === FALSE) {
            log_message('error', 'LDAP Get user: Failure, user-dn could not be fetched');
            return FALSE;
        }

        ldap_close($ldapconn);

        return $user_attrs;
    }

    function perform_ldap_auth_for_chpass($username = '', $password = NULL, $domain = NULL) {

        $time_start = microtime(true);

        try {
            if (empty($username)) {
                log_message('debug', 'LDAP Auth: failure, empty username');
                return FALSE;
            }

            log_message('debug', 'LDAP Auth: Loading configuration');

            $this->config->load('ldap.php', TRUE);

            $ldap = [
                'timeout' => $this->config->item('timeout', 'ldap'),
                'host' => $this->config->item('server', 'ldap'),
                'port' => $this->config->item('port', 'ldap'),
                'rdn' => $this->config->item('binduser', 'ldap'),
                'pass' => $this->config->item('bindpw', 'ldap'),
                'basedn' => $this->config->item('basedn', 'ldap'),
                'defaultdn' => $this->config->item('defaultdn', 'ldap')
            ];

            // 	        var_dump($ldap); exit;
            // 	        echo("debug Login:[". $username . "|" . $password ."|". $domain) ;

            log_message('debug', 'LDAP Auth: Connect to ' . (isset($ldaphost) ? $ldaphost : '[ldap not configured]'));

            // Connect to the ldap server
            $ldapconn = ldap_connect($ldap['host'], $ldap['port']);
            if ($ldapconn) {
                log_message('debug', 'Setting timeout to ' . $ldap['timeout'] . ' seconds');

                ldap_set_option($ldapconn, LDAP_OPT_NETWORK_TIMEOUT, $ldap['timeout']);
                ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);
                ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);


                log_message('debug', 'LDAP Auth: Binding to ' . $ldap['host'] . ' with dn ' . $ldap['rdn']);

                // Binding to the ldap server
                $ldapbind = ldap_bind($ldapconn, $ldap['rdn'], $ldap['pass']);
                // 				var_dump($ldapbind); exit;
                // Verify the binding
                if ($ldapbind === FALSE) {
                    log_message('error', 'LDAP Auth: bind was unsuccessful');
                    return FALSE;
                }

                log_message('debug', 'LDAP Auth: bind successful');
            }

            log_message('debug', 'domain=' . $domain . ',username=' . $username);
            if (!empty($domain)) {
                // 	        	$newBaseDn = 'CN=Users,DC='. $domain . ",DC=" . $ldap['basedn'] ;
                $newBaseDn = 'DC=' . $domain . ",DC=" . $ldap['basedn'];
                // 	        	$dcList = explode('.',$domain) ;
                // 	        	foreach ($dcList as $dc){
                // 	        		$newBaseDn .= (",DC=".$dc) ;
                // 	        	}
                // 	        	$username = explode( "\\", $username ) [1];
                $ldap['basedn'] = $newBaseDn;
            } else {
                // 	        	$newBaseDn = 'CN=Users,DC='. $ldap['defaultdn'] . ",DC=" . $ldap['basedn'] ;
                $newBaseDn = 'DC=' . $ldap['defaultdn'] . ",DC=" . $ldap['basedn'];
                // 	        	$dcList = explode('.',$domain) ;
                // 	        	foreach ($dcList as $dc){
                // 	        		$newBaseDn .= (",DC=".$dc) ;
                // 	        	}

                $ldap['basedn'] = $newBaseDn;
            }
            // 	        var_dump($ldapconn) ; exit;
            // 	        echo("debug Login:[". $username . "| newBaseDn " . $newBaseDn."|". $domain .", userName" . $username) ; exit;
            // 	        $command = "(&(objectCategory=person)(objectClass=user)(!(displayName=CN*))(!(displayName=NUL))(sAMAccountName=miuser))" ;
            $command = ("(&(objectCategory=person)(objectClass=user)(!(displayName=CN*))(!(displayName=NUL))(sAMAccountName=$username))");
            // 	        $aaa = ldap_search($ldapconn, $newBaseDn ,$command) ;
            // 	        var_dump($aaa) ; exit;
            log_message("debug", "Command : " . $command);
            // Search for user
            // 	        if (($res_id = ldap_search($ldapconn, $newBaseDn , "sAMAccountName=$username")) === FALSE)
            if (($res_id = ldap_search($ldapconn, $newBaseDn, $command)) === FALSE) {
                log_message('error', 'LDAP Auth: User ' . $username . ' not found in search');
                return FALSE;
            }
            log_message("debug", "res_id : " . $res_id);
            if (ldap_count_entries($ldapconn, $res_id) !== 1) {
                log_message('error', 'LDAP Auth: Failure, username ' . $username . 'found more than once');
                return FALSE;
            }

            if (($entry_id = ldap_first_entry($ldapconn, $res_id)) === FALSE) {
                log_message('error', 'LDAP Auth: Failure, entry of search result could not be fetched');
                return FALSE;
            }

            if (($user_dn = ldap_get_dn($ldapconn, $entry_id)) === FALSE) {
                log_message('error', 'LDAP Auth: Failure, user-dn could not be fetched');
                return FALSE;
            }

            // 	        echo "user:".$user_dn. "|" . $password . "|" . $ldapconn ."<br><br>" ;
            $link_id = ldap_bind($ldapconn, $user_dn, $password);
            log_message('debug', 'LDAP Auth: user_dn: ' . $user_dn . ', password:' . $password . ', link_id=' . json_encode($link_id));
            // 	      	var_dump($link_id) ; exit;
            // 	       	echo "Link::".json_encode($link_id) ; exit;
            // User found, could not authenticate as user
            if (!$link_id || empty($link_id) || $link_id === FALSE) {
                log_message('error', 'LDAP Auth: Failure, username/password did not match: ' . $user_dn);
                return FALSE;
            }

            log_message('debug', 'LDAP Auth: Success ' . $user_dn . ' authenticated successfully');

            $this->_user_ldap_dn = $user_dn;

            ldap_close($ldapconn);


            $time_end = microtime(true);
            $time = $time_end - $time_start;
            log_message('debug', "LDAP Authen Time: $time_start - $time_end  usage_time is $time seconds");


            $auth = array(
                'ldap_conn' => $ldapconn,
                'ldap_link' => $link_id,
                'user_dn' => $user_dn
            );

            return $auth;
        } catch (Exception $e) {
            echo "Error:" . json_encode($e);
        }
        return false;
    }

}
