<?php
class Client_model extends CI_Model {
	
	const TABLE_NAME = 'client' ;

	function findByPk($id){
		$this->db->where('client_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
// 	function findByUuid($uuid , $lat=null, $long=null,$app_id=null, $type=null, $os=null , $os_version=null, $app_version=null ,$push_token = null){
	function findByUuid($uuid , $lat="", $long="",$app_id="", $type="", $os="" , $os_version="", $app_version="" ,$push_token = ""){
	
		try{
		$this->db->where('uuid', $uuid);
		$query = $this->db->get(self::TABLE_NAME) ;
		$clientModel = $query->row() ;
		if(!empty($clientModel)){
			$data = array(
					'latitude' => !empty($lat)?$lat:$clientModel->latitude ,
					'longitude' => !empty($long)?$long:$clientModel->longitude ,
					'client_type' => !empty($type)?$type:$clientModel->client_type ,
					'app_id' => !empty($app_id)?$app_id:$clientModel->app_id ,
					'os' => !empty($os)?$os:$clientModel->os ,
					'os_version' => !empty($os_version)?$os_version:$clientModel->os_version ,
					'app_version' => !empty($app_version)?$app_version:$clientModel->app_version ,
					'updated_at' => date('Y-m-d H:i:s') ,
					'push_token' => !empty($push_token)?$push_token:$clientModel->push_token ,
			
			) ;
			
			$this->db->where('client_id',$clientModel->client_id) ;
			$this->db->update(self::TABLE_NAME ,$data);
			return $clientModel ;
		}else{
			$data = array(
				'uuid' => $uuid ,
					'latitude' => $lat ,
					'longitude' => $long ,
					'client_type' => $type ,
					'app_id' => $app_id ,
					'os' => $os  ,
					'os_version' => $os_version ,
					'app_version' => $app_version ,
					'created_at' =>date('Y-m-d H:i:s') ,
					'updated_at' =>date('Y-m-d H:i:s'),
					'push_token' => $push_token 
		
			) ;
			$this->db->insert(self::TABLE_NAME ,$data) ;
			$insert_id = $this->db->insert_id();
			return $this->findByPk($insert_id);
		}
		
		}catch (Exception $e){
			log_message("error", "Exception Error :" . json_encode($message)) ;
		}
	}
	
	public function findByContactId($contact_id) {
		$sql = "select * from client " ;
		$sql .= "where  client_id in ( " ;
		$sql .= "	select client_id from user_token  " ;
		$sql .= "	where user_id in (  " ;
		$sql .= "		select user_id from user  " ;
		$sql .= "		where contact_id = ?  " ;
		$sql .= "	) " ;
		$sql .= ") " ;
		
		$query = $this->db->query($sql,array($contact_id)) ;
		return $query->result();
	}
}