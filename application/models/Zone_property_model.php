<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Zone_property_model extends CI_Model {
	
	const TABLE_NAME = 'zone_property' ;
	
	
	public function findAllByZoneId($id){
		$query = $this->db->get_where(self::TABLE_NAME, array('zone_id'=>$id)) ;
		return $query->result();
	}
	
	
}