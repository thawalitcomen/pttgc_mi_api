<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Theme_model extends CI_Model {
	
	const TABLE_NAME = 'theme' ;
	
	function findByPk($id){
		$this->db->where( "theme_id",$id ) ;
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	function findByRoomId($room_id){
		$sql = "select * from theme where theme_id in( select theme_id from room_theme where room_id = ? ) " ;
		$param = array($room_id) ;
		$query = $this->db->query($sql,$param) ;
		return $query->row();
	}

}