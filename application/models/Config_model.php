<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Config_model extends CI_Model
{

    const TABLE_NAME = 'config';


    public function __construct() {
        parent::__construct();
    }

    public function findByKey($key) {
        //query
//        $r = $this->db
//            ->get_where(self::TABLE_NAME, array('key' => $key))
//            ->row();

        $this->db->where('key', $key);
        $query = $this->db->get(self::TABLE_NAME) ;

//        log_message("debug","Config last query :" . $this->db->last_query() ) ;
        return $query->row();
    }

}