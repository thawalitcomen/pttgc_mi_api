<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room_type_model extends CI_Model {
	
	const TABLE_NAME = 'room_type' ;

	function findAll(){

		$cacheKey = "Room_type_model-findAll"  ;

		$result = $this->cache->memcached->get($cacheKey) ;

		if(empty($result)){

			$query = $this->db->get(self::TABLE_NAME) ;
			$tmp_result =  $query->result() ;

			if(!empty($tmp_result)){
				$this->cache->memcached->save($cacheKey ,$tmp_result, 86400 ) ; // cache 24 hour
				$result = $tmp_result ;
			}

		}
		return $result ;
	}
	public function findByPk($id){
		$this->db->where('room_type_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
}
