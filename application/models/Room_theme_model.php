<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room_theme_model extends CI_Model {
	
	const TABLE_NAME = 'room_theme' ;


	function findByPk($id){
		$this->db->where('room_theme_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	

	function findByEmail($email){
		$this->db->where('email', $email);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	function insert($data){

		$this->db->insert(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();
		return $this->findByPk($insert_id);
	}
}
