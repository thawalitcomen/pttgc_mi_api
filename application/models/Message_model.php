<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message_model extends CI_Model {

	const TABLE_NAME = 'message' ;

	function findByPk($id){
		$this->db->where('message_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}

	function insert($data){

		$this->db->insert(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();
		return $this->findByPk($insert_id);
	}

	function findAll(){

		$cacheKey = "Message_model-findAll"  ;

		$result = $this->cache->memcached->get($cacheKey) ;

		if(empty($result)){
			$this->db->where('is_active',1) ;
			$query = $this->db->get(self::TABLE_NAME) ;
			$tmp_result =  $query->result() ;

			if(!empty($tmp_result)){
				$this->cache->memcached->save($cacheKey ,$tmp_result, 86400 ) ; // cache 24 hour
				$result = $tmp_result ;
			}

		}
		return $result ;
	}
	//
	// function findAllWithLanguage(){
	//
	// 	$cacheKey = "Message_model-findAllWithLanguage"  ;
	//
	// 	$result = $this->cache->memcached->get($cacheKey) ;
	//
	// 	if(empty($result)){
	//
	// 		$this->db->select('*');
	// 		$this->db->from(self::TABLE_NAME);
	// 		$this->db->join("language","language.language_id = ". self::TABLE_NAME .".language_id");
	// 		$this->db->where(self::TABLE_NAME .".is_active",1) ;
	// 		$query = $this->db->get() ;
	// 		$tmp_result =  $query->result() ;
	//
	// 		if(!empty($tmp_result)){
	// 			$this->cache->memcached->save($cacheKey ,$tmp_result, 86400 ) ; // cache 24 hour
	// 			$result = $tmp_result ;
	// 		}
	//
	// 	}
	// 	return $result ;
	// }
}
