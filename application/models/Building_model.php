<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Building_model extends CI_Model {
	
	const TABLE_NAME = 'building' ;
	
	function findByPk($id){
		$this->db->where('building_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	function insert($data){
	
		$this->db->insert(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();
		return $this->findByPk($insert_id);
	}

	function findAll(){

		$cacheKey = "Building_model-findAll"  ;

		$result = $this->cache->memcached->get($cacheKey) ;

		if(empty($result)){

			$query = $this->db->get(self::TABLE_NAME) ;
			$tmp_result =  $query->result() ;

			if(!empty($tmp_result)){
				$this->cache->memcached->save($cacheKey ,$tmp_result, 86400 ) ; // cache 24 hour
				$result = $tmp_result ;
			}

		}
		return $result ;
	}
}
	