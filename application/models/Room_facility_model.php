<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room_facility_model extends CI_Model {
	
	const TABLE_NAME = 'room_facility' ;

	function findByPk($room_id,$facility_id){
            
            
		$this->db->where(array('room_id'=>$facility_id,"facility_id"=>$facility_id ));
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	function findAllByRoomId($room_id){
		
		$cacheKey = "Room_facility_model-findAllByRoomId". (!empty($room_id)?"_".$room_id:"" ) ;
		
		$result = $this->cache->memcached->get($cacheKey) ;

		if(empty($result)){
			
			$this->db->where('room_id',$room_id );
			$this->db->where('quantity>',0 );
			$query = $this->db->get(self::TABLE_NAME) ;
// 			return  $query->result() ; 
			$tmp_result =  $query->result() ;
			
			
			if(!empty($tmp_result)){
				$this->cache->memcached->save($cacheKey ,$tmp_result, 86400 ) ; // cache 24 hour
				$result = $tmp_result ;
			}
			
		}
		return $result ;
	}

	function findByRoomIdAndFacilityId($room_id , $facility_id , $quantity_require = 1){
		$cacheKey = "Room_facility_model-findByRoomIdAndFacilityId". (!empty($room_id)?"_".$room_id:"" ) . (!empty($facility_id)?"_".$facility_id:"" ). (!empty($quantity_require)?"_".$quantity_require:"" ) ;

//		log_message("debug","cacheKey=".$cacheKey) ;
		$result = $this->cache->memcached->get($cacheKey) ;

		if(empty($result)){

			$this->db->where('room_id',$room_id );
			$this->db->where('facility_id',$facility_id );
			$this->db->where('quantity >= ' ,$quantity_require ) ;
			$query = $this->db->get(self::TABLE_NAME) ;
			$tmp_result = $query->result() ;

			$str = $this->db->last_query();
//			log_message("debug","sql=".$str) ;

			if(!empty($tmp_result)){
				$this->cache->memcached->save($cacheKey ,$tmp_result, 86400 ) ; // cache 24 hour
				$result = $tmp_result ;
			}

		}

//		$last_query = $this->db->last_query();
//		log_message('debug','LAST-QUERY:'.$last_query) ;
		return $result ;
	}
}
