<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Facility_model extends CI_Model {
    
    
    const FACILITY_ID_CHAIR = 4 ;


    const TABLE_NAME = 'facility' ;
	
	function findByPk($facility_id){
		$this->db->where( "facility_id",$facility_id) ;
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}

	function findAllForSearch(){

		$cacheKey = "Facility_model-findAllForSearch" ;

		$result = $this->cache->memcached->get($cacheKey) ;
		if(empty($result)){

			$sql = "select * from facility " ;
			$sql .= "where search_enable = 1 " ;
			$sql .= "order by seq " ;
			$query = $this->db->query($sql) ;
			$tmp_result =  $query->result() ;


			if(!empty($tmp_result)){
				$this->cache->memcached->save($cacheKey ,$tmp_result, 86400 ) ; // cache 24 hour
				$result = $tmp_result ;
			}
		}
		return $result ;
	}

	
	
// 	function findAllByRoomId($room_id){
// 		$sql = "select facility.* from facility f inner join room_facility rf on f.facility_id = rf.facility_id " ;
// 		$sql .= " where rf.room_id = ? order by rf.seq " ;
		
// 		$param = array($room_id);
		
// 		$query = $this->db->query($sql,$param) ;
// 		return $query->result();
// 	}
}