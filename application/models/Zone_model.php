<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
	
	const TABLE_NAME = 'zone' ;
	
	
	public function findByPk($id){
		$this->db->where('zone_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	
}