<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Event_model extends CI_Model {
	const TABLE_NAME = 'event' ;
	
	 public function findByPk($id){
		$this->db->where('event_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	public function findAllByUserId ($userId , $start = "" , $end = ""){
		$result = NULL ;
		
		if(empty($start)){
			$start = date ( 'Y-m-d H:i:s' );
		}
		
		if(empty($end)){
			$end = date ( 'Y-m-d H:i:s', strtotime ( $start . "+1 years" ) );
		}
		
// 		$sql = "select * from event where status_id in ( 1 ) " ;
// 		$sql .= " and ( (? between start_time and end_time) OR (? between start_time and end_time)  or  (? <= start_time and ? >= end_time) )  " ;
// 		$sql .= " and ( create_by = ( select contact_id from user where user_id = ? limit 1) or event_id in ( " ;
// 		$sql .= "        select event_id from event_attendee where 0=0 " ;
// 		$sql .= "        and contact_id in( select contact_id from user where user_id = ?) ) ) " ;		
// 		$sql .= " order by start_time " ;
// 		$param = array($start,$end,$start,$end,$userId,$userId);
		
		$sql = "select * from event where status_id in ( 1 ) " ;
		$sql .= " and ( (? between start_time and end_time) OR (? between start_time and end_time)  or  (? <= start_time and ? >= end_time) )  " ;
		$sql .= " and ( create_by = ( select contact_id from user where user_id = ? limit 1)) " ;
		$sql .= " and ( room_id in ( select room_id from room where status_id = 1 )) " ;
		$sql .= " order by start_time " ;
		$param = array($start,$end,$start,$end,$userId);
		
		
		
		$query = $this->db->query($sql,$param) ;
		if ($query->num_rows() > 0){
			$result = $query->result();
		}

		return $result ;
	}
	
	public function update($data){
		$this->db->where('event_id', $data->event_id);
		$this->db->update(self::TABLE_NAME, $data);
	}
	
	public function findByRoom($roomId , $start = "" , $end = "" ){
		if(empty($start)){
			$start = date ( 'Y-m-d H:i:s' );
		}
		
		if(empty($end)){
			$end = date ( 'Y-m-d H:i:s', strtotime ( $start . "+1 years" ) );
		}
		
		
		$sql = "select * from event where 0=0 and status_id = 1 " ;
		$sql .= "and ( (? between start_time and end_time) OR (? between start_time and end_time)  or  (? <= start_time and ? >= end_time) )  " ;
		$sql .= "and ( room_id = ? ) " ;		
		$sql .= " order by start_time " ;
// 		echo $sql ; exit;
		$param = array($start,$end,$start,$end,$roomId);
		
		$query = $this->db->query($sql,$param) ;
// 		return $query->result();

		$result = NULL ;
		if ($query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result ;
	}
	
	function insert($data , $attendees = null){
		$this->db->trans_begin();
		
		$this->db->insert(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();
		if(!empty($insert_id) && !empty($attendees) && count($attendees) > 0){
			for($i=0;$i<count($attendees);$i++){
// 				echo json_encode($attendees[$i]) ;
				$attendees[$i]['event_id'] = $insert_id ;
				$this->db->insert('event_attendee', $attendees[$i]);
			}
		}
		if ($this->db->trans_status() === FALSE){
		    $this->db->trans_rollback();
		}else{
		    $this->db->trans_commit();
		}
		// 		echo "Insert ID:".$insert_id ;
		return $this->findByPk($insert_id);
	}
	
	public function findNextEventByRoom($room_id, $start_time=null ){
		
		if(empty($start_time)){
			$start_time = date('Y-m-d H:i:s') ;
		}
		$param = array($room_id,$start_time) ;
		$sql = " select * from event where room_id = ? and status_id = 1 " ;
		$sql .= " AND start_time > ? " ;
		$sql .= " AND DATE(start_time) = DATE(NOW()) " ;
		$sql .= " order by start_time limit 2" ;
		
		$query = $this->db->query($sql,$param) ;
// 		return $query->result();
		$result = NULL ;
		if ($query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result ;
	}
	
	
	
	


}
 