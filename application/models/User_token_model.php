<?php
class User_token_model extends CI_Model {
	const TABLE_NAME = 'user_token' ;
	
	function findByPk($id){
		$this->db->where('token_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	function findByClientId($clientId){
		$sql = "select * from user_token where client_id = ? and expired_at > now() " ;
		$query = $this->db->query($sql,array($clientId)) ;
		return $query->row();
	}
	
	function findByAccessToken($accessToken){
		$sql = "select * from user_token where access_token = ? and expired_at > now() " ;
		$query = $this->db->query($sql,array($accessToken)) ;
		return $query->row();
	}
	
	function insert($userId,$client_id,$photo,$expiredTime){
		$data = array(
				'user_id'=>$userId,
				'client_id'=>$client_id,
				'access_token'=>$photo,
				'created_at'=>date('Y-m-d H:i:s'),
				'expired_at'=>$expiredTime 
		);
		$this->db->insert(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();
		return $this->findByPk($insert_id);
	}
	
	function update($data){
		$this->db->where("token_id",$data->token_id) ;
		$this->db->update(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();
		return $this->findByPk($insert_id);
	}
}