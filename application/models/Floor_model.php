<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Floor_model extends CI_Model {
	
	const TABLE_NAME = 'floor' ;
	
	function findByPk($id){
		$this->db->where('floor_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	function insert($data){
	
		$this->db->insert(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();
		return $this->findByPk($insert_id);
	}

	function findAll(){

		$cacheKey = "Floor_model-findAll"  ;

		$result = $this->cache->memcached->get($cacheKey) ;

		if(empty($result)){

			$query = $this->db->get(self::TABLE_NAME) ;
			$tmp_result =  $query->result() ;

			if(!empty($tmp_result)){
				$this->cache->memcached->save($cacheKey ,$tmp_result, 86400 ) ; // cache 24 hour
				$result = $tmp_result ;
			}

		}
		return $result ;
	}
}
	