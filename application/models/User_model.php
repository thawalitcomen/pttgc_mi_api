<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	const TABLE_NAME = 'user' ;

	function findByPk($id){
		$this->db->where('user_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}

	function findByEmployeeId($employeeId){
		$sql = "select * from user where contact_id in (select contact_id from contact where employee_id = ? ) " ;
		$query = $this->db->query($sql,array($employeeId)) ;
		return $query->row();
	}

	function findByUsername($userName , $domain ){
		
// 		$this->db->where('user_name', $userName);
// 		$query = $this->db->get(self::TABLE_NAME) ;

		$sql = "select * from user where user_name = ? and company_id in(select company_id from company where domain_name = ? ) " ;
		$query = $this->db->query($sql,array($userName,$domain)) ;
		return $query->row();
	}

// 	function findByAccessTokenAndClientId($accessToken,$clientId){
// 		$sql = "select * from user "
// 				." where is_active = 1 "
// 				." and user_id in ( "
// 				."	select user_id from user_token "
// 				."  where access_token = ? and client_id = ? "
// 				.") " ;
// 		$query = $this->db->query($sql,array($accessToken,$clientId)) ;
// 		return $query->row();
// 	}

	function insert($data){
		$this->db->insert(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();
		return $this->findByPk($insert_id);
	}

	function findByContactId($contactId){
		$this->db->where('contact_id', $contactId);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	function findByVerifyCode($verify_code) {
		$sql = "select * from user "
				. " where is_active = 1 "
				. " and user_id in ( "
				. "	select user_id from ocr "
				. "  where verify_code = ? "
				. ") ";
		$query = $this->db->query ( $sql, array (
				$verify_code
		) );
		return $query->row ();
	}

	function update($data,$user_id){
		$this->db->where('user_id' , $user_id);
		$this->db->update(self::TABLE_NAME , $data);
		return $this->findByPk($user_id);
	}

	function findByMobile($mobile){
		$this->db->where('is_active', 1);
		$this->db->where('mobile', $mobile);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
}
