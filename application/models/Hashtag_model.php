<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hashtag_model extends CI_Model {

	const TABLE_NAME = 'hashtag' ;

	const STATUS_WAIT = 0 ;
	const STATUS_VERIFIED = 1 ;
	const STATUS_RESETED = 2 ;

	function findByPk($id){
		$this->db->where('hashtag_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}

	function insert($data){

		$this->db->insert(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();
		return $this->findByPk($insert_id);
	}

	function findAll(){

		$cacheKey = TABLE_NAME."-findAll"  ;

		$result = $this->cache->memcached->get($cacheKey) ;

		if(empty($result)){

			$query = $this->db->get(self::TABLE_NAME) ;
			$tmp_result =  $query->result() ;

			if(!empty($tmp_result)){
				$this->cache->memcached->save($cacheKey ,$tmp_result, 86400 ) ; // cache 24 hour
				$result = $tmp_result ;
			}

		}
		return $result ;
	}
	
	function findAllByKeyword($keyword) {
		$cacheKey = TABLE_NAME . "-findAllByKeyword";
		
		$result = $this->cache->memcached->get ( $cacheKey );
		
		if (empty ( $result )) {
			
			$query = $this->db->get_where ( self::TABLE_NAME, array (
					'keyword' => $keyword ,
					'is_active' =>1
			) );
			
			$tmp_result = $query->result ();
			
			if (! empty ( $tmp_result )) {
				$this->cache->memcached->save ( $cacheKey, $tmp_result, 86400 ); // cache 24 hour
				$result = $tmp_result;
			}
		}
		return $result;
	}
	
	

  
}
