<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signage_content_model extends CI_Model {
    const TABLE_NAME = 'signage_content' ;
	
	
    public function findByPk($id){
        $this->db->where('signage_content_id', $id);
        $query = $this->db->get(self::TABLE_NAME) ;
        return $query->row();
    }
    
    function findAllBySignageId($id){
        $query = $this->db->get_where(self::TABLE_NAME, array('signage_id'=>$id)) ;
        return $query->result() ;
    }
}