<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_model extends CI_Model {
	
	const TABLE_NAME = 'contact' ;


	function findByPk($id){
		$this->db->where('contact_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	

	function findByEmail($email){
		$this->db->where('email', $email);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	function findByUserLogon($logonName,$domain){

// 		$this->db->where('user_logon', $logonName);
// 		$query = $this->db->get(self::TABLE_NAME) ;
// 		return $query->row();

		$sql = "select * from contact where user_logon = ? and company_id in(select company_id from company where upper(domain_name) = upper(?) ) " ;
		$query = $this->db->query($sql,array($logonName,$domain)) ;
		return $query->row();
	}
	
	function findByEmployeeId($employeeId){
		$this->db->where('employee_id', $employeeId);
		$this->db->or_where('employee_card_id', $employeeId);
		$query = $this->db->get(self::TABLE_NAME) ;
                $result =  $query->row();
                $lastSQL = $this->db->last_query();
                log_message("debug", "SQL[findByEmployeeId]: ".$lastSQL) ;
                        
		return $result ;
	}
	
	function insert($data){

		$this->db->insert(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();
		return $this->findByPk($insert_id);
	}
	
	function findAllByKeyword($page=1,$pageSize=10,$keyword=""){
		
		$cache_key = 'Contact_model-findAllByKeyword'. (!empty($page)?"_".$page:"" ) . ( !empty($pageSize)?"_".$pageSize:"")  . ( !empty($keyword)?"_".$keyword:"" ) ;

		$result = $this->cache->memcached->get($cache_key) ;
		log_message("dubeg", "Cache Key: $cache_key result:".json_encode($result)) ;
		if(empty($result)){
			
		
			$datas = array() ;
			$totalPage = 0 ;
			$totalRecord = 0 ;
			
//			$this->db->like("first_name",$keyword);
//			$this->db->or_like("last_name",$keyword);
//			$this->db->or_like("display_name",$keyword);
//			$this->db->or_like("telephone",$keyword);
//			$this->db->or_like("email",$keyword);
//			$this->db->or_like("employee_id",$keyword);
//			$this->db->where("employee_id IS NOT NULL and employee_id <> ''", null  );
//                        $totalRecord = $this->db->count_all_results(self::TABLE_NAME) ;
                        
                        $sql_count = "select * from ".self::TABLE_NAME." where employee_id IS NOT NULL and employee_id <> '' " ;
                        $sql_count .= "and ( " ;
                        $sql_count .=  " first_name like '%".$keyword."%' " ;
                        $sql_count .=  " or last_name like '%".$keyword."%' " ;
                        $sql_count .=  " or display_name like '%".$keyword."%' " ;
                        $sql_count .=  " or telephone like '%".$keyword."%' " ;
                        $sql_count .=  " or email like '%".$keyword."%' " ;
                        $sql_count .=  " or employee_id like '%".$keyword."%' " ;
			$sql_count .= ") " ;
			
                        $totalRecord =  count($this->db->query($sql_count)) ;
                        
			if( !empty($totalRecord )) {
				$totalPage = $totalRecord%$pageSize==0?$totalRecord/$pageSize: intval($totalRecord/$pageSize) +1 ;
			}else{
				return null ;
			}
			
//			$this->db->like("first_name",$keyword);
//			$this->db->or_like("last_name",$keyword);
//			$this->db->or_like("display_name",$keyword);
//			$this->db->or_like("telephone",$keyword);
//			$this->db->or_like("email",$keyword);
//			$this->db->or_like("employee_id",$keyword);
//			$this->db->where("employee_id IS NOT NULL and employee_id <> ''", null  );
                        
                        $sql = "select * from ".self::TABLE_NAME." where employee_id IS NOT NULL and employee_id <> '' " ;
                        $sql .= "and ( " ;
                        $sql .=  " first_name like '%".$keyword."%' " ;
                        $sql .=  " or last_name like '%".$keyword."%' " ;
                        $sql .=  " or display_name like '%".$keyword."%' " ;
                        $sql .=  " or telephone like '%".$keyword."%' " ;
                        $sql .=  " or email like '%".$keyword."%' " ;
                        $sql .=  " or employee_id like '%".$keyword."%' " ;
			$sql .= ") order by display_name , first_name " ;
			
			$start = (intval($page)-1) * intval($pageSize)  ;
			$limit = $pageSize ;
	// 		echo $start. "|" .$limit  ; exit;
			$this->db->limit( $limit, $start );
			$query = $this->db->query($sql);
			
			
// 			$result = $query->result() ;
			$tmp_result = array(
				"page"=>$page,
				"total_page"=>$totalPage,
				"contacts"=> $query->result()
			);
// 			$lastSQL = $this->db->last_query();
//                        log_message("debug", "Last query : ".$lastSQL) ;
// 		echo $lastSQL ; exit;
//			$tmp_result =  $datas ;
			
			if(!empty($tmp_result)){
//				log_message("debug", "Cache save ".$cache_key." ".$tmp_result) ;
				$this->cache->memcached->save($cache_key ,$tmp_result, 1800 ) ; // cache 60 secs
//				log_message("debug", "Cache save ".$cache_key." result ".json_encode($tmp_result)) ;
				$result = $tmp_result ;
			}
		}

		return $result ;
	}
}
