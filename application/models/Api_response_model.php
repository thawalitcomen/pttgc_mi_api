<?php
class Api_response_model extends CI_Model {
	
	const TABLE_NAME = 'api_response' ;

	function findByCode($code){
		$this->db->where('response_code',$code);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	function findAll(){
		
		if ( ( $this->cache->apc->s_supported ) && $this->cache->memcached->get($cacheKey) ){
			$cacheKey = "Api_response_model-findAll"  ;
		
			$result = $this->cache->memcached->get($cacheKey) ;
		
			if(empty($result)){
				$query = $this->db->get(self::TABLE_NAME) ;
				$tmp_result =  $query->result() ;
		
				if(!empty($tmp_result)){
					$this->cache->memcached->save($cacheKey ,$tmp_result, 86400 ) ; // cache 24 hour
					$result = $tmp_result ;
				}
		
			}
		}else{
			$query = $this->db->get(self::TABLE_NAME) ;
			$result =  $query->result() ;
		}
		return $result ;
	}
}