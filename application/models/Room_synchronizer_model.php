<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room_synchronizer_model extends CI_Model {
	
	const TABLE_NAME = 'room_synchronizer' ;
	
	

	public function findByRoomId($room_id){
		$this->db->where('room_id', $room_id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
}
