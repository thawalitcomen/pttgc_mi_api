<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room_model extends CI_Model {
	
	const TABLE_NAME = 'room' ;

	public function findByPk($id){
		$this->db->where('room_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	public function findAllAvailableNow($user_id=null) {
		
// 		$user_id = null; // don't check freguencecy of user
		$cache_key = 'Room_model-findAllAvailableNow'. (!empty($user_id)?"_".$user_id:"") ;
		
		$result = $this->cache->memcached->get($cache_key) ;
		if(empty($result)){
			
			log_message("debug", "Cache ".$cache_key." Empty!") ;
			
			$param = array() ;
			$sql  = " select room.*,location_name from room inner join floor on room.floor_id = floor.floor_id inner join building on building.building_id = floor.building_id inner join location on location.location_id = building.location_id where status_id = 1  " ;
			$sql .= " and room_id not in ( " ;
			$sql .= "	select room_id from event where status_id = 1 " ;
			$sql .= " 	AND (  " ;
			$sql .= "		(	NOW() BETWEEN start_time AND end_time	) OR  " ;
			$sql .= "		(	DATE_ADD(NOW(),interval 15 MINUTE ) BETWEEN start_time AND end_time	) OR   " ;
			$sql .= "		(	DATE_ADD(NOW(),interval 15 MINUTE ) <= start_time AND NOW() >= end_time	)  " ;
			$sql .= "	) " ;
			$sql .= " ) " ;
			$sql .= " order by " ;

			/*if( !empty($user_id)){
				$sql .= " 	( select count(room_id) as cnt from room " ;
				$sql .= " 	where room_id in (" ;
				$sql .= " 		select room_id from event " ;
				$sql .= " 		where create_by in(  " ;
				$sql .= " 			select contact_id from user where user_id = ? " ;
				$sql .= " 		)" ;
				$sql .= " 	)) desc ," ;
				
				$param = array_merge ($param , array($user_id) ) ;
			}*/
			$sql .= "  location_name, room_name " ;
			
			$rooms = [] ;
			$query = $this->db->query($sql,$param) ;
// 			return  $query->result() ;
			$tmp_result = $query->result() ;
			
			if(!empty($tmp_result)){
//				log_message("debug", "Cache save ".$cache_key." ".$tmp_result) ;
				$this->cache->memcached->save($cache_key ,$tmp_result, 60 ) ; // cache 10 secs
//				log_message("debug", "Cache save ".$cache_key." result ".json_encode($tmp_result)) ;
				$result = $tmp_result ;
			}

		}
		return $result ;
	}
	
	public function findAllAvailableNextHour($user_id=null , $exclude = null) {
	
// 		$user_id = null; // don't check freguencecy of user
		$cache_key = 'Room_model-findAllAvailableNextHour'. (!empty($user_id)?"_".$user_id:"" ) . ( !empty($exclude)?"_".$exclude:"") ;
		
		$result = $this->cache->memcached->get($cache_key) ;
		if(empty($result)){
				
			log_message("debug", "Cache ".$cache_key." Empty!") ;
			
			$param = array() ;
			$sql  = "select room.*,location_name from room inner join floor on room.floor_id = floor.floor_id inner join building on building.building_id = floor.building_id inner join location on location.location_id = building.location_id  where status_id = 1  " ;
			$sql .= "and room_id not in ( " ;
			$sql .= "	select room_id from event where status_id = 1 " ;
			$sql .= " 	and ( " ;
// 					( DATE_ADD(NOW(), INTERVAL 1 HOUR)  BETWEEN start_time AND end_time) OR 
// 					( DATE_ADD(DATE_ADD(NOW(), INTERVAL 1 HOUR),INTERVAL 15 MINUTE) BETWEEN start_time AND end_time)  OR  
// 					( DATE_ADD(DATE_ADD(NOW(), INTERVAL 1 HOUR),INTERVAL 15 MINUTE) <= start_time AND DATE_ADD(NOW(), INTERVAL 1 HOUR) >= end_time) ) " ;
			
			$sql .= "			( DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d %H'), INTERVAL 61 MINUTE)  BETWEEN start_time AND end_time) OR " ;
			$sql .= "			( DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d %H'), INTERVAL 74 MINUTE) BETWEEN start_time AND end_time)   " ;
			//$sql .= "			( DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d %H'), INTERVAL 75 MINUTE) <= start_time  AND DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d %H'), INTERVAL 60 MINUTE )) " ;

			$sql .= "	) " ;
			$sql .= ") 	" ;
			if(!empty($exclude)){
				$sql .= "and room_id not in( ".$exclude." ) ";
			}
			$sql .= " order by " ;
			/*if( !empty($user_id)){
				$sql .= " 	( select count(room_id) as cnt from room " ;
				$sql .= " 	where room_id in (" ;
				$sql .= " 		select room_id from event " ;
				$sql .= " 		where create_by in(  " ;
				$sql .= " 			select contact_id from user where user_id = ? " ;
				$sql .= " 		)" ;
				$sql .= " 	)) desc ," ;
					
// 				$param = array_merge ($param , array($user_id) ) ;
				$param = array($user_id) ;
			}*/
			$sql .= "location_name, room_name " ;
		
			$rooms = [] ;
			$query = $this->db->query($sql,$param) ;
			
			//log_message("debug", "Next Availabel SQL:".$this->db->last_query()) ;
// 			return $query->result() ;
// 			$tmp_result =  $query->result() ;
			$tmp_result = NULL ;
			if ($query->num_rows() > 0){
				$tmp_result = $query->result();
			}
				
			if(!empty($tmp_result)){
//				log_message("debug", "Cache save ".$cache_key." ".$tmp_result) ;
				$this->cache->memcached->save($cache_key ,$tmp_result, 60 ) ; // cache 10 secs
//				log_message("debug", "Cache save ".$cache_key." result ".json_encode($tmp_result)) ;
				$result = $tmp_result ;
			}
			
		}
		return $result ;
	}

	/*public function findAll($room_type_id=0,$floor_id=0,$building_id=0,$location_id=0 , $contact_id=0 ){
		$result = array() ;
		$sql = "select * from room where status_id = 1" ;
		if($room_type_id > 0){
			$sql .= ' and room_type_id = ' . $room_type_id ;
		}
		if($floor_id > 0){
			$sql .= ' and floor_id  = ' . $floor_id ;
		}
		if($building_id > 0){
			$sql .= ' and floor_id in ( select floor_id from floor where building_id = ' . $building_id . ')' ;
		}
		if($location_id > 0){
			$sql .= ' and floor_id in ( select floor_id from floor where building_id in ( select building_id from building where location_id = ' . $location_id  . '))' ;
		}

		if($contact_id > 0){
			$sql .= " order by (select count(*) from event where create_by = ".$contact_id." ) desc" ;
		}

		$query = $this->db->query($sql) ;

// 		if(!empty($query->result())){
// 			$result = $query->result() ;
// 		}
		$result = NULL ;
		if ($query->num_rows() > 0){
			$result = $query->result();
		}
		
		return $result ;
	}*/

    public function findAll($room_type_id=0,$floor_id=0,$building_id=0,$location_id=0 , $contact_id=0 ){
        $result = array() ;
        $sql = "select room.*,location_name from room inner join floor on room.floor_id = floor.floor_id inner join building on building.building_id = floor.building_id inner join location on location.location_id = building.location_id where status_id = 1" ;
        if($room_type_id > 0){
            $sql .= ' and room.room_type_id = ' . $room_type_id ;
        }
        if($floor_id > 0){
            $sql .= ' and room.floor_id  = ' . $floor_id ;
        }
        if($building_id > 0){
//            $sql .= ' and floor_id in ( select floor_id from floor where building_id = ' . $building_id . ')' ;
            $sql .= ' and room.floor_id in ( select floor_id from floor where building_id = ' . $building_id . ')' ;
        }
        if($location_id > 0){
            $sql .= ' and room.floor_id in ( select floor_id from floor where building_id in ( select building_id from building where location_id = ' . $location_id  . '))' ;
        }

//        if($contact_id > 0){
//            $sql .= " order by (select count(*) from event where create_by = ".$contact_id." ) desc" ;
//        }

        $sql .= " order by location_name , room.room_name " ;

        $query = $this->db->query($sql) ;

// 		if(!empty($query->result())){
// 			$result = $query->result() ;
// 		}
        $result = NULL ;
        if ($query->num_rows() > 0){
            $result = $query->result();
        }

        return $result ;
    }
	
	public function findByCompanyId($company_id) {
		$cache_key = 'Room_model-findByCompanyId_'. $company_id ;
		
		$result = $this->cache->memcached->get($cache_key) ;
		if(empty($result)){
			log_message("debug", "Cache ".$cache_key." Empty!") ;
			$sql = "select * from room where status_id = 1 " ;
			$sql .= "and floor_id in ( " ;
			$sql .= 	"select floor_id from floor where building_id in ( " ;
			$sql .=     	"select building_id from building where location_id in (" ;
			$sql .=				"select location_id from company_location where company_id = ? " ;
			$sql .= "		) " ;		
			$sql .= "	) " ;
			$sql .= ") order by room_name " ;
			
// 			$param =  array($company_id) ;
			$query = $this->db->query($sql,$company_id) ;
// 			$tmp_result = $query->result() ;
			$tmp_result = NULL ;
			if ($query->num_rows() > 0){
				$tmp_result = $query->result();
			}
				
			if(!empty($tmp_result)){
//				log_message("debug", "Cache save ".$cache_key." ".$tmp_result) ;
				$this->cache->memcached->save($cache_key ,$tmp_result, 60 ) ; // cache 10 secs
//				log_message("debug", "Cache save ".$cache_key." result ".json_encode($tmp_result)) ;
				$result = $tmp_result ;
			}
			
		}
		return $result ;
	}

}
