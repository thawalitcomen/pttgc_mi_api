<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LdapDomainModel extends CI_Model
{
	const TABLE_NAME = 'ldap_domain' ;
	
	function findAll(){
		$this->db->where('is_active',1);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->result();
	}
}