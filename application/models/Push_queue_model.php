<?php
class Push_queue_model extends CI_Model {
	
	const TABLE_NAME = 'push_queue' ;

	public function findByPk($id){
		$this->db->where('queue_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	public function fetchQueue($number=10){
		$sql =  " select * from push_queue " ;
		$sql .= " where attemps < max_attemps " ;
		$sql .= " and status = 'Queue' " ;
		$sql .= " order by  last_attemp,created_at " ;
		$sql .= " limit ? " ;
		$query = $this->db->query($sql,array($number)) ;
		return $query->result();
	}
	public function updateStatusQueue($queueId,$status,$attemps=1,$detail=null){
		$this->db->trans_start();
		
		$this->db->set('status',$status);
		switch ($status){
			case "Processing" :
				$this->db->set('attemps',$attemps);
				$this->db->set('last_attemp',date("Y-m-d H:i:s"));
				break ;
			case "Success" :
				$this->db->set('success_at',date("Y-m-d H:i:s"));
				if(!empty($detail)){
					$this->db->set('detail',$detail);
				}
				break ;
			case "Failed" :

				if(!empty($detail)){
					$this->db->set('detail',$detail);
				}
				break ;
					
		}
		
		$this->db->where('queue_id', $queueId);
		$this->db->update(self::TABLE_NAME);
		
		$this->db->trans_complete();
		
		return $this->db->trans_status() ;
	}
	
}