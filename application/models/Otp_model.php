<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Otp_model extends CI_Model {

	const TABLE_NAME = 'otp' ;

	const STATUS_WAIT = 0 ;
	const STATUS_VERIFIED = 1 ;
	const STATUS_RESETED = 2 ;

	function findByPk($id){
		$this->db->where('otp_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}

	function insert($data){

		$this->db->insert(self::TABLE_NAME , $data);
		$insert_id = $this->db->insert_id();
		return $this->findByPk($insert_id);
	}

	function findAll(){

		$cacheKey = "Otp_model-findAll"  ;

		$result = $this->cache->memcached->get($cacheKey) ;

		if(empty($result)){

			$query = $this->db->get(self::TABLE_NAME) ;
			$tmp_result =  $query->result() ;

			if(!empty($tmp_result)){
				$this->cache->memcached->save($cacheKey ,$tmp_result, 86400 ) ; // cache 24 hour
				$result = $tmp_result ;
			}

		}
		return $result ;
	}

  function findByMobile($mobile) {
    $this->db->where('mobile', $mobile);
    $this->db->where('expired_at >', date('Y-m-d H:i:s') );
    $this->db->where('status_id', srp_helper::STATUS_WAIT);
    $this->db->order_by('created_at','desc') ;
    $this->db->limit(1);
    $query = $this->db->get(self::TABLE_NAME) ;

    // log_message("debug",$this->db->last_query()) ;
    return $query->row();
  }
  
  function findByToken($id){
  	$this->db->where('otp_token', $id);
  	$this->db->where('status_id', 0);
	$this->db->where('expired_at >', date('Y-m-d H:i:s') );
  	$query = $this->db->get(self::TABLE_NAME) ;
  	return $query->row();
  }
  
  function update($data,$id){
  	$this->db->where('otp_id' , $id);
  	$this->db->update(self::TABLE_NAME , $data);
  	return $this->findByPk($id);
  }
  
  function findByResetToken($id){
  	$this->db->where('reset_token', $id);
//	$this->db->where('expired_at >', date('Y-m-d H:i:s') );
  	$this->db->where('status_id', 1);
  	$query = $this->db->get(self::TABLE_NAME) ;
  	return $query->row();
  }
}
