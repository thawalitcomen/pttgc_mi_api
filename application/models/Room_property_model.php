<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Room_property_model extends CI_Model
{

    const TABLE_NAME = 'room_property';

    function findAllByRoomId($id)
    {
        $query = $this->db->get_where(self::TABLE_NAME, array('room_id' => $id, 'is_active' => 1));
        return $query->result();
    }

    function findByPk($id, $property, $use_default_value = true)
    {
        $query = $this->db->get_where(self::TABLE_NAME, array('room_id' => $id, "property_name" => $property));
        $room_property_model = $query->row();
        if (empty($room_property_model) || empty($room_property_model->property_value)) {
            $query = $this->db->get_where("room_property_master", array('property_name' => $property, 'is_active' => 1));

            $master_model = $query->row();
            if (!empty($master_model) && $use_default_value) {

                $room_property_model = new Room_property_model();
                $room_property_model->id = $id;
                $room_property_model->property_name = $master_model->property_name;
                $room_property_model->property = $master_model->property;
                $room_property_model->property_value = $master_model->default_value;
                $room_property_model->is_active = 1;

            }
        }
        return $room_property_model;
    }

}
