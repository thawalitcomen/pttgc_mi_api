<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service_model extends CI_Model {
	
	 const SERVICE_BS01 = "BS01";
	 const SERVICE_SN01 = "SN01";
	 const SERVICE_FL01 = "FL01";
	 const SERVICE_RM01 = "RM01";
	 const SERVICE_FL02 = "FL02";
	 const SERVICE_FL03 = "FL03";
	 const SERVICE_BD01 = "BD01";
	 const SERVICE_US01 = "US01";
	 const SERVICE_RM02 = "RM02";
	 const SERVICE_US02 = "US02";
	 const SERVICE_US03 = "US03";
	 const SERVICE_US04 = "US04";
	 const SERVICE_RM03 = "RM03";
	 const SERVICE_RM04 = "RM04";
	 const SERVICE_RM05 = "RM05";
	 const SERVICE_LC01 = "LC01";
	 const SERVICE_PN01 = "PN01";
	 const SERVICE_US05 = "US05";
	 const SERVICE_US06 = "US06";
	 const SERVICE_US07 = "US07";
	 const SERVICE_US08 = "US08" ;
	
	const TABLE_NAME = 'service' ;

}
