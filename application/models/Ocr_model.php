<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ocr_model extends CI_Model {
	
	const TABLE_NAME = 'ocr' ;

	function findByPk($id){
		$this->db->where('ocr_id', $id);
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
	
	function insert($userId,$verify_code,$photo,$contact_id){
		$data = array(
				'user_id'=>$userId,
				'verify_code'=>$verify_code,
				'photo'=>$photo,
				'created_at'=>date('Y-m-d H:i:s'),
				'contact_id'=>$contact_id
		);
		$this->db->insert(self::TABLE_NAME , $data); 
		$insert_id = $this->db->insert_id();
// 		echo "Insert ID:".$insert_id ;
		return $this->findByPk($insert_id);
	}
	
	function findByVerifyCode($verify_code){
		$this->db->where('verify_code', $verify_code );
		$query = $this->db->get(self::TABLE_NAME) ;
		return $query->row();
	}
}