<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CodeIgniter Delete Database Demo</title>
    <!--link the bootstrap css file-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<br><br>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover">
                <thead>
                    <tr class="bg-primary">
                        <th>#</th>
                        <th>Code</th>
                        <th>Status</th>
                        <th>Title</th>
                        <th>Message</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($datas as $i=>$data) { ?>
                    <tr>
                        <td><?php echo ($i+1); ?></td>
                        <td width="10%"><?php echo $data->response_code; ?></td>
                        <td width="20%"><?php echo $data->response_status; ?></td>
                        <td width="25%">
                            <?php echo $data->response_title; ?>
                            <br/><?php echo $data->response_title_th; ?>
                           
                        </td>
                        <td width="45%">
                            <?php echo $data->response_message; ?>
                            <br/><?php echo $data->response_message_th; ?>
                           
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>



