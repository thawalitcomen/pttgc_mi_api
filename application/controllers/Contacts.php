<?php

require(APPPATH . 'libraries/REST_Controller.php');

class Contacts extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('service_model');
        $this->load->model('client_model');

        $this->load->model('contact_model');
    }

    function index_get() {
        $reqHeaders = http_helper::requestHeaders($this);
        $clientModel = http_helper::getClientModel($this, $reqHeaders);

        $keyword = !empty($this->get('keyword')) ? $this->get('keyword') : "";
        $page = !empty($this->get('page')) ? $this->get('page') : 1;
        $page_size = !empty($this->get('page_size')) ? $this->get('page_size') : 10000;

// 		echo "keyword: $keyword , page: $page , page_size: $page_size "; exit;
        $contacts = array();
        $pagingData = $this->contact_model->findAllByKeyword($page, $page_size, $keyword);
// 		var_dump( $pagingData['contacts']) ;
        if (empty($pagingData) || empty($pagingData['contacts'])) {
            http_helper::response($this, http_helper::STATUS_DATA_NOT_FOUND);
        }
        $contact_list = $pagingData['contacts'];

        for ($i = 0; $i < count($contact_list); $i++) {
            $contact = $contact_list[$i];
            $contact_json = array(
                'id' => intval($contact->contact_id),
                'name' => $contact->display_name !== null ? $contact->display_name : "",
                'phone' => $contact->telephone !== null ? $contact->telephone : "",
                'email' => $contact->email !== null ? $contact->email : "",
                'employee_id' => $contact->employee_id !== null ? $contact->employee_id : "");

            $contacts[] = $contact_json;
        }
        $data = array(
            "page" => $pagingData['page'],
            "total_page" => $pagingData['total_page'],
            "contacts" => $contacts
        );
        http_helper::response($this, http_helper::STATUS_SUCCESS, $data);
    }

}
