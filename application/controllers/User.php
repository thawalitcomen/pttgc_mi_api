<?php

defined('BASEPATH') or exit('No direct script access allowed');

require (APPPATH . 'libraries/REST_Controller.php');

class User extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('ocr_model');
        $this->load->model('user_token_model');
        $this->load->model('service_model');
        $this->load->model('client_model');
        $this->load->model('ldap_client_model');
// 		$this->load->model ( 'ldap_domain_model' );

        $this->load->model('contact_model');
        $this->load->model('event_model');
        $this->load->model('room_model');
        $this->load->model('floor_model');
        $this->load->model('building_model');
        $this->load->model('location_model');

        $this->load->model('otp_model');
        $this->load->model('company_model');
        $this->load->model('country_model');
        $this->load->model('module_model');

        $this->load->model('config_model');
        $this->load->model('event_attendee_model');


        $this->config->load('ldap');
        $this->load->model('room_type_model');
        $this->load->model('room_property_model');
    }

    public function imageCreation($data, $save_path, $save_name) {
        try {
            $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));
            $f = finfo_open();
            $mime_type = finfo_buffer($f, $data, FILEINFO_MIME_TYPE);

            switch ($mime_type) {
                case 'image/png' :
                    $imagename = $save_path . $save_name . '.png';
                    $return = $save_name . '.png';
                    break;
                case 'image/jpg' :
                case 'image/jpeg' :
                case 'image/JPG' :
                case 'image/JPEG' :
                    $imagename = $save_path . $save_name . '.jpg';
                    $return = $save_name . '.jpg';
                    break;
                default :
                    return false;
            }
            if (file_put_contents($imagename, $data)) {
                chmod($imagename, 0755);
                return $return;
            }
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
        return false;
    }

    function index_get($emplyee_id = '') {
        $this->load->view('api/us01', array(
            "employeeId" => $emplyee_id
        ));
    }

    /**
     * Verify user by OCR
     *
     * @param string $emplyee_id
     */
    function index_post($employeeId = '') {

        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);
        $clientModel = http_helper::getClientModel($this, $reqHeaders);

		$time_now = $this->input->post('time_now');
		$sign = $this->input->post('sign');

		$checkSign = hash_hmac('sha256', $employeeId.$time_now, $this->config->item('signkey'));
		if($checkSign==$sign){
			$contact_model = $this->contact_model->findByEmployeeId($employeeId);
			if (empty($contact_model)) {
				return http_helper::response($this, http_helper::STATUS_EMPLOYEE_NOT_FOUND);
			}
			$contact_model = null;
			$employeeId = 999999;
			//$userModel = $this->user_model->findByPk(1);
		}
		//log_message("debug", "checkSign :$checkSign , sign :$sign");
        $contact_model = $this->contact_model->findByEmployeeId($employeeId);
        if (empty($contact_model)) {
            return http_helper::response($this, http_helper::STATUS_EMPLOYEE_NOT_FOUND);
        }

        log_message("debug", "employeeId :$employeeId , contact:". json_encode($contact_model));
//		$userModel = $this->user_model->findByEmployeeId ( $employeeId );
        $userModel = $this->user_model->findByContactId($contact_model->contact_id);
        log_message("debug", "userModel :" . json_encode($userModel));
        if (empty($userModel)) {

            // If user not found , need to create new user with password
            $password = $this->post("password");
            if (!empty($password)) {

                $contactId = $contact_model->contact_id;
                $username = $contact_model->user_logon;
                $domain = "";
                $company_id = $contact_model->company_id;

                $company = $this->company_model->findByPk($company_id);
                if (!empty($company)) {
                    $domain = $company->domain_name;
                }

                $loginLdapUser = srp_helper::get_ldap_connection($this, $domain, $username, $password);
                log_message("debug", "Ldap authen[domain:" . $domain . ",user:" . $username . ",password:" . $password . "]");
                if (empty($loginLdapUser) || !$loginLdapUser) {
                    return http_helper::response($this, http_helper::STATUS_INVALID_PASSWORD);
                }

                $salt = user_helper::generateSalt();
                $encryptPass = user_helper::setPassword($password, $salt);

                $insertData = array(
                    'user_name' => $username,
                    'password' => $encryptPass,
                    'contact_id' => $contactId,
                    'role_id' => 3, // EmployeeId
                    'created_at' => date('Y-m-d H:i:s'),
                    'is_active' => 1,
                    'auth_key' => $salt,
                    'company_id' => $company_id
                );

                $userModel = $this->user_model->insert($insertData);
                log_message("debug", "Inserted UserModel:|" . json_encode($userModel) + "| for user Logon " . $username);
            } else {
                /*                 * ****************** fix default contact ***********************
                  $default_employee_id = '9999999' ;
                  $userModel = $this->user_model->findByEmployeeId ( $default_employee_id );
                 * ****************** fix default contact ******************* */
                log_message("error", "UserModel for OCR:" . json_encode($userModel));
//                return http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                return http_helper::response($this, http_helper::STATUS_USER_NOT_REGISTER);
            }
        } else {
            $password = $this->post("password");
            if (!empty($password)) {

                $contactId = $contact_model->contact_id;
                $username = $contact_model->user_logon;
                $domain = "";
                $company_id = $contact_model->company_id;

                $company = $this->company_model->findByPk($company_id);
                if (!empty($company)) {
                    $domain = $company->domain_name;
                }

                $loginLdapUser = srp_helper::get_ldap_connection($this, $domain, $username, $password);
                log_message("debug", "Ldap authen[domain:" . $domain . ",user:" . $username . ",password:" . $password . "]");
                if (empty($loginLdapUser) || !$loginLdapUser) {
                    return http_helper::response($this, http_helper::STATUS_INVALID_PASSWORD);
                }

                $salt = user_helper::generateSalt();
                $encryptPass = user_helper::setPassword($password, $salt);

                $updateData = array(
                    'password' => $encryptPass,
                    'auth_key' => $salt,
                );

                $userModel = $this->user_model->update($updateData, $userModel->user_id);
                log_message("debug", "Updated UserModel:|" . json_encode($updateData) + "| for user Logon " . $username);
            }
			
        }

        $file = $this->post('image');
        $save_path = "data/ocr/";
        $save_name = $userModel->user_id . "_" . date('YmdHis');
        $fileName = $this->imageCreation($file, $save_path, $save_name);

        if (empty($fileName)) {
            return http_helper::response($this, http_helper::STATUS_UPLOAD_FAILED);
        }

        $verifyCode = md5($userModel->user_id . ":" . date('Y-m-d H:i:s'));
        // $this->load->model('ocr_model');
        $ocrModel = $this->ocr_model->insert($userModel->user_id, $verifyCode, $fileName, $contact_model->contact_id);
        if (empty($ocrModel)) {
            return http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR);
        }


        $image = $this->config->base_url() . $save_path . $ocrModel->photo;
        $data = array(
            "verify_code" => $ocrModel->verify_code,
            "verify_image" => $image,
            'host' => array(
                'id' => intval($contact_model->contact_id),
                'name' => $contact_model->display_name,
                'phone' => $contact_model->telephone,
                'email' => $contact_model->email,
                'employee_id' => $contact_model->employee_id
            )
        );
        return http_helper::response($this, Http_helper::STATUS_SUCCESS, $data);
    }

    function login_get($client_id = '') {

        echo base_url();
        $this->load->view('api/us02', array(
            "client_id" => $client_id
        ));
    }

    function login_identity_post() {

        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);
		$clientModel = http_helper::getClientModel($this, $reqHeaders);
        $client_id = $reqHeaders ['client_id'];
        $username = trim($this->post('username'));
        $password = trim($this->post('password'));
        $identity = $this->post('identity');

//        if (empty($username) || empty($password)) {
//            log_message("error", "Login failed! , empty username or password [" . $username . "|" . $password . "]");
//            http_helper::response($this, http_helper::STATUS_INVALID_USER_OR_PASSWORD);
//        }

        $domain = "";
        $userLogon = NULL ;
        if (count(explode("\\", $username)) > 1) {
            $domain = explode("\\", $username)[0];
            $username = explode("\\", $username)[1];
            $userLogon = $username ;
        }

        if (empty($domain)) {
            $domain = strtolower($this->config->item('domain_default'));
            log_message("debug", "Domain from Config :" . strtolower($domain));
        }

        if (empty($userLogon)) {
            log_message("error", "Login failed! , empty username ");
            return http_helper::response($this, http_helper::STATUS_INVALID_USERNAME);
        }

        if (empty($password)) {
            log_message("error", "Login failed! , empty password ");
            return http_helper::response($this, http_helper::STATUS_INVALID_PASSWORD);
        }
//        if (empty($identity)) {
//            log_message("error", "Login failed! , empty identity ");
//            return http_helper::response($this, http_helper::STATUS_INVALID_IDENTITY_NUMBER);
//        }




        log_message("debug", "Login:[" . $username . "|" . $password . "|" . $domain);

        $company_id = 0;
        $company = $this->company_model->findByDomainName($domain);

        //log_message("debug", "Company:[" . json_encode($company)."]");
        if (!empty($company)) {
            $company_id = $company->company_id;
        }

        log_message("error", "Identity :".$identity);

// 		$loginLdapUser = $this->ldap_client_model->perform_ldap_auth ( $username, $password, $domain );
        $loginLdapUser = srp_helper::get_ldap_connection($this, $domain, $username, $password, $identity);
        log_message("debug", "loginLdapUser:|" . json_encode($loginLdapUser) . "|");
        if (empty($loginLdapUser) || !$loginLdapUser) {
            return http_helper::response($this, http_helper::STATUS_LOGIN_LDAP_FAILED);
        }

        if ($loginLdapUser === -1) {
            return http_helper::response($this, http_helper::STATUS_INVALID_IDENTITY_NUMBER);
        }

        $userModel = $this->user_model->findByUsername($username, $domain);
        log_message("debug", "userModel:|" . json_encode($userModel) . "|");



        // validate Mobile
// 		http_helper::response ( $this, http_helper::STATUS_NOT_REGISTER_MOBILE );
        $salt = user_helper::generateSalt();
        $encryptPass = user_helper::setPassword($password, $salt);

        $contact_model = $this->contact_model->findByUserLogon($username, $domain);
        log_message("debug", "ContactModel [$username, $domain]: " . json_encode($contact_model));
        if (empty($contact_model)) {
            http_helper::response($this, http_helper::STATUS_USER_NOT_FOUND);
        }


        $contactId = $contact_model->contact_id;

        if (empty($userModel)) {


            log_message("debug", "ContactModel:|" . json_encode($contact_model) . "| for user Logon " . $username . " | " . $domain);
            if (!empty($contact_model)) {

                // Create new Contact
// 				http_helper::response ( $this, http_helper::STATUS_USER_NOT_FOUND );
                $contactId = $contact_model->contact_id;

                $insertData = array(
                    'user_name' => $username,
                    'password' => $encryptPass,
                    'contact_id' => $contactId,
                    'role_id' => 3, // EmployeeId
                    'created_at' => date('Y-m-d H:i:s'),
                    'is_active' => 1,
                    'auth_key' => $salt,
                    'company_id' => $company_id
                );

                $userModel = $this->user_model->insert($insertData);
                log_message("debug", "Inserted UserModel:|" . json_encode($userModel) + "| for user Logon " . $username);
            }
        }else{

            $userModel->auth_key = $salt;
            $userModel->password = $encryptPass;
            $userModel->contact_id = $contact_model->contact_id;

            $userModel = $this->user_model->update($userModel, $userModel->user_id);
        }

        log_message("debug", "userModel:|" . json_encode($userModel));


        if (empty($userModel) || empty($userModel->mobile)) {
            return $this->getLoginInfo($client_id, $userModel, http_helper::STATUS_NOT_REGISTER_MOBILE);
        } else {
            return $this->getLoginInfo($client_id, $userModel);
        }
    }

    function getLoginInfo($client_id, $userModel, $statusCode = http_helper::STATUS_SUCCESS , $title=NULL , $message=NULL , $reset_token = NULL) {

//        if (empty($userModel)) {
//            log_message("error", "Empty user_model :" . json_encode($userModel));
//            http_helper::response($this, http_helper::STATUS_NOT_REGISTER_MOBILE);
//        }

        $contact_model = $this->contact_model->findByPk($userModel->contact_id);


        if (empty($contact_model)) {
            log_message("error", "Empty contact_model :" . json_encode($contact_model));
            http_helper::response($this, http_helper::STATUS_USER_NOT_FOUND);
        }

        $contactId = $contact_model->contact_id;

        $clientModel = $this->client_model->findByUuid($client_id);
        if (empty($clientModel)) {
            http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR);
        }

        if ($statusCode == http_helper::STATUS_SUCCESS) {
            $accessToken = "";
            $userTokenModel = $this->user_token_model->findByClientId($clientModel->client_id);
            if (!empty($userTokenModel)) {
                // $userToken->expired_at = date('Y-m-d H:i:s') ;
                // $this->user_token_model->update($userToken);
                $accessToken = $userTokenModel->access_token;

                if ($userTokenModel->user_id != $userModel->user_id) {
                    $userTokenModel->user_id = $userModel->user_id;
                    $this->user_token_model->update($userTokenModel);
                }
            } else {
                $createTime = date('Y-m-d H:i:s');
                $expiredTime = date('Y-m-d H:i:s', strtotime($createTime . "+1 years"));

                $accessToken = md5($userModel->user_id . $createTime);

                $user_token_model = $this->user_token_model->insert($userModel->user_id, $clientModel->client_id, $accessToken, $expiredTime);
                if (!$user_token_model) {
                    http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR);
                }
            }
        }

        if (!empty($contact_model)) {
            $contact_model = $this->contact_model->findByPk($userModel->contact_id);
        }

        if (!empty($contact_model)) {
            $contact = array(
                'id' => intval($contact_model->contact_id),
                // 'name'=>!empty($contact_tmp)?$contact_tmp:$contactModel->display_name,
                'name' => $contact_model->display_name === null ? "" : $contact_model->display_name,
                'phone' => $contact_model->telephone === null ? "" : $contact_model->telephone,
                'email' => $contact_model->email === null ? "" : $contact_model->email,
                'employee_id' => $contact_model->employee_id === null ? "" : $contact_model->employee_id
            );
        }

        $company = array();
        $companyId = $userModel->company_id;
        if (!empty($companyId)) {
            $company_model = $this->company_model->findByPk($companyId);
            if (!empty($company_model)) {
                $company = array(
                    'company_id' => intval($company_model->company_id),
                    'company_name' => $company_model->name,
                    'domain' => $company_model->domain_name
                );
            }
        }

        $country = array();
        $countryId = $userModel->country_id;
        if (!empty($countryId)) {
            $country_model = $this->country_model->findByPk($countryId);
            if (!empty($country_model)) {
                $country = array(
                    'country_id' => intval($country_model->country_id),
                    'country_name' => $country_model->name,
                    'short_name' => $country_model->short_name,
                    'country_code' => $country_model->code
                );
            }
        }

        // module
        $modules = [];
        $module_all = $this->module_model->findAll();
        log_message("error", "Module All :" . json_encode($module_all));
        if (!empty($module_all)) {

            $module_models = $this->module_model->findByCompanyId($companyId);
            log_message("error", "module_models :" . json_encode($module_models));
            foreach ($module_all as $module) {
                $enable = 0;
                if (!empty($module_models)) {
                    foreach ($module_models as $Module_enable) {
                        if ($module->module_id === $Module_enable->module_id) {
                            $enable = 1;
                            break;
                        }
                    }
                }

                $modules[] = array(
                    'module_id' => intval($module->module_id),
                    'module_name' => $module->module_name,
                    'url' => $module->url,
                    'ios_url' => $module->ios_url,
                    'android_url' => $module->android_url,
                    'enable' => $enable
                );
            }
        }

        $mobile = $userModel->mobile;

        $data = array(
// 				'access_token' => $accessToken  ,
            'contact' => $contact,
            'company' => $company,
            'country' => $country
        );

        if ($statusCode == http_helper::STATUS_SUCCESS) {
            $data['access_token'] = $accessToken;
            $data['modules'] = $modules;
            $data['mobile'] = utils_helper::startsWith( $mobile , '0' ) && (strlen($mobile) === 10 )  ? substr($mobile , 1) : $mobile ;
//            $data['identity'] = $identity;
        }

        if(!empty($reset_token)){
            $data['reset_token'] = $reset_token ;
        }

        http_helper::response($this, $statusCode, $data , $title , $message );
    }

    function meetings_get() {
        $serviceId = Service_model::SERVICE_US03;

        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);
        $clientModel = http_helper::getClientModel($this, $reqHeaders);

        $access_token = $reqHeaders ['access_token'];
        log_message("debug", "meetings_get access_tokn=" . $access_token);
        if (empty($access_token)) {
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }

        $user_token_model = $this->user_token_model->findByAccessToken($access_token);
        log_message("debug", "meetings_get  user_token_model=" . json_encode($user_token_model));
        if (empty($user_token_model)) {
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }

        $userLogin = $this->user_model->findByPk($user_token_model->user_id);
        log_message("debug", "meetings_get userLogin=" . json_encode(userLogin));
        if (empty($userLogin)) {
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }
// 		echo json_encode ( $userLogin );

        $view = $this->get('view');
        if (empty($view)) {
            $view = "d";
        }

        $start = "";
        $end = "";
        if ($view == "d") {
// 			$start = date('Y-m-d H:i:s') ;
// 			$end = date ( 'Y-m-d H:i:s', strtotime ( $start . "+1 days" ) );

            $start = date('Y-m-d') . " 00:00:01";
// 			$end = 	date('Y-m-d')." 23:59:59" ;
            $end = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . "+2 days")) . " 00:00:00";
        } else if ($view == "m") {

// 			$start = date('Y-m-d H:i:s', strtotime ( $start . "-90 days" )) ;
// 			$end = date ( 'Y-m-d H:i:s', strtotime ( $start . "+180 days" ) );
            $start = date('Y-m-d H:i:s', strtotime($start . "-30 days"));
            $end = date('Y-m-d H:i:s', strtotime($start . "+90 days"));
        }

        log_message("debug", "Find Meetings from " . $start . " to " . $end);
        $events = $this->event_model->findAllByUserId($userLogin->user_id, $start, $end);

// 		echo json_encode($events) ;
        $eventDatas = array();
        if (!empty($events)) {
            foreach ($events as $i => $event) {
                $roomModel = $this->room_model->findByPk($event->room_id);
                if (!empty($roomModel)) {
                    $eventDatas[$i]['room_id'] = $roomModel->room_id;
                    $eventDatas[$i]['room_name'] = $roomModel->room_name;
                    $eventDatas[$i]['room_type'] = $roomModel->room_type_id;

					$eventDatas[$i]['event_type'] = $event->event_type;
					$eventDatas[$i]['accept'] = $event->is_accept;

                    $room_type = $this->room_type_model->findByPk($roomModel->room_type_id);

                    $room_type_name = "";
                    $room_type_color = "";
                    if (!empty($room_type)) {
                        $room_type_name = $room_type->type_name;
                        $room_type_color = $room_type->color;
                    }

                    $eventDatas[$i]['room_type_name'] = $room_type_name;
                    $eventDatas[$i]['room_type_color'] = $room_type_color;

                    $floor_id = $roomModel->floor_id;


                    $floor_model = $this->floor_model->findByPk($floor_id);
                    if (empty($floor_model)) {
                        http_helper::response($this, http_helper::STATUS_DATA_NOT_FOUND);
                    }

                    $building_model = $this->building_model->findByPk($floor_model->building_id);
                    if (empty($building_model)) {
                        http_helper::response($this, http_helper::STATUS_DATA_NOT_FOUND);
                    }

                    $location_model = $this->location_model->findByPk($building_model->location_id);
                    if (empty($location_model)) {
                        http_helper::response($this, http_helper::STATUS_DATA_NOT_FOUND);
                    }
                    $tablet = array();

                    $eventDatas[$i]['building_id'] = intval($building_model->building_id);
                    $eventDatas[$i]['building_name'] = $building_model->building_name;

                    $eventDatas[$i]['location_id'] = intval($location_model->location_id);
                    $eventDatas[$i]['location_name'] = $location_model->location_name;

                    $eventDatas[$i]['floor_id'] = intval($floor_model->floor_id);
                    $eventDatas[$i]['floor_name'] = $floor_model->floor_name;


                    //$now_availables [$i] ['layout'] = $layout . (strpos($layout, '?') ? "&t=" . time() : "?t=" . time());

                    $phoneno_property_model = $this->room_property_model->findByPk($roomModel->room_id, "phone_no", false);
                    if (!empty($phoneno_property_model)) {
                        $phone_no = $phoneno_property_model->property_value;
                    }
                    $eventDatas [$i] ['phone_no'] = $phone_no;

                    $isdn_property_model = $this->room_property_model->findByPk($roomModel->room_id, "isdn", false);
                    if (!empty($isdn_property_model)) {
                        $isdn = $isdn_property_model->property_value;
                    }
                    $eventDatas [$i] ['isdn'] = $isdn;
                }

                $eventDatas[$i]['meeting']['meeting_id'] = intval($event->event_id);
// 				$eventDatas[$i]['meeting']['subject'] = $event->topic ;

                /*
                  $contact_tmp = "" ;
                  preg_match('/##(\w+)##/', $event->topic , $matches);
                  if(!empty($matches)){
                  $subject = str_replace($matches[0],"", $event->topic );
                  $contact_id_tmp = intval(str_replace("#","",$matches[0])) ;
                  if(!empty($contact_id_tmp) && $contact_id_tmp>0){
                  $contact_tmp_model = $this->contact_model->findByPk($contact_id_tmp) ;
                  if(!empty($contact_tmp_model)){
                  $contact_tmp = $contact_tmp_model->display_name ;
                  }
                  }

                  }else{
                  $subject = $event->topic ;
                  }
                 */

                $subject = $event->topic;
                $eventDatas[$i]['meeting']['subject'] = $subject;

                $eventDatas[$i]['meeting']['start'] = $event->start_time;
                $eventDatas[$i]['meeting']['end'] = $event->end_time;

                $ownerContactId = $event->create_by;
                if ($ownerContactId == $userLogin->contact_id) {
                    $eventDatas[$i]['meeting']['is_owner'] = 1;
                } else {
                    $eventDatas[$i]['meeting']['is_owner'] = 0;
                }

                $contact = array();
                if ($ownerContactId > 0) {
                    $contactModel = $this->contact_model->findByPk($ownerContactId);
                    if (!empty($contactModel)) {
                        $contact = array(
                            'id' => intval($contactModel->contact_id),
                            'name' => is_null($contactModel->display_name) ? "" : $contactModel->display_name,
                            'phone' => is_null($contactModel->telephone) ? "" : $contactModel->telephone,
                            'email' => is_null($contactModel->email) ? "" : $contactModel->email,
                            'employee_id' => $contactModel->employee_id
                        );
                    }
                }

                $eventDatas[$i]['meeting']['status'] = intval($event->status_id);
                $eventDatas[$i]['meeting']['host'] = $contact;

                $hashtag = [];

//                $note = strip_tags(nl2br($event->detail));
                $body_note = "" ;
                $note = $event->detail;
                if (!is_null($note)) {
//                    $hastags_json = utils_helper::getHashtags(strip_tags($note));
                    $hastags_json = utils_helper::getHashtags($note);
                    if ($hastags_json) {
                        foreach ($hastags_json as $hastag) {
                            $hashtag[] = array(
                                'name' => explode(":", $hastag)[0],
                                'value' => explode(":", $hastag)[1]
                            );
                        }

//                        $note = trim(preg_replace("/\r|\n/", " ", preg_replace('/#\S+ */', '', $note)));
//                        $note = trim( preg_replace('/#\S+ */', '', $note));
                    }
                }
                $details = explode("#", $note);
                //log_message("debug","details=".print_r($details,TRUE)) ;
                if(!empty($details)){
                    foreach ($details as $detail){
                        $detail_name = explode(":", $detail)[0];
                        $detail_value = explode(":", $detail)[1];
                        //log_message("debug","detail_name=".$detail_name.",detail_value=".$detail_value) ;
                        if($detail_name === "note"){
                            $body_note = $detail_value ;
                        }
                    }
                }
                //** getnote */
                /** note & attendees * */
                //log_message("debug","final body=".nl2br($body_note)) ;
                $eventDatas[$i]['meeting']['note'] = nl2br($body_note);
                $eventDatas[$i]['meeting']['hashtags'] = $hashtag;

                $attendeeDatas = [];
                $attendees = $this->event_attendee_model->findByEventId($event->event_id);
                if (!empty($attendees)) {
                    foreach ($attendees as $attendee) {

                        $contactModel = $this->contact_model->findByPk($attendee->contact_id);
                        if (!empty($contactModel)) {

                            if (!empty($contactModel->user_logon)) {
                                $attendeeDatas[] = array(
//                                    'id' => intval($contactModel->contact_id),
//                                    'name' => $contactModel->display_name,
//                                    'phone' => $contactModel->telephone,
//                                    'email' => $contactModel->email,
//                                    'employee_id' => $contactModel->employee_id
                                    'id' => intval($contactModel->contact_id),
                                    'name' => is_null($contactModel->display_name) ? "" : $contactModel->display_name,
                                    'phone' => is_null($contactModel->telephone) ? "" : $contactModel->telephone,
                                    'email' => is_null($contactModel->email) ? "" : $contactModel->email,
                                    'employee_id' => is_null($contactModel->employee_id) ? "" : $contactModel->employee_id
                                );
                            }
                        } else {
                            $attendeeDatas[] = array(
                                'id' => 0,
                                'name' => "",
                                'phone' => "",
                                'email' => is_null($attendee->email) ? "" : $attendee->email,
                                'employee_id' => ""
                            );
                        }
                    }
                }
                $eventDatas[$i]['meeting']['attendees'] = $attendeeDatas;
            }
        }
        $data = array('schedules' => $eventDatas);
        http_helper::response($this, http_helper::STATUS_SUCCESS, $data);
    }

    function meeting_delete($meetingId) {

        $serviceId = Service_model::SERVICE_US04;

        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);

        $access_token = $reqHeaders ['access_token'];


        if (empty($access_token)) {
            log_message("debug", "Empty Token !!");
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }

        $client_id = $reqHeaders ['client_id'];
// 		echo $client_id ; exit;
        $clientModel = $this->client_model->findByUuid($client_id);
        if (empty($clientModel)) {
            log_message("debug", "Empty client model !!");
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }

// 		$userLogin = $this->user_model->findByAccessTokenAndClientId ( $access_token, $clientModel->client_id );
// // 		echo json_encode($userLogin) ; exit;
// 		if (empty($userLogin)) {
// 			log_message("debug", "Empty user login !!") ;
// 			http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
// 		}
        // 		$userLogin = $this->user_model->findByAccessTokenAndClientId ( $access_token, $clientModel->client_id );
        // 		if (empty($userLogin)) {
        // 			http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
        // 		}

        $user_token_model = $this->user_token_model->findByAccessToken($access_token);
        if (empty($user_token_model)) {
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }

        $userLogin = $this->user_model->findByPk($user_token_model->user_id);
        if (empty($userLogin)) {
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }

        $event_model = $this->event_model->findByPk($meetingId);
        if (empty($event_model) || $event_model->status_id != 1) {
            log_message("debug", "Empty cancel event selected model !!");
            http_helper::response($this, http_helper::STATUS_MEETING_NOT_FOUND);
        }
        log_message("debug", json_encode($event_model));



// 		echo  $userLogin->contact_id . "  |  " . $event_model->create_by  ; exit;
        if ($userLogin->contact_id == $event_model->create_by) {

// 			$event_model->status_id = 3 ;
// 			$event_model->update_by = $userLogin->contact_id ;
// 			$event_model->update_time = date('Y-m-d H:i:s') ;
// 			$this->event_model->update($event_model);

            $event_code = $event_model->event_code;
            $room_id = $event_model->room_id;

            log_message("debug", $event_code);
            if (empty($event_code)) {
                http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR);
            }

// 			$this->config->load('ews.php', TRUE);
// 			$ews_config = [
// 					'host' => $this->config->item('server', 'ews'),
// 					'admin' => $this->config->item('adminuser', 'ews'),
// 					'pass' => $this->config->item('adminpassword', 'ews'),
// 			];
// 			$contact_model = $this->contact_model->findByPk($userLogin->contact_id) ;
// 			log_message("debug","contact=". json_encode($contact_model)) ;
// 			if(empty($contact_model)){
// 				http_helper::response ( $this, http_helper::STATUS_INTERNAL_SERVER_ERROR );
// 			}
// 			$host = $ews_config['host'] ;
// 			if(crypt_helper::IS_ENCRYPTION){
// 				$password = user_helper::getPassword($contact_model->user_logon, $userLogin->password, $userLogin->auth_key, $userLogin->created_at) ;
// 				$password = user_helper::getPassword($userLogin->password , $userLogin->auth_key ) ;
// 			}else{
// 				$password = $userLogin->password ;
// 			}
// 			$changeKey = null ;
// 			$this->load->library('ews');
//                         log_message("debug","Cancel : ".$contact_model->user_logon."::::::::".$password) ;
// 			$ews = $this->ews->create_ews_instants($host,$contact_model->user_logon,$password);
// //                         log_message("debug","EWS Credentail : ".$ews) ;
// //			var_dump($ews); exit;
// 			$reserve_result = $this->ews->calendar_delete_item($ews,$event_code,$changeKey) ;
// 			$ResponseCode = $reserve_result->ResponseCode ;
// 			$ResponseClass = $reserve_result->ResponseClass ;
//                        if($ResponseCode == "NoError" && $ResponseClass == "Success" ) {
//				http_helper::response ( $this, http_helper::STATUS_SUCCESS );
//			}else {
//				http_helper::response ( $this, http_helper::STATUS_EWS_ERROR );
//			}
//

            $data = array(
                'room_id' => $room_id,
                'event_code' => $event_code,
            );
            log_message('error', "Request Data\n" . json_encode($data));
            $ch = curl_init();

            // $reserv_url = "http://172.17.31.111:8080/micore/reserve" ;
            $cancel_url = "http://192.168.117.130:8080/micore/cancel";

            $this->config->load('micore.php', TRUE);
            $url_config = $this->config->item('url', 'micore');

            log_message("debug", "url_config = " + $url_config);
            if (!empty($url_config)) {
                $cancel_url = $url_config . "cancel";
            }

            log_message("debug", "cancel_url = " + $cancel_url);

            curl_setopt($ch, CURLOPT_URL, $cancel_url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $json_response = curl_exec($ch);
            if (empty($json_response)) {
                log_message('error', "Request MI-Core Failed!");
                http_helper::response($this, http_helper::STATUS_REQUEST_MICORE_FAILED);
            }

            curl_close($ch);
            log_message('error', $json_response);
            $json = json_decode($json_response);
            log_message('error', json_encode($json));
            $response_code = $json->responseCode;
            $response_status = $json->responseStatus;
            log_message('error', "response_code = $response_code and response_status = $response_status");
            http_helper::response($this, $response_code, $response_status);
        } else {
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }
    }

    function register_post() {
        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);

//        $client_id = $reqHeaders ['client_id'];
        $username = trim($this->post('username'));
//        $password = trim($this->post('password'));

        $country_id = (null !== $this->post('country_id')) ? intval($this->post('country_id')) : 1;
        $country = $this->country_model->findByPk($country_id) ;

        $mobile = trim($this->post('mobile'));
        $identity = trim($this->post('identity'));

        if (empty($username)) {
            log_message("error", "Login failed! , empty username [" . $username . "]");
            http_helper::response($this, http_helper::STATUS_INVALID_USERNAME);
        }

//        if (empty($password)) {
//            log_message("error", "Login failed! , empty $password [" . $password . "]");
//            http_helper::response($this, http_helper::STATUS_INVALID_PASSWORD);
//        }

        if (empty($mobile)) {
            log_message("error", "Empty mobile $mobile");
            http_helper::response($this, http_helper::STATUS_INVALID_MOBILE);
        }

        // Validate Mobile Number
        $mobile = utils_helper::formatMobile($mobile);
        log_message("debug", "mobile formatted : " . $mobile);
        if (!$mobile) {
            http_helper::response($this, http_helper::STATUS_INVALID_MOBILE);
        }

//        $userModel = $this->user_model->findByMobile($mobile);
//        if (!empty($userModel)) {
//            http_helper::response($this, http_helper::STATUS_MOBILE_ALREADY_EXIST);
//        }

        $domain = "" ;

        if (count(explode("\\", $username)) > 1) {
            $domain = explode("\\", $username)[0];
            $username = explode("\\", $username)[1];
        }
        
        if (empty($domain)) {
            $domain = $this->config->item('domain_default');
            log_message("debug", "Domain from Config :" . strtolower($domain));
        }

        log_message("debug", "Find user:[$domain|$username] !!");

        //$loginLdapUser = $this->ldap_client_model->perform_ldap_auth ( $username, $password, $domain );

        $loginLdapUser = srp_helper::get_anonymous_ldap_connection($this, $domain, $username, $identity );
        if (empty($loginLdapUser) || !$loginLdapUser) {
            http_helper::response($this, http_helper::STATUS_LOGIN_LDAP_FAILED);
        }

        if ($loginLdapUser === -1) {
            return http_helper::response($this, http_helper::STATUS_INVALID_IDENTITY_NUMBER);
        }

        $userModel = $this->user_model->findByUsername($username, $domain);
        log_message("debug", "userModel:" . json_encode($userModel));

        $temp_password = "p@ssW0rD!" ;
        // Update pass from AD
        $salt = user_helper::generateSalt();
        $encryptPass = user_helper::setPassword($temp_password, $salt);

        $contact_model = $this->contact_model->findByUserLogon($username, $domain);

        log_message("debug", "Contact Model " . json_encode($contact_model));
        if (empty($contact_model)) {
            http_helper::response($this, http_helper::STATUS_USER_NOT_FOUND);
        }

        $contactId = $contact_model->contact_id;

        if (empty($userModel) ) {


            $insertData = array(
                'user_name' => $username,
                'password' => $encryptPass,
                'contact_id' => $contactId,
                'role_id' => 3, // EmployeeId
                'created_at' => date('Y-m-d H:i:s'),
                'is_active' => 0,
                'auth_key' => $salt
            );

            $userModel = $this->user_model->insert($insertData);

        }else{
            if (!empty( $userModel->mobile ) ) {
                // Already registered
                $params[] = array(
                    "mobile"=> utils_helper::toToyotaFormat($userModel->mobile)
                );

                return http_helper::response($this, http_helper::STATUS_USER_ALREADY_REGISTER ,null, null,null, $params );
            }
        }

        // check last reset password
        $otp_model = $this->otp_model->findByMobile($mobile);
        log_message("debug", "otp_model :" . json_encode($otp_model));
        if (!empty($otp_model)) {

            $waiting_enable = TRUE;
            try {
                $waiting_enable_model = $this->config_model->findByKey("otp.time.waiting.enable");
                log_message("debug", "waiting_enable_model :" . json_encode($waiting_enable_model));

                if(!empty($waiting_enable_model)){
                    $waiting_enable = $waiting_enable_model->value ;
                }
            } catch (Exception $e) {
                log_message("error", "Exception :" .  print_r($e,TRUE ) );
            }
            log_message("debug", "waiting_enable :" . json_encode($waiting_enable));

            if ($waiting_enable) {

                $waiting_seconds = 60;
                try {
                    $waiting_seconds = $this->config_model->findByKey("otp.time.waiting.seconds")->value;
                } catch (Exception $e) {
                    
                }

                $second_diff = utils_helper::second_diff($otp_model->created_at, date('YmdHis'));
                log_message("debug", "time diff $otp_model->created_at " . date('YmdHis') . " : $second_diff ");
                if ($second_diff < $waiting_seconds) { // waiting new request after 60 seconds
                    return http_helper::response($this, http_helper::STATUS_WAITNG_FOR_VERIFY_CODE);
                }
            }
        }

        /** Gnerate OTP * */
        $ref_code = srp_helper::generateRefCode();
        log_message("debug", "Ref code :" . $ref_code);

        $otp = srp_helper::generateVerifyCode();
        log_message("debug", "OTP : " . $otp);

        $otp_token = srp_helper::generateToken();
        log_message("debug", "OTP token : " . $otp_token);

        $otp_model = array(
            'otp_token' => $otp_token,
            'created_at' => date('YmdHis'),
            'expired_at' => date('YmdHis', strtotime("+15 minutes", strtotime(date('YmdHis')))),
            'ref_code' => $ref_code,
            'otp' => $otp,
            'mobile' => $mobile,
            'status_id' => srp_helper::STATUS_WAIT,
            'user_id' => $userModel->user_id,
            'action' => 'register'
        );

        $otp_model = $this->otp_model->insert($otp_model);
        log_message("debug", "otp_model inserted : " . json_encode($otp_model));

        if (empty($otp_model->otp_id)) {
            return http_helper::response($this, http_helper::STATUS_UNKNOWN_ERROR);
        }

        // send sms
        sms_helper::send_otp($this, $mobile, $otp, $ref_code);

        $response_data = array(
            'otp_token' => $otp_token,
            'ref_code' => $ref_code,
            'expired' => date('Y-m-d H:i', strtotime($otp_model->expired_at))
        );

        $show_otp = 0;
        try {
            $show_otp = $this->config_model->findByKey("otp.show")->value;
        } catch (Exception $e) {
            
        }

        if ($show_otp) {
            $response_data['tmp_otp'] = $otp;
        }

        /** don't use  custom message */
        $mobile_toyota = utils_helper::toToyotaFormat($country->code . substr($mobile,1,9)) ;
        log_message("debug","mobile_toyota:$mobile_toyota")  ;
        $title = "Registration" ;
        $message = "System has sent one time password (OTP) via SMS to $mobile_toyota.<br>Please use OTP (Ref.No.:$ref_code) to verify identity" ;
        log_message("debug","message:$message")  ;

        return http_helper::response($this, http_helper::STATUS_SUCCESS, $response_data , $title , $message );
    }

    function register_confirm_post() {
        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);
        if (NULL === $reqHeaders || empty($reqHeaders)) {
            http_helper::response($this, http_helper::STATUS_INVALID_HEADER);
        }

        $client_id = $reqHeaders['client_id'];
        $otp_token = $reqHeaders['otp_token'];

        $otp = $this->post("otp");

        if (NULL === $otp_token) {
            http_helper::response($this, http_helper::STATUS_INVALID_HEADER);
        }

        $otp_model = $this->otp_model->findByToken($otp_token);
        if (empty($otp_model)) {
            http_helper::response($this, http_helper::STATUS_INVALID_OTP);
        }


        if (empty($otp)) {
            http_helper::response($this, http_helper::STATUS_INVALID_OTP);
        }

        if ($otp !== $otp_model->otp) {
            http_helper::response($this, http_helper::STATUS_INVALID_OTP);
        }

        /*  Valid OTP
         * 1) update user info(mobile,identity)
         * 2) set otp_status to Accept
         */

        $user_model = $this->user_model->findByPk($otp_model->user_id);
        if (empty($user_model)) {
            http_helper::response($this, http_helper::STATUS_UNKNOWN_ERROR);
        }

        /* 1) */
        $update_data = array(
            'mobile' => $otp_model->mobile ,
            'is_active' => 1
        );

        $user_model = $this->user_model->update($update_data, $user_model->user_id);

        /* 2) */
        $update_data_otp = array('status_id' => Otp_model::STATUS_VERIFIED, 'verified_at' => date('YmdHis'));
        $this->otp_model->update($update_data_otp, $otp_model->otp_id);

        // send email to notify user
        $contact_model = $this->contact_model->findByPk($user_model->contact_id);
        if (!empty($contact_model)) {
            $send_email_result = srp_helper::send_email($contact_model->email, $contact_model->user_logon, $contact_model->display_name, "regis");
            log_message("debug", "Send Email Result " . json_encode($send_email_result) . " \n " . json_encode($contact_model));
        }

        // Don't use Login information
//        $company_model = $this->company_model->findByPk($user_model->company_id);
//        $user_name = $company_model->domain_name . "\\" . $user_model->user_name;
//        $password = user_helper::getPassword($user_model->password, $user_model->auth_key);
//        return $this->getLoginInfo($client_id, $user_model);




        /** send reset verify token if user want ot reset **/
        $reset_pass_after_regis = false ;
        try {
            $reset_pass_after_regis = $this->config_model->findByKey("srp.reset_after_regis.enable")->value;

            if($reset_pass_after_regis){

                $ref_code = srp_helper::generateRefCode();
                log_message("debug", "Ref code :" . $ref_code);

                $otp = srp_helper::generateVerifyCode();
                log_message("debug", "OTP : " . $otp);

                $otp_token = srp_helper::generateToken();
                log_message("debug", "OTP token : " . $otp_token);

                $reset_token = srp_helper::generateToken();
                log_message("debug", "Reset token : " . $reset_token);

                $otp_model = array(
                    'otp_token' => $otp_token,
                    'created_at' => date('YmdHis'),
                    'expired_at' => date('YmdHis', strtotime("+15 minutes", strtotime(date('YmdHis')))),
                    'ref_code' => $ref_code,
                    'otp' => $otp,
                    'mobile' => $otp_model->mobile,
                    'status_id' => srp_helper::STATUS_VERIFIED,
                    'user_id' => $user_model->user_id,
                    'action' => 'reset',
                    'reset_token' => $reset_token,
                    'verified_at' => date('YmdHis')
                );

                $otp_model = $this->otp_model->insert($otp_model);
                log_message("debug", "otp_model inserted : " . json_encode($otp_model));


            }

        } catch (Exception $e) {
            log_message('error',"Exception Error". print_r($e,true)) ;
        }


//        return http_helper::response($this, http_helper::STATUS_SUCCESS , $response_data , $title , $message );


//        $title = "Registration" ;
//        $message = "Register Successful<br>System has sent the confirmation mail to ". $contact_model->email ;
        $title = "Registration Successful" ;
        $message = "System has sent the confirmation mail to ". $contact_model->email ;
        log_message("debug","message:$message")  ;

        return $this->getLoginInfo($client_id,$user_model,http_helper::STATUS_SUCCESS ,$title , $message , $reset_token);
    }

    function update_post() {
        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);
        if (NULL === $reqHeaders || empty($reqHeaders)) {
            http_helper::response($this, http_helper::STATUS_INVALID_HEADER);
        }
        $access_token = $reqHeaders ['access_token'];
        log_message("debug", "meetings_get access_token=" . $access_token);
        if (empty($access_token)) {
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }

        $user_token_model = $this->user_token_model->findByAccessToken($access_token);
        log_message("debug", "meetings_get  user_token_model=" . json_encode($user_token_model));
        if (empty($user_token_model)) {
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }

        $user_model = $this->user_model->findByPk($user_token_model->user_id);
        if (empty($user_model)) {
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }

        $mobile = trim($this->post('mobile'));

        if (empty($mobile)) {
            log_message("error", "Empty mobile $mobile");
            http_helper::response($this, http_helper::STATUS_INVALID_MOBILE);
        }

        // Validate Mobile Number
        $mobile = utils_helper::formatMobile($mobile);
        log_message("debug", "mobile formatted : " . $mobile);
        if (!$mobile) {
            http_helper::response($this, http_helper::STATUS_INVALID_MOBILE);
        }

//        $userModel = $this->user_model->findByMobile($mobile);
//        if (!empty($userModel)) {
//            http_helper::response($this, http_helper::STATUS_MOBILE_ALREADY_EXIST);
//        }

//        if ($user_model->mobile === $mobile ) {
//            // same value
//            log_message("info","update mobile number to the same.") ;
//        }



        // check last reset password
        $otp_model = $this->otp_model->findByMobile($mobile);
        log_message("debug", "otp_model :" . json_encode($otp_model));
        if (!empty($otp_model)) {

// 			$second_diff = utils_helper::second_diff($otp_model->created_at , date('YmdHis')) ;
// 			log_message("debug","time diff $otp_model->created_at ". date('YmdHis')." : $second_diff ") ;
// 			if($second_diff < 60) { // waiting new request after 60 seconds
// 				return http_helper::response ( $this, http_helper::STATUS_WAITNG_FOR_VERIFY_CODE);
// 			}

            $waiting_enable = TRUE;
            try {
                $waiting_enable = $this->config_model->findByKey("otp.time.waiting.enable")->value;
            } catch (Exception $e) {
                
            }


            if ($waiting_enable) {

                $waiting_seconds = 60;
                try {
                    $waiting_seconds = $this->config_model->findByKey("otp.time.waiting.seconds")->value;
                } catch (Exception $e) {
                    
                }

                $second_diff = utils_helper::second_diff($otp_model->created_at, date('YmdHis'));
                log_message("debug", "time diff $otp_model->created_at " . date('YmdHis') . " : $second_diff ");
                if ($second_diff < $waiting_seconds) { // waiting new request after 60 seconds
                    return http_helper::response($this, http_helper::STATUS_WAITNG_FOR_VERIFY_CODE);
                }
            }
        }

        /** Gnerate OTP * */
        $ref_code = srp_helper::generateRefCode();
        log_message("debug", "Ref code :" . $ref_code);

        $otp = srp_helper::generateVerifyCode();
        log_message("debug", "OTP : " . $otp);

        $otp_token = srp_helper::generateToken();
        log_message("debug", "OTP token : " . $otp_token);

        $otp_model = array(
            'otp_token' => $otp_token,
            'created_at' => date('YmdHis'),
            'expired_at' => date('YmdHis', strtotime("+15 minutes", strtotime(date('YmdHis')))),
            'ref_code' => $ref_code,
            'otp' => $otp,
            'mobile' => $mobile,
            'status_id' => srp_helper::STATUS_WAIT ,
            'user_id' => $user_model->user_id,
            'action' => 'update'
        );

        $otp_model = $this->otp_model->insert($otp_model);
        log_message("debug", "otp_model inserted : " . json_encode($otp_model));

        if (empty($otp_model->otp_id)) {
            return http_helper::response($this, http_helper::STATUS_UNKNOWN_ERROR);
        }

        // send sms
        sms_helper::send_otp($this, $mobile, $otp, $ref_code);

        $response_data = array(
            'otp_token' => $otp_token,
            'ref_code' => $ref_code,
            'expired' => date('Y-m-d H:i', strtotime($otp_model->expired_at)) /* ,
                  'tmp_otp' => $otp */
        );
        $show_otp = 0;
        try {
            $show_otp = $this->config_model->findByKey("otp.show")->value;
        } catch (Exception $e) {
            
        }

        if ($show_otp) {
            $response_data['tmp_otp'] = $otp;
        }

        $country_id = $user_model->country_id ;
        $country = $this->country_model->findByPk($country_id) ;

        $mobile = utils_helper::formatMobile($mobile) ;

        $mobile_toyota = utils_helper::toToyotaFormat($country->code . substr($mobile,1,9)) ;
        log_message("debug","mobile_toyota:$mobile_toyota")  ;
        $title = "Update Info" ;
        $message = "System has sent one time password (OTP) via SMS to $mobile_toyota.<br>Please use OTP (Ref.No.:$ref_code) to verify identity" ;
        log_message("debug","message:$message")  ;

        return http_helper::response($this, http_helper::STATUS_SUCCESS, $response_data , $title , $message );

    }

    function update_confirm_post() {

        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);
        if (NULL === $reqHeaders || empty($reqHeaders)) {
            http_helper::response($this, http_helper::STATUS_INVALID_HEADER);
        }

        $client_id = $reqHeaders['client_id'];
        $otp_token = $reqHeaders['otp_token'];

        if (NULL === $otp_token) {
            http_helper::response($this, http_helper::STATUS_INVALID_HEADER);
        }

        $otp_model = $this->otp_model->findByToken($otp_token);
        if (empty($otp_model)) {
            http_helper::response($this, http_helper::STATUS_INVALID_OTP);
        }

        $otp = $this->post("otp");
        if (empty($otp)) {
            log_message("error", "Empty OTP!");
            http_helper::response($this, http_helper::STATUS_INVALID_OTP);
        }

        if ($otp !== $otp_model->otp) {
            log_message("error", "Empty OTP Model!");
            http_helper::response($this, http_helper::STATUS_INVALID_OTP);
        }

        /*  Valid OTP
         * 1) update user info(mobile,identity)
         * 2) set otp_status to Accept
         */

        $user_model = $this->user_model->findByPk($otp_model->user_id);
        if (empty($user_model)) {
            http_helper::response($this, http_helper::STATUS_UNKNOWN_ERROR);
        }

        /* 1) */
        $update_data = array(
            'mobile' => $otp_model->mobile ,

        );

        $user_model = $this->user_model->update($update_data, $user_model->user_id);

        /* 2) */
        $update_data_otp = array('status_id' => Otp_model::STATUS_VERIFIED, 'verified_at' => date('YmdHis'));
        $this->otp_model->update($update_data_otp, $otp_model->otp_id);

        // send email to notify user
        $contact_model = $this->contact_model->findByPk($user_model->contact_id);
        if (!empty($contact_model)) {
            $send_email_result = srp_helper::send_email($contact_model->email, $contact_model->user_logon, $contact_model->display_name, "update");
            log_message("debug", "Send Email Result " . json_encode($send_email_result) . " \n " . json_encode($contact_model));
        }


//        $title = "Update Info";
//        $message = "Update Successful<br>System has sent the confirmation mail to ".$contact_model->email;
        $title = "Update Info Successful";
        $message = "System has sent the confirmation mail to ".$contact_model->email;
        log_message("debug", "message:$message");

//        return http_helper::response($this, http_helper::STATUS_SUCCESS, $response_data, $title, $message);
        return $this->getLoginInfo($client_id,$user_model,http_helper::STATUS_SUCCESS ,$title , $message);
    }

    function reset_post() {
        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);
        if ( NULL === $reqHeaders || empty($reqHeaders)) {
            http_helper::response($this, http_helper::STATUS_INVALID_HEADER);
        }

        $username = trim($this->post('username'));
        $identity = trim($this->post('identity'));

        $domain = "";
        if (count(explode("\\", $username)) > 1) {
            $domain = explode("\\", $username)[0];
            $username = explode("\\", $username)[1];
        }
        log_message("debug", "Reset:[" . $username . "|" . $domain . "]");

//        $password = "";
// 		$loginLdapUser = $this->ldap_client_model->perform_ldap_auth ( $username, "", $domain );
        $loginLdapUser = srp_helper::get_anonymous_ldap_connection($this, $domain, $username, $identity );
        if (empty($loginLdapUser) || !$loginLdapUser) {
            http_helper::response($this, http_helper::STATUS_USER_NOT_FOUND);
        }

        if ($loginLdapUser === -1) {
            return http_helper::response($this, http_helper::STATUS_INVALID_IDENTITY_NUMBER);
        }

        $user_model = $this->user_model->findByUsername($username, $domain);
        if (empty($user_model)) {
            http_helper::response($this, http_helper::STATUS_INVALID_USERNAME);
        }

 		$mobile = $user_model->mobile ;
        if(empty($mobile)){
            http_helper::response($this, http_helper::STATUS_NOT_REGISTER_MOBILE);
        }

        // check last reset password
        $otp_model = $this->otp_model->findByMobile($mobile);
        log_message("debug", "otp_model :" . json_encode($otp_model) );
        if (!empty($otp_model)) {

// 			$second_diff = utils_helper::second_diff($otp_model->created_at , date('YmdHis')) ;
// 			log_message("debug","time diff $otp_model->created_at ". date('YmdHis')." : $second_diff ") ;
// 			if($second_diff < 60) { // waiting new request after 60 seconds
// 				return http_helper::response ( $this, http_helper::STATUS_WAITNG_FOR_VERIFY_CODE);
// 			}

            $waiting_enable = TRUE;
            try {
                $waiting_enable = $this->config_model->findByKey("otp.time.waiting.enable")->value;
            } catch (Exception $e) {
                
            }


            if ($waiting_enable) {

                $waiting_seconds = 60;
                try {
                    $waiting_seconds = $this->config_model->findByKey("otp.time.waiting.seconds")->value;
                } catch (Exception $e) {
                    
                }

                $second_diff = utils_helper::second_diff($otp_model->created_at, date('YmdHis'));
                log_message("debug", "time diff $otp_model->created_at " . date('YmdHis') . " : $second_diff ");
                if ($second_diff < $waiting_seconds) { // waiting new request after 60 seconds
                    return http_helper::response($this, http_helper::STATUS_WAITNG_FOR_VERIFY_CODE);
                }
            }
        }

        /** Gnerate OTP * */
        $ref_code = srp_helper::generateRefCode();
        log_message("debug", "Ref code :" . $ref_code);

        $otp = srp_helper::generateVerifyCode();
        log_message("debug", "OTP : " . $otp);

        $otp_token = srp_helper::generateToken();
        log_message("debug", "OTP token : " . $otp_token);

        $otp_model = array(
            'otp_token' => $otp_token,
            'created_at' => date('YmdHis'),
            'expired_at' => date('YmdHis', strtotime("+15 minutes", strtotime(date('YmdHis')))),
            'ref_code' => $ref_code,
            'otp' => $otp,
            'mobile' => $mobile,
            'status_id' => srp_helper::STATUS_WAIT ,
            'user_id' => $user_model->user_id,
            'action' => 'reset'
        );

        $otp_model = $this->otp_model->insert($otp_model);
        log_message("debug", "otp_model inserted : " . json_encode($otp_model));

        if (empty($otp_model->otp_id)) {
            return http_helper::response($this, http_helper::STATUS_UNKNOWN_ERROR);
        }

        // send sms
        sms_helper::send_otp($this, $mobile, $otp, $ref_code);

        $response_data = array(
            'otp_token' => $otp_token,
            'ref_code' => $ref_code,
            'expired' => date('Y-m-d H:i', strtotime($otp_model->expired_at)) /* ,
                  'tmp_otp' => $otp */
        );

        $show_otp = 0;
        try {
            $show_otp = $this->config_model->findByKey("otp.show")->value;
        } catch (Exception $e) {
            
        }

        if ($show_otp) {
            $response_data['tmp_otp'] = $otp;
        }


        $country_id = $user_model->country_id ;
        $country = $this->country_model->findByPk($country_id) ;


        $mobile_toyota = utils_helper::toToyotaFormat($country->code . substr($mobile,1,9)) ;
        log_message("debug","mobile_toyota:$mobile_toyota")  ;
        $title = "Reset Password" ;
        $message = "System has sent one time password (OTP) via SMS to $mobile_toyota.<br>Please use OTP (Ref.No.:$ref_code) to verify identity" ;
        log_message("debug","message:$message")  ;

        return http_helper::response($this, http_helper::STATUS_SUCCESS, $response_data , $title , $message );

    }

    function reset_confirm_post() {

        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);
        if (NULL === $reqHeaders || empty($reqHeaders)) {
            http_helper::response($this, http_helper::STATUS_INVALID_HEADER);
        }

//        $client_id = $reqHeaders['client_id'];
        $otp_token = $reqHeaders['otp_token'];

        if (NULL === $otp_token) {
            http_helper::response($this, http_helper::STATUS_INVALID_HEADER);
        }

        $otp_model = $this->otp_model->findByToken($otp_token);
        if (empty($otp_model)) {
            http_helper::response($this, http_helper::STATUS_INVALID_OTP);
        }

        $otp = $this->post("otp");
        if (empty($otp)) {
            log_message("error", "Empty OTP!");
            http_helper::response($this, http_helper::STATUS_INVALID_OTP);
        }

        if ($otp !== $otp_model->otp) {
            log_message("error", "Empty OTP Model!");
            http_helper::response($this, http_helper::STATUS_INVALID_OTP);
        }

        $user_model = $this->user_model->findByPk($otp_model->user_id);
        if (empty($user_model)) {
            http_helper::response($this, http_helper::STATUS_INVALID_OTP);
        }

        $reset_token = srp_helper::generateToken();
        log_message("debug", "Reset token : " . $reset_token);


        $update_data_otp = array(
            'status_id' => Otp_model::STATUS_VERIFIED,
            'reset_token' => $reset_token,
            'verified_at' => date('YmdHis')
        );

        $this->otp_model->update($update_data_otp, $otp_model->otp_id);

        $response_data = array(
            'reset_token' => $reset_token
        );

        return http_helper::response($this, http_helper::STATUS_SUCCESS, $response_data);
    }

    function reset_chpass_post() {

        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);
        if (NULL === $reqHeaders || empty($reqHeaders)) {
            return http_helper::response($this, http_helper::STATUS_INVALID_HEADER);
        }

        $reset_token = $reqHeaders['reset_token'];
        $new_password = trim($this->post("new_password"));
        log_message("debug", 'reset_token=' . json_encode($reset_token));
        $otp_model = $this->otp_model->findByResetToken($reset_token);
        if (empty($otp_model)) {
            return http_helper::response($this, http_helper::STATUS_INVALID_RESET_TOKEN);
        }


        log_message("debug", 'otp_model=' . json_encode($otp_model));
        $user_id = $otp_model->user_id;

        $user_model = $this->user_model->findByPk($user_id);
        if (empty($user_model)) {
            log_message("error", 'Empty user_model =' . json_encode($user_model));
            return http_helper::response($this, http_helper::STATUS_USER_NOT_FOUND);
        }

        log_message("debug", 'user_model=' . json_encode($user_model));

        $valid_pass = $this->valid_pass($new_password, $user_model->user_name);
        log_message("debug", '$valid pass=' . $valid_pass);
        if ($valid_pass !== 0) {
            $err_msg = "";
            switch ($valid_pass) {
                case 1:
//                  $err_msg = "Minimum 8 characters length" ;
                    return http_helper::response($this, http_helper::STATUS_INVALID_PASSWORD_POLICY_1);
                    break;
                case 2:
//                  $err_msg = "Consist of a mixed combination of alphabets (a-z, A-Z), number (0-9)  and symbols (!@#$%^....)" ;
                    return http_helper::response($this, http_helper::STATUS_INVALID_PASSWORD_POLICY_2);

                    break;
                case 3:
//                  $err_msg = "Not include your first , last names or Windows username" ;
                    return http_helper::response($this, http_helper::STATUS_INVALID_PASSWORD_POLICY_3);
                    break;
            }
        }

        log_message("debug", "valid password!");


        $company = $this->company_model->findByPk($user_model->company_id);

        // Request reset pass to AD
        $reset_result = srp_helper::change_pass($this, $user_model->user_name, $new_password, $company->domain_name);
        log_message("debug", 'reset_result=' . json_encode($reset_result));
        if (!$reset_result['status']) {
            log_message("error", 'Change password failed! ' . $reset_result['message']);
            return http_helper::response($this, http_helper::STATUS_RESET_PASSWORD_FAILED, null, $reset_result['message'], $reset_result['message']);
        }

        // update password of user
        $salt = user_helper::generateSalt();
        $encryptPass = user_helper::setPassword($new_password, $salt);
        log_message("debug", "userModel before reset" . json_encode($user_model));
        $user_model->auth_key = $salt;
        $user_model->password = $encryptPass;
        $user_model->is_active = 1 ;

        $user_model_updated = $this->user_model->update($user_model, $user_model->user_id);
        log_message("debug", "userModel after reset success!! " . json_encode($user_model_updated));
        log_message("debug", "Reset success!!!");
        if(!$user_model_updated) {
            return http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR ) ;
        }

        $update_data_otp = array(
            'status_id' => Otp_model::STATUS_RESETED,
            'reset_at' => date('YmdHis')
        );

        $this->otp_model->update($update_data_otp, $otp_model->otp_id);

        $contact_model = $this->contact_model->findByPk($user_model->contact_id);
        if (!empty($contact_model)) {
            $send_email_result = srp_helper::send_email($contact_model->email, $contact_model->user_logon, $contact_model->display_name, "reset");
            log_message("debug", "Send Email Result " . json_encode($send_email_result) . ":" . json_encode($contact_model));
        }

//        $title = "Reset Password";
//        $message = "Reset Password Successful<br>System has created your windows password";
//        log_message("debug", "message:$message");

        // ** current version **/
//        $title = "Reset Password";
//        $message = "Reset Password Successful<br>and sent the confirmation mail to<br>".$contact_model->email;

        $title = "Reset Password Successful";
        $message = "System has sent the confirmation mail to<br>".$contact_model->email;


        log_message("debug", "message:$message");


        return http_helper::response($this, http_helper::STATUS_SUCCESS, $response_data, $title, $message);

    }

    function valid_pass($password, $user_name = NULL) {

        // Minimum 8 characters length
        if (strlen(trim($password)) < 8) {
            return 1;
        }

        $policy_count = 0 ;
        $containNumber = preg_match('#[\d]#', $password);
        if($containNumber) $policy_count++ ;


        $containLowerAlphabet = preg_match('#[a-z]#', $password);
        if($containLowerAlphabet) $policy_count++ ;

        $containUpperAlphabet = preg_match('#[A-Z]#', $password);
        if($containUpperAlphabet) $policy_count++ ;

        $containSpecialCharacter = preg_match('#[\W]+#', $password);
        if($containSpecialCharacter) $policy_count++ ;

        if($policy_count < 3){
            return 2;
        }

//        if (preg_match('/\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*/', $password)) {
//            return 2;
//        }
//        if (preg_match('/\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*/', $password)) {
//            return 2;
//        }
//        if (preg_match('/\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*/', $password)) {
//            return 2;
//        }

        // Not include your first , last names or Windows username
        if (strpos(strtolower($password), strtolower($user_name)) !== FALSE) {
            return 3;
        }

        return 0;
    }

    function forgot_post() {


        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);

        $client_id = $reqHeaders ['client_id'];
        $username = trim($this->post('username'));

        if (empty($username)) {
            log_message("error", "Login failed! , empty username or password [" . $username  . "]");
            http_helper::response($this, http_helper::STATUS_INVALID_USER_OR_PASSWORD);
        }

        $domain = "";
        if (count(explode("\\", $username)) > 1) {
            $domain = explode("\\", $username)[0];
            $username = explode("\\", $username)[1];
        }

        // set password empty for check 
        $password = "";

        log_message("debug", "Forgot:[username=" . $username . "|password=" . $password . "|domain=" . $domain);

// 		$loginLdapUser = $this->ldap_client_model->perform_ldap_auth ( $username, $password, $domain );
        $loginLdapUser = srp_helper::get_ldap_connection($this, $domain, $username, $password);

        if (empty($loginLdapUser) || !$loginLdapUser) {
            return http_helper::response($this, http_helper::STATUS_LOGIN_LDAP_FAILED);
        }

        $userModel = $this->user_model->findByUsername($username, $domain);
        if (empty($userModel)) {

            $salt = user_helper::generateSalt();
            $encryptPass = user_helper::setPassword($password, $salt);

            $contactId = 0;
            $contact_model = $this->contact_model->findByUserLogon($username, $domain);

            log_message("debug", "Contact Model " . json_encode($contact_model));
            if (empty($contact_model)) {

                http_helper::response($this, http_helper::STATUS_USER_NOT_FOUND);
            }

            $contactId = $contact_model->contact_id;

            $insertData = array(
                'user_name' => $username,
                'password' => $encryptPass,
                'contact_id' => $contactId,
                'role_id' => 3, // EmployeeId
                'created_at' => date('Y-m-d H:i:s'),
                'is_active' => 1,
                'auth_key' => $salt
            );

            $userModel = $this->user_model->insert($insertData);
        }


        // Update pass from AD

        $salt = user_helper::generateSalt();
        $encryptPass = user_helper::setPassword($password, $salt);

        $contact_model = $this->contact_model->findByUserLogon($username, $domain);

        log_message("debug", "Contact Model " . json_encode($contact_model));
        if (empty($contact_model)) {
            http_helper::response($this, http_helper::STATUS_USER_NOT_FOUND);
        }

        $contactId = $contact_model->contact_id;

        $userModel->auth_key = $salt;
        $userModel->password = $encryptPass;
        $userModel->contact_id = $contact_model->contact_id;

        $userModel = $this->user_model->update($userModel, $userModel->user_id);

        if (empty($userModel->mobile) || empty($userModel->identity_number)) {
            return $this->getLoginInfo($client_id, $userModel, http_helper::STATUS_NOT_REGISTER_MOBILE);
        } else {
            return $this->getLoginInfo($client_id, $userModel);
        }
    }


    function validate_post() {

        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders($this);
        $client_id = $reqHeaders ['client_id'];
        $username = trim($this->post('username'));
//        $password = trim($this->post('password'));
        $identity = trim($this->post('identity'));

//        if (empty($username) || empty($password)) {
//            log_message("error", "Login failed! , empty username or password [" . $username . "|" . $password . "]");
//            http_helper::response($this, http_helper::STATUS_INVALID_USER_OR_PASSWORD);
//        }

        $domain = "";
        $userLogon = NULL ;
        if (count(explode("\\", $username)) > 1) {
            $domain = explode("\\", $username)[0];
            $username = explode("\\", $username)[1];
            $userLogon = $username;
        }

        if (empty($domain)) {
            $domain = strtolower($this->config->item('domain_default'));
            log_message("debug", "Domain from Config :" . strtolower($domain));
        }
        log_message("debug", "userLogon = $userLogon");
        if (empty($userLogon)) {
            log_message("error", "Login failed! , empty username ");
            return http_helper::response($this, http_helper::STATUS_INVALID_USERNAME);
        }

//        if (empty($password)) {
//            log_message("error", "Login failed! , empty password ");
//            return http_helper::response($this, http_helper::STATUS_INVALID_PASSWORD);
//        }

        if (empty($identity)) {
            log_message("error", "Login failed! , empty identity ");
            return http_helper::response($this, http_helper::STATUS_INVALID_IDENTITY_NUMBER);
        }




        log_message("debug", "Validate USer:[" . $username .  "|" . $domain);

        $company_id = 0;
        $company = $this->company_model->findByDomainName($domain);

        //log_message("debug", "Company:[" . json_encode($company)."]");
        if (!empty($company)) {
            $company_id = $company->company_id;
        }

// 		$loginLdapUser = $this->ldap_client_model->perform_ldap_auth ( $username, $password, $domain );
        $loginLdapUser = srp_helper::get_anonymous_ldap_connection($this, $domain, $username, $identity);
        log_message("debug", "loginLdapUser:|" . print_r($loginLdapUser,true) . "|");
        if (empty($loginLdapUser) || !$loginLdapUser) {
            return http_helper::response($this, http_helper::STATUS_LOGIN_LDAP_FAILED);
        }

        if ($loginLdapUser === -1) {
            return http_helper::response($this, http_helper::STATUS_INVALID_IDENTITY_NUMBER);
        }


        $userModel = $this->user_model->findByUsername($username, $domain);
        log_message("debug", "userModel:|" . json_encode($userModel) . "|");



        // validate Mobile
// 		http_helper::response ( $this, http_helper::STATUS_NOT_REGISTER_MOBILE );

        $defaultPass = "1234@abcd" ;

        $salt = user_helper::generateSalt();
        $encryptPass = user_helper::setPassword($defaultPass, $salt);

        $contact_model = $this->contact_model->findByUserLogon($username, $domain);
        log_message("debug", "ContactModel [$username, $domain]: " . json_encode($contact_model));
        if (empty($contact_model)) {
            http_helper::response($this, http_helper::STATUS_USER_NOT_FOUND);
        }


        $contactId = $contact_model->contact_id;

        if (empty($userModel)) {


            log_message("debug", "ContactModel:|" . json_encode($contact_model) . "| for user Logon " . $username . " | " . $domain);
            if (!empty($contact_model)) {

                // Create new Contact
// 				http_helper::response ( $this, http_helper::STATUS_USER_NOT_FOUND );
                $contactId = $contact_model->contact_id;

                $insertData = array(
                    'user_name' => $username,
                    'password' => $encryptPass,
                    'contact_id' => $contactId,
                    'role_id' => 3, // EmployeeId
                    'created_at' => date('Y-m-d H:i:s'),
                    'is_active' => 1,
                    'auth_key' => $salt,
                    'company_id' => $company_id
                );

                $userModel = $this->user_model->insert($insertData);
                log_message("debug", "Inserted UserModel:|" . json_encode($userModel) + "| for user Logon " . $username);
            }
        }else{

            $userModel->auth_key = $salt;
            $userModel->password = $encryptPass;
            $userModel->contact_id = $contact_model->contact_id;

            $userModel = $this->user_model->update($userModel, $userModel->user_id);
        }

        log_message("debug", "userModel:|" . json_encode($userModel));


        if (empty($userModel) || empty($userModel->mobile)) {
            return $this->getLoginInfo($client_id, $userModel, http_helper::STATUS_NOT_REGISTER_MOBILE);
        } else {
            $title = "Registered" ;
            $message = "You have already registered<br>Mobile Phone:" . utils_helper::toToyotaFormat($userModel->mobile) ;
            return $this->getLoginInfo($client_id, $userModel, $statusCode = http_helper::STATUS_SUCCESS , $title , $message  );
        }
    }



    function login_post() {
		log_message("error", " login_post call") ;
        $response_data = NULL;
        $reqHeaders = http_helper::requestHeaders ($this);

        $client_id = $reqHeaders ['client_id'];
        $username = trim( $this->post('username') ) ;
        $password = trim( $this->post('password') );
		log_message("error", "username : ".$username);
        if(empty($username)){
            log_message("error", "Login failed! , empty username!") ;
            http_helper::response ( $this, http_helper::STATUS_INVALID_USERNAME );
        }

        if(empty($password)){
            log_message("error", "Login failed! , empty password!") ;
            http_helper::response ( $this, http_helper::STATUS_INVALID_PASSWORD );
        }

        $domain = "" ;
        if(count(explode( "\\", $username )) > 1 ){
            $domain = explode( "\\", $username )[0] ;
            $username = explode( "\\", $username )[1] ;
        }

        if (empty($domain)) {
            $domain = strtolower($this->config->item('domain_default'));
            log_message("debug", "Domain from Config :" . strtolower($domain));
        }

        log_message("debug","Login:[". $username . "|" . $password ."|". $domain) ;

        $company_id = 0 ;
        $company = $this->company_model->findByDomainName($domain) ;
        if(!empty($company)){
            $company_id = $company->company_id ;
        }

        $loginLdapUser = srp_helper::get_ldap_connection($this , $domain, $username, $password) ;
        log_message("debug","loginLdapUser:|". print_r($loginLdapUser,true)."|") ;
        if ( empty( $loginLdapUser) || !$loginLdapUser) {
            return http_helper::response ( $this, http_helper::STATUS_LOGIN_LDAP_FAILED );
        }

        $user_model = $this->user_model->findByUsername ( $username , $domain);
        log_message("debug","userModel:|". json_encode($user_model)."|") ;


        $salt = user_helper::generateSalt();
        $encryptPass = user_helper::setPassword($password, $salt);

        $contact_model = $this->contact_model->findByUserLogon($username, $domain);

        log_message("debug", "ContactModel:" . json_encode($contact_model));
        if (empty($contact_model)) {
            http_helper::response($this, http_helper::STATUS_USER_NOT_FOUND);
        }

        $contactId = $contact_model->contact_id;

        if (empty( $user_model ) ) {

            $insertData = array (
                'user_name' => $username,
                'password' => $encryptPass,
                'contact_id' => $contactId,
                'role_id' => 3, // EmployeeId
                'created_at' => date ( 'Y-m-d H:i:s' ),
                'is_active' => 1,
                'auth_key' => $salt,
                'company_id' => $company_id
            );

            $user_model = $this->user_model->insert ( $insertData );
            log_message("debug", "Inserted UserModel:|".json_encode($user_model) +"| for user Logon ".$username) ;


        }else {
            // Update pass from AD

            log_message("debug", "userModel:|" . json_encode($user_model));
            $user_model->auth_key = $salt;
            $user_model->password = $encryptPass;
            $user_model->contact_id = $contactId ;

            $user_model = $this->user_model->update($user_model, $user_model->user_id);

        }
        //
//        if(empty($user_model->mobile) || empty($user_model->identity_number) ){
//            return $this->getLoginInfo($client_id,$user_model , http_helper::STATUS_NOT_REGISTER_MOBILE) ;
//        }else{
//            return $this->getLoginInfo($client_id,$user_model) ;
//        }
        return $this->getLoginInfo($client_id,$user_model) ;
    }

	function employee_post($employee_id=null){
		$time_now = $this->input->post('time_now');
		$num_attendee = $this->input->post('image');
		$sign = $this->input->post('sign');

		$checkSign = hash_hmac('sha256', $employee_id.$time_now, $this->config->item('signkey'));
		//if($checkSign!=$sign)http_helper::response($this,400, array('sign'=>$sign,'checkSign'=>$checkSign,'detail'=>'Sign not match')) ;
		
		$head = getallheaders ();
		// Post to /user/login
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL,"http://127.0.0.1/miapi-srp/user/login");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = [
			'lat: '.$head['lat'],
			'long: '.$head['long'],
			'client-id: employee-'.$employee_id,
			'client-type: '.$head['client-type'],
			'os: '.$head['os'],
			'os-version: '.$head['os-version'],
			'app-id: '.$head['app-id'],
			'app-version: '.$head['app-version'],
		];
		$this->config->load('ews.php', TRUE);
		$admin = $this->config->item('adminuser', 'ews');
		$pass = $this->config->item('adminpassword', 'ews');

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query(array('username' => $admin,'password'=>$pass)));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);

		curl_close ($ch);

		// further processing ....
		if ($server_output != "") {
			$json = json_decode($server_output);
			$return = array();
			if($json->response_code==0){
				$return['verify_code'] = $json->response_data->access_token;
			}
			http_helper::response($this,http_helper::STATUS_SUCCESS, $return);
		}
		http_helper::response($this,400, array('detail'=>'Cannot post to login','server_time'=>$now));
	}

	function test_get(){
		$key = "12017-01-24 17:00:00";
		$checkSign = hash_hmac('sha256', $key, $this->config->item('signkey'));
		echo $checkSign;
		/*
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL,"http://127.0.0.1/miapi-srp/user/employee/1");
		curl_setopt($ch, CURLOPT_POST, 1);
		$headers = [
			'lat: 13',
			'long: 100',
			'client-id: ',
			'client-type: WB',
			'os: ANDROID',
			'os-version: 9.0.2',
			'app-id: 1',
			'app-version: 1.0.0',
		];
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		//curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query(array('username' => $admin,'password'=>$pass)));

		// in real life you should use something like:
		// curl_setopt($ch, CURLOPT_POSTFIELDS, 
		//          http_build_query(array('postvar1' => 'value1')));

		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);

		curl_close ($ch);

		// further processing ....
		if ($server_output != "") {
			echo $server_output;
		}*/
	}

}
