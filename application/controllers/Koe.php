<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require (APPPATH . 'libraries/REST_Controller.php');
class Koe extends REST_Controller {
	function __construct() {
		parent::__construct ();
	}
	

	public function ews_get(){
		$host = "192.168.117.133" ;
		$username = "thawalit" ;
		$password = "fcD!1234" ;
		
		$ews = new ExchangeWebServices($host, $username, $password);
		
		// start building the find folder request
		$request = new EWSType_FindFolderType();
		$request->Traversal = EWSType_FolderQueryTraversalType::SHALLOW;
		$request->FolderShape = new EWSType_FolderResponseShapeType();
		$request->FolderShape->BaseShape = EWSType_DefaultShapeNamesType::ALL_PROPERTIES;
		
		// configure the view
		$request->IndexedPageFolderView = new EWSType_IndexedPageViewType();
		$request->IndexedPageFolderView->BasePoint = 'Beginning';
		$request->IndexedPageFolderView->Offset = 0;
		
		// set the starting folder as the inbox
		$request->ParentFolderIds = new EWSType_NonEmptyArrayOfBaseFolderIdsType();
		$request->ParentFolderIds->DistinguishedFolderId = new EWSType_DistinguishedFolderIdType();
		$request->ParentFolderIds->DistinguishedFolderId->Id = EWSType_DistinguishedFolderIdNameType::INBOX;
		
		// make the actual call
		$response = $this->ews->FindFolder($request);
		echo '<pre>'.print_r($response, true).'</pre>';
	}
	
	public function base_get(){
		echo base_url() ;
	}
	
	public function cache_get(){
		$this->load->driver('cache');
		$this->cache->memcached->save('foo', 'bar', 10);
		$foo = $this->cache->memcached->get('foo') ;
		echo "Cache:".$foo ;
// 		$this->cache->memcached->save('foo', 'bar', 10);
	}
	
	public function hashtag_get(){
		$topic ="Test ทดสอบ  テスト ##Jeerawat Rakcheewong##" ;
		
		preg_match('/##(\w+)(.*)(\w+)##/',$topic , $matches);
		if(!empty($matches)){
// 			$subject = $matches[0] ;
			$subject = str_replace($matches[0],"",$topic);
			$contactArr = implode(" ",$matches) ;
			$contact = str_replace("#","",$contactArr) ;
		}else{
			$subject = $topic;
			$contact = "AA" ;
		}
		
		echo $subject ." : " .  $contact ;
		
	}
	
	public function password_get(){
		$keyStr = "9390F3B7690F03C5" ;
		$salt = "9390F3B7690F03C5" ;
		$plainText = "Test ttt tttt" ;
		
		$encryptedText = user_helper::encrypt($keyStr, $salt, $plainText) ;
		echo "<br/>Encrypted:".$encryptedText;
		$decryptedText = user_helper::decrypt($keyStr, $salt, $encryptedText) ;
		echo "<br/>Decrypted:".$decryptedText;
	}
	
	
}