<?php

defined('BASEPATH') or exit('No direct script access allowed');

require (APPPATH . 'libraries/REST_Controller.php');

class Room extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('service_model');
        $this->load->model('client_model');
        $this->load->model('room_model');
        $this->load->model('event_model');
        $this->load->model('user_model');
        $this->load->model('user_token_model');
        $this->load->model('contact_model');
        $this->load->model('floor_model');
        $this->load->model('room_facility_model');
        $this->load->model('theme_model');
        $this->load->model('room_property_model');
        $this->load->model('facility_model');
        $this->load->model('facility_property_model');
        $this->load->model('room_synchronizer_model');
        $this->load->model('building_model');
        $this->load->model('location_model');
        $this->load->model('event_queue_model');
        $this->load->model('ocr_model');
        $this->load->model('config_model');

        $this->load->model('hashtag_model');

        $this->load->model('room_type_model');
    }

    function schedules_get($roomId) {
        $reqHeaders = http_helper::requestHeaders($this);
        $clientModel = http_helper::getClientModel($this, $reqHeaders);

        $start_now = !empty($this->input->get('start_now')) ? intval($this->input->get('start_now')) : 0;

        $view = !empty($this->input->get('view')) ? $this->input->get('view') : 'd';

        // echo json_encode($clientModel) ; exit;
        $currentTime = date('Y-m-d H:i:s');

        if ('m' === $view) {
            $startTime = date('Y-m-d H:i:s', strtotime($currentTime . "-1 months"));
            $endTime = date('Y-m-d H:i:s', strtotime($currentTime . "+2 months"));
        } else {
            $startTime = $start_now ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s', strtotime($currentTime . "-2 hours"));
            $endTime = date('Y-m-d H:i:s', strtotime($currentTime . "+5 hours"));
        }
        // echo $startTime." - ".$endTime ; exit;
        $eventDatas = [];//array();
        $events = $this->event_model->findByRoom($roomId, $startTime, $endTime);
        // echo json_encode($events) ; exit;
        if (!empty($events)) {
            foreach ($events as $i => $event) {
            	$eventtemp = null;
                $eventtemp ['meeting_id'] = intval($event->event_id);
                $subject = $event->topic;

                $eventtemp ['subject'] = $subject;

                $eventtemp ['start'] = $event->start_time;
                $eventtemp ['end'] = $event->end_time;

                $ownerContactId = $event->create_by;
                // echo $ownerContactId ; exit;
                $eventtemp ['host'] = array();
                if (!empty($ownerContactId)) {
                    $contactModel = $this->contact_model->findByPk($ownerContactId);

                    if (!empty($contactModel)) {

                        if (!empty($contactModel->user_logon)) {
                            $eventtemp ['host'] = array(
                                'id' => intval($contactModel->contact_id),
                                'name' => is_null($contactModel->display_name) ? "" : $contactModel->display_name,
                            	'display_name' => is_null($contactModel->display_name) ? "" : $contactModel->display_name,
                                'phone' => is_null($contactModel->telephone) ? "" : $contactModel->telephone,
                                'email' => is_null($contactModel->email) ? "" : $contactModel->email,
                                'employee_id' => is_null($contactModel->employee_id) ? "" : $contactModel->employee_id
                            );
                        }
                    }
                }

                $eventtemp ['is_checkin'] = intval($event->is_checkin);

                //
                // $hashtag = [];
                // $note = strip_tags(nl2br($event->detail));
                // if (!is_null($note)) {
                //     $hastags_json = utils_helper::getHashtags(strip_tags($note));
                //     if ($hastags_json) {
                //         foreach ($hastags_json as $hastag) {
                //             $hashtag[] = array(
                //                 'name' => explode(":", $hastag)[0],
                //                 'value' => explode(":", $hastag)[1]
                //             );
                //         }
                //
                //         $note = trim(preg_replace("/\r|\n/", " ", preg_replace('/#\S+ */', '', $note)));
                //     }
                // }
                //
                // $eventDatas[$i]['note'] = $note;


                $hashtag = [];

              //                $note = strip_tags(nl2br($event->detail));
                $body_note = "" ;
                $note = $event->detail;
                if (!is_null($note)) {
              //                    $hastags_json = utils_helper::getHashtags(strip_tags($note));
                    $hastags_json = utils_helper::getHashtags($note);
                    if ($hastags_json) {
                        foreach ($hastags_json as $hastag) {
                            $hashtag[] = array(
                                'name' => explode(":", $hastag)[0],
                                'value' => explode(":", $hastag)[1]
                            );
                        }

              //                        $note = trim(preg_replace("/\r|\n/", " ", preg_replace('/#\S+ */', '', $note)));
              //                        $note = trim( preg_replace('/#\S+ */', '', $note));
                    }
                }
                $details = explode("#", $note);
                //log_message("debug","details=".print_r($details,TRUE)) ;
                if(!empty($details)){
                    foreach ($details as $detail){

                        $detail_arr = explode(":", $detail) ;
                        $detail_count = count($detail_arr) ;

                        // $detail_name = explode(":", $detail)[0];
                        $detail_value = "";
                        for($i=0; $i < $detail_count; $i++ ) {
                          if($i === 0){
                              $detail_name = $detail_arr[$i] ;

                          }else{
                              if($i < $detail_count-1){
                                $detail_value .= $detail_arr[$i] .":" ;
                              }else{
                                $detail_value .= $detail_arr[$i] ;
                              }
                          }
                        }

                        // $detail_value = explode(":", $detail)[1];
                        //log_message("debug","detail_name=".$detail_name.",detail_value=".$detail_value) ;
                        if($detail_name === "note"){
                            $body_note = $detail_value ;
                        }
                        
                        if($detail_name === "employee_id"){
                        	$employeeId2 = trim($detail_value);
                        	$contact_model2 = $this->contact_model->findByEmployeeId($employeeId2);
                        	if (!empty($contact_model2)) {
                        		$eventtemp ['host']['display_name'] = is_null($contact_model2->display_name) ? "" : $contact_model2->display_name;
                        	}
                        }
                    }
                }
                //** getnote */
                /** note & attendees * */
                //log_message("debug","final body=".nl2br($body_note)) ;
                $eventtemp['meeting']['note'] = nl2br($body_note);

                $eventtemp['hashtags'] = $hashtag;
                array_push($eventDatas, $eventtemp);
            }
        }

        $room_type_name = "";
        $room_type_color = "";
        $floor_name = "";
        $building_name = "";
        $location_name = "";

        $room = $this->room_model->findByPk($roomId);

        $room_type = $this->room_type_model->findByPk($room->room_type_id);

        if (!empty($room_type)) {
            $room_type_name = $room_type->type_name;
            $room_type_color = $room_type->color;
        }

        $floor_model = $this->floor_model->findByPk($room->floor_id);
        if (!empty($floor_model)) {
            $floor_name = $floor_model->floor_name;

            $building_model = $this->building_model->findByPk($floor_model->building_id);
            if (!empty($building_model)) {
                $building_name = $building_model->building_name;
                $location_model = $this->location_model->findByPk($building_model->location_id);
                if (!empty($location_model)) {
                    $location_name = $location_model->location_name;
                }
            }
        }


        $phoneno_property_model = $this->room_property_model->findByPk($room->room_id, "phone_no", false);
        if (!empty($phoneno_property_model)) {
            $phone_no = $phoneno_property_model->property_value;
        }

        $isdn_property_model = $this->room_property_model->findByPk($room->room_id, "isdn", false);
        if (!empty($isdn_property_model)) {
            $isdn = $isdn_property_model->property_value;
        }

        $room_data = array(
            'id' => intval($room->room_id),
            'name' => $room->room_name,
            'description' => $room->description,
            'type_id' => intval($room->room_type_id),
            'type_name' => $room_type_name,
            'type_color' => $room_type_color,
            'floor' => $floor_name,
            'building' => $building_name,
            'location' => $location_name,
            'phone_no' => $phone_no ,
            'isdn' => $isdn
        );
        $data = array(
            'room' => $room_data,
            'schedules' => $eventDatas
        );
        http_helper::response($this, http_helper::STATUS_SUCCESS, $data);
    }

    function reserve_get() {
        $data = array();
        $this->load->view('api/rm02', $data);
    }

    function clear_get($room_id) {
        $result = $this->cache->memcached->delete("findAllByRoomId_" . $room_id);
        echo json_encode($result);
    }

    // public function reserve2_get(){
    // $this->load->library('ews');
    // $ews = $this->ews->create_ews_instants('192.168.117.133','thawalit@midemo.local','fcD!1234');
    // // $ews = $this->ews->create_ews_instants('192.168.117.133','Thawalit.Junpoung@midemo.local','fcD!1234');
    // if(empty($ews)){
    // return null ;
    // }
    // // var_dump($ews); exit;
    // $date = new DateTime();
    // $subject = "Testtt subject ".$date->format('Y-m-d\TH:i:s') ;
    // $body = "Testtt body".$date->format('Y-m-d\TH:i:s') ;
    // $start_date = $date->modify('+5 hours');
    // $start_date = $date->format('Y-m-d\TH:i:s');
    // // $end_date = $date->modify('+2 hours');
    // $end_date = $date->modify('+15 minutes');
    // $end_date = $date->format('Y-m-d\TH:i:s');
    // $allday = false;
    // $location ="Test location" ;
    // $attendees = "pinit|Pinit@midemo.local;ranon|Ranon@midemo.local" ;
    // $importance = true ;
    // $sensitivity = false ;
    // $room_resource = "2A-10@midemo.local" ;
    // $reserve_result = $this->ews->calendar_add_item($ews, $subject, $body, $start_date, $end_date, $allday, $location, $attendees, $importance, $sensitivity ,$room_resource);
    // echo json_encode($reserve_result) ;
    // // return $reserve_result;
    // }
    function reserve_post() {
        try {
            $serviceId = Service_model::SERVICE_RM04;
            // $post_params = $this->input->post();
            // log_message("debug", json_encode($post_params)) ;
            $response_data = NULL;

            $reqHeaders = http_helper::requestHeaders($this);

            // var_dump($reqHeaders) ; exit;

            $clientModel = http_helper::getClientModel($this, $reqHeaders);

            // var_dump($clientModel) ; exit;

            $reserveFrom = "";
            $userModel = null;
            $clientType = $clientModel->client_type;
            switch ($clientType) {
                case "MB" :
                case "WB" :

                    $access_token = isset($reqHeaders ['access_token']) ? $reqHeaders ['access_token'] : "";
                    if (empty($access_token)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }
                    // echo 'access_koen='.json_encode($access_token) ; exit;

                    $user_token_model = $this->user_token_model->findByAccessToken($access_token);
                    if (empty($user_token_model)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }
                    // echo 'user_token_model='.json_encode($user_token_model) ; exit;

                    $userModel = $this->user_model->findByPk($user_token_model->user_id);
                    if (empty($userModel)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }

                    // echo '$userModel='.json_encode($userModel) ; exit;
                    $reserveFrom = "mobile";

                    break;

                case "TB" :
                case "DK" :
                case "DT" :
                    // var_dump($reqHeaders['verify_code']) ; exit;

                    $verify_code = isset($reqHeaders ['verify_code']) ? $reqHeaders ['verify_code'] : "";

                    if (empty($verify_code)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }
                    $userModel = $this->user_model->findByVerifyCode($verify_code);
                    $reserveFrom = "dashboard";
                    break;

                default :

                    http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);

                    break;
            }

            // echo json_encode($userModel) ;
            if (empty($userModel)) {
                http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
            }

            $roomId = $this->post('room_id');
            $startTime = $this->post('start');
            $endTime = $this->post('end');
            $contacts = $this->post('attendees');
            $subject = $this->post('subject');
            $reminder = $this->post('reminder') ? $this->post('reminder') : 1;
            $note = $this->post('note') ? nl2br ($this->post('note')) : "";

            /**
             * recurrence function *
             */
            $pattern = (NULL == $this->post('pattern')) ? 0 : intval($this->post('pattern'));
            $interval = (NULL == $this->post('interval')) ? - 1 : intval($this->post('interval'));
            $occurrence = (NULL == $this->post('occurrence')) ? - 1 : intval($this->post('occurrence'));
            $recurr_end_date = (NULL == $this->post('recurr_end_date')) ? "" : $this->post('recurr_end_date');
            $day_of_week = (NULL == $this->post('day_of_week')) ? "" : $this->post('day_of_week');
            $day_of_month = (NULL == $this->post('day_of_month')) ? - 1 : intval($this->post('day_of_month'));
            $month = (NULL == $this->post('month')) ? - 1 : intval($this->post('month'));
            $index_day_of_week = (NULL == $this->post('index_day_of_week')) ? - 1 : intval($this->post('index_day_of_week'));

            $tag = "" ;
            $management_no = (NULL == $this->post('management_no')) ? 0 : intval($this->post('management_no'));
            if ($management_no > 0) {
//                $note .= " #management_no:" . $management_no . " ";
                $tag .= " #management_no:" . $management_no . " ";
            }

            $nonmanagement_no = (NULL == $this->post('nonmanagement_no')) ? 0 : intval($this->post('nonmanagement_no'));
            if ($nonmanagement_no > 0) {
//                $note .= " #nonmanagement_no:" . $nonmanagement_no . " ";
                $tag .= " #nonmanagement_no:" . $nonmanagement_no . " ";
            }

            $ext_no = (NULL == $this->post('ext_no')) ? "" : trim($this->post('ext_no'));
            if (!empty($ext_no)) {
//                $note .= " #ext_no:" . $ext_no . " ";
                $tag .= " #ext_no:" . $ext_no . " ";
            }

			$employee_id = (NULL == $this->post('employee_id')) ? "" : trim($this->post('employee_id'));
            if (!empty($employee_id)) {
                $tag .= " #employee_id:" . $employee_id . " ";
            }

            $tag .= " #client_id:" . $clientModel->client_id . "" ;
            $tag .= " #pattern:" . $pattern . "" ;

            /**
             * Hashtag param *
             */
            $files = array();

            $seat_layout_image = "" ;

            $seat_layout_id = (NULL == $this->post('seat_layout_id')) ? 0 : intval($this->post('seat_layout_id'));
            if ($seat_layout_id > 0) {
                $seat_layout_model_list = $this->hashtag_model->findAllByKeyword('seat_layout');
                if (!empty($seat_layout_model_list)) {
                    foreach ($seat_layout_model_list as $seat_layout_model) {
                        $seat_layout = json_decode($seat_layout_model->content);
                        if ($seat_layout->id == $seat_layout_id) {
                            // $note .= " #seat_layout:".$seat_layout->image." " ;

                            /*
                            $ext = pathinfo($seat_layout->image, PATHINFO_EXTENSION);

                            $files [] = array(
                                "name" => "seat_layout." . $ext,
                                "data" => base64_encode(file_get_contents($seat_layout->image))
                            );
                            */

                            $seat_layout_image = $seat_layout->image ;
                            break;
                        }
                    }
                }
            }

            if (empty($roomId) || empty($startTime) || empty($endTime) || empty($subject)) {
                log_message("error", "[Invalid Input!!]roomId=" . $roomId . "|startTime=" . $startTime . "|endTime=" . $endTime . "|subject=" . $subject . "|");
                http_helper::response($this, http_helper::STATUS_INVALID_INPUT);
            }


            $contact_id = $userModel->contact_id;

            $contact_model = $this->contact_model->findByPk($contact_id);
            if (empty($contact_model)) {
                http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR);
            }

            $body = "";
            $tz = new DateTimeZone("Asia/Bangkok");

            $start_date = new DateTime($startTime);
            $start_date->setTimeZone($tz);
            $start_date_str = $start_date->format('Y-m-d\TH:i:s');

            $end_date = new DateTime($endTime);
            $end_date->setTimeZone($tz);
            $end_date_str = $end_date->format('Y-m-d\TH:i:s');

            if (!$start_date_str) {

            }

            if (!$end_date_str) {

            }
            /*
             * log_message("debug","startdate > enddate :=> ". json_encode($start_date > $end_date)) ;
             * if( $start_date > $end_date ) {
             * http_helper::response ( $this, http_helper::STATUS_INVALID_INPUT );
             * }
             * log_message("debug","enddate - start_date :=> ". $this->second_diff($start_date_str,$end_date_str) ) ;
             * if($this->second_diff($start_date_str,$end_date_str) > 28800 ) {
             * http_helper::response ( $this, http_helper::STATUS_INVALID_INPUT );
             * }
             */

            // Start Time cannot more than endtime
            $date = new DateTime ();
            $date->sub(new DateInterval('PT15M'));

            if ($start_date < $date) {
                http_helper::response($this, http_helper::STATUS_START_LESSTHAN_NOW);
            }

            if ($start_date > $end_date) {
                http_helper::response($this, http_helper::STATUS_START_LESSTHAN_END);
            }

            /*
              $max_reserve_minutes = 28800;
              try {
              $max_reserve_minutes = intval($this->config_model->findByKey("limitation.reserve.max_minutes")->value);
              } catch (Exception $e) {

              }

              if ($this->second_diff($start_date_str, $end_date_str) > $max_reserve_minutes) {
              http_helper::response($this, http_helper::STATUS_RESERVE_OVER_LIMIT);
              }

             */

            $allday = false;
            $location = "";

            $contactList = null;
            if (!empty($contacts)) {
                $contactList = explode(",", $contacts);
            }

            $attendees = array();
            // $attendeesCnt = 0;
            if (!empty($contactList)) {
                // $attendees = "" ;
                foreach ($contactList as $i => $contact_id_attendee) {

                    if (is_numeric($contact_id_attendee)) {

                        $contact_model_attendee = $this->contact_model->findByPk($contact_id_attendee);
                        if (!empty($contact_model_attendee)) {
                            // $attendees .= ( $contact_model->display_name."|".$contact_model->email . ";" ) ;
                            $attendees [] = $contact_model_attendee->email;
                        }
                    } else {
                        $attendees [] = $contact_id_attendee;
                    }
                }
            }
            $attendeesStr = implode(",", $attendees);

            $data = array(
                'room_id' => $roomId,
                'event_code' => '',
                'start_time' => $startTime,
                'end_time' => $endTime,
                'topic' => $subject,
                'detail' => $note,
                'create_time' => date('Y-m-d H:i:s'),
                'create_by' => $userModel->contact_id,
                'status_id' => 2,
                'reserve_from' => $reserveFrom,
                'attendees' => $attendeesStr,
                'reminder' => $reminder,
                'pattern' => $pattern,
                'interval' => $interval,
                'occurrence' => $occurrence,
                'endDate' => $recurr_end_date,
                'dayOfweek' => $day_of_week,
                'dayOfMonth' => $day_of_month,
                'month' => $month,
                'indexDayOfweek' => $index_day_of_week,
                'files' => json_encode($files) ,
                'tag'=>$tag
            );

            log_message('debug', "Request Data\n" . json_encode($data));
            $ch = curl_init();

            // $reserv_url = "http://172.17.31.111:8080/micore/reserve" ;
            $reserv_url = "http://192.168.117.130:8080/micore/reserve";

            $this->config->load('micore.php', TRUE);
            $reserv_url_config = $this->config->item('url', 'micore');

            log_message("debug", "reserv_url_config = " + $reserv_url_config);
            if (!empty($reserv_url_config)) {
                $reserv_url = $reserv_url_config . "reserve";
            }

            log_message("debug", "$reserv_url = " + $reserv_url);

            curl_setopt($ch, CURLOPT_URL, $reserv_url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $json_response = curl_exec($ch);
            if (empty($json_response)) {
                log_message('error', "Request MI-Core Failed!");
                http_helper::response($this, http_helper::STATUS_REQUEST_MICORE_FAILED);
            }

            curl_close($ch);
            log_message('error', $json_response);
            $json = json_decode($json_response);
            log_message('error', json_encode($json));
            $response_code = $json->responseCode;
            $response_status = $json->responseStatus;
            log_message('error', "response_code = $response_code and response_status = $response_status");

            //================== Custom Message ========================//
            //http_helper::response($this, $response_code, $response_status);

            /** ================== Check auto-accept **/
            if( intval($response_code) !== 0 ){
//                return http_helper::response($this, $response_code, $response_status);
                return http_helper::response($this, $response_code );
            }

            // If not auto-accept , then send email with room layout attachment to room owner
            try {
                log_message("debug" , "Seat Layout Image = ".print_r($seat_layout_image,TRUE)) ;
                if(!empty($seat_layout_image)) {
                    $admin_email = $this->room_property_model->findByPk($roomId, "admin_email")->property_value;
                    if (!empty($admin_email)) {

                        $room_model = $this->room_model->findByPk($roomId) ;

                        $reserve_date_str = $start_date->format('Y-m-d');
                        $reserve_start_str = $start_date->format('H:i');
                        $reserve_end_str = $end_date->format('H:i');

                        $subject = "Seat Layout Arrangement for ".$room_model->room_name ."(". $reserve_date_str .",". $reserve_start_str ." - ". $reserve_end_str.")" ;
                        $body = "This request was forwarded to you for seat layout arrangement." ;
                        $body .= "<br/><b>Organizer</b> : ". $contact_model->display_name ;
                        $body .= "<br/><b>When</b> : ". $reserve_date_str  .",". $reserve_start_str ." - ". $reserve_end_str ;
                        $body .= "<br/><b>Location</b> : " . $room_model->room_name ;
                        $body .= "<br/>" ;
                        email_helper::sendLayout($subject, $body, $admin_email, NULL, NULL, NULL, $seat_layout_image);
                    }
                }

            }catch (Exception $ee){
                log_message("error", "Send Mail to room owner error:" . print_r($ee,true));
            }

            $is_auto_accept = TRUE ;
            $is_auto_accept_model = $this->room_property_model->findByPk($roomId,"auto_accept") ;
            log_message("debug" , "Auto Accept Model:" . print_r($is_auto_accept_model,TRUE)) ;
            if(!empty($is_auto_accept_model)){
//                $is_auto_accept = filter_var($is_auto_accept_model->property_value, FILTER_VALIDATE_BOOLEAN) ;

                if($is_auto_accept_model->property_value  === 'false' || $is_auto_accept_model->property_value  === '0'){
                    $is_auto_accept = FALSE ;
                }
            }
            log_message("debug" , "Auto Accept : " . print_r($is_auto_accept,TRUE)) ;
            if($is_auto_accept ){
//                $title = "Title: Room Auto Accept" ;
//                $message = "Message: Room Auto Accept Value" ;
//                return http_helper::response($this, $response_code, $response_status , NULL ,$title , $message );

//                return http_helper::response($this, $response_code, $response_status);
                return http_helper::response($this, $response_code );
            }




            $title = "Warning";
            $message = "This room requires approval. Your request has tentatively accepted.<br>Please see response in your mail";
            return http_helper::response($this, $response_code, NULL ,$title , $message );
            /**
             * ==================== End Reserve by Servlet Listener =============================*
             */
        } catch (Exception $e) {
            log_message("error", "Exception Error" . json_encode($e));
            http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR);
        }
    }

    function reserveews_post() {
        try {
            $serviceId = Service_model::SERVICE_RM04;
            // $post_params = $this->input->post();
            // log_message("debug", json_encode($post_params)) ;
            $response_data = NULL;

            $reqHeaders = http_helper::requestHeaders($this);

            // var_dump($reqHeaders) ; exit;

            $clientModel = http_helper::getClientModel($this, $reqHeaders);

            // var_dump($clientModel) ; exit;

            $reserveFrom = "";
            $userModel = null;
            $clientType = $clientModel->client_type;
            switch ($clientType) {
                case "MB" :

                    $access_token = isset($reqHeaders ['access_token']) ? $reqHeaders ['access_token'] : "";
                    if (empty($access_token)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }
                    // echo 'access_koen='.json_encode($access_token) ; exit;

                    $user_token_model = $this->user_token_model->findByAccessToken($access_token);
                    if (empty($user_token_model)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }
                    // echo 'user_token_model='.json_encode($user_token_model) ; exit;

                    $userModel = $this->user_model->findByPk($user_token_model->user_id);
                    if (empty($userModel)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }

                    // echo '$userModel='.json_encode($userModel) ; exit;
                    $reserveFrom = "mobile";

                    break;

                case "TB" :
                case "DK" :
                case "DT" :
                    // var_dump($reqHeaders['verify_code']) ; exit;

                    $verify_code = isset($reqHeaders ['verify_code']) ? $reqHeaders ['verify_code'] : "";

                    if (empty($verify_code)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }
                    $userModel = $this->user_model->findByVerifyCode($verify_code);
                    $reserveFrom = "dashboard";
                    break;

                default :

                    http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);

                    break;
            }

            // echo json_encode($userModel) ;
            if (empty($userModel)) {
                http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
            }

            $roomId = $this->post('room_id');
            $startTime = $this->post('start');
            $endTime = $this->post('end');
            $contacts = $this->post('attendees');
            $subject = $this->post('subject');

            // echo " $roomId = ". json_encode(empty($roomId)) ;
            // echo " $startTime = ". json_encode(empty($startTime) ) ;
            // echo " $endTime = ". json_encode(empty($endTime )) ;
            // echo " $subject = ". json_encode(empty($subject) ) ;

            if (empty($roomId) || empty($startTime) || empty($endTime) || empty($subject)) {
                http_helper::response($this, http_helper::STATUS_INVALID_INPUT);
            }

            $events = $this->event_model->findByRoom($roomId, $startTime, $endTime);
            if (!empty($events)) {
                http_helper::response($this, http_helper::STATUS_ROOM_NOT_AVAILABLE);
            }

            /**
             * =================================== begin request booking to Exchange ===================================== *
             */
            if (crypt_helper::IS_ENCRYPTION) {
                $password = user_helper::getPassword($userModel->user_name, $userModel->password, $userModel->auth_key, $userModel->created_at);
            } else {
                $password = $userModel->password;
            }

            $this->config->load('ews.php', TRUE);
            $ews_config = [
                'host' => $this->config->item('server', 'ews'),
                'admin' => $this->config->item('adminuser', 'ews'),
                'pass' => $this->config->item('adminpassword', 'ews')
            ];

            $host = $ews_config ['host'];

            $contact_model = $this->contact_model->findByPk($userModel->contact_id);
            if (empty($contact_model)) {
                http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR);
            }

            $this->load->library('ews');
            $ews = $this->ews->create_ews_instants($host, $contact_model->user_logon, $password);
            // var_dump($ews) ; exit;
            if (empty($ews)) {
                log_message("debug", "EWS Empty!! by input $host,$contact_model->user_logon,$password");
                http_helper::response($this, http_helper::STATUS_EWS_ERROR);
            }

            $body = "";
            $tz = new DateTimeZone("Asia/Bangkok");

            $start_date = new DateTime($startTime);
            $start_date->setTimeZone($tz);
            $start_date_str = $start_date->format('Y-m-d\TH:i:s');

            $end_date = new DateTime($endTime);
            $end_date->setTimeZone($tz);
            $end_date_str = $end_date->format('Y-m-d\TH:i:s');

            $allday = false;
            $location = "";

            $contactList = null;
            if (!empty($contacts)) {
                $contactList = explode(",", $contacts);
            }

            $attendees = [];
            if (!empty($contactList)) {
                // $attendees = "" ;
                foreach ($contactList as $i => $contact_id) {
                    $contact_model = $this->contact_model->findByPk($contact_id);
                    if (!empty($contact_model)) {
                        $attendees .= ($contact_model->display_name . "|" . $contact_model->email . ";");
                    }
                }
            }

            $importance = false;
            $sensitivity = false;

            // echo "roomId $roomId"; exit;

            $room_resource = "";
            $room_synchronzer_model = $this->room_synchronizer_model->findByRoomId($roomId);
            if (!empty($room_synchronzer_model)) {
                $room_resource = $room_synchronzer_model->user;
            }

            $requestString = "================== RESERVE-REQ =====================\n" . "|EWS=" . json_encode($ews) . "|SUBJECT=" . $subject . "|BODY=" . $body . "|START=" . $start_date_str . "|END=" . $end_date_str . "|ALLDAY=" . json_encode($allday) . "|LOCATION=" . $location . "|ATTENDEES=" . json_encode($attendees) . "|IMPORTANCE=" . json_encode($importance) . "|SENSITIVITY=" . json_encode($sensitivity) . "|RESOURCE=" . $room_resource . "|";
            log_message('debug', $requestString);

            $reserve_result = $this->ews->calendar_add_item($ews, $subject, $body, $start_date_str, $end_date_str, $allday, $location, $attendees, $importance, $sensitivity, $room_resource);

            // echo json_encode($reserve_result) ; exit;
            log_message('debug', 'RESERVE-RES {"room_id":"' . $roomId . '", "result": ' . json_encode($reserve_result));
            if (empty($reserve_result)) {
                http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR);
            }

            $ResponseCode = $reserve_result->ResponseCode;
            $ResponseClass = $reserve_result->ResponseClass;

            if ($ResponseCode == "NoError" && $ResponseClass == "Success") {
                $event_id = $reserve_result->Items->CalendarItem->ItemId->Id;
                $changeKey = $reserve_result->Items->CalendarItem->ItemId->ChangeKey;

                log_message("debug", "reserve success :" . json_encode($reserve_result));

                $itemResult = $this->ews->calendar_get_item($ews, $event_id);
                log_message("debug", "itemResult :" . json_encode($itemResult));
                http_helper::response($this, http_helper::STATUS_SUCCESS, $reserve_result);
            } else {
                http_helper::response($this, http_helper::STATUS_EWS_ERROR, $reserve_result);
            }

            /**
             * =================================== begin request booking to Exchange ===================================== *
             */
        } catch (Exception $e) {
            var_dump($e);
        }
    }

    function index_get($roomId) {
        $reqHeaders = http_helper::requestHeaders($this);
        $clientModel = http_helper::getClientModel($this, $reqHeaders);

        // echo json_encode($clientModel) ; exit;
        $currentTime = date('Y-m-d H:i:s');
        $startTime = date('Y-m-d H:i:s', strtotime($currentTime . "-2 hours"));
        $endTime = date('Y-m-d H:i:s', strtotime($currentTime . "+5 hours"));
        // echo $startTime." - ".$endTime ; exit;
        $room = array();
        $roomInfo = $this->room_model->findByPk($roomId);
        // echo json_encode($events) ; exit;
        if (!empty($roomInfo)) {
            $room ['room_id'] = intval($roomInfo->room_id);
            $room ['room_name'] = $roomInfo->room_name;
            $room ['description'] = $roomInfo->description;


            $floor_name = "";
            $floor_id = $roomInfo->floor_id;
            if (!empty($floor_id)) {
                $floor_model = $this->floor_model->findByPk($floor_id);
                if (!empty($floor_model)) {
                    $floor_name = $floor_model->floor_name;
                }
            }

            $room ['floor'] = $floor_name;
            $room ['vdo'] = base_url() . "data/contents/s1_01_vdo.mp4";

            $contact = array();
            $ownerContactId = $roomInfo->contact_id;
            if ($ownerContactId > 0) {
                $contactModel = $this->contact_model->findByPk($ownerContactId);
                if (!empty($contactModel)) {
                    $contact = array(
                        'id' => intval($contactModel->contact_id),
                        'name' => $contactModel->display_name,
                        'phone' => $contactModel->telephone,
                        'email' => $contactModel->email,
                        'employee_id' => $contactModel->employee_id
                    );
                }
            }
            $room ['contact'] = $contact;

            $facilities = [];
            $facility_list = $this->room_facility_model->findAllByRoomId($roomInfo->room_id);
            if (!empty($facility_list)) {

                for ($i = 0; $i < count($facility_list); $i ++) {
                    $facility = $facility_list [$i];

                    $facility_id = $facility->facility_id;
                    $facility_model = $this->facility_model->findByPk($facility_id);

                    $image = "";

                    $facility_property_model = $this->facility_property_model->findByPk($facility_id, "image");
                    if (!empty($facility_property_model)) {

                        $image = base_url() . "data/facility/" . $facility_property_model->property_value;
                    }

                    $showType = "";
                    $facility_property_model = $this->facility_property_model->findByPk($facility_id, "showtype");
                    if (!empty($facility_property_model)) {
                        $showType = $facility_property_model->property_value;
                    }

                    $facilities [$i] ['facility'] = $facility_model->facility_name;
                    $facilities [$i] ['qty'] = intval($facility->quantity);

                    $facilities [$i] ['image'] = $image;
                    $facilities [$i] ['show_type'] = $showType;

                    $icon = "";
                    $facility_property_model = $this->facility_property_model->findByPk($facility_id, "image-mobile");
                    if (!empty($facility_property_model)) {
                        $icon = base_url() . "data/facility/" . $facility_property_model->property_value;
                    }
                    $facilities [$i] ['icon'] = $icon;
                }
            }

            $room ['facilities'] = $facilities;

            // Contact contact_id
            $contact = array();
            $room_id = $roomInfo->room_id;
            $contactId = $roomInfo->contact_id;
            if (!empty($contactId)) {
                $contact_model = $this->contact_model->findByPk($contactId);
                if (!empty($contact_model)) {

                    $contact ['name'] = $contact_model->display_name;
                    $contact ['email'] = $contact_model->email;
                    $contact ['phone'] = $contact_model->telephone;
                }
            }
            $room ['contact'] = $contact;

            // Theme
            $theme = array();
            $theme_model = $this->theme_model->findByRoomId($room_id);
            if (!empty($theme_model)) {
                $theme ['theme_id'] = intval($theme_model->theme_id);
                $theme ['theme_name'] = $theme_model->name;
                $theme ['org_bg'] = base_url() . "images/room/theme/" . $theme_model->bg_image;
                $theme ['bg'] = base_url() . "images/room/theme/" . $theme_model->bg_convert;
                $theme ['color1'] = $theme_model->color_1;
                $theme ['color2'] = $theme_model->color_2;
                $theme ['color3'] = $theme_model->color_3;
            }
            $room ['theme'] = $theme;

            $available_time = array();

            $currentTime = date('Y-m-d H:i:s');
            $room ['available_from'] = $currentTime;

            $toTime = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . "+1 days")) . "00:00:00";
            $next_meeting = $this->event_model->findNextEventByRoom($room_id);
            if (!empty($next_meeting)) {
                $toTime = date('Y-m-d H:i:s', strtotime($next_meeting [0]->start_time));
            }
            $room ['available_to'] = $toTime;

            /*             * ============== Begin Room Property ==================* */

            $layout = "";
            $phone_no = "";
            $isdn = "";

            /*
              $room_property_model = $this->room_property_model->findByPk ( $room_id, "layout_image" );
              // log_message("debug" , json_encode($room_property_model)) ;
              if (! empty ( $room_property_model )) {
              $layout = base_url ( "data/room_property/" . $room_property_model->property_value );
              } else {
              $layout = base_url ( "data/room_property/default.png" );
              }
              $room ['layout'] = $layout;
             */


            $layout_property_model = $this->room_property_model->findByPk($room_id, "layout_image" , true);
            if (!empty($layout_property_model)) {
                $layout = base_url("data/room_property/" . $layout_property_model->property_value);
            }
            /*             * else {
              $layout = base_url ( "data/room_property/default.png" );
              }* */
            $room ['layout'] = $layout;

            $phoneno_property_model = $this->room_property_model->findByPk($room_id, "phone_no", false);
            if (!empty($phoneno_property_model)) {
                $phone_no = $phoneno_property_model->property_value;
            }
            $room ['phone_no'] = $phone_no;

            $isdn_property_model = $this->room_property_model->findByPk($room_id, "isdn", false);
            if (!empty($isdn_property_model)) {
                $isdn = $isdn_property_model->property_value;
            }
            $room ['isdn'] = $isdn;



            /*             * ============== Begin Room Property ==================* */
        }
        // $data = array('room' => $room);
        http_helper::response($this, http_helper::STATUS_SUCCESS, $room);
    }

    function facilities_get($roomId) {
        $reqHeaders = http_helper::requestHeaders($this);
        $clientModel = http_helper::getClientModel($this, $reqHeaders);

        $facilities = [];
        $facility_list = $this->room_facility_model->findAllByRoomId($roomId);
        if (!empty($facility_list)) {

            for ($i = 0; $i < count($facility_list); $i ++) {
                $facility = $facility_list [$i];

                $facility_id = $facility->facility_id;
                $facility_model = $this->facility_model->findByPk($facility_id);

                $image = "";

                $facility_property_model = $this->facility_property_model->findByPk($facility_id, "image");
                if (!empty($facility_property_model)) {

                    $image = base_url() . "data/facility/" . $facility_property_model->property_value;
                }

                $showType = "";
                $facility_property_model = $this->facility_property_model->findByPk($facility_id, "showtype");
                if (!empty($facility_property_model)) {
                    $showType = $facility_property_model->property_value;
                }

                $facilities [$i] ['facility'] = $facility_model->facility_name;
                $facilities [$i] ['qty'] = intval($facility->quantity);

                $facilities [$i] ['image'] = $image;
                $facilities [$i] ['show_type'] = $showType;

                $icon = "";
                $facility_property_model = $this->facility_property_model->findByPk($facility_id, "image-mobile");
                if (!empty($facility_property_model)) {
                    $icon = base_url() . "data/facility/" . $facility_property_model->property_value;
                }
                $facilities [$i] ['icon'] = $icon;
            }
        }

        $data = array(
            'facilities' => $facilities
        );
        http_helper::response($this, http_helper::STATUS_SUCCESS, $data);
    }

    /**
     * RM03 - Room available
     */
    function availables_get() {
        $reqHeaders = http_helper::requestHeaders($this);
        $clientModel = http_helper::getClientModel($this, $reqHeaders);

        $allfac = (null !== $this->input->get("allfac"))?$this->input->get("allfac"):1 ;

        $access_token = $reqHeaders ['access_token'];

        if (empty($access_token)) {
            log_message("debug", "Empty Token !!");
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }

        $user_token_model = $this->user_token_model->findByAccessToken($access_token);
        if (empty($user_token_model)) {
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }

        $userLogin = $this->user_model->findByPk($user_token_model->user_id);
        if (empty($userLogin)) {
            http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
        }

        /**
         * =========================== Begin Now Available ======================================*
         */
        $room_available_list = $this->room_model->findAllAvailableNow($userLogin->user_id);

        $now_availables = [];
        $room_ids = "";

        $this->load->library('ews');

        $this->config->load('ews.php', TRUE);
        $ews_config = [
            'host' => $this->config->item('server', 'ews'),
            'admin' => $this->config->item('adminuser', 'ews'),
            'pass' => $this->config->item('adminpassword', 'ews')
        ];

        $is_enable_ews_check = false;

        if ($is_enable_ews_check) {

            $host = $ews_config ['host'];
            $user = $ews_config ['admin'];
            $pass = $ews_config ['pass'];

            log_message("debug", "Host:" . $host . " , User: " . $user . " , Pass: " . $pass);

            $this->load->library('ews');
            $credential = $this->ews->create_ews_instants($host, $user, $pass);
            if (empty($credential)) {
                http_helper::response($this, http_helper::STATUS_EWS_ERROR);
            }
        }

        if (!empty($room_available_list)) {

            foreach ($room_available_list as $i => $room_available) {

                if ($is_enable_ews_check) {
                    $room_synchronize = $this->room_synchronizer_model->findByRoomId($room_available->room_id);
                    if (empty($room_synchronize)) {
                        continue;
                    }

                    $room_resource = $room_synchronize->user;

                    $start = date('Y-m-d H:i:s');
                    $end = date('Y-m-d', strtotime(date('Y-m-d') . "+1 days")) . " 00:00:00";

                    $tz = new DateTimeZone("Asia/Bangkok");

                    $start_date = new DateTime($start);
                    $start_date->setTimeZone($tz);
                    $start_date_str = $start_date->format('Y-m-d\TH:i:s');

                    $end_date = new DateTime($end);
                    $end_date->setTimeZone($tz);
                    $end_date_str = $end_date->format('Y-m-d\TH:i:s');

                    // check with Exchange ( some case recurrence etc. can't sync from exchange )
                    $events = $this->ews->calendar_check_available($credential, $start_date_str, $end_date_str, $room_resource);
                    // log_message("debug", "calendar_check_available:[$room_resource ,$start_date_str-$end_date_str=> ".json_encode($events)) ;
                    if (!empty($events)) {
                        continue;
                    }
                }
                // if ($i != 0) {
                // $room_ids .= "," . $room_available->room_id;
                // } else {
                // $room_ids .= $room_available->room_id;
                // }

                if ($room_ids !== "") {
                    $room_ids .= "," . $room_available->room_id;
                } else {
                    $room_ids .= $room_available->room_id;
                }

                $now_availables [$i] ['id'] = intval($room_available->room_id);
                $now_availables [$i] ['name'] = $room_available->room_name;
                $now_availables [$i] ['description'] = $room_available->description;
                $now_availables [$i] ['type_id'] = intval($room_available->room_type_id);


                /*
                  $floor_name = "";
                  $floor_model = $this->floor_model->findByPk($room_available->floor_id);
                  if (!empty($floor_model)) {
                  $floor_name = $floor_model->floor_name;
                  }
                  $now_availables [$i] ['floor'] = $floor_name;
                 *
                 */

                $room_type_name = "";
                $room_type_color = "";
                $floor_name = "";
                $building_name = "";
                $location_name = "";

//                $room = $this->room_model->findByPk($roomId);

                $room_type = $this->room_type_model->findByPk($room_available->room_type_id);

                if (!empty($room_type)) {
                    $room_type_name = $room_type->type_name;
                    $room_type_color = $room_type->color;
                }
                $now_availables [$i] ['type_name'] = $room_type_name;
                $now_availables [$i] ['type_color'] = $room_type_color;

                $floor_model = $this->floor_model->findByPk($room_available->floor_id);
                if (!empty($floor_model)) {
                    $floor_name = $floor_model->floor_name;

                    $building_model = $this->building_model->findByPk($floor_model->building_id);
                    if (!empty($building_model)) {
                        $building_name = $building_model->building_name;
                        $location_model = $this->location_model->findByPk($building_model->location_id);
                        if (!empty($location_model)) {
                            $location_name = $location_model->location_name;
                        }
                    }
                }

                $now_availables [$i] ['floor'] = $floor_name;
                $now_availables [$i] ['building'] = $building_name;
                $now_availables [$i] ['location'] = $location_name;


                $fromTime = date('Y-m-d H:i:s');
                $toTime = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . "+1 days")) . " 00:00:00";
                $next_event_model_list = $this->event_model->findNextEventByRoom($room_available->room_id);
                if (!empty($next_event_model_list)) {
                    $toTime = $next_event_model_list [0]->start_time;
                }

                $now_availables [$i] ['available_from'] = $fromTime;
                $now_availables [$i] ['available_to'] = $toTime;
                $now_availables [$i] ['up_to'] = $this->dateDiff($fromTime, $toTime);

                $contact_name = "";
                $contact_model = $this->contact_model->findByPk($room_available->contact_id);
                if (!empty($contact_model)) {
                    $contact_name = $contact_model->display_name;

                    $owner = array(
                        'id' => intval($contact_model->contact_id),
                        'name' => is_null($contact_model->display_name) ? "" : $contact_model->display_name,
                    	'display_name' => is_null($contact_model->display_name) ? "" : $contact_model->display_name,
                        'phone' => is_null($contact_model->telephone) ? "" : $contact_model->telephone,
                        'email' => is_null($contact_model->email) ? "" : $contact_model->email,
                        'employee_id' => is_null($contact_model->employee_id) ? "" : $contact_model->employee_id
                    );
                    $now_availables [$i] ['owner'] = $owner;
                }

                $now_availables [$i] ['contact'] = $contact_name;
                
                $admin_name = "";
                $admin_email = "";
                
                $admin_name_model = $this->room_property_model->findByPk($room_available->room_id, "admin_name", false);
                if (!empty($admin_name_model)) {
                	$admin_name = $admin_name_model->property_value;
                }
                
                $admin_email_model = $this->room_property_model->findByPk($room_available->room_id, "admin_email", false);
                if (!empty($admin_email_model)) {
                	$admin_email = $admin_email_model->property_value;
                }
                
                $now_availables [$i] ['admin'] = array('name'=>$admin_name,'email'=>$admin_email);
				
                $seat_layout_req = false;
                $seat_layout_req_model = $this->room_property_model->findByPk($room_available->room_id, "seat_layout_req", false);
                if (!empty($seat_layout_req_model)&&$seat_layout_req_model->property_value=='true') {
                	$seat_layout_req = true;
                }
                
                $now_availables [$i] ['seat_layout_req'] = $seat_layout_req;
                /*
                  $location_name = "";
                  $building_model = $this->building_model->findByPk($floor_model->building_id);
                  if (!empty($building_model)) {
                  $location_model = $this->location_model->findByPk($building_model->location_id);
                  if (!empty($location_model)) {
                  $location_name = $location_model->location_name;
                  }
                  }
                  $now_availables [$i] ['location'] = $location_name;
                 *
                 */





                $facilities = [];
                if($allfac){
                    $facility_list = $this->room_facility_model->findAllByRoomId($room_available->room_id);
                }else{
                    $facility_list = $this->room_facility_model->findByRoomIdAndFacilityId($room_available->room_id ,  Facility_model::FACILITY_ID_CHAIR ) ;
                }
                if (!empty($facility_list)) {

                    foreach ($facility_list as $j => $facility) {
                        // var_dump($facility) ;exit;

                        $facility_id = $facility->facility_id;
                        $facility_model = $this->facility_model->findByPk($facility_id);

                        $image = "";

                        $facility_property_model = $this->facility_property_model->findByPk($facility_id, "image");
                        if (!empty($facility_property_model)) {

                            $image = base_url( "data/facility/" . $facility_property_model->property_value );
                        }

                        $showType = "";
                        $facility_property_model = $this->facility_property_model->findByPk($facility_id, "showtype");
                        if (!empty($facility_property_model)) {
                            $showType = $facility_property_model->property_value;
                        }
                        $facilities [$j] ['facility_id'] = $facility_id;
                        $facilities [$j] ['facility'] = $facility_model->facility_name;
                        $facilities [$j] ['qty'] = intval($facility->quantity);

                        $facilities [$j] ['image'] = $image;
                        $facilities [$j] ['show_type'] = $showType;

                        $icon = "";
                        $facility_property_model = $this->facility_property_model->findByPk($facility_id, "image-mobile");
                        if (!empty($facility_property_model)) {
                            $icon = base_url() . "data/facility/" . $facility_property_model->property_value;
                        }
                        $facilities [$j] ['icon'] = $icon;
                    }
                }
                $now_availables [$i] ['facilities'] = $facilities;

                /**
                 * Room Layout
                 */
                /* $layout = "";
                  $room_property_model = $this->room_property_model->findByPk ( $room_available->room_id, "layout_image" );
                  // log_message("debug" , json_encode($room_property_model)) ;
                  if (! empty ( $room_property_model )) {
                  $layout = base_url ( "data/room_property/" . $room_property_model->property_value );
                  } else {
                  $layout = base_url ( "data/room_property/default.png" );
                  }
                  $now_availables [$i] ['layout'] = $layout; */

                $layout_property_model = $this->room_property_model->findByPk($room_available->room_id, "layout_image" , true);
                if (!empty($layout_property_model)) {
                    $layout = base_url("data/room_property/" . $layout_property_model->property_value);
                }


                /*                 * else {
                  $layout = base_url ( "data/room_property/default.png" );
                  }* */
                $now_availables [$i] ['layout'] = $layout ;//. (strpos($layout, '?') ? "&t=" . time() : "?t=" . time());

                $phoneno_property_model = $this->room_property_model->findByPk($room_available->room_id, "phone_no", false);
                if (!empty($phoneno_property_model)) {
                    $phone_no = $phoneno_property_model->property_value;
                }
                $now_availables [$i] ['phone_no'] = $phone_no;

                $isdn_property_model = $this->room_property_model->findByPk($room_available->room_id, "isdn", false);
                if (!empty($isdn_property_model)) {
                    $isdn = $isdn_property_model->property_value;
                }
                $now_availables [$i] ['isdn'] = $isdn;
            }
        }

        /**
         * =========================== End Now Available ======================================*
         */
        /**
         * =========================== Begin Next Available ======================================*
         */
        $next_availables = [];
        $room_next_list = $this->room_model->findAllAvailableNextHour($userLogin->user_id, $room_ids);
        if (!empty($room_next_list)) {

            foreach ($room_next_list as $i => $room_available) {
                if ($is_enable_ews_check) {
                    $room_synchronize = $this->room_synchronizer_model->findByRoomId($room_available->room_id);
                    if (empty($room_synchronize)) {
                        continue;
                    }

                    $room_resource = $room_synchronize->user;

                    $start = date('Y-m-d H:i:s');
                    $end = date('Y-m-d', strtotime(date('Y-m-d') . "+1 days")) . " 00:00:00";

                    $tz = new DateTimeZone("Asia/Bangkok");

                    $start_date = new DateTime($start);
                    $start_date->setTimeZone($tz);
                    $start_date_str = $start_date->format('Y-m-d\TH:i:s');

                    $end_date = new DateTime($end);
                    $end_date->setTimeZone($tz);
                    $end_date_str = $end_date->format('Y-m-d\TH:i:s');

                    // check with Exchange ( some case recurrence etc. can't sync from exchange )
                    $events = $this->ews->calendar_check_available($credential, $start_date_str, $end_date_str, $room_resource);
                    // log_message("debug", "calendar_check_available:[$room_resource ,$start_date_str-$end_date_str=> ".json_encode($events)) ;
                    if (!empty($events)) {
                        continue;
                    }
                }

                $next_availables [$i] ['id'] = intval($room_available->room_id);
                $next_availables [$i] ['name'] = $room_available->room_name;
                $next_availables [$i] ['description'] = $room_available->description;
                $next_availables [$i] ['type_id'] = intval($room_available->room_type_id);

                $room_type_name = "";
                $room_type_color = "";
                $floor_name = "";
                $building_name = "";
                $location_name = "";

//                $room = $this->room_model->findByPk($roomId);

                $room_type = $this->room_type_model->findByPk($room_available->room_type_id);

                if (!empty($room_type)) {
                    $room_type_name = $room_type->type_name;
                    $room_type_color = $room_type->color;
                }
                $next_availables [$i] ['type_name'] = $room_type_name;
                $next_availables [$i] ['type_color'] = $room_type_color;

                $floor_model = $this->floor_model->findByPk($room_available->floor_id);
                if (!empty($floor_model)) {
                    $floor_name = $floor_model->floor_name;

                    $building_model = $this->building_model->findByPk($floor_model->building_id);
                    if (!empty($building_model)) {
                        $building_name = $building_model->building_name;
                        $location_model = $this->location_model->findByPk($building_model->location_id);
                        if (!empty($location_model)) {
                            $location_name = $location_model->location_name;
                        }
                    }
                }

                $next_availables [$i] ['floor'] = $floor_name;
                $next_availables [$i] ['building'] = $building_name;
                $next_availables [$i] ['location'] = $location_name;
                /*
                  $floor_name = "";
                  $floor_model = $this->floor_model->findByPk($room_available->floor_id);
                  if (!empty($floor_model)) {
                  $floor_name = $floor_model->floor_name;
                  }
                  $next_availables [$i] ['floor'] = $floor_name;
                 *
                 */

                $fromTimeNext = date('Y-m-d H', strtotime(date('Y-m-d H:i:s') . "+1 hours")) . ":00:00";
                $toTimeNext = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . "+1 days")) . " 00:00:00";
                $next_event_model_list = $this->event_model->findNextEventByRoom($room_available->room_id, $fromTimeNext);
                if (!empty($next_event_model_list)) {
                    $toTimeNext = $next_event_model_list [0]->start_time;
                }

                $next_availables [$i] ['available_from'] = $fromTimeNext;
                $next_availables [$i] ['available_to'] = $toTimeNext;
                $next_availables [$i] ['up_to'] = $this->dateDiff($fromTimeNext, $toTimeNext);

                $contact_name = "";
                $contact_model = $this->contact_model->findByPk($room_available->contact_id);
                if (!empty($contact_model)) {
                    $contact_name = $contact_model->display_name;

                    $owner = array(
                        'id' => intval($contact_model->contact_id),
                        'name' => is_null($contact_model->display_name) ? "" : $contact_model->display_name,
                    	'display_name' => is_null($contact_model->display_name) ? "" : $contact_model->display_name,
                        'phone' => is_null($contact_model->telephone) ? "" : $contact_model->telephone,
                        'email' => is_null($contact_model->email) ? "" : $contact_model->email,
                        'employee_id' => is_null($contact_model->employee_id) ? "" : $contact_model->employee_id
                    );
                    $next_availables [$i] ['owner'] = $owner;
                }

                $next_availables [$i] ['contact'] = $contact_name;
                
                $admin_name = "";
                $admin_email = "";
                
                $admin_name_model = $this->room_property_model->findByPk($room_available->room_id, "admin_name", false);
                if (!empty($admin_name_model)) {
                	$admin_name = $admin_name_model->property_value;
                }
                
                $admin_email_model = $this->room_property_model->findByPk($room_available->room_id, "admin_email", false);
                if (!empty($admin_email_model)) {
                	$admin_email = $admin_email_model->property_value;
                }
                
                $next_availables [$i] ['admin'] = array('name'=>$admin_name,'email'=>$admin_email);
				
                $seat_layout_req = false;
                $seat_layout_req_model = $this->room_property_model->findByPk($room_available->room_id, "seat_layout_req", false);
                if (!empty($seat_layout_req_model)&&$seat_layout_req_model->property_value=='true') {
                	$seat_layout_req = true;
                }
                
                $next_availables [$i] ['seat_layout_req'] = $seat_layout_req;
                /*
                  $location_name = "";
                  $building_model = $this->building_model->findByPk($floor_model->building_id);
                  if (!empty($building_model)) {
                  $location_model = $this->location_model->findByPk($building_model->location_id);
                  if (!empty($location_model)) {
                  $location_name = $location_model->location_name;
                  }
                  }
                  $next_availables [$i] ['location'] = $location_name;
                 */


                $facilities = [];
                $facility_list = null;//$this->room_facility_model->findAllByRoomId($room_available->room_id);

                if($allfac){
                    $facility_list = $this->room_facility_model->findAllByRoomId($room_available->room_id);
                }else{
                    $facility_list = $this->room_facility_model->findByRoomIdAndFacilityId($room_available->room_id ,  Facility_model::FACILITY_ID_CHAIR ) ;
                }

                if (!empty($facility_list)) {

                    foreach ($facility_list as $j => $facility) {
                        // var_dump($facility) ;exit;

                        $facility_id = $facility->facility_id;
                        $facility_model = $this->facility_model->findByPk($facility_id);

                        $image = "";

                        $facility_property_model = $this->facility_property_model->findByPk($facility_id, "image");
                        if (!empty($facility_property_model)) {

                            $image = base_url( "data/facility/" . $facility_property_model->property_value );
                        }

                        $showType = "";
                        $facility_property_model = $this->facility_property_model->findByPk($facility_id, "showtype");
                        if (!empty($facility_property_model)) {
                            $showType = $facility_property_model->property_value;
                        }

                        $facilities [$j] ['facility_id'] = $facility_id;
                        $facilities [$j] ['facility'] = $facility_model->facility_name;
                        $facilities [$j] ['qty'] = intval($facility->quantity);

                        $facilities [$j] ['image'] = $image;
                        $facilities [$j] ['show_type'] = $showType;

                        $icon = "";
                        $facility_property_model = $this->facility_property_model->findByPk($facility_id, "image-mobile");
                        if (!empty($facility_property_model)) {
                            $icon = base_url() . "data/facility/" . $facility_property_model->property_value;
                        }
                        $facilities [$j] ['icon'] = $icon;
                    }
                }
                $next_availables [$i] ['facilities'] = $facilities;

                /**
                 * Room Layout
                 */
                $layout = "";
                $room_property_model = $this->room_property_model->findByPk($room_available->room_id, "layout_image" , true);
                // log_message("debug" , json_encode($room_property_model)) ;
                if (!empty($room_property_model)) {
                    $layout = base_url("data/room_property/" . $room_property_model->property_value);
                }
//                else {
//                    $layout = base_url("data/room_property/default.png");
//                }
                $next_availables [$i] ['layout'] = $layout;


                $phoneno_property_model = $this->room_property_model->findByPk($room_available->room_id, "phone_no", false);
                if (!empty($phoneno_property_model)) {
                    $phone_no = $phoneno_property_model->property_value;
                }
                $next_availables [$i] ['phone_no'] = $phone_no;

                $isdn_property_model = $this->room_property_model->findByPk($room_available->room_id, "isdn", false);
                if (!empty($isdn_property_model)) {
                    $isdn = $isdn_property_model->property_value;
                }
                $next_availables [$i] ['isdn'] = $isdn;
            }
        }

        /**
         * =========================== End Next Available ======================================*
         */
        $data = array(
            'now_availables' => $now_availables,
            'next_availables' => $next_availables
        );

        http_helper::response($this, http_helper::STATUS_SUCCESS, $data);
    }

    // Code written by purpledesign.in Jan 2014
    function dateDiff($formdate, $todat) {
        $d_one = new DateTime($formdate);
        $d_two = new DateTime($todat);

        // get the difference object
        $d_diff = $d_one->diff($d_two);

        $h_diff = $d_diff->format('%h');
        if ($h_diff < 1) {
            return $d_diff->format('%i mins');
        } else {
            $m_diff = $d_diff->format('%i');
            if ($m_diff > 0) {
                return $d_diff->format('%h hrs %i mins');
            } else {
                return $d_diff->format('%h hrs');
            }
        }

        // dump it
        // var_dump($d_diff);
        // use the format string
        // var_dump($d_diff->format('%H hour %i minute %s second %d day %m month %Y year'));
    }

    function second_diff($start, $end) {
        $uts ['start'] = strtotime($start);
        $uts ['end'] = strtotime($end);

        return $uts ['end'] - $uts ['start'];
    }

    /**
     * RM06 - Get custom search available room
     */
    function search_get() {
        $reqHeaders = http_helper::requestHeaders($this);
        $client_model = http_helper::getClientModel($this, $reqHeaders);

        $allfac = (null !== $this->input->get("allfac"))?$this->input->get("allfac"):1 ;


        $contact_id = 0;

        $client_type = $reqHeaders ['client_type'];
        switch ($client_type) {

            case 'MB' :
                $access_token = $reqHeaders ['access_token'];

                if (empty($access_token)) {
                    log_message("error", "Empty Token !!");
                    http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                }

                $user_token_model = $this->user_token_model->findByAccessToken($access_token);
                if (empty($user_token_model)) {
                    http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                }

                $userLogin = $this->user_model->findByPk($user_token_model->user_id);
                if (empty($userLogin)) {
                    http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                }

                $contact_id = $userLogin->contact_id;

                break;
        }

        /**
         * =========================== End Prepare Criteria ===================================== *
         */
        $start = $this->input->get('start') ? $this->input->get('start') : "";
        $end = $this->input->get('end') ? $this->input->get('end') : "";

        if (empty($start)) {
            $start = date('Y-m-d H:i:s');
        }

        if (empty($end)) {
            $end = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . "+1 days")) . " 00:00:00";
        }

        $location_id = $this->input->get('location_id') ? $this->input->get('location_id') : 0;
        $building_id = $this->input->get('building_id') ? $this->input->get('building_id') : 0;
        $floor_id = $this->input->get('floor_id') ? $this->input->get('floor_id') : 0;
        $room_type_id = $this->input->get('room_type_id') ? $this->input->get('room_type_id') : 0;

        // Optional Criteria
        $in_facilities = $this->input->get('facilities') ? explode(",", $this->input->get('facilities')) : [];

        // log_message ( "debug", "in_facilities=" . $in_facilities );
        /**
         * =========================== Begin Now Available ====================================== *
         */
        $tz = new DateTimeZone("Asia/Bangkok");

        $start_date = new DateTime($start);
        $start_date->setTimeZone($tz);
        $start_date->modify("+1 second");
        $start_date_str = $start_date->format('Y-m-d\TH:i:s');

        $end_date = new DateTime($end);
        $end_date->setTimeZone($tz);
        $end_date->modify("-1 second");
        $end_date_str = $end_date->format('Y-m-d\TH:i:s');

        // Start Time cannot more than endtime
        $date = new DateTime ();
        $date->sub(new DateInterval('PT15M'));

        if ($start_date < $date) {
            http_helper::response($this, http_helper::STATUS_START_LESSTHAN_NOW);
        }

        if ($start_date > $end_date) {
            http_helper::response($this, http_helper::STATUS_START_LESSTHAN_END);
        }

        /*
        if ($this->second_diff($start_date_str, $end_date_str) > 28800) {
            http_helper::response($this, http_helper::STATUS_RESERVE_OVER_LIMIT);
        }
         *
         */

        $this->load->library('ews');

        $this->config->load('ews.php', TRUE);
        $ews_config = [
            'host' => $this->config->item('server', 'ews'),
            'admin' => $this->config->item('adminuser', 'ews'),
            'pass' => $this->config->item('adminpassword', 'ews')
        ];

        $host = $ews_config ['host'];
        $user = $ews_config ['admin'];
        $pass = $ews_config ['pass'];

        log_message("debug", "Host:" . $host . " , User: " . $user . " , Pass: " . $pass);

        $credential = $this->ews->create_ews_instants($host, $user, $pass);
        if (empty($credential)) {
            http_helper::response($this, http_helper::STATUS_EWS_ERROR);
        }

        $availables = array();
        // $availables_count = 0;

        $rooms = $this->room_model->findAll($room_type_id, $floor_id, $building_id, $location_id, $contact_id);
        log_message("debug", "RoomList:" . json_encode($rooms));
        if (!empty($rooms)) {
            foreach ($rooms as $i => $room) {

                // Check facility
                if (!empty($in_facilities)) {
                    foreach ($in_facilities as $in_facility) {

                        // log_message ( "debug", "in_facility=" . json_encode ( $in_facility ) );

                        $in_facility_id = explode(":", $in_facility) [0];
                        // log_message ( "debug", "in_facility_id=" .$in_facility_id );
                        if (!empty($in_facility_id)) {

                            $in_facility_require = explode(":", $in_facility) [1];
                            // log_message ( "debug", "in_facility_require=" .$in_facility_require );
                            // if (! isset ( $in_facility_require ) || empty ( $in_facility_require )) {
                            // $in_facility_require = 1;
                            // }
                            // log_message("")

                            $room_facilities = $this->room_facility_model->findByRoomIdAndFacilityId($room->room_id, $in_facility_id, $in_facility_require);
                            if (empty($room_facilities)) {
                                continue 2;
                            }
                        }
                    }
                }

                $room_synchronize = $this->room_synchronizer_model->findByRoomId($room->room_id);
                if (empty($room_synchronize)) {
                    continue;
                }

                $room_resource = $room_synchronize->user;

                log_message("debug", "calendar_check_available[". $room_resource ."] from:" . $start_date_str . " to " . $end_date_str );

                $events = $this->ews->calendar_check_available($credential, $start_date_str, $end_date_str, $room_resource);
                if (!empty($events)) {
                    continue;
                }


                $floor_name = "";
                $building_name = "";
                $location_name = "";
                $floor_model = $this->floor_model->findByPk($room->floor_id);
                if (!empty($floor_model)) {
                    $floor_name = $floor_model->floor_name;

                    $building_model = $this->building_model->findByPk($floor_model->building_id);
                    if (!empty($building_model)) {
                        $building_name = $building_model->building_name;
                        $location_model = $this->location_model->findByPk($building_model->location_id);

                        if (!empty($location_model)) {
                            $location_name = $location_model->location_name;
                        }
                    }
                }

                $contact_name = "";
                $contact_model = $this->contact_model->findByPk($room->contact_id);
                if (!empty($contact_model)) {
                    $contact_name = $contact_model->display_name;

                    $owner = array(
                            'id' => intval($contact_model->contact_id),
                            'name' => is_null($contact_model->display_name) ? "" : $contact_model->display_name,
                            'phone' => is_null($contact_model->telephone) ? "" : $contact_model->telephone,
                            'email' => is_null($contact_model->email) ? "" : $contact_model->email,
                            'employee_id' => is_null($contact_model->employee_id) ? "" : $contact_model->employee_id
                    );
                }

                $facilities = array();
//                $facility_list = $this->room_facility_model->findAllByRoomId($room->room_id);
                if($allfac){
                    $facility_list = $this->room_facility_model->findAllByRoomId($room->room_id);
                }else{
                    $facility_list = $this->room_facility_model->findByRoomIdAndFacilityId($room->room_id ,  Facility_model::FACILITY_ID_CHAIR ) ;
                }
                if (!empty($facility_list)) {

                    foreach ($facility_list as $j => $facility) {
                        // var_dump($facility) ;exit;

                        $facility_id = $facility->facility_id;
                        $facility_model = $this->facility_model->findByPk($facility_id);

                        $image = "";

                        $facility_property_model = $this->facility_property_model->findByPk($facility_id, "image");
                        if (!empty($facility_property_model)) {

                            $image = base_url() . "data/facility/" . $facility_property_model->property_value;
                        }

                        $showType = "";
                        $facility_property_model = $this->facility_property_model->findByPk($facility_id, "showtype");
                        if (!empty($facility_property_model)) {
                            $showType = $facility_property_model->property_value;
                        }

                        // $facilities [$j] ['facility_id'] = $facility_id;
                        // $facilities [$j] ['facility'] = $facility_model->facility_name;
                        // $facilities [$j] ['qty'] = intval ( $facility->quantity );
                        //
						// $facilities [$j] ['image'] = $image;
                        // $facilities [$j] ['show_type'] = $showType;

                        $facilities [] = array(
                            'facility_id' => intval($facility_id),
                            'facility' => $facility_model->facility_name,
                            'qty' => intval($facility->quantity),
                            'image' => $image,
                            'show_type' => $showType
                        );

                        $icon = "";
                        $facility_property_model = $this->facility_property_model->findByPk($facility_id, "image-mobile");
                        if (!empty($facility_property_model)) {
                            $icon = base_url() . "data/facility/" . $facility_property_model->property_value;
                        }
                        $facilities [$j] ['icon'] = $icon;
                    }
                }

                /**
                 * Room Layout
                 */
                /*
                  $layout = "";
                  $room_property_model = $this->room_property_model->findByPk ( $room->room_id, "layout_image" );
                  // log_message("debug" , json_encode($room_property_model)) ;
                  if (! empty ( $room_property_model )) {
                  $layout = base_url ( "data/room_property/" . $room_property_model->property_value );
                  } else {
                  $layout = base_url ( "data/room_property/default.png" );
                  } */

                $layout = "";
                $phone_no = "";
                $isdn = "";

                $layout_property_model = $this->room_property_model->findByPk($room->room_id, "layout_image" , true);
                if (!empty($layout_property_model)) {
                    $layout = base_url("data/room_property/" . $layout_property_model->property_value);
                }

                $layout = $layout ;//. (strpos($layout, '?') ? "&t=" . time() : "?t=" . time());

                $phoneno_property_model = $this->room_property_model->findByPk($room->room_id, "phone_no", false);
                if (!empty($phoneno_property_model)) {
                    $phone_no = $phoneno_property_model->property_value;
                }

                $isdn_property_model = $this->room_property_model->findByPk($room->room_id, "isdn", false);
                if (!empty($isdn_property_model)) {
                    $isdn = $isdn_property_model->property_value;
                }
                
                $admin_name = "";
                $admin_email = "";
                
                $admin_name_model = $this->room_property_model->findByPk($room->room_id, "admin_name", false);
                if (!empty($admin_name_model)) {
                	$admin_name = $admin_name_model->property_value;
                }
                
                $admin_email_model = $this->room_property_model->findByPk($room->room_id, "admin_email", false);
                if (!empty($admin_email_model)) {
                	$admin_email = $admin_email_model->property_value;
                }
                
                $admin = array('name'=>$admin_name,'email'=>$admin_email);
                
                $seat_layout_req = false;
                $seat_layout_req_model = $this->room_property_model->findByPk($room->room_id, "seat_layout_req", false);
                if (!empty($seat_layout_req_model)&&$seat_layout_req_model->property_value=='true') {
                	$seat_layout_req = true;
                }

                // $availables [$availables_count]['id'] = intval ( $room->room_id );
                // $availables [$availables_count]['name'] = $room->room_name;
                // $availables [$availables_count]['description'] = $room->description;
                // $availables [$availables_count]['type_id'] = intval ( $room->room_type_id );
                // $availables [$availables_count]['floor'] = $floor_name;
                // $availables [$availables_count]['available_from'] = $start;
                // $availables [$availables_count]['available_to'] = $end;
                // $availables [$availables_count]['contact'] = $contact_name;
                // $availables [$availables_count]['location'] = $location_name;
                // $availables [$availables_count]['facilities'] = $facilities;
                // $availables [$availables_count]['layout'] = $layout;
                // $availables_count ++;


                $room_type = $this->room_type_model->findByPk($room->room_type_id);
                $room_type_name = "";
                $room_type_color = "";

                if (!empty($room_type)) {
                    $room_type_name = $room_type->type_name;
                    $room_type_color = $room_type->color;
                }

                $availables [] = array(
                    'id' => intval($room->room_id),
                    'name' => $room->room_name,
                    'description' => $room->description,
                    'type_id' => intval($room->room_type_id),
                    'type_name' => $room_type_name,
                    'type_color' => $room_type_color,
                    'floor' => $floor_name,
                    'building' => $building_name,
                    'location' => $location_name,
                    'available_from' => $start,
                    'available_to' => $end,
                    'contact' => $contact_name,
                    'owner' => $owner,
                	'admin' => $admin,
                	'seat_layout_req'=>$seat_layout_req,
                    'facilities' => $facilities,
                    'layout' => $layout,
                    'phone_no' => $phone_no,
                    'isdn' => $isdn
                );
            }
        }
        // log_message ( "debug", "Room Search:" . json_encode ( $availables ) );
        $suggests = array();
        // $suggests_count = 0;
        if (empty($availables)) {
            // log_message ( "debug", "Find room suggest starting...." );

            $room_suggests = $this->room_model->findAll($room_type_id, $floor_id, $building_id, $location_id, $contact_id);
            // echo json_encode($room_suggests); exit;
            if (!empty($room_suggests)) {
                foreach ($room_suggests as $i => $room) {

                    $room_synchronize = $this->room_synchronizer_model->findByRoomId($room->room_id);
                    if (empty($room_synchronize)) {
                        continue;
                    }

                    $room_resource = $room_synchronize->user;

                    $tz = new DateTimeZone("Asia/Bangkok");

                    $start_date = new DateTime($start);
                    $start_date->setTimeZone($tz);
                    $start_date_str = $start_date->format('Y-m-d\TH:i:s');

                    if (!$start_date_str) {

                    }

                    $end_date = new DateTime($end);
                    $end_date->setTimeZone($tz);
                    $end_date_str = $end_date->format('Y-m-d\TH:i:s');
                    if (!$end_date_str) {

                    }

                    $events = $this->ews->calendar_check_available($credential, $start_date_str, $end_date_str, $room_resource);

                    if (!empty($events)) {
                        continue;
                    }

                    $floor_name = "";
                    $location_name = "";
                    $floor_model = $this->floor_model->findByPk($room->floor_id);
                    if (!empty($floor_model)) {
                        $floor_name = $floor_model->floor_name;

                        $building_model = $this->building_model->findByPk($floor_model->building_id);
                        if (!empty($building_model)) {

                            $location_model = $this->location_model->findByPk($building_model->location_id);

                            if (!empty($location_model)) {
                                $location_name = $location_model->location_name;
                            }
                        }
                    }

                    $contact_name = "";
                    $contact_model = $this->contact_model->findByPk($room->contact_id);
                    if (!empty($contact_model)) {
                        $contact_name = $contact_model->display_name;

                        $owner = array(
                            'id' => intval($contact_model->contact_id),
                            'name' => is_null($contact_model->display_name) ? "" : $contact_model->display_name,
                            'phone' => is_null($contact_model->telephone) ? "" : $contact_model->telephone,
                            'email' => is_null($contact_model->email) ? "" : $contact_model->email,
                            'employee_id' => is_null($contact_model->employee_id) ? "" : $contact_model->employee_id
                        );

                    }

                    $facilities = array();
                    //$facility_list = $this->room_facility_model->findAllByRoomId($room->room_id);
                     if($allfac){
                        $facility_list = $this->room_facility_model->findAllByRoomId($room->room_id);
                    }else{
                        $facility_list = $this->room_facility_model->findByRoomIdAndFacilityId($room->room_id ,  Facility_model::FACILITY_ID_CHAIR ) ;
                    }

                    if (!empty($facility_list)) {

                        foreach ($facility_list as $j => $facility) {
                            // var_dump($facility) ;exit;

                            $facility_id = $facility->facility_id;
                            $facility_model = $this->facility_model->findByPk($facility_id);

                            $image = "";

                            $facility_property_model = $this->facility_property_model->findByPk($facility_id, "image");
                            if (!empty($facility_property_model)) {

                                $image = base_url() . "data/facility/" . $facility_property_model->property_value;
                            }

                            $showType = "";
                            $facility_property_model = $this->facility_property_model->findByPk($facility_id, "showtype");
                            if (!empty($facility_property_model)) {
                                $showType = $facility_property_model->property_value;
                            }

                            // $facilities [$j] ['facility_id'] = intval ($facility_id);
                            // $facilities [$j] ['facility'] = $facility_model->facility_name;
                            // $facilities [$j] ['qty'] = intval ( $facility->quantity );
                            //
							// $facilities [$j] ['image'] = $image;
                            // $facilities [$j] ['show_type'] = $showType;

                            $facilities [] = array(
                                'facility_id' => intval($facility_id),
                                'facility' => $facility_model->facility_name,
                                'qty' => intval($facility->quantity),
                                'image' => $image,
                                'show_type' => $showType
                            );

                            $icon = "";
                            $facility_property_model = $this->facility_property_model->findByPk($facility_id, "image-mobile");
                            if (!empty($facility_property_model)) {
                                $icon = base_url() . "data/facility/" . $facility_property_model->property_value;
                            }
                            $facilities [$j] ['icon'] = $icon;
                        }
                    }

                    /**
                     * Room Layout
                     */
                    /*
                      $layout = "";
                      $room_property_model = $this->room_property_model->findByPk ( $room->room_id, "layout_image" );
                      // log_message("debug" , json_encode($room_property_model)) ;
                      if (! empty ( $room_property_model )) {
                      $layout = base_url ( "data/room_property/" . $room_property_model->property_value );
                      } else {
                      $layout = base_url ( "data/room_property/default.png" );
                      }
                     */

                    $layout = "";
                    $phone_no = "";
                    $isdn = "";

                    $layout_property_model = $this->room_property_model->findByPk($room->room_id, "layout_image" , true);
                    if (!empty($layout_property_model)) {
                        $layout = base_url("data/room_property/" . $layout_property_model->property_value);
                    }
                    /*                     * else {
                      $layout = base_url ( "data/room_property/default.png" );
                      }* */

                    $phoneno_property_model = $this->room_property_model->findByPk($room->room_id, "phone_no", false);
                    if (!empty($phoneno_property_model)) {
                        $phone_no = $phoneno_property_model->property_value;
                    }

                    $isdn_property_model = $this->room_property_model->findByPk($room->room_id, "isdn", false);
                    if (!empty($isdn_property_model)) {
                        $isdn = $isdn_property_model->property_value;
                    }
                    
                    $admin_name = "";
                    $admin_email = "";
                    
                    $admin_name_model = $this->room_property_model->findByPk($room->room_id, "admin_name", false);
                    if (!empty($admin_name_model)) {
                    	$admin_name = $admin_name_model->property_value;
                    }
                    
                    $admin_email_model = $this->room_property_model->findByPk($room->room_id, "admin_email", false);
                    if (!empty($admin_email_model)) {
                    	$admin_email = $admin_email_model->property_value;
                    }
                    
                    $admin = array('name'=>$admin_name,'email'=>$admin_email);
                    
                    $seat_layout_req = false;
                    $seat_layout_req_model = $this->room_property_model->findByPk($room->room_id, "seat_layout_req", false);
                    if (!empty($seat_layout_req_model)&&$seat_layout_req_model->property_value=='true') {
                    	$seat_layout_req = true;
                    }
                    /*
                      $suggest = array(
                      'id' => intval($room->room_id),
                      'name' => $room->room_name,
                      'description' => $room->description,
                      'type_id' => intval($room->room_type_id),
                      'floor' => $floor_name,
                      'available_from' => $start,
                      'available_to' => $end,
                      'contact' => $contact_name,
                      'location' => $location_name,
                      'facilities' => $facilities,
                      'layout' => $layout,
                      'phone_no' => $phone_no,
                      'isdn' => $isdn
                      );
                     *
                     */

                    $suggest[] = array(
                        'id' => intval($room->room_id),
                        'name' => $room->room_name,
                        'description' => $room->description,
                        'type_id' => intval($room->room_type_id),
                        'type_name' => $room_type_name,
                        'type_color' => $room_type_color,
                        'floor' => $floor_name,
                        'building' => $building_name,
                        'location' => $location_name,
                        'available_from' => $start,
                        'available_to' => $end,
                        'contact' => $contact_name,
                        'owner'=>$owner,
                    	'admin' => $admin,
                    	'seat_layout_req'=>$seat_layout_req,
                        'facilities' => $facilities,
                        'layout' => $layout,
                        'phone_no' => $phone_no,
                        'isdn' => $isdn
                    );

                    $suggests [] = $suggest;
                    // $suggests [$suggests_count]['id'] = intval ( $room->room_id );
                    // $suggests [$suggests_count]['name'] = $room->room_name;
                    // $suggests [$suggests_count]['description'] = $room->description;
                    // $suggests [$suggests_count]['type_id'] = intval ( $room->room_type_id );
                    // $suggests [$suggests_count]['floor'] = $floor_name;
                    // $suggests [$suggests_count]['available_from'] = $start;
                    // $suggests [$suggests_count]['available_to'] = $end;
                    // $suggests [$suggests_count]['contact'] = $contact_name;
                    // $suggests [$suggests_count]['location'] = $location_name;
                    // $suggests [$suggests_count]['facilities'] = $facilities;
                    // $suggests [$suggests_count]['layout'] = $layout;
                    //
                    // $suggests_count ++;
                }
            }
        }

        // if($contact_id == 41) {
        // $data = array (
        // 'availables' => array(),
        // "suggests" => array()
        // );
        // }else{

        $data = array(
            'availables' => $availables,
            "suggests" => $suggests
        );
        // }

        http_helper::response($this, http_helper::STATUS_SUCCESS, $data);
    }

    function reserve_multipart_post() {
        try {

            $serviceId = Service_model::SERVICE_RM04;
            // $post_params = $this->input->post();
            // log_message("debug", json_encode($post_params)) ;
            $response_data = NULL;

            $reqHeaders = http_helper::requestHeaders($this);

            // var_dump($reqHeaders) ; exit;

            $clientModel = http_helper::getClientModel($this, $reqHeaders);

            // var_dump($clientModel) ; exit;

            $reserveFrom = "";
            $userModel = null;
            $clientType = $clientModel->client_type;
            switch ($clientType) {
                case "MB" :
                case "WB" :

                    $access_token = isset($reqHeaders ['access_token']) ? $reqHeaders ['access_token'] : "";
                    if (empty($access_token)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }
                    // echo 'access_koen='.json_encode($access_token) ; exit;

                    $user_token_model = $this->user_token_model->findByAccessToken($access_token);
                    if (empty($user_token_model)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }
                    // echo 'user_token_model='.json_encode($user_token_model) ; exit;

                    $userModel = $this->user_model->findByPk($user_token_model->user_id);
                    if (empty($userModel)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }

                    // echo '$userModel='.json_encode($userModel) ; exit;
                    $reserveFrom = "mobile";

                    break;

                case "TB" :
                case "DK" :
                case "DT" :
                    // var_dump($reqHeaders['verify_code']) ; exit;

                    $verify_code = isset($reqHeaders ['verify_code']) ? $reqHeaders ['verify_code'] : "";

                    if (empty($verify_code)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }
                    $userModel = $this->user_model->findByVerifyCode($verify_code);
                    $reserveFrom = "dashboard";
                    break;

                default :

                    http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);

                    break;
            }

            // echo json_encode($userModel) ;
            if (empty($userModel)) {
                http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
            }

            $roomId = $this->post('room_id');
            $startTime = $this->post('start');
            $endTime = $this->post('end');
            $contacts = $this->post('attendees');
            $subject = $this->post('subject');
            $reminder = $this->post('reminder') ? $this->post('reminder') : 1;
//            $note = $this->post('note');
            $note = $this->post('note') ? nl2br ($this->post('note')) : "";

            /**
             * reserve with attachment *
             */
            $upload_files = array();
            log_message("debug", "_FILES:" . json_encode($_FILES));
            if (!empty($_FILES)) {
                $upload_files = $this->do_upload();

                log_message("debug", "upload_files:" . json_encode($upload_files));
                if (!$upload_files) {
                    http_helper::response($this, http_helper::STATUS_OVER_LIMIT_FILE_SIZE);
                    return;
                }
            }

            /**
             * recurrence function *
             */
            $pattern = (NULL == $this->post('pattern')) ? 0 : intval($this->post('pattern'));
            $interval = (NULL == $this->post('interval')) ? - 1 : intval($this->post('interval'));
            $occurrence = (NULL == $this->post('occurrence')) ? - 1 : intval($this->post('occurrence'));
            $recurr_end_date = (NULL == $this->post('recurr_end_date')) ? "" : $this->post('recurr_end_date');
            $day_of_week = (NULL == $this->post('day_of_week')) ? "" : $this->post('day_of_week');
            $day_of_month = (NULL == $this->post('day_of_month')) ? - 1 : intval($this->post('day_of_month'));
            $month = (NULL == $this->post('month')) ? - 1 : intval($this->post('month'));
            $index_day_of_week = (NULL == $this->post('index_day_of_week')) ? - 1 : intval($this->post('index_day_of_week'));

            if (empty($roomId) || empty($startTime) || empty($endTime) || empty($subject)) {
                log_message("error", "[Invalid Input!!]roomId=" . $roomId . "|startTime=" . $startTime . "|endTime=" . $endTime . "|subject=" . $subject . "|");
                http_helper::response($this, http_helper::STATUS_INVALID_INPUT);
            }

            $contact_id = $userModel->contact_id;
            log_message("debug", "Reserve contact id:" . $contact_id);
            log_message("debug", "Reserve user id:" . $userModel->user_id);

            /*if ($userModel->user_id == 35) {

                $ocr_model = $this->ocr_model->findByVerifyCode($verify_code);
                if (!empty($ocr_model)) {
                    $contact_id = $ocr_model->contact_id;
                }
            }
             */

            $contact_model = $this->contact_model->findByPk($contact_id);
            log_message("debug", "contact_model:" .  json_encode($contact_model));
            if (empty($contact_model)) {
                http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR);
            }

            $body = "";
            $tz = new DateTimeZone("Asia/Bangkok");

            $start_date = new DateTime($startTime);
            $start_date->setTimeZone($tz);
            $start_date_str = $start_date->format('Y-m-d\TH:i:s');

            $end_date = new DateTime($endTime);
            $end_date->setTimeZone($tz);
            $end_date_str = $end_date->format('Y-m-d\TH:i:s');
            log_message("debug", "start_date_str:" .  $start_date_str);
            log_message("debug", "end_date_str:" .  $end_date_str);
            // if( $start_date > $end_date ) {
            // http_helper::response ( $this, http_helper::STATUS_INVALID_INPUT );
            // }
            // if( $end_date - $start_date > 28800 ) {
            // http_helper::response ( $this, http_helper::STATUS_INVALID_INPUT );
            // }
            // log_message("debug","enddate - start_date :=> ". $this->second_diff($start_date_str,$end_date_str) ) ;
            // if($this->second_diff($start_date_str,$end_date_str) > 28800 ) {
            // http_helper::response ( $this, http_helper::STATUS_INVALID_INPUT );
            // }
            // Start Time cannot more than endtime
            $date = new DateTime ();
            $date->sub(new DateInterval('PT15M'));

            if ($start_date < $date) {
                http_helper::response($this, http_helper::STATUS_START_LESSTHAN_NOW);
            }

            if ($start_date > $end_date) {
                http_helper::response($this, http_helper::STATUS_START_LESSTHAN_END);
            }
            log_message("debug", "datetime ok!");
            /*
              $max_reserve_minutes = 28800;
              try {
              $max_reserve_minutes = intval($this->config_model->findByKey("limitation.reserve.max_minutes")->value);
              } catch (Exception $e) {

              }

              if ($this->second_diff($start_date_str, $end_date_str) > $max_reserve_minutes) {
              http_helper::response($this, http_helper::STATUS_RESERVE_OVER_LIMIT);
              }

             */

            $allday = false;
            $location = "";

            $contactList = null;
            if (!empty($contacts)) {
                $contactList = explode(",", $contacts);
            }
            log_message("debug", "Contacts:" . $contacts);


            $attendees = array();
            if (!empty($contactList)) {
                // $attendees = "" ;
                foreach ($contactList as $i => $contact_id_attendee) {

                    if (is_numeric($contact_id_attendee)) {

                        $contact_model_attendee = $this->contact_model->findByPk($contact_id_attendee);
                        if (!empty($contact_model_attendee)) {
                            // $attendees .= ( $contact_model->display_name."|".$contact_model->email . ";" ) ;
                            $attendees [] = $contact_model_attendee->email;
                        }
                    } else {
                        $attendees [] = $contact_id_attendee;
                    }
                }
            }

            $attendeesStr = implode(",", $attendees);

            /**
             * ==================== Begin Reserve by Servlet Listener =============================*
             */
            $files = array();
            if (!empty($upload_files)) {
                foreach ($upload_files as $upload_file) {
                    $name = $upload_file ['file_name'];
                    $path = $upload_file ['full_path'];

                    $files [] = array(
                        "name" => $name,
                        "data" => base64_encode(file_get_contents($path))
                    );
                }
            }

            $tag = "" ;

            $management_no = (NULL == $this->post('management_no')) ? 0 : intval($this->post('management_no'));
            if ($management_no > 0) {
//                $note .= " #management_no:" . $management_no . " ";
                $tag .= " #management_no:" . $management_no . " ";
            }

            $nonmanagement_no = (NULL == $this->post('nonmanagement_no')) ? 0 : intval($this->post('nonmanagement_no'));
            if ($nonmanagement_no > 0) {
//                $note .= " #nonmanagement_no:" . $nonmanagement_no . " ";
                $tag .= " #nonmanagement_no:" . $nonmanagement_no . " ";
            }

            $ext_no = (NULL == $this->post('ext_no')) ? "" : trim($this->post('ext_no'));
            if (!empty($ext_no)) {
//                $note .= " #ext_no:" . $ext_no . " ";
                $tag .= " #ext_no:" . $ext_no . " ";
            }

            $tag .= " #client_id:" . $clientModel->client_id . "" ;
            $tag .= " #pattern:" . $pattern . "" ;

            /**
             * Hashtag param *
             */
            $seat_layout_image = "" ;

            $seat_layout_id = (NULL == $this->post('seat_layout_id')) ? 0 : intval($this->post('seat_layout_id'));
            if ($seat_layout_id > 0) {
                $seat_layout_model_list = $this->hashtag_model->findAllByKeyword('seat_layout');
                if (!empty($seat_layout_model_list)) {
                    foreach ($seat_layout_model_list as $seat_layout_model) {
                        $seat_layout = json_decode($seat_layout_model->content);
                        if ($seat_layout->id == $seat_layout_id) {
                            // $note .= " #seat_layout:".$seat_layout->image." " ;

                            /*
                            $ext = pathinfo($seat_layout->image, PATHINFO_EXTENSION);

                            $files [] = array(
                                "name" => "seat_layout." . $ext,
                                "data" => base64_encode(file_get_contents($seat_layout->image))
                            );
                            */


                            $seat_layout_image = $seat_layout->image ;

                            break;
                        }
                    }
                }
            }

            // log_message ( 'debug', "Request Data\n" . json_encode ( $files ) );

            $data = array(
                'room_id' => $roomId,
                // 'event_code' => '',
                'start_time' => $startTime,
                'end_time' => $endTime,
                'topic' => $subject,
                'detail' => $note,
                'create_time' => date('Y-m-d H:i:s'),
                'create_by' => $userModel->contact_id,
                'status_id' => 2,
                'reserve_from' => $reserveFrom,
                'attendees' => $attendeesStr,
                'reminder' => $reminder,
                'files' => json_encode($files),
                'pattern' => $pattern,
                'interval' => $interval,
                'occurrence' => $occurrence,
                'endDate' => $recurr_end_date,
                'dayOfweek' => $day_of_week,
                'dayOfMonth' => $day_of_month,
                'month' => $month,
                'indexDayOfweek' => $index_day_of_week ,
                'tag'=> $tag
            );

            // file_put_contents ("/mnt/mi/data/test".date("YmdHis").".txt" , json_encode ( $files ) ) ;
            // log_message ( 'error', "Request Data\n" . json_encode ( $data ) );
            $ch = curl_init();

            // $reserv_url = "http://172.17.31.111:8080/micore/reserve" ;
            $reserv_url = "http://192.168.117.130:8080/micore/reserve";

            $this->config->load('micore.php', TRUE);
            $reserv_url_config = $this->config->item('url', 'micore');

            log_message("debug", "reserv_url_config = " . json_encode($reserv_url_config));
            if (!empty($reserv_url_config)) {
                $reserv_url = $reserv_url_config . "reserve";
            }

            log_message("debug", "reserv_url = " . $reserv_url);

            // $headers = array("Content-Type:multipart/form-data");

            curl_setopt($ch, CURLOPT_URL, $reserv_url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt ( $ch, CURLOPT_HTTPHEADER , $headers ) ;

            $log_data = $data;
            $log_data ['files'] = $log_data ['files'] == NULL ? "None" : "****";
            log_message('error', "\n============== Request MI-Core =============\nURL:" . $reserv_url . "\nDATA:" . json_encode($log_data) . "\n========================");
            $json_response = curl_exec($ch);
            if (empty($json_response)) {
                log_message('error', "Request MI-Core Failed!");
                http_helper::response($this, http_helper::STATUS_REQUEST_MICORE_FAILED);
            }

            curl_close($ch);
            log_message('error', $json_response);
            $json = json_decode($json_response);
            log_message('error', json_encode($json));
            $response_code = $json->responseCode;
            $response_status = $json->responseStatus;
            log_message('error', "response_code = $response_code and response_status = $response_status");
//            http_helper::response($this, $response_code, $response_status);

            if( intval($response_code) !== 0 ){
//                return http_helper::response($this, $response_code, $response_status);
                return http_helper::response($this, $response_code );
            }


            // If not auto-accept , then send email with room layout attachment to room owner
            try {
                log_message("debug" , "Seat Layout Image = ".print_r($seat_layout_image,TRUE)) ;
                if(!empty($seat_layout_image)) {
                    $admin_email = $this->room_property_model->findByPk($roomId, "admin_email")->property_value;
                    if (!empty($admin_email)) {

                        $room_model = $this->room_model->findByPk($roomId) ;

                        $reserve_date_str = $start_date->format('Y-m-d');
                        $reserve_start_str = $start_date->format('H:i');
                        $reserve_end_str = $end_date->format('H:i');

                        $subject = "Seat Layout Arrangement for ".$room_model->room_name ."(". $reserve_date_str .",". $reserve_start_str ." - ". $reserve_end_str.")" ;
                        $body = "This request was forwarded to you for seat layout arrangement." ;
                        $body .= "<br/><b>Organizer</b> : ". $contact_model->display_name ;
                        $body .= "<br/><b>When</b> : ". $reserve_date_str  .",". $reserve_start_str ." - ". $reserve_end_str ;
                        $body .= "<br/><b>Location</b> : " . $room_model->room_name ;
                        $body .= "<br/>" ;

                        email_helper::sendLayout($subject, $body, $admin_email, NULL, NULL, NULL, $seat_layout_image);
                    }
                }

            }catch (Exception $ee){
                log_message("error", "Send Mail to room owner error:" . print_r($ee,true));
            }

            $is_auto_accept = TRUE ;
            $is_auto_accept_model = $this->room_property_model->findByPk($roomId,"auto_accept") ;
            log_message("debug" , "Auto Accept Model:" . print_r($is_auto_accept_model,TRUE)) ;
            if(!empty($is_auto_accept_model)){
                //$is_auto_accept = !filter_var($is_auto_accept_model->property_value, FILTER_VALIDATE_BOOLEAN)?filter_var($is_auto_accept_model->property_value, FILTER_VALIDATE_BOOLEAN):TRUE ;
                if($is_auto_accept_model->property_value  === 'false' || $is_auto_accept_model->property_value  === '0'){
                    $is_auto_accept = FALSE ;
                }
            }
            log_message("debug" , "Auto Accept : " . json_encode($is_auto_accept,TRUE)) ;
            if($is_auto_accept){

                /*

                $title = "Title: Room Auto Accept" ;
                $message = "Message: Room Auto Accept Value" ;
                return http_helper::response($this, $response_code, $response_status , NULL ,$title , $message );
                */

                return http_helper::response($this, $response_code );
            }

            $title = "Warning";
            $message = "This room requires approval. Your request has tentatively accepted.<br>Please see response in your mail";
            return http_helper::response($this, $response_code, NULL ,$title , $message );

            /**
             * ==================== End Reserve by Servlet Listener =============================*
             */
        } catch (Exception $e) {
            log_message("error", "Exception Error" . json_encode($e));
            http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR);
        }
    }

    function do_upload() {
        $totalFileSize = 0;
        $uploads = array();

        $this->load->library('upload');

        $files = $_FILES;
        log_message("debug", "upload result:" . json_encode($files));
        $cpt = count($_FILES ['file'] ['name']);
        for ($i = 0; $i < $cpt; $i ++) {
            $_FILES ['file'] ['name'] = $files ['file'] ['name'] [$i];
            $_FILES ['file'] ['type'] = $files ['file'] ['type'] [$i];
            $_FILES ['file'] ['tmp_name'] = $files ['file'] ['tmp_name'] [$i];
            $_FILES ['file'] ['error'] = $files ['file'] ['error'] [$i];
            $_FILES ['file'] ['size'] = $files ['file'] ['size'] [$i];

            $totalFileSize += intval($_FILES ['file'] ['size']);

            $this->upload->initialize($this->set_upload_options());
            $upload_result = $this->upload->do_upload('file');
             log_message("debug", "upload result:". json_encode($upload_result)) ;

            if ($upload_result) {
                log_message("debug", "upload result:". json_encode($upload_result)) ;
                $data = $this->upload->data();
                log_message("debug", "upload data:". json_encode($data)) ;
                $uploads [] = $data;
            } else {
                $error = $this->upload->display_errors();
                log_message("debug", "Upload Error!!" . json_encode($error));
            }
        }
         log_message("debug", "totalFileSize:".$totalFileSize) ;

        if ($totalFileSize > 10240000) { // 10 Mb
            return false;
        }
         log_message("debug", "Upload....". json_encode($uploads)) ;
        return $uploads;
    }

    private function getResourcePath() {
        $resource_path = "/mnt/mi/data/attach_files/" . date("Ymd") . "/";
        ;
        $config_model = $this->config_model->findByKey("resource_path.base");
        if (!empty($config_model)) {
            $resource_path = $config_model->value . "attach_files/" . date("Ymd") . "/";
        }
        log_message("debug", "resource_path=" . $resource_path);
        if (!file_exists($resource_path)) {
            mkdir($resource_path);
            chmod($resource_path, 0755);
        }

        return $resource_path;
    }

    private function set_upload_options() {
        // upload an image options
        $config = array();
        // $config ['upload_path'] = './Images/';
        // $config ['allowed_types'] = 'gif|jpg|png';

        $config ['upload_path'] = $this->getResourcePath();
        $config ['allowed_types'] = '*';
        $config ['max_size'] = '0';
        $config ['overwrite'] = TRUE;

        return $config;
    }


    function reserve_direct_post() {
        try {

            $serviceId = Service_model::SERVICE_RM04;
            // $post_params = $this->input->post();
            // log_message("debug", json_encode($post_params)) ;
            $response_data = NULL;

//            $reqHeaders = http_helper::requestHeaders($this);

            // var_dump($reqHeaders) ; exit;

//            $clientModel = http_helper::getClientModel($this, $reqHeaders);

            $client_id = $this->post('client-id');
            $lat = $this->post('lat');
            $long = $this->post('long');
            $app_id = $this->post('app-id');
            $type = $this->post('client-type');
            $os = $this->post('os');
            $os_version = $this->post('os-version');
            $app_version = $this->post('app-version');
            $push_token = $this->post('push-token');

            $clientModel = $this->client_model->findByUuid($client_id, $lat, $long, $app_id, $type, $os, $os_version, $app_version, $push_token);
            if (empty($clientModel)) {
                log_message("error", "Find Client By UUID Failed!!! $client_id  , $lat,$long,$app_id, $type ,$os, $os_version, $app_version, $push_token");
                self::response($this, http_helper::STATUS_INVALID_INPUT);
            }

            // var_dump($clientModel) ; exit;

            $reserveFrom = "";
            $userModel = null;
            $clientType = $clientModel->client_type;
            switch ($clientType) {
                case "MB" :
                case "WB" :

                    $access_token = !empty($this->post('access-token')) ? $this->post('access-token') : "";
                    if (empty($access_token)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }
                    // echo 'access_koen='.json_encode($access_token) ; exit;

                    $user_token_model = $this->user_token_model->findByAccessToken($access_token);
                    if (empty($user_token_model)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }
                    // echo 'user_token_model='.json_encode($user_token_model) ; exit;

                    $userModel = $this->user_model->findByPk($user_token_model->user_id);
                    if (empty($userModel)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }

                    // echo '$userModel='.json_encode($userModel) ; exit;
                    $reserveFrom = "mobile";

                    break;

                case "TB" :
                case "DK" :
                case "DT" :
                    // var_dump($reqHeaders['verify_code']) ; exit;

                    $verify_code = !empty($this->post('verify_code')) ? $this->post('verify_code') : "";

                    if (empty($verify_code)) {
                        http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
                    }
                    $userModel = $this->user_model->findByVerifyCode($verify_code);
                    $reserveFrom = "dashboard";
                    break;

                default :

                    http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);

                    break;
            }

            // echo json_encode($userModel) ;
            if (empty($userModel)) {
                http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED);
            }

            $roomId = $this->post('room_id');
            $startTime = $this->post('start');
            $endTime = $this->post('end');
            $contacts = $this->post('attendees');
            $subject = $this->post('subject');
            $reminder = $this->post('reminder') ? $this->post('reminder') : 1;
//            $note = $this->post('note');
            $note = $this->post('note') ? nl2br ($this->post('note')) : "";

            /**
             * reserve with attachment *
             */
            $upload_files = array();
            log_message("debug", "_FILES:" . json_encode($_FILES));
            if (!empty($_FILES)) {
                $upload_files = $this->do_upload();

                log_message("debug", "upload_files:" . json_encode($upload_files));
                /*if (!$upload_files) {
                    http_helper::response($this, http_helper::STATUS_OVER_LIMIT_FILE_SIZE);
                    return;
                }*/
            }

            /**
             * recurrence function *
             */
            $pattern = (NULL == $this->post('pattern')) ? 0 : intval($this->post('pattern'));
            $interval = (NULL == $this->post('interval')) ? - 1 : intval($this->post('interval'));
            $occurrence = (NULL == $this->post('occurrence')) ? - 1 : intval($this->post('occurrence'));
            $recurr_end_date = (NULL == $this->post('recurr_end_date')) ? "" : $this->post('recurr_end_date');
            $day_of_week = (NULL == $this->post('day_of_week')) ? "" : $this->post('day_of_week');
            $day_of_month = (NULL == $this->post('day_of_month')) ? - 1 : intval($this->post('day_of_month'));
            $month = (NULL == $this->post('month')) ? - 1 : intval($this->post('month'));
            $index_day_of_week = (NULL == $this->post('index_day_of_week')) ? - 1 : intval($this->post('index_day_of_week'));

            if (empty($roomId) || empty($startTime) || empty($endTime) || empty($subject)) {
                log_message("error", "[Invalid Input!!]roomId=" . $roomId . "|startTime=" . $startTime . "|endTime=" . $endTime . "|subject=" . $subject . "|");
                http_helper::response($this, http_helper::STATUS_INVALID_INPUT);
            }

            $contact_id = $userModel->contact_id;
            log_message("debug", "Reserve contact id:" . $contact_id);
            log_message("debug", "Reserve user id:" . $userModel->user_id);

            /*if ($userModel->user_id == 35) {

                $ocr_model = $this->ocr_model->findByVerifyCode($verify_code);
                if (!empty($ocr_model)) {
                    $contact_id = $ocr_model->contact_id;
                }
            }
             */

            $contact_model = $this->contact_model->findByPk($contact_id);
            log_message("debug", "contact_model:" .  json_encode($contact_model));
            if (empty($contact_model)) {
                http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR);
            }

            $body = "";
            $tz = new DateTimeZone("Asia/Bangkok");

            $start_date = new DateTime($startTime);
            $start_date->setTimeZone($tz);
            $start_date_str = $start_date->format('Y-m-d\TH:i:s');

            $end_date = new DateTime($endTime);
            $end_date->setTimeZone($tz);
            $end_date_str = $end_date->format('Y-m-d\TH:i:s');
            log_message("debug", "start_date_str:" .  $start_date_str);
            log_message("debug", "end_date_str:" .  $end_date_str);
            // if( $start_date > $end_date ) {
            // http_helper::response ( $this, http_helper::STATUS_INVALID_INPUT );
            // }
            // if( $end_date - $start_date > 28800 ) {
            // http_helper::response ( $this, http_helper::STATUS_INVALID_INPUT );
            // }
            // log_message("debug","enddate - start_date :=> ". $this->second_diff($start_date_str,$end_date_str) ) ;
            // if($this->second_diff($start_date_str,$end_date_str) > 28800 ) {
            // http_helper::response ( $this, http_helper::STATUS_INVALID_INPUT );
            // }
            // Start Time cannot more than endtime
            $date = new DateTime ();
            $date->sub(new DateInterval('PT15M'));

            if ($start_date < $date) {
                http_helper::response($this, http_helper::STATUS_START_LESSTHAN_NOW);
            }

            if ($start_date > $end_date) {
                http_helper::response($this, http_helper::STATUS_START_LESSTHAN_END);
            }
            log_message("debug", "datetime ok!");
            /*
              $max_reserve_minutes = 28800;
              try {
              $max_reserve_minutes = intval($this->config_model->findByKey("limitation.reserve.max_minutes")->value);
              } catch (Exception $e) {

              }

              if ($this->second_diff($start_date_str, $end_date_str) > $max_reserve_minutes) {
              http_helper::response($this, http_helper::STATUS_RESERVE_OVER_LIMIT);
              }

             */

            $allday = false;
            $location = "";

            $contactList = null;
            if (!empty($contacts)) {
                $contactList = explode(",", $contacts);
            }
            log_message("debug", "Contacts:" . $contacts);


            $attendees = array();
            if (!empty($contactList)) {
                // $attendees = "" ;
                foreach ($contactList as $i => $contact_id_attendee) {

                    if (is_numeric($contact_id_attendee)) {

                        $contact_model_attendee = $this->contact_model->findByPk($contact_id_attendee);
                        if (!empty($contact_model_attendee)) {
                            // $attendees .= ( $contact_model->display_name."|".$contact_model->email . ";" ) ;
                            $attendees [] = $contact_model_attendee->email;
                        }
                    } else {
                        $attendees [] = $contact_id_attendee;
                    }
                }
            }

            $attendeesStr = implode(",", $attendees);

            /**
             * ==================== Begin Reserve by Servlet Listener =============================*
             */
            $files = array();
            if (!empty($upload_files)) {
                foreach ($upload_files as $upload_file) {
                    $name = $upload_file ['file_name'];
                    $path = $upload_file ['full_path'];

                    $files [] = array(
                        "name" => $name,
                        "data" => base64_encode(file_get_contents($path))
                    );
                }
            }

            $tag = "" ;

            $management_no = (NULL == $this->post('management_no')) ? 0 : intval($this->post('management_no'));
            if ($management_no > 0) {
//                $note .= " #management_no:" . $management_no . " ";
                $tag .= " #management_no:" . $management_no . " ";
            }

            $nonmanagement_no = (NULL == $this->post('nonmanagement_no')) ? 0 : intval($this->post('nonmanagement_no'));
            if ($nonmanagement_no > 0) {
//                $note .= " #nonmanagement_no:" . $nonmanagement_no . " ";
                $tag .= " #nonmanagement_no:" . $nonmanagement_no . " ";
            }

            $ext_no = (NULL == $this->post('ext_no')) ? "" : trim($this->post('ext_no'));
            if (!empty($ext_no)) {
//                $note .= " #ext_no:" . $ext_no . " ";
                $tag .= " #ext_no:" . $ext_no . " ";
            }


            $tag .= " #client_id:" . $clientModel->client_id . "" ;
            $tag .= " #pattern:" . $pattern . "" ;
            /**
             * Hashtag param *
             */
            $seat_layout_image = "" ;

            $seat_layout_id = (NULL == $this->post('seat_layout_id')) ? 0 : intval($this->post('seat_layout_id'));
            if ($seat_layout_id > 0) {
                $seat_layout_model_list = $this->hashtag_model->findAllByKeyword('seat_layout');
                if (!empty($seat_layout_model_list)) {
                    foreach ($seat_layout_model_list as $seat_layout_model) {
                        $seat_layout = json_decode($seat_layout_model->content);
                        if ($seat_layout->id == $seat_layout_id) {
                            // $note .= " #seat_layout:".$seat_layout->image." " ;

                            /*
                            $ext = pathinfo($seat_layout->image, PATHINFO_EXTENSION);

                            $files [] = array(
                                "name" => "seat_layout." . $ext,
                                "data" => base64_encode(file_get_contents($seat_layout->image))
                            );
                            */


                            $seat_layout_image = $seat_layout->image ;

                            break;
                        }
                    }
                }
            }

            // log_message ( 'debug', "Request Data\n" . json_encode ( $files ) );

            $data = array(
                'room_id' => $roomId,
                // 'event_code' => '',
                'start_time' => $startTime,
                'end_time' => $endTime,
                'topic' => $subject,
                'detail' => $note,
                'create_time' => date('Y-m-d H:i:s'),
                'create_by' => $userModel->contact_id,
                'status_id' => 2,
                'reserve_from' => $reserveFrom,
                'attendees' => $attendeesStr,
                'reminder' => $reminder,
                'files' => json_encode($files),
                'pattern' => $pattern,
                'interval' => $interval,
                'occurrence' => $occurrence,
                'endDate' => $recurr_end_date,
                'dayOfweek' => $day_of_week,
                'dayOfMonth' => $day_of_month,
                'month' => $month,
                'indexDayOfweek' => $index_day_of_week ,
                'tag'=> $tag
            );

            // file_put_contents ("/mnt/mi/data/test".date("YmdHis").".txt" , json_encode ( $files ) ) ;
            // log_message ( 'error', "Request Data\n" . json_encode ( $data ) );
            $ch = curl_init();

            // $reserv_url = "http://172.17.31.111:8080/micore/reserve" ;
            $reserv_url = "http://192.168.117.130:8080/micore/reserve";

            $this->config->load('micore.php', TRUE);
            $reserv_url_config = $this->config->item('url', 'micore');

            log_message("debug", "reserv_url_config = " . json_encode($reserv_url_config));
            if (!empty($reserv_url_config)) {
                $reserv_url = $reserv_url_config . "reserve";
            }

            log_message("debug", "reserv_url = " . $reserv_url);

            // $headers = array("Content-Type:multipart/form-data");

            curl_setopt($ch, CURLOPT_URL, $reserv_url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt ( $ch, CURLOPT_HTTPHEADER , $headers ) ;

            $log_data = $data;
            $log_data ['files'] = $log_data ['files'] == NULL ? "None" : "****";
            log_message('error', "\n============== Request MI-Core =============\nURL:" . $reserv_url . "\nDATA:" . json_encode($log_data) . "\n========================");
            $json_response = curl_exec($ch);
            if (empty($json_response)) {
                log_message('error', "Request MI-Core Failed!");
                http_helper::response($this, http_helper::STATUS_REQUEST_MICORE_FAILED);
            }

            curl_close($ch);
            log_message('error', $json_response);
            $json = json_decode($json_response);
            log_message('error', json_encode($json));
            $response_code = $json->responseCode;
            $response_status = $json->responseStatus;
            log_message('error', "response_code = $response_code and response_status = $response_status");
//            http_helper::response($this, $response_code, $response_status);

            if( intval($response_code) !== 0 ){
//                return http_helper::response($this, $response_code, $response_status);
                return http_helper::response($this, $response_code );
            }


            // If not auto-accept , then send email with room layout attachment to room owner
            try {
                log_message("debug" , "Seat Layout Image = ".print_r($seat_layout_image,TRUE)) ;
                if(!empty($seat_layout_image)) {
                    $admin_email = $this->room_property_model->findByPk($roomId, "admin_email")->property_value;
                    if (!empty($admin_email)) {

                        $room_model = $this->room_model->findByPk($roomId) ;

                        $reserve_date_str = $start_date->format('Y-m-d');
                        $reserve_start_str = $start_date->format('H:i');
                        $reserve_end_str = $end_date->format('H:i');

                        $subject = "Seat Layout Arrangement for ".$room_model->room_name ."(". $reserve_date_str .",". $reserve_start_str ." - ". $reserve_end_str.")" ;
                        $body = "This request was forwarded to you for seat layout arrangement." ;
                        $body .= "<br/><b>Organizer</b> : ". $contact_model->display_name ;
                        $body .= "<br/><b>When</b> : ". $reserve_date_str  .",". $reserve_start_str ." - ". $reserve_end_str ;
                        $body .= "<br/><b>Location</b> : " . $room_model->room_name ;
                        $body .= "<br/>" ;

                        email_helper::sendLayout($subject, $body, $admin_email, NULL, NULL, NULL, $seat_layout_image);
                    }
                }

            }catch (Exception $ee){
                log_message("error", "Send Mail to room owner error:" . print_r($ee,true));
            }

            $is_auto_accept = TRUE ;
            $is_auto_accept_model = $this->room_property_model->findByPk($roomId,"auto_accept") ;
            log_message("debug" , "Auto Accept Model:" . print_r($is_auto_accept_model,TRUE)) ;
            if(!empty($is_auto_accept_model)){
                //$is_auto_accept = !filter_var($is_auto_accept_model->property_value, FILTER_VALIDATE_BOOLEAN)?filter_var($is_auto_accept_model->property_value, FILTER_VALIDATE_BOOLEAN):TRUE ;
                if($is_auto_accept_model->property_value  === 'false' || $is_auto_accept_model->property_value  === '0'){
                    $is_auto_accept = FALSE ;
                }
            }
            log_message("debug" , "Auto Accept : " . json_encode($is_auto_accept,TRUE)) ;
            if($is_auto_accept){

                /*

                $title = "Title: Room Auto Accept" ;
                $message = "Message: Room Auto Accept Value" ;
                return http_helper::response($this, $response_code, $response_status , NULL ,$title , $message );
                */

                return http_helper::response($this, $response_code );
            }

            $title = "Warning";
            $message = "This room requires approval. Your request has tentatively accepted.<br>Please see response in your mail";
            return http_helper::response($this, $response_code, NULL ,$title , $message );

            /**
             * ==================== End Reserve by Servlet Listener =============================*
             */
        } catch (Exception $e) {
            log_message("error", "Exception Error" . json_encode($e));
            http_helper::response($this, http_helper::STATUS_INTERNAL_SERVER_ERROR);
        }
    }

	function sensor_post($room_id=null){
		$time_now = $this->input->post('time_now');
		$num_attendee = $this->input->post('num_attendee');
		$sign = $this->input->post('sign');
		$ip = $this->input->post('ip');
		$time_come_to_meeting = $this->input->post('time_come_to_meeting');

		$checkSign = hash_hmac('sha256', $room_id.$time_now, $this->config->item('signkey'));
		if($checkSign!=$sign)http_helper::response($this,400, array('detail'=>'Sign not match')) ;
		
		$now = date('Y-m-d H:i:s');
		$events = $this->event_model->findByRoom($room_id,$now,$now);

		if(!empty($events)){
			foreach ($events as $row){
					$event = $row;
			}
			$updatedata = array('num_attendee'=>$num_attendee);
			if(!empty($time_come_to_meeting)) $updatedata['time_come_to_meeting'] = $time_come_to_meeting;
			if(!empty($ip)) $updatedata['ip'] = $ip;
			if($num_attendee>$event->num_attendee)$this->db->where('event_id',$event->event_id)->update('event',$updatedata);
			http_helper::response($this,http_helper::STATUS_SUCCESS, 
				array(
					'detail'=>'No Error',
					'meeting_id'=>$event->event_id,
					'start'=>$event->start_time,
					'end'=>$event->end_time,
					'is_checkin'=>$event->is_checkin,
					'server_time'=>$now
				)
			) ;
		}
		http_helper::response($this,400, array('detail'=>'Event not found','meeting_id'=>0,'start'=>'','end'=>'','is_checkin'=>0,'server_time'=>$now)) ;
	}
}
