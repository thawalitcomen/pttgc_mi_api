<?php
require(APPPATH.'libraries/REST_Controller.php');

class Http404 extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($code='404')
	{
		/*
		lat
long
client_id
client_type
version
app_id
app_version
		*/
		$response_code = $code;
		$response_status = 'Not Found';
		$response_message = 'Not Found';
		$data = array('response_code'=>$response_code,'response_status'=>$response_status,'response_message'=>$response_message);
        $this->response($data);
	}
}
