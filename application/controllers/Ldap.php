<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require (APPPATH . 'libraries/REST_Controller.php');
class Ldap extends REST_Controller { 


	public function connect_get(){
		$this->config->load('ldap.php', TRUE);
		
		$ldap = [
				'timeout' => $this->config->item('timeout', 'ldap'),
				'host' => $this->config->item('server', 'ldap'),
				'port' => $this->config->item('port', 'ldap'),
				'rdn' => $this->config->item('binduser', 'ldap'),
				'pass' => $this->config->item('bindpw', 'ldap'),
				'basedn' => $this->config->item('basedn', 'ldap'),
				'defaultdn' => $this->config->item('defaultdn', 'ldap')
		];
		
		echo 'test test'. $ldap['timeout'] ."  " . json_encode($ldap);
		$host = "10.254.1.6"; // 10.254.1.5, 10.254.1.6
		$port = "389" ;
		
		log_message('debug', 'LDAP Auth: Connect to ' . (isset($ldaphost) ? $ldaphost : '[ldap not configured]'));
		echo 'test';
		// Connect to the ldap server
		$ldapconn = ldap_connect($host, $port );
		
		var_dump($ldapconn) ;
		
		if ($ldapconn)
		{
			echo ">>>>>>>>>>>>>> <br/>" ;
			var_dump($ldapconn) ;
			
			log_message('debug', 'Setting timeout to ' . $ldap['timeout'] . ' seconds');
		
			ldap_set_option($ldapconn, LDAP_OPT_NETWORK_TIMEOUT, $ldap['timeout']);
		
			log_message('debug', 'LDAP Auth: Binding to ' . $ldap['host'] . ' with dn ' . $ldap['rdn']);
		
			// Binding to the ldap server
			$ldapbind = ldap_bind($ldapconn, $ldap['rdn'], $ldap['pass']);
			echo "Bind<br/>" ;
			var_dump($ldapbind) ;
			// Verify the binding
			if ($ldapbind === FALSE)
			{
				log_message('error', 'LDAP Auth: bind was unsuccessful');
				return FALSE;
			}
		
			log_message('debug', 'LDAP Auth: bind successful');
		}
		
	}
	
}
