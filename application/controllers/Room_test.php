<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require (APPPATH . 'libraries/REST_Controller.php');
class Room_test extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model('service_model' );
		$this->load->model('client_model' );
		$this->load->model('room_model' );
		$this->load->model('event_model' );
		$this->load->model('user_model' );
		$this->load->model('user_token_model' );
		$this->load->model('contact_model' );
		$this->load->model('floor_model' );
		$this->load->model('room_facility_model' );
		$this->load->model('theme_model' );
		$this->load->model('room_property_model');
		$this->load->model('facility_model' );
		$this->load->model('facility_property_model' );
		
		$this->load->model('room_synchronizer_model') ;
		$this->load->model('building_model') ;
		$this->load->model('location_model') ;
		
		$this->load->model('event_queue_model' );
	}
	
	function reserve_post() {
		try{
			$serviceId = Service_model::SERVICE_RM04 ;
			$response_data = NULL;
			$reqHeaders = http_helper::requestHeaders ( $this );	
			$clientModel = http_helper::getClientModel($this,$reqHeaders) ;
			$reserveFrom = "" ;
			$userModel = null ;
			$clientType = $clientModel->client_type ;
			switch($clientType){
				case "MB" :
	
					$access_token = isset($reqHeaders['access_token'])?$reqHeaders ['access_token']:"";
					if (empty($access_token)) {
						http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
					}
					
					$user_token_model = $this->user_token_model->findByAccessToken($access_token) ;
					if(empty($user_token_model)){
						http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
					}
					
					$userModel = $this->user_model->findByPk($user_token_model->user_id) ;
					if(empty($userModel)){
						http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
					}
					
					$reserveFrom = "mobile" ;
					break ;
						
				case "TB" :
				case "DK" :
				case "DT" :
					$verify_code = isset($reqHeaders['verify_code'])?$reqHeaders['verify_code']:"";
	
					if (empty($verify_code)) {
						http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
					}
					$userModel = $this->user_model->findByVerifyCode($verify_code) ;
					$reserveFrom = "dashboard" ;
					break ;
	
				default :
					http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
					break ;
			}
			
			if(empty($userModel)){
				http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
			}
	
			$roomId = $this->post('room_id') ;
			$startTime =$this->post('start') ;
			$endTime = $this->post('end') ;
			$contacts = $this->post('attendees') ;
			$subject = $this->post('subject') ;
			
			if(empty($roomId) || empty($startTime) || empty($endTime) || empty($subject) ){
				log_message("error", "[Invalid Input!!]roomId=".$roomId ."|startTime=". $startTime ."|endTime=". $endTime ."|subject=". $subject ."|");
				http_helper::response ( $this, http_helper::STATUS_INVALID_INPUT );
			}

			if(crypt_helper::IS_ENCRYPTION){
				$password = user_helper::getPassword($userModel->user_name, $userModel->password, $userModel->auth_key, $userModel->created_at) ;
			}else{
				$password = $userModel->password ;
			}
	
			$contact_model = $this->contact_model->findByPk($userModel->contact_id) ;
			if(empty($contact_model)){
				http_helper::response ( $this, http_helper::STATUS_INTERNAL_SERVER_ERROR );
			}
			
			$body = "" ;
			$tz = new DateTimeZone("Asia/Bangkok");
	
			$start_date = new DateTime($startTime);
			$start_date->setTimeZone($tz) ;
			$start_date_str = $start_date->format('Y-m-d H:i');
	
			$end_date =  new DateTime($endTime);
			$end_date->setTimeZone($tz) ;
			$end_date_str = $end_date->format('Y-m-d H:i');
	
			$allday = false;
			$location ="" ;
	
			$contactList = null ;
			if(!empty($contacts)){
				$contactList = explode(",",$contacts) ;
			}
	
			$attendees = [] ;
			$attendeesCnt = 0 ;
			if(!empty($contactList)){
				foreach ($contactList as $i=>$contact_id ){
					$contact_model = $this->contact_model->findByPk($contact_id) ;
					if(!empty($contact_model)){
						$attendees[$attendeesCnt++] = $contact_model->email ;
					}
				}
			}
			$attendeesStr = implode(",",$attendees) ;

			$data = array(
					'room_id' => $roomId ,
					'event_code' => '' ,
					'start_time' => $start_date_str ,
					'end_time' => $end_date_str ,
					'topic' => $subject ,
					'detail' => '' ,
					'create_time' => date('Y-m-d H:i:s'),
					'create_by' => $userModel->contact_id,
					'status_id' => 2 ,
					'reserve_from' => $reserveFrom ,
					'attendees'=>$attendeesStr
			);
			
			/** ==================== Begin Reserve by Servlet Listener =============================**/
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL,"http://172.17.31.111:8080/micore/reserve");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
	 		$json_response = curl_exec($ch);
	 		
	 		curl_close ($ch);
	 		log_message('error', $json_response);
	 		$json = json_decode($json_response);
	 		log_message('error', print_r($json,true));
	 		$response_code = $json->responseCode;
			$response_status = $json->responseStatus;
			log_message('error', "response_code = $response_code and response_status = $response_status");
			http_helper::response ( $this, $response_code);
			
			/** ==================== End Reserve by Servlet Listener =============================**/
			
// 			if($response_code==0&&$response_status=='Success'){
// // 				$eventInserted = $this->event_queue_model->insert( $data ) ;
// // 				if(empty($eventInserted->event_id)){
// // 					http_helper::response ( $this, http_helper::STATUS_INTERNAL_SERVER_ERROR );
// // 				}
// 				http_helper::response ( $this, http_helper::STATUS_SUCCESS );
// 			}else{
				
				
// 				http_helper::response ( $this, http_helper::STATUS_ROOM_NOT_AVAILABLE);
// 			}
	 		
	 		
	 		/**========================================= End Reserve to DB =============================================**/
	 		
		}catch (Exception $e){
			log_message("error","Exception Error".json_encode($e)) ;
		}
	}
	
	//Code written by purpledesign.in Jan 2014
	function dateDiff($formdate,$todat){
		$d_one = new DateTime($formdate);
		$d_two = new DateTime($todat);
		
		//get the difference object
		$d_diff = $d_one->diff($d_two);
		
		$h_diff = $d_diff->format('%h') ;
		if($h_diff < 1) {
			return $d_diff->format('%i mins') ;
		}else{
			$m_diff = $d_diff->format('%i') ;
			if($m_diff > 0){
				return $d_diff->format('%h hrs %i mins') ;
			}else{
				return $d_diff->format('%h hrs') ;
			}
		}
		
		//dump it
// 		var_dump($d_diff);
		
		//use the format string
// 		var_dump($d_diff->format('%H hour %i minute %s second %d day %m month %Y year'));
		
	}
	 
}
