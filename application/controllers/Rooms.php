<?php

defined('BASEPATH') or exit('No direct script access allowed');

require (APPPATH . 'libraries/REST_Controller.php');

class Rooms extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('room_model');
        $this->load->model('floor_model');
    }

    function index_get( $company_id=1 ) {

        $rooms = array();
        $room_model_list = $this->room_model->findByCompanyId($company_id);
        if (!empty($room_model_list)) {
            foreach ($room_model_list as $room_model) {
                
                
                $floor_name = "";
                $floor_id = $room_model->floor_id;
                if (!empty($floor_id)) {
                    $floor_model = $this->floor_model->findByPk($floor_id);
                    if (!empty($floor_model)) {
                        $floor_name = $floor_model->floor_name;
                    }
                }

                $room ['floor'] = $floor_name;
            
                $rooms[] = array(
                    'room_id' => intval($room_model->room_id),
                    'room_name' => $room_model->room_name,
                    'room_desc' => $room_model->description,
                    'floor_id' => $room_model->floor_id
                );
             }
        }
        http_helper::response($this, http_helper::STATUS_SUCCESS, $rooms);
    }

}
