<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

class Bootstrap extends REST_Controller {

	function __construct() {
		// Construct the parent class
		parent::__construct ();

		$this->load->model ( 'client_model' );
		$this->load->model ( 'device_model' );
		$this->load->model ( 'room_model' );
		$this->load->model ( 'floor_model' );
		$this->load->model ( 'room_property_model' );
		$this->load->model ( 'facility_model' );
		$this->load->model ( 'room_facility_model' );
		$this->load->model ( 'facility_property_model' ) ;
		$this->load->model ( 'theme_model' ) ;
		$this->load->model ( 'contact_model' ) ;
		$this->load->model ( 'room_type_model' );
		$this->load->model ( 'building_model' ) ;
		$this->load->model ( 'location_model' ) ;


		// Self Reset Password
		$this->load->model('company_model');
		$this->load->model('message_model');
		$this->load->model('country_model');
		
		$this->load->model('hashtag_model') ;
		
		$this->load->model ( 'config_model' );
	}

	public function index_get(){

		log_message("debug", "Bootstrap was called!!!!!!!!") ;

		$reqHeaders = http_helper::requestHeaders ( $this );

		$client_id = $reqHeaders['client_id'];
		$lat = $reqHeaders['lat'];
		$long = $reqHeaders['long'];
		$app_id = $reqHeaders['app_id'];
		$type = $reqHeaders['client_type'];
		$os = $reqHeaders['os'];
		$os_version = $reqHeaders['os_version'];
		$app_version = $reqHeaders['app_version'];

		$clientModel = $this->client_model->findByUuid($client_id, $lat, $long, $app_id, $type,$os, $os_version, $app_version);
		if(empty($clientModel)){
			http_helper::response($this,http_helper::STATUS_NOT_AUTHORIZED) ;
		}

		$client_type = $clientModel->client_type ;
		$data = array();

		if(!empty($os)&&($os=='IOS'||$os=='ANDROID')){
			$data['ios_version'] = $this->config->item('ios_version');
			$data['android_version'] = $this->config->item('android_version');
			$data['downloadUrl'] = $this->config->item('downloadUrl');
		}

		switch($client_type){

			case "DK" :
			case "DT" :
				$device_model = $this->device_model->findByUuid($clientModel->uuid) ;
// 				echo var_dump( $device_model); exit;
				if(empty($device_model)){
					http_helper::response($this,http_helper::STATUS_INTERNAL_SERVER_ERROR) ;
				}

				if( $device_model->is_registered != "1"){
					http_helper::response($this,http_helper::STATUS_DEVICE_NOT_REGISTER) ;
				}

				$dashboard = array(
						'hour_prev'=>2,
						'hour_next'=>5,
						"content_per_page"=>10,
						"you_are_here"=>$device_model->position

				) ;

				$floor_id = $device_model->floor_id ;
				if(empty($floor_id)){
					http_helper::response($this,http_helper::STATUS_DEVICE_NOT_REGISTER) ;
				}

				$floor_model = $this->floor_model->findByPk($floor_id) ;
				if(empty($floor_model)){
					http_helper::response($this, http_helper::STATUS_DATA_NOT_FOUND) ;
				}

				$building_model = $this->building_model->findByPk($floor_model->building_id) ;
				if(empty($building_model)){
					http_helper::response($this, http_helper::STATUS_DATA_NOT_FOUND) ;
				}

				$location_model = $this->location_model->findByPk($building_model->location_id) ;
				if(empty($location_model)){
					http_helper::response($this, http_helper::STATUS_DATA_NOT_FOUND) ;
				}

				$dashboard['building_id'] =  intval($building_model->building_id) ;
				$dashboard['building_name'] = $building_model->building_name ;

				$dashboard['location_id'] =  intval($location_model->location_id) ;
				$dashboard['location_name'] = $location_model->location_name ;

				$dashboard['floor_id'] =  intval($floor_model->floor_id) ;
				$dashboard['floor_name'] = $floor_model->floor_name ;

				$data['dashboard'] = $dashboard;


				break ;
			case "TB" :
				$device_model = $this->device_model->findByUuid($clientModel->uuid) ;
				if(empty($device_model)){
					http_helper::response($this,http_helper::STATUS_INTERNAL_SERVER_ERROR) ;
				}

				if(!$device_model->is_registered){
					http_helper::response($this,http_helper::STATUS_DEVICE_NOT_REGISTER) ;
				}

				if(empty($device_model->room_id)){
					http_helper::response($this,http_helper::STATUS_DEVICE_NOT_REGISTER) ;
				}
				$room_id = $device_model->room_id ;
				$room_model = $this->room_model->findByPk($room_id) ;
				if(empty($room_model)){
					http_helper::response($this,http_helper::STATUS_INTERNAL_SERVER_ERROR) ;
				}

				$floor_name = "" ;
				$floor_model = $this->floor_model->findByPk($room_model->floor_id) ;
				if(!empty($floor_model)){
					$floor_name = $floor_model->floor_name ;
				}

				$video = "" ;
				$room_property_model = $this->room_property_model->findByPk($room_id , "vdo" ) ;
				if(!empty($room_property_model)){
					$video = $room_property_model->property_value;
				}

				$room = array(
						"id"=> intval($room_id) ,
						"name"=>$room_model->room_name ,
						"description"=>$room_model->description ,
						"type"=>intval($room_model->room_type_id) ,
						"floor" => $floor_name,
						"vdo"=>base_url()."data/contents/s1_01_vdo.mp4"
				) ;

				$facilities = [] ;
				$facility_list = $this->room_facility_model->findAllByRoomId($room_id) ;
				if(!empty($facility_list)) {

					for($i = 0 ; $i < count($facility_list) ; $i++ ){
						$facility = $facility_list[$i] ;

						$facility_id = $facility->facility_id ;
						$facility_model = $this->facility_model->findByPk($facility_id) ;

						$image = "" ;

						$facility_property_model = $this->facility_property_model->findByPk($facility_id,"image") ;
						if(!empty($facility_property_model)){

							$image = base_url() . "data/facility/" . $facility_property_model->property_value ;
						}

						$showType = "" ;
						$facility_property_model = $this->facility_property_model->findByPk($facility_id,"showtype") ;
						if(!empty($facility_property_model)){
							$showType = $facility_property_model->property_value ;
						}
						
						

						$facilities[$i]['facility'] = $facility_model->facility_name ;
						$facilities[$i]['qty'] = intval($facility->quantity) ;

						$facilities[$i]['image'] = $image ;
						$facilities[$i]['show_type'] = $showType ;
						
						
						$icon = "" ;
						$facility_property_model = $this->facility_property_model->findByPk($facility_id,"image-mobile") ;
						if(!empty($facility_property_model)){
							$icon = base_url() . "data/facility/" .$facility_property_model->property_value ;
						}
						$facilities[$i]['icon'] = $icon ;
					}
				}

				$room['facilities'] = $facilities ;


				// Contact contact_id
				$contact = array() ;

				$contactId = $room_model->contact_id ;
				if(!empty($contactId)){
					$contact_model = $this->contact_model->findByPk($contactId) ;
					if(!empty($contact_model)){

						$contact['name'] = $contact_model->display_name  ;
						$contact['email'] = $contact_model->email ;
						$contact['phone'] = $contact_model->telephone ;

					}
				}
				$room['contact'] = $contact ;

				// Theme
				$theme = array() ;
				$theme_model = $this->theme_model->findByRoomId($room_id) ;
				log_message("debug","Room:".$room_id." , Theme:". json_encode($theme_model)) ;
				if(!empty($theme_model)){
					$theme['theme_id'] = intval($theme_model->theme_id) ;
					$theme['theme_name'] = $theme_model->name ;
					$theme['org_bg'] = base_url("data/theme/" .$theme_model->theme_id."/".$theme_model->bg_image) ;
					$theme['bg'] 	= base_url("data/theme/" .$theme_model->theme_id."/".$theme_model->bg_convert) ;
					$theme['color1'] = $theme_model->color_1 ;
					$theme['color2'] = $theme_model->color_2 ;
					$theme['color3'] = $theme_model->color_3 ;
				}
				$room['theme'] = $theme ;

				// Layout
				$layout = "" ;
				$room_property_model = $this->room_property_model->findByPk($room_id,"layout_image") ;
				if(!empty($room_property_model)){
					$layout =  base_url() . "data/room/layout/" . $room_property_model->property_value ;
				}
				$room['layout'] = $layout ;

				//$floor_id = $room_model->floor_id ;

				// ** floor
				//$floor_model = $this->floor_model->findByPk($floor_id) ;
				//if(empty($floor_model)){
				//	http_helper::response($this, http_helper::STATUS_DATA_NOT_FOUND) ;
				//}

				$building_model = $this->building_model->findByPk($floor_model->building_id) ;
				if(empty($building_model)){
					http_helper::response($this, http_helper::STATUS_DATA_NOT_FOUND) ;
				}

				$location_model = $this->location_model->findByPk($building_model->location_id) ;
				if(empty($location_model)){
					http_helper::response($this, http_helper::STATUS_DATA_NOT_FOUND) ;
				}
				$tablet = array() ;

				$room['building_id'] =  intval($building_model->building_id) ;
				$room['building_name'] = $building_model->building_name ;

				$room['location_id'] =  intval($location_model->location_id) ;
				$room['location_name'] = $location_model->location_name ;

				$room['floor_id'] =  intval($floor_model->floor_id) ;
				$room['floor_name'] = $floor_model->floor_name ;



				$tablet['room'] = $room ;

				$data['tablet'] = $tablet;
				break;

			case "MB" :


				break;

			case "WB" :
				$data['client_id'] = $clientModel->uuid ;


				break;

			default:

				http_helper::response($this, http_helper::STATUS_NOT_AUTHORIZED ) ;
				break ;
		}


		// List Facilities
		$facilities = [] ;
		$facility_list = $this->facility_model->findAllForSearch() ;

//                var_dump($facility_list); exit;
		log_message('debug',json_encode($facility_list)) ;

		if(!empty($facility_list)){

			foreach ($facility_list as $j=>$facility){

				$facility_id = $facility->facility_id ;

				$facility_model = $this->facility_model->findByPk($facility_id) ;

				$image = "" ;

				$facility_property_model = $this->facility_property_model->findByPk($facility_id,"image") ;
				if(!empty($facility_property_model)){

					$image = base_url() . "data/facility/" . $facility_property_model->property_value ;
				}

				$showType = "" ;
				$facility_property_model = $this->facility_property_model->findByPk($facility_id,"showtype") ;
				if(!empty($facility_property_model)){
					$showType = $facility_property_model->property_value ;
				}
				$facilities[$j]['facility_id'] = intval($facility_id) ;
				$facilities[$j]['facility'] = $facility_model->facility_name ;
				$facilities[$j]['image'] = $image ;
				$facilities[$j]['show_type'] = $showType ;
				
				$icon = "" ;
				$facility_property_model = $this->facility_property_model->findByPk($facility_id,"image-mobile") ;
				if(!empty($facility_property_model)){
					$icon =  base_url() . "data/facility/" .$facility_property_model->property_value ;
				}
				$facilities[$j]['icon'] = $icon ;

			}


		}
		$data['facilities'] = $facilities ;


		// List Facilities
		$room_types = [] ;
		$room_type_list = $this->room_type_model->findAll() ;
		if(!empty($room_type_list)){
			foreach($room_type_list as $i=>$room_type){
				$room_types[$i]['room_type_id'] =  intval($room_type->room_type_id) ;
				$room_types[$i]['room_type_name'] =  $room_type->type_name ;
			}
		}
		$data['room_types'] = $room_types ;


		$localtions = [] ;
		$localtion_list = $this->location_model->findAll() ;
		if(!empty($localtion_list)){
			foreach($localtion_list as $i=>$localtion){
				$localtions[$i]['location_id'] =  intval($localtion->location_id) ;
				$localtions[$i]['location_name'] =  $localtion->location_name ;
				$localtions[$i]['location_desc'] =  !empty($localtion->description)?trim($localtion->description):"" ;
			}
		}
		$data['locations'] = $localtions ;

		$buildings = [] ;
		$building_list = $this->building_model->findAll();
		if(!empty($building_list)){
			foreach($building_list as $i=>$building){
				$buildings[$i]['building_id'] =  intval($building->building_id) ;
				$buildings[$i]['building_name'] =  $building->building_name ;
				$buildings[$i]['building_desc'] = !empty($building->description)?trim($building->description):"" ;
				$buildings[$i]['location_id'] =  intval($building->location_id) ;
			}
		}
		$data['buildings'] = $buildings ;

		$floors = [] ;
		$floor_list = $this->floor_model->findAll();
		if(!empty($floor_list)){
			foreach($floor_list as $i=>$floor){
				$floors[$i]['floor_id'] =  intval($floor->floor_id) ;
				$floors[$i]['floor_name'] =  $floor->floor_name ;
				$floors[$i]['floor_desc'] =  !empty($floor->description)?trim($floor->description):"" ;
				$floors[$i]['building_id'] =  intval($floor->building_id) ;
			}
		}
		$data['floors'] = $floors ;

		$companies = array() ;
		$company_model_list = $this->company_model->findAll() ;
		// log_message('debug', print_r($company_model_list));
		foreach ($company_model_list as $i => $company_model) {
			$companies[] = array(
				'company_id' => intval($company_model->company_id),
				'company_name' => $company_model->name ,
				'domain' => $company_model->domain_name
			);
		}
		$data['companies'] = $companies ;


		$messages = array() ;
		$th_messages = array() ;
		$en_messages = array() ;

		$message_model_list = $this->message_model->findAll();
		// log_message('debug', print_r($message_model_list));
		if(!empty($message_model_list)){
			foreach ($message_model_list as $message_model) {


				if($message_model->language === 'en'){
					$en_messages[] = array(
						'key'=>$message_model->key,
						'message'=>$message_model->message
					);
				}elseif($message_model->language === 'th'){
					$th_messages[] = array(
						'key'=>$message_model->key,
						'message'=>$message_model->message
					);
				}

			}

			$messages[] = array(
				'en'=>$en_messages,
				'th'=>$th_messages
			);
		}
		$data['messages'] = $messages ;

		// Country List
		$countries = array() ;
		$country_model_list = $this->country_model->findAll() ;
		// log_message('debug', print_r($company_model_list));
		foreach ($country_model_list as $i => $country_model) {
			$countries[] = array(
				'country_id' => intval($country_model->country_id),
				'country_name' => $country_model->name ,
				'short_name' => $country_model->short_name,
				'country_code' => $country_model->code
			);
		}
		$data['countries'] = $countries ;
		
		
		/**
		 * #Hashtag
		 */
		// #seatlayouts
		$seat_layouts = array() ;
		$seat_layout_list = $this->hashtag_model->findAllByKeyword('seat_layout') ;
		if(!empty($seat_layout_list)){
			foreach($seat_layout_list as $seat_layout) {
				$seat_layouts[] = json_decode($seat_layout->content , true) ;
			}
		}
		$data['seat_layouts'] = $seat_layouts ;
		
		
		/** limitations **/
		$max_reserve_minutes = 28800 ;
		try{
			$max_reserve_minutes = intval($this->config_model->findByKey("limitation.reserve.max_minutes")->value) ;
		}catch (Exception $e){}
		
		$data['limitations'] = array(
				'max_reserve' => $max_reserve_minutes
		);

		$data['event_config'] = array(
			'upcoming_minutes'=>intval($this->config_model->findByKey("event.config.upcoming_minutes")->value),
			'release_status'=>$this->config_model->findByKey("event.config.release_status")->value,
			'release_minutes'=>intval($this->config_model->findByKey("event.config.release_minutes")->value),
			'confirm_minutes'=>intval($this->config_model->findByKey("event.config.confirm_minutes")->value)
		);
		
		http_helper::response($this, http_helper::STATUS_SUCCESS ,$data) ;

	}
}
