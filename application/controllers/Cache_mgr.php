<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cache_mgr extends CI_Controller {
	
	public function index(){

		try{

				/*$memcached = new Memcached();
			$memcached->addServer('127.0.0.1',11211); // edit here if your memcached server differs from localhost
				
				
// 				echo json_encode($list) ;
				
				if (isset($_GET['del'])) {
					$memcached->delete($_GET['del']);
// 					header("Location: " . $_SERVER['PHP_SELF']);
				}
				if (isset($_GET['flush'])) {
					$memcached->flush();
// 					header("Location: " . $_SERVER['PHP_SELF']);
				}
				if (isset($_GET['set'])) {
					$memcached->set($_GET['set'], $_GET['value']);
// 					header("Location: " . $_SERVER['PHP_SELF']);
				}
				
				$list = array();
				$allSlabs = $memcached->getExtendedStats('slabs');
				$items = $memcached->getExtendedStats('items');
				foreach($allSlabs as $server => $slabs) {
					foreach($slabs AS $slabId => $slabMeta) {
						$cdump = $memcached->getExtendedStats('cachedump',(int)$slabId);
						foreach($cdump AS $server => $entries) {
							if($entries) {
								foreach($entries AS $eName => $eData) {
									$list[$eName] = array(
											'key' => $eName,
											'value' => $memcached->get($eName)
									);
								}
							}
						}
					}
				}
				ksort($list);
				*/

			$memcache = new Memcached();
			$memcache->connect('127.0.0.1', 11211) or die ("Could not connect");
			$list = array();
			$allSlabs = $memcache->getExtendedStats('slabs');
			$items = $memcache->getExtendedStats('items');
			foreach($allSlabs as $server => $slabs) {
				foreach($slabs AS $slabId => $slabMeta) {
					if (!is_int($slabId)) {
						continue;
					}
					$cdump = $memcache->getExtendedStats('cachedump', (int) $slabId, 100000000);
					foreach($cdump AS $server => $entries) {
						if ($entries) {
							foreach($entries AS $eName => $eData) {
								print_r($eName);
								print_r(":");
								$val = $memcache->get($eName);
								print_r($val);
								print_r("\n");
							}
						}
					}
				}
			}

			$this->load->view("cache",array("list"=>$list) ) ;

		}catch(Exception $e){
			echo "Exception Error:".json_encode($e) ;
		}
	}
}