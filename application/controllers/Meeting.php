<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require (APPPATH . 'libraries/REST_Controller.php');
class Meeting extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model ( 'event_model' );
		$this->load->model ( 'client_model' ) ;
	
	}
	
	function checkin_post($meeting_id=null) {
		$checkin_type = $this->input->post('checkin_type');
		$checkin_time = $this->input->post('checkin_time');
		$sign = $this->input->post('sign');

		$checkSign = hash_hmac('sha256', $meeting_id.$checkin_time, $this->config->item('signkey'));
		if($checkSign!=$sign)http_helper::response($this,400, array('detail'=>'Sign not match')) ;
		
		$confirm_minutes = 0;
		$this->load->model('config_model');
		try {
			$config_confirm_minutes = $this->config_model->findByKey("event.config.confirm_minutes")->value;
			if($config_confirm_minutes>0) $confirm_minutes = $config_confirm_minutes;
		} catch (Exception $e) {
		
		}
		
		$this->db->where('event_id', $meeting_id);
		$this->db->update('event', array('checkindatetime'=>$checkin_time,'is_checkin'=>1,'checkin_type'=>$checkin_type, 'confirm_minutes'=>$confirm_minutes));
		
		log_message('error','checkin_post '.$meeting_id);
		http_helper::response($this,http_helper::STATUS_SUCCESS) ;
	}

	function checkout_post($meeting_id=null) {
		//$meeting_id = $this->input->post('meeting_id');
		$checkout_type = $this->input->post('checkout_type');
		$checkout_time = $this->input->post('checkout_time');
		$sign = $this->input->post('sign');

		$checkSign = hash_hmac('sha256', $meeting_id.$checkout_time, $this->config->item('signkey'));
		if($checkSign!=$sign)http_helper::response($this,400, array('detail'=>'Sign not match')) ;

		$this->db->where('event_id', $meeting_id);
		$this->db->update('event', array('checkoutdatetime'=>$checkout_time,'checkout_type'=>$checkout_type));
		
		$query = $this->db->query("select end_time > checkoutdatetime as isLogoutBeforeTime from event where event_id = ".$meeting_id);

		$detail = array('status'=>"Success",'MessageText'=>'');
		$row = $query->row_array();
		if($row['isLogoutBeforeTime']==1){
			$detail = $this->event_logout($meeting_id,$checkout_time);
			if($detail['status']=='Success'){
				$this->db->where('event_id', $meeting_id);
				$this->db->update('event', array('end_time'=>$checkout_time));
			}
		}

		http_helper::response($this,http_helper::STATUS_SUCCESS, array('detail'=>$detail)) ;
	}

	function release_post($meeting_id=null) {
		$checkout_type = $this->input->post('release_type');
		$checkout_time = $this->input->post('release_time');
		$sign = $this->input->post('sign');

		$checkSign = hash_hmac('sha256', $meeting_id.$checkout_time, $this->config->item('signkey'));
		if($checkSign!=$sign)http_helper::response($this,400, array('detail'=>'Sign not match')) ;

		$this->db->where('event_id', $meeting_id);
		$this->db->update('event', array('releasedatetime'=>$checkout_time,'release_type'=>$checkout_type));

		$query = $this->db->query("select end_time > releasedatetime as isLogoutBeforeTime from event where event_id = ".$meeting_id);
		
		$detail = array('status'=>"Success",'MessageText'=>'');
		$row = $query->row_array();
		if($row['isLogoutBeforeTime']==1){
			$detail = $this->event_logout($meeting_id,$checkout_time);
			if($detail['status']=='Success'){
				$this->db->where('event_id', $meeting_id);
				$this->db->update('event', array('end_time'=>$checkout_time));
			}
		}

		http_helper::response($this,http_helper::STATUS_SUCCESS, array('detail'=>$detail)) ;
	}
	
	function confirm_motion_post($meeting_id=null) {
		$confirm_type = $this->input->post('confirm_type');
		$confirm_time = $this->input->post('confirm_time');
		$detected_motion = $this->input->post('detected_motion');
		$sign = $this->input->post('sign');
	
		$checkSign = hash_hmac('sha256', $meeting_id.$confirm_time, $this->config->item('signkey'));
		if($checkSign!=$sign)http_helper::response($this,400, array('detail'=>'Sign not match'));
	
		$this->db->where('event_id', $meeting_id);
		$this->db->update('event', array('confirm_type'=>$confirm_type,'confirm_time'=>$confirm_time,'detected_motion'=>$detected_motion));
	
		http_helper::response($this,http_helper::STATUS_SUCCESS) ;
	}
		
	function event_logout($event_id,$end){
		$event = $this->event_model->findByPk($event_id);
		if(empty($event)) return array('status'=>"Error","MessageText"=>'Not found Event');
		$this->config->load('ews.php', TRUE);
		$host = $this->config->item('server', 'ews');
		$admin = $this->config->item('adminuser', 'ews');
		$pass = $this->config->item('adminpassword', 'ews');

		$this->load->library('ews');
		$credential = $this->ews->create_ews_instants($host, $admin, $pass);
		if (empty($credential)) {
			http_helper::response($this, http_helper::STATUS_EWS_ERROR);
		}
		$tz = new DateTimeZone("Asia/Bangkok");

		$end_date = new DateTime($end);
		$end_date->setTimeZone($tz);
		$calId = $event->event_code;
		$xx = $this->ews->calendar_logout($credential, $calId, $end_date);
		$status = $xx->ResponseMessages->UpdateItemResponseMessage->ResponseClass;
		if($status!="Success"){
			return array('status'=>$status,'MessageText'=>$xx->ResponseMessages->UpdateItemResponseMessage->MessageText);
		}
		return array('status'=>$status,'MessageText'=>'');
	}

	function test_get() {
		$this->config->load('ews.php', TRUE);
		$host = $this->config->item('server', 'ews');
		$admin = $this->config->item('adminuser', 'ews');
		$pass = $this->config->item('adminpassword', 'ews');

		$this->config->load('ews');
		$adminuser = $this->config->item('adminuser');
		$adminpassword = $this->config->item('adminpassword');
		$this->load->library('ews');
		$credential = $this->ews->create_ews_instants($host, $admin, $pass);
		if (empty($credential)) {
			http_helper::response($this, http_helper::STATUS_EWS_ERROR);
		}
		$end = '2017-01-17 16:45';
		$tz = new DateTimeZone("Asia/Bangkok");

		$end_date = new DateTime($end);
		$end_date->setTimeZone($tz);
		$calId = "AAMkADUxYjc2NTFhLTRlNzktNDViZS04NmFiLTNjZWYzYzFiNWFlYQBGAAAAAAD6PunDwxLyT57rXEGgEPghBwCy9AFU7V3kRr3vLy8o2fiYAAAAAAEOAACy9AFU7V3kRr3vLy8o2fiYAAEv8WKqAAA=";
		$xx = $this->ews->calendar_logout($credential, $calId, $end_date);
		$status = $xx->ResponseMessages->UpdateItemResponseMessage->ResponseClass;
//{"response_code":0,"response_status":"Success","response_title":"Success","response_message":"Success","response_data":{"host":"mi-exchange.midemo.local","admin":"midemo\\roomadmin","res":{"ResponseMessages":{"UpdateItemResponseMessage":{"ResponseCode":"NoError","ResponseClass":"Success","Items":{"CalendarItem":{"ItemId":{"Id":"AAMkADUxYjc2NTFhLTRlNzktNDViZS04NmFiLTNjZWYzYzFiNWFlYQBGAAAAAAD6PunDwxLyT57rXEGgEPghBwCy9AFU7V3kRr3vLy8o2fiYAAAAAAEOAACy9AFU7V3kRr3vLy8o2fiYAAEv8WKqAAA=","ChangeKey":"DwAAABYAAACy9AFU7V3kRr3vLy8o2fiYAAEv8dtz"}}},"ConflictResults":{"Count":0}}}}}}
//{"response_code":0,"response_status":"Success","response_title":"Success","response_message":"Success","response_data":{"host":"mi-exchange.midemo.local","admin":"midemo\\roomadmin","res":{"ResponseMessages":{"UpdateItemResponseMessage":{"MessageText":"EndDate is earlier than StartDate","ResponseCode":"ErrorCalendarEndDateIsEarlierThanStartDate","DescriptiveLinkKey":0,"ResponseClass":"Error","Items":{}}}}}}
		http_helper::response($this,http_helper::STATUS_SUCCESS, array('host'=>$host,'admin'=>$admin,'res'=>$xx)) ;
	}


}