<?php
require(APPPATH.'libraries/REST_Controller.php');

class Locations extends REST_Controller {

	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model ( 'client_model' ) ;
	
	}
	public function index_get(){
		
		$reqHeaders = http_helper::requestHeaders ($this);
		$clientModel = http_helper::getClientModel($this,$reqHeaders) ;
		
		$response_code = 0;
		$response_status = 'Success';
		$response_message = 'Success';
		
		$locations = null;
		$locationslist = $this->db->get('location')->result_array();
		if(!empty($locationslist)){
			$locations = array();
			foreach($locationslist as $location){
				array_push($locations,array('location_id'=>$location['location_id'],
					'location_name'=>$location['location_name'],'description'=>$location['description'])
				);
			}
		}
		$response_data = array('locations'=>$locations);
		$data = array('response_code'=>$response_code,'response_status'=>$response_status,'response_message'=>$response_message,'response_data'=>$response_data);
        $this->response($data);
	}
}
