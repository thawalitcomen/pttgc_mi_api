<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require (APPPATH . 'libraries/REST_Controller.php');
class Password extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
	}
	function index_get() {
		$this->load->view ( 'password' );
	}
	function index_post() {
		$password = $this->input->post ( 'password' );
		
		$salt = user_helper::generateSalt ();
		$pass = user_helper::setPassword ( $password, $salt );
		
		$data = array (
				'password' => $pass,
				'salt' => $salt 
		);
		$this->load->view ( 'password', $data );
	}
	
	function validate_get() {
		$this->load->view ( 'validate' );
	}
	function validate_post() {
		$password = $this->input->post ( 'password' );
		
		
		if (strlen ( trim ( $password ) ) < 8) {
			$result = 1;
			
			$data = array (
					'valid' => $result
			);
			return $this->load->view ( 'validate', $data );
		}
		
		if (! preg_match( '/\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*/', $password )) {
			$result = 2;
			
			$data = array (
					'valid' => $result
			);
			return $this->load->view ( 'validate', $data );
		}
		
		if (strpos ( strtolower ( $password ), strtolower ( $user_name ) ) !== FALSE) {
			$result = 3;
			
			$data = array (
					'valid' => $result
			);
			return $this->load->view ( 'validate', $data );
		}
		
		$data = array (
				'valid' => 0
		);
		return $this->load->view ( 'validate', $data );
		
	}
}
