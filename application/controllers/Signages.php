<?php
require(APPPATH.'libraries/REST_Controller.php');

class Signages extends REST_Controller {
	function __construct() {
            // Construct the parent class
            parent::__construct ();

            $this->load->model ( 'client_model' );

            $this->load->model ( 'signage_model' );
            $this->load->model ( 'signage_content_model' );
            $this->load->model ( 'device_model' );
            $this->load->model ( 'user_model' );
	}
	/**
	 * Index Page for this controller method GET.
	 */
	public function index_temp_get(){
// 		$lat = $this->input->get_request_header('lat');
// 		$long = $this->input->get_request_header('long');
// 		$client_id = $this->input->get_request_header('client_id');
// 		$client_type = $this->input->get_request_header('client_type');
// 		$version = $this->input->get_request_header('version');
// 		$app_id = $this->input->get_request_header('app_id');
// 		$app_version = $this->input->get_request_header('app_version');
		
// 		$response_code = 0;
// 		$response_status = 'Success';
// 		$response_message = 'Success';

		$contents = [] ;
		$contents[0] = array(
						"content"=>base_url(). 'data/contents/s1_01_vdo.mp4',
						'type'=>'video'
						);
		$contents[1] =  array(
						"content"=>base_url(). 'data/contents/s1_01_img.jpg',
						'type'=>'image'
						);
		$contents[2] =  array(
						"content"=>base_url(). 'data/contents/s1_02_img.jpg',
						'type'=>'image'
						);
		$contents[3] =  array(
						"content"=>base_url(). 'data/contents/s1_03_img.jpg',
						'type'=>'image'
						);
		$contents[4] =  array(
						"content"=>base_url(). 'data/contents/s1_04_img.jpg',
						'type'=>'image'
						);
		$contents[5] =  array(
						"content"=>base_url(). 'data/contents/s1_05_img.jpg',
						'type'=>'image'
						);
		
		$signages = [array(
				'id'=>13,
				'create_time'=>date("Y-m-d H:i:s"),
				'create_by'=>'Administrator',
				'title'=>'Lerem Imsum title',
				'contents'=>$contents )] ;

		$reqHeaders = http_helper::requestHeaders ($this);
		$clientModel = http_helper::getClientModel($this,$reqHeaders) ;
		

		$response_data = array('signages'=>$signages);
// 		$data = array('response_code'=>$response_code,'response_status'=>$response_status,'response_message'=>$response_message,'response_data'=>$response_data);
//         $this->response($data);
        
		http_helper::response($this, http_helper::STATUS_SUCCESS ,$response_data) ;
	}
        
        public function index_get(){

            $signages = array() ;
            $contents = array() ;
            
            $reqHeaders = http_helper::requestHeaders ($this);
            $clientModel = http_helper::getClientModel($this,$reqHeaders) ;

            $device_model = $this->device_model->findByUuid($clientModel->uuid) ;
            if(!empty($device_model)){
                
                $signage_model = $this->signage_model->findByDeviceId($device_model->device_id) ;
                if(!empty($signage_model)){
                    $signage_contents = $this->signage_content_model->findAllBySignageId($signage_model->signage_id) ;
                    if(!empty($signage_contents)){
                    foreach ($signage_contents as $signage_content){
                            
                            switch($signage_content->type) {
                                case 'link' :
                                    $content = $signage_content->content ;
                                    
                                    break;
                                default :
                                    $content = base_url("data/".$signage_content->content) ;
                            }
                            $contents[] = array(
                                "content"=>$content,
                                "type"=>$signage_content->type
                            );
                        }
                    }
                    
                    $creator = "" ;
                    if(!empty($signage_model->created_by)){
                        $user_model = $this->user_model->findByPk($signage_model->created_by) ;
                        if(!empty($user_model)){
                            $creator = $user_model->user_name  ;
                        }
                    }
                    
                    $signages[] = array(
				'id'=>$signage_model->signage_id,
				'create_time'=>date("Y-m-d H:i:s",strtotime($signage_model->created_at)),
				'create_by'=>$creator,
				'title'=>$signage_model->title,
				'contents'=>$contents ) ;
                }
               
            }

            $response_data = array('signages'=>$signages);
            http_helper::response($this, http_helper::STATUS_SUCCESS ,$response_data) ;
	}
}
