<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require (APPPATH . 'libraries/REST_Controller.php');
class EWSRoom extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model('service_model' );
		$this->load->model('client_model' );
		$this->load->model('room_model' );
		$this->load->model('event_model' );
		$this->load->model('user_model' );
		$this->load->model('user_token_model' );
		$this->load->model('contact_model' );
		$this->load->model('floor_model' );
		$this->load->model('room_facility_model' );
		$this->load->model('theme_model' );
		$this->load->model('room_property_model');
		$this->load->model('facility_model' );
		$this->load->model('facility_property_model' );
		
		$this->load->model('room_synchronizer_model') ;
		$this->load->model('building_model') ;
		$this->load->model('location_model') ;
		
		$this->load->model('event_queue_model' );
	}
	
	function schedules_get($roomId=null){

		if(is_null($roomId)){
			
		}
		
		$reqHeaders = http_helper::requestHeaders ( $this );
		$clientModel = http_helper::getClientModel($this,$reqHeaders) ;
		
		
		$start_now = $this->input->get('start_now') ;
		if(is_null($start_now)||  empty($start_now) ){
			$start_now = 0 ;
		}
		
		
// 		echo json_encode($clientModel) ; exit;
		$currentTime = date('Y-m-d H:i:s');
		$currentTime = date('Y-m-d H:i:s');
		$startTime = $start_now ? date('Y-m-d H:i:s'):date('Y-m-d H:i:s',  strtotime ( $currentTime . "-2 hours" ) );
		$endTime = date ('Y-m-d H:i:s', strtotime($currentTime . "+5 hours"));
// 		echo $startTime." - ".$endTime ; exit;
		$eventDatas = array() ;
		$events = $this->event_model->findByRoom($roomId,$startTime,$endTime);
// 		echo json_encode($events) ; exit;
		if(!empty($events)){
			foreach ($events as $i=>$event){
		
				$eventDatas[$i]['meeting_id'] = intval($event->event_id);
				$eventDatas[$i]['subject'] = $event->topic;
				$eventDatas[$i]['start'] = $event->start_time;
				$eventDatas[$i]['end'] = $event->end_time;
					
				/*$ownerId = $event->create_by ;
				$ownerUser = $this->user_model->findByPk($ownerId) ;
				if(!empty($ownerUser)){
					$eventDatas[$i]['host'] = $ownerUser->user_name ;
				}else{
					$eventDatas[$i]['host'] = "" ;
				}*/
				
				$ownerContactId = $event->create_by;
// 				echo $ownerContactId ; exit;
				$eventDatas[$i]['host'] = array();
				if(!empty($ownerContactId )){
					$contactModel = $this->contact_model->findByPk($ownerContactId);
					if(!empty($contactModel)){
						$eventDatas[$i]['host'] = array(
								'id'=>$contactModel->contact_id,
								'name'=>$contactModel->display_name,
								'phone'=>$contactModel->telephone,
								'email'=>$contactModel->email,
								'employee_id'=>$contactModel->employee_id,
								
						);
					}			

				}
				
				$eventDatas[$i]['is_checkin'] = intval( $event->is_checkin );
			}
		}
		$data = array ('schedules' => $eventDatas  );
		http_helper::response ( $this, http_helper::STATUS_SUCCESS, $data );
		
	}
	
	function reserve_get() {
		$data = array() ;
		$this->load->view ( 'api/rm02', $data );
	}
	
	
// 	public function reserve2_get(){
	
// 		$this->load->library('ews');
// 		$ews = $this->ews->create_ews_instants('192.168.117.133','thawalit@midemo.local','fcD!1234');
// 		// 		$ews = $this->ews->create_ews_instants('192.168.117.133','Thawalit.Junpoung@midemo.local','fcD!1234');
// 		if(empty($ews)){
// 			return null ;
// 		}
// 		// 		var_dump($ews); exit;
	
// 		$date = new DateTime();
// 		$subject = "Testtt subject ".$date->format('Y-m-d\TH:i:s') ;
// 		$body = "Testtt body".$date->format('Y-m-d\TH:i:s') ;
	
	
// 		$start_date = $date->modify('+5 hours');
// 		$start_date = $date->format('Y-m-d\TH:i:s');
	
// 		// 		$end_date = $date->modify('+2 hours');
// 		$end_date = $date->modify('+15 minutes');
// 		$end_date = $date->format('Y-m-d\TH:i:s');
	
	
// 		$allday = false;
// 		$location ="Test location" ;
// 		$attendees = "pinit|Pinit@midemo.local;ranon|Ranon@midemo.local" ;
// 		$importance = true ;
// 		$sensitivity = false ;
	
// 		$room_resource = "2A-10@midemo.local" ;
	
// 		$reserve_result = $this->ews->calendar_add_item($ews, $subject, $body, $start_date, $end_date, $allday, $location, $attendees, $importance, $sensitivity ,$room_resource);
	
// 		echo json_encode($reserve_result) ;
// 		// 		return $reserve_result;
// 	}
	
	function reserve_post() {
		try{
			$serviceId = Service_model::SERVICE_RM04 ;
			// 		$post_params = $this->input->post();
			// 		log_message("debug", json_encode($post_params)) ;
			$response_data = NULL;
	
			$reqHeaders = http_helper::requestHeaders ( $this );
	
			// 		var_dump($reqHeaders) ; exit;
	
			$clientModel = http_helper::getClientModel($this,$reqHeaders) ;
	
			// 		var_dump($clientModel) ; exit;
	
			$reserveFrom = "" ;
			$userModel = null ;
			$clientType = $clientModel->client_type ;
			switch($clientType){
				case "MB" :
	
					$access_token = isset($reqHeaders['access_token'])?$reqHeaders ['access_token']:"";
					if (empty($access_token)) {
						http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
					}
					// 				echo 'access_koen='.json_encode($access_token) ; exit;
	
					$user_token_model = $this->user_token_model->findByAccessToken($access_token) ;
					if(empty($user_token_model)){
						http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
					}
					// 				echo 'user_token_model='.json_encode($user_token_model) ; exit;
	
					$userModel = $this->user_model->findByPk($user_token_model->user_id) ;
					if(empty($userModel)){
						http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
					}
	
					// 				echo '$userModel='.json_encode($userModel) ; exit;
					$reserveFrom = "mobile" ;
	
	
					break ;
						
				case "TB" :
				case "DK" :
				case "DT" :
					// 				var_dump($reqHeaders['verify_code']) ; exit;
	
					$verify_code = isset($reqHeaders['verify_code'])?$reqHeaders['verify_code']:"";
	
					if (empty($verify_code)) {
						http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
					}
					$userModel = $this->user_model->findByVerifyCode($verify_code) ;
					$reserveFrom = "dashboard" ;
					break ;
	
				default :
	
					http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
	
					break ;
			}
	
			// 		echo json_encode($userModel) ;
			if(empty($userModel)){
				http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
			}
	
			$roomId = $this->post('room_id') ;
			$startTime =$this->post('start') ;
			$endTime = $this->post('end') ;
			$contacts = $this->post('attendees') ;
			$subject = $this->post('subject') ;
	
			// 		echo " $roomId = ". json_encode(empty($roomId)) ;
			// 		echo " $startTime = ". json_encode(empty($startTime) ) ;
			// 		echo " $endTime = ". json_encode(empty($endTime )) ;
			// 		echo " $subject = ". json_encode(empty($subject) ) ;
	
	
			if(empty($roomId) || empty($startTime) || empty($endTime) || empty($subject) ){
				log_message("error", "[Invalid Input!!]roomId=".$roomId ."|startTime=". $startTime ."|endTime=". $endTime ."|subject=". $subject ."|");
				http_helper::response ( $this, http_helper::STATUS_INVALID_INPUT );
			}
	
	/*
			$events = $this->event_model->findByRoom($roomId,$startTime,$endTime) ;
			if(!empty($events)){
				http_helper::response ( $this, http_helper::STATUS_ROOM_NOT_AVAILABLE );
			}
	*/
	
			/** =================================== begin request booking to Exchange ===================================== **/
			if(crypt_helper::IS_ENCRYPTION){
				$password = user_helper::getPassword($userModel->user_name, $userModel->password, $userModel->auth_key, $userModel->created_at) ;
			}else{
				$password = $userModel->password ;
			}
	
			$this->config->load('ews.php', TRUE);
			$ews_config = [
					'host' => $this->config->item('server', 'ews'),
					'admin' => $this->config->item('adminuser', 'ews'),
					'pass' => $this->config->item('adminpassword', 'ews'),
			];
	
			$host = $ews_config['host'] ;
	
	
			$contact_model = $this->contact_model->findByPk($userModel->contact_id) ;
			if(empty($contact_model)){
				http_helper::response ( $this, http_helper::STATUS_INTERNAL_SERVER_ERROR );
			}
	
			$this->load->library('ews');
			$ews = $this->ews->create_ews_instants($host,$contact_model->user_logon,$password , $ews_config);
			log_message("debug","EWS1:". json_encode($ews)) ;
			$ews_admin = $this->ews->create_ews_instants($host,$ews_config['admin'],$ews_config['pass']);
			log_message("debug","EWS ADMIN:". json_encode($ews_admin)) ;
			log_message("debug","EWS2:". json_encode($ews)) ;
			// 		var_dump($ews) ; exit;
			if(empty($ews)){
				log_message("debug","EWS Empty!! by input $host,$contact_model->user_logon,$password") ;
				http_helper::response ( $this, http_helper::STATUS_EWS_ERROR );
			}
	
			$body = "" ;
			$tz = new DateTimeZone("Asia/Bangkok");
	
			$start_date = new DateTime($startTime);
			$start_date->setTimeZone($tz) ;
			$start_date_str = $start_date->format('Y-m-d\TH:i:s');
	
			$end_date =  new DateTime($endTime);
			$end_date->setTimeZone($tz) ;
			$end_date_str = $end_date->format('Y-m-d\TH:i:s');
	
			$allday = false;
			$location ="" ;
	
			$contactList = null ;
			if(!empty($contacts)){
				$contactList = explode(",",$contacts) ;
			}
	
			$attendees = [] ;
			if(!empty($contactList)){
				// 			$attendees = "" ;
				foreach ($contactList as $i=>$contact_id ){
					$contact_model = $this->contact_model->findByPk($contact_id) ;
					if(!empty($contact_model)){
						$attendees .= ( $contact_model->display_name."|".$contact_model->email  . ";"  ) ;
					}
				}
			}
	
			$importance = false ;
			$sensitivity = false ;
	
			// 		echo "roomId $roomId"; exit;
	
			
			/**=========================================  Reserve to EWS =============================================**/
			
			$room_resource = "" ;
			$room_synchronzer_model = $this->room_synchronizer_model->findByRoomId($roomId) ;
			if(!empty($room_synchronzer_model)){
				$room_resource = $room_synchronzer_model->user;
			}
	
			$requestString = "================== RESERVE-REQ =====================\n" .
					"|EWS=".json_encode($ews)."|SUBJECT=".$subject."|BODY=".$body .
					"|START=".$start_date_str .
					"|END=".$end_date_str."|ALLDAY=".json_encode($allday)."|LOCATION=".$location."|ATTENDEES=". json_encode($attendees) .
					"|IMPORTANCE=".json_encode($importance)."|SENSITIVITY=".json_encode($sensitivity)."|RESOURCE=".$room_resource."|" ;
			log_message('debug',$requestString );
	
			$reserve_result = $this->ews->calendar_add_item($ews, $subject, $body, $start_date_str, $end_date_str, $allday, $location, $attendees, $importance, $sensitivity ,$room_resource ,$ews_admin);
	

			log_message('debug', 'RESERVE-RES {"room_id":"'.$roomId.'", "result": '. json_encode($reserve_result));
			if(empty($reserve_result)){
				http_helper::response ( $this, http_helper::STATUS_ROOM_NOT_AVAILABLE );
			}
	
			$ResponseCode = $reserve_result->ResponseCode ;
			$ResponseClass = $reserve_result->ResponseClass ;
	
			if($ResponseCode == "NoError" && $ResponseClass == "Success" ) {
				$event_id = $reserve_result->Items->CalendarItem->ItemId->Id ;
				$changeKey = $reserve_result->Items->CalendarItem->ItemId->ChangeKey ;
					
				log_message("debug","reserve success :" . json_encode($reserve_result)) ;
					
				$itemResult = $this->ews->calendar_get_item($ews,$event_id) ;
				log_message("debug","itemResult :" . json_encode($itemResult)) ;
				http_helper::response ( $this, http_helper::STATUS_SUCCESS  ,$reserve_result);
			}else{
				http_helper::response ( $this, http_helper::STATUS_EWS_ERROR ,$reserve_result );
			}
			
			/** =================================== End  request booking to Exchange ===================================== **/
			
			/**========================================= Begin Reserve to DB =============================================**/
			/*
			$data = array(
					'room_id' => $roomId ,
					'event_code' => '' ,
					'start_time' => $startTime ,
					'end_time' => $endTime ,
					'topic' => $subject ,
					'detail' => '' ,
					'create_time' => date('Y-m-d H:i:s'),
					'create_by' => $userModel->contact_id,
					'status_id' => 2 ,
					'reserve_from' => $reserveFrom ,
					'attendees'=>$contacts
			);
			
			
			
	 		$eventInserted = $this->event_queue_model->insert( $data ) ;
	 		if(empty($eventInserted)){
	 			http_helper::response ( $this, http_helper::STATUS_INTERNAL_SERVER_ERROR );
	 		}
			
	 		http_helper::response ( $this, http_helper::STATUS_SUCCESS , array("event_id"=>$eventInserted->event_id));
	 		*/
	 		
	 		/**========================================= End Reserve to DB =============================================**/
		}catch (Exception $e){
			var_dump($e) ;
		}
	}
	
	function reserveews_post() {
		try{
		$serviceId = Service_model::SERVICE_RM04 ;
// 		$post_params = $this->input->post();
// 		log_message("debug", json_encode($post_params)) ;
		$response_data = NULL;
		
		$reqHeaders = http_helper::requestHeaders ( $this );
		
// 		var_dump($reqHeaders) ; exit;
		
		$clientModel = http_helper::getClientModel($this,$reqHeaders) ;
		
// 		var_dump($clientModel) ; exit;
		
		$reserveFrom = "" ;
		$userModel = null ;
		$clientType = $clientModel->client_type ;
		switch($clientType){
			case "MB" :
				
				$access_token = isset($reqHeaders['access_token'])?$reqHeaders ['access_token']:"";
				if (empty($access_token)) {
					http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
				}
// 				echo 'access_koen='.json_encode($access_token) ; exit;
				
				$user_token_model = $this->user_token_model->findByAccessToken($access_token) ;
				if(empty($user_token_model)){
					http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
				}
// 				echo 'user_token_model='.json_encode($user_token_model) ; exit;
				
				$userModel = $this->user_model->findByPk($user_token_model->user_id) ;
				if(empty($userModel)){
					http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
				}
				
// 				echo '$userModel='.json_encode($userModel) ; exit;
				$reserveFrom = "mobile" ;
				
				
				break ;
			
			case "TB" :
			case "DK" :
			case "DT" :
// 				var_dump($reqHeaders['verify_code']) ; exit;
				
				$verify_code = isset($reqHeaders['verify_code'])?$reqHeaders['verify_code']:"";
				
				if (empty($verify_code)) {
					http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
				}
				$userModel = $this->user_model->findByVerifyCode($verify_code) ;
				$reserveFrom = "dashboard" ;
				break ;
				
			default :
				
				http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
				
				break ;
		}
		
// 		echo json_encode($userModel) ;
		if(empty($userModel)){
			http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
		}
		
		$roomId = $this->post('room_id') ;
		$startTime =$this->post('start') ;
		$endTime = $this->post('end') ;
		$contacts = $this->post('attendees') ;
		$subject = $this->post('subject') ;
		
// 		echo " $roomId = ". json_encode(empty($roomId)) ;
// 		echo " $startTime = ". json_encode(empty($startTime) ) ;
// 		echo " $endTime = ". json_encode(empty($endTime )) ;
// 		echo " $subject = ". json_encode(empty($subject) ) ;
		
	
		if(empty($roomId) || empty($startTime) || empty($endTime) || empty($subject) ){
			http_helper::response ( $this, http_helper::STATUS_INVALID_INPUT );
		}

		
		$events = $this->event_model->findByRoom($roomId,$startTime,$endTime) ;
		if(!empty($events)){
			http_helper::response ( $this, http_helper::STATUS_ROOM_NOT_AVAILABLE );
		}
	

		/** =================================== begin request booking to Exchange ===================================== **/
		if(crypt_helper::IS_ENCRYPTION){
			$password = user_helper::getPassword($userModel->user_name, $userModel->password, $userModel->auth_key, $userModel->created_at) ;
		}else{
			$password = $userModel->password ;
		}
		
		$this->config->load('ews.php', TRUE);
		$ews_config = [
				'host' => $this->config->item('server', 'ews'),
				'admin' => $this->config->item('adminuser', 'ews'),
				'pass' => $this->config->item('adminpassword', 'ews'),
		];

		$host = $ews_config['host'] ;
		
		
		$contact_model = $this->contact_model->findByPk($userModel->contact_id) ;
		if(empty($contact_model)){
			http_helper::response ( $this, http_helper::STATUS_INTERNAL_SERVER_ERROR );
		}

		$this->load->library('ews');
		$ews = $this->ews->create_ews_instants($host,$contact_model->user_logon,$password);
// 		var_dump($ews) ; exit;
		if(empty($ews)){
			log_message("debug","EWS Empty!! by input $host,$contact_model->user_logon,$password") ;
			http_helper::response ( $this, http_helper::STATUS_EWS_ERROR );
		}

		$body = "" ;
		$tz = new DateTimeZone("Asia/Bangkok");
		
		$start_date = new DateTime($startTime);
		$start_date->setTimeZone($tz) ;
		$start_date_str = $start_date->format('Y-m-d\TH:i:s');

		$end_date =  new DateTime($endTime);
		$end_date->setTimeZone($tz) ;
		$end_date_str = $end_date->format('Y-m-d\TH:i:s');
		
		$allday = false;
		$location ="" ;
		
		$contactList = null ;
		if(!empty($contacts)){
			$contactList = explode(",",$contacts) ;
		}
		
		$attendees = [] ;
		if(!empty($contactList)){
// 			$attendees = "" ;
			foreach ($contactList as $i=>$contact_id ){
				$contact_model = $this->contact_model->findByPk($contact_id) ;
				if(!empty($contact_model)){
					$attendees .= ( $contact_model->display_name."|".$contact_model->email  . ";"  ) ;
				}
			}
		}

		$importance = false ;
		$sensitivity = false ;
		
// 		echo "roomId $roomId"; exit;
		
		$room_resource = "" ;
		$room_synchronzer_model = $this->room_synchronizer_model->findByRoomId($roomId) ;
		if(!empty($room_synchronzer_model)){
			$room_resource = $room_synchronzer_model->user;
		}
		
		$requestString = "================== RESERVE-REQ =====================\n" .
		"|EWS=".json_encode($ews)."|SUBJECT=".$subject."|BODY=".$body .
		"|START=".$start_date_str . 
		"|END=".$end_date_str."|ALLDAY=".json_encode($allday)."|LOCATION=".$location."|ATTENDEES=". json_encode($attendees) .
		"|IMPORTANCE=".json_encode($importance)."|SENSITIVITY=".json_encode($sensitivity)."|RESOURCE=".$room_resource."|" ;
		log_message('debug',$requestString );
		
		$reserve_result = $this->ews->calendar_add_item($ews, $subject, $body, $start_date_str, $end_date_str, $allday, $location, $attendees, $importance, $sensitivity ,$room_resource);
		
// 		echo json_encode($reserve_result) ; exit;
		log_message('debug', 'RESERVE-RES {"room_id":"'.$roomId.'", "result": '. json_encode($reserve_result));
		if(empty($reserve_result)){
			http_helper::response ( $this, http_helper::STATUS_INTERNAL_SERVER_ERROR );
		}
		
		$ResponseCode = $reserve_result->ResponseCode ;
		$ResponseClass = $reserve_result->ResponseClass ;
		
		if($ResponseCode == "NoError" && $ResponseClass == "Success" ) {
			$event_id = $reserve_result->Items->CalendarItem->ItemId->Id ;
			$changeKey = $reserve_result->Items->CalendarItem->ItemId->ChangeKey ;
			
			log_message("debug","reserve success :" . json_encode($reserve_result)) ;
			
			$itemResult = $this->ews->calendar_get_item($ews,$event_id) ;
			log_message("debug","itemResult :" . json_encode($itemResult)) ;
			http_helper::response ( $this, http_helper::STATUS_SUCCESS  ,$reserve_result);
		}else{
			http_helper::response ( $this, http_helper::STATUS_EWS_ERROR ,$reserve_result );
		}
		
		/** =================================== begin  request booking to Exchange ===================================== **/
		}catch (Exception $e){
			var_dump($e) ;
		}
	}
	
	function index_get($roomId) {
	
		$reqHeaders = http_helper::requestHeaders ( $this );
		$clientModel = http_helper::getClientModel( $this , $reqHeaders) ;
		
		// 		echo json_encode($clientModel) ; exit;
		$currentTime = date ( 'Y-m-d H:i:s' );
		$startTime = date ( 'Y-m-d H:i:s', strtotime ( $currentTime . "-2 hours" ) );
		$endTime = date ( 'Y-m-d H:i:s', strtotime ( $currentTime . "+5 hours" ) );
		// 		echo $startTime." - ".$endTime ; exit;
		$room = array() ;
		$roomInfo = $this->room_model->findByPk($roomId);
		// 		echo json_encode($events) ; exit;
		if(!empty($roomInfo)){
			$room['room_id'] = intval($roomInfo->room_id) ;
			$room['room_name'] = $roomInfo->room_name ;
			$room['description'] = $roomInfo->description ;
			
			$floor_name = "" ;
			$floor_id = $roomInfo->floor_id;
			if(!empty($floor_id)){
				$floor_model = $this->floor_model->findByPk($floor_id) ;
				if(!empty($floor_model)){
					$floor_name = $floor_model->floor_name ;
				}
			}
			
			$room['floor'] = $floor_name ;
			$room['vdo'] = base_url(). "data/contents/s1_01_vdo.mp4";
			
			
			$contact = array() ;
			$ownerContactId = $roomInfo->contact_id;
			if($ownerContactId > 0){
				$contactModel = $this->contact_model->findByPk($ownerContactId) ;
				if(!empty($contactModel)){
					$contact= array(
							'id'=> intval($contactModel->contact_id) ,
							'name'=>$contactModel->display_name,
							'phone'=>$contactModel->telephone,
							'email'=>$contactModel->email,
							'employee_id'=>$contactModel->employee_id
					) ;
					
				}

			}
			$room['contact'] = $contact ;
			
			$facilities = [] ;
				$facility_list = $this->room_facility_model->findAllByRoomId($roomInfo->room_id) ;
				if(!empty($facility_list)) {
					
					for($i = 0 ; $i < count($facility_list) ; $i++ ){
						$facility = $facility_list[$i] ;
						
						$facility_id = $facility->facility_id ;
						$facility_model = $this->facility_model->findByPk($facility_id) ;
						
						$image = "" ;
						
						$facility_property_model = $this->facility_property_model->findByPk($facility_id,"image") ;
						if(!empty($facility_property_model)){
							
							$image = base_url() . "data/facility/" . $facility_property_model->property_value ;
						}
						
						$showType = "" ;
						$facility_property_model = $this->facility_property_model->findByPk($facility_id,"showtype") ;
						if(!empty($facility_property_model)){
							$showType = $facility_property_model->property_value ;
						}
						
						$facilities[$i]['facility'] = $facility_model->facility_name ;
						$facilities[$i]['qty'] = intval($facility->quantity) ;
						
						$facilities[$i]['image'] = $image ;
						$facilities[$i]['show_type'] = $showType ;
					}
				}
					
				$room['facilities'] = $facilities ;
				
				
				// Contact contact_id
				$contact = array() ;
				$room_id = $roomInfo->room_id ;
				$contactId = $roomInfo->contact_id ;
				if(!empty($contactId)){
					$contact_model = $this->contact_model->findByPk($contactId) ;
					if(!empty($contact_model)){
						
						$contact['name'] = $contact_model->display_name  ;
						$contact['email'] = $contact_model->email ;
						$contact['phone'] = $contact_model->telephone ;
						
					}
				}
				$room['contact'] = $contact ;
					
				// Theme
				$theme = array() ;
				$theme_model = $this->theme_model->findByRoomId($room_id) ;
				if(!empty($theme_model)){
					$theme['theme_id'] = intval($theme_model->theme_id) ;
					$theme['theme_name'] = $theme_model->name ;
					$theme['org_bg'] = base_url() . "images/room/theme/" .$theme_model->bg_image ;
					$theme['bg'] 	= base_url() . "images/room/theme/" .$theme_model->bg_convert ;
					$theme['color1'] = $theme_model->color_1 ;
					$theme['color2'] = $theme_model->color_2 ;
					$theme['color3'] = $theme_model->color_3 ;
				}
				$room['theme'] = $theme ;
					
				// Layout
				$layout = "" ;
				$room_property_model = $this->room_property_model->findByPk($room_id,"layout_image") ;
				if(!empty($room_property_model)){
					$layout =  base_url() . "images/room/layout/" . $room_property_model->property_value ;
				}
				$room['layout'] = $layout ;
				
				$available_time = array() ;
				

				$currentTime =  date('Y-m-d H:i:s');
				$room['available_from'] = $currentTime ;
				$next_meeting = $this->event_model->findNextEventByRoom($room_id) ;
				if(!empty($next_meeting)){
					$room['available_to'] = $next_meeting->start_time;
				}else{
					$room['available_to'] = date('Y-m-d H:i:s',  strtotime ( $currentTime . "+1 hours" ) );
				}
				
				
				
				
		}
// 		$data = array('room' => $room);
		http_helper::response($this, http_helper::STATUS_SUCCESS, $room);	
	}
	
	function facilities_get($roomId) {
	
		$reqHeaders = http_helper::requestHeaders ( $this );
		$clientModel = http_helper::getClientModel($this,$reqHeaders) ;
		
	
		$facilities = [] ;
		$facility_list = $this->room_facility_model->findAllByRoomId($roomId) ;
		if(!empty($facility_list)) {
				
			for($i = 0 ; $i < count($facility_list) ; $i++ ){
				$facility = $facility_list[$i] ;
		
				$facility_id = $facility->facility_id ;
				$facility_model = $this->facility_model->findByPk($facility_id) ;
		
				$image = "" ;
		
				$facility_property_model = $this->facility_property_model->findByPk($facility_id,"image") ;
				if(!empty($facility_property_model)){
						
					$image = base_url() . "data/facility/" . $facility_property_model->property_value ;
				}
		
				$showType = "" ;
				$facility_property_model = $this->facility_property_model->findByPk($facility_id,"showtype") ;
				if(!empty($facility_property_model)){
					$showType = $facility_property_model->property_value ;
				}
		
				$facilities[$i]['facility'] = $facility_model->facility_name ;
				$facilities[$i]['qty'] = intval($facility->quantity) ;
		
				$facilities[$i]['image'] = $image ;
				$facilities[$i]['show_type'] = $showType ;
			}
		}
			
		
		
		$data = array('facilities' => $facilities);
		http_helper::response($this, http_helper::STATUS_SUCCESS, $data);
	}
	
	function availables_get() {
	
		$reqHeaders = http_helper::requestHeaders ($this);
		$clientModel = http_helper::getClientModel($this,$reqHeaders) ;
		
		$access_token = $reqHeaders ['access_token'];
		
		if (empty($access_token)) {
			log_message("debug", "Empty Token !!") ;
			http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
		}
		
		$user_token_model = $this->user_token_model->findByAccessToken($access_token) ;
		if(empty($user_token_model)){
			http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
		}
		
		$userLogin = $this->user_model->findByPk($user_token_model->user_id) ;
		if(empty($userLogin)){
			http_helper::response ( $this, http_helper::STATUS_NOT_AUTHORIZED );
		}
		
		$room_available_list = $this->room_model->findAllAvailableNow($userLogin->user_id) ;
		
		$now_availables = [] ;
		$room_ids = "" ;
		
		if(!empty($room_available_list)){
			
			
			foreach ($room_available_list as $i=>$room_available){
				
				if($i != 0 ){
					$room_ids .= ",".$room_available->room_id;
				}else{
					$room_ids .= $room_available->room_id;
				}
				
				$up_to = date('Y-m-d H:i:s', strtotime(date ( 'Y-m-d H:i:s' ) . "+1 hours")) ;

				$now_availables[$i]['id'] = intval($room_available->room_id) ;
				$now_availables[$i]['name'] = $room_available->room_name ;
				$now_availables[$i]['description'] =$room_available->description;
				$now_availables[$i]['type_id'] = intval($room_available->room_type_id) ;
				
				$floor_name = "" ;
				$floor_model = $this->floor_model->findByPk($room_available->floor_id) ;
				if(!empty($floor_model)){
					$floor_name = $floor_model->floor_name ;
				}
				$now_availables[$i]['floor'] = $floor_name;
				
				$now_availables[$i]['available_from'] = date('Y-m-d H:i:s');
				$next_event_model_list = $this->event_model->findNextEventByRoom($room_available->room_id) ;
				if(!empty($next_event_model_list)){
					$now_availables[$i]['available_to'] = $next_event_model_list[0]->start_time ;
					$now_availables[$i]['up_to'] = $this->dateDiff(date('Y-m-d H:i:s'),$next_event_model_list[0]->start_time) ;
				}else{
					$now_availables[$i]['available_to'] = $up_to ;
					$now_availables[$i]['up_to'] = $this->dateDiff(date('Y-m-d H:i:s'),$up_to) ;
				}
				
				$contact_name = "" ;
				$contact_model = $this->contact_model->findByPk($room_available->contact_id) ;
				if(!empty($contact_model)){
					$contact_name = $contact_model->display_name ;
				}
				
				$now_availables[$i]['contact'] = $contact_name ;
				
				
				$location_name = "" ;
				$building_model = $this->building_model->findByPk($floor_model->building_id );
				if(!empty($building_model)){
					$location_model = $this->location_model->findByPk($building_model->location_id) ;
					if(!empty($location_model)){
						$location_name = $location_model->location_name ;
					}
				}
				$now_availables[$i]['location'] = $location_name ;
				
				
				$facilities = [] ;
				$facility_list = $this->room_facility_model->findAllByRoomId($room_available->room_id) ;
				if(!empty($facility_list)){

					foreach ($facility_list as $j=>$facility){
// 						var_dump($facility) ;exit;
						
						$facility_id = $facility->facility_id ;
						$facility_model = $this->facility_model->findByPk($facility_id) ;
						
						$image = "" ;
						
						$facility_property_model = $this->facility_property_model->findByPk($facility_id,"image") ;
						if(!empty($facility_property_model)){
							
							$image = base_url() . "data/facility/" . $facility_property_model->property_value ;
						}
						
						$showType = "" ;
						$facility_property_model = $this->facility_property_model->findByPk($facility_id,"showtype") ;
						if(!empty($facility_property_model)){
							$showType = $facility_property_model->property_value ;
						}
						$facilities[$j]['facility_id'] = $facility_id ;
						$facilities[$j]['facility'] = $facility_model->facility_name ;
						$facilities[$j]['qty'] = intval($facility->quantity) ;
						
						$facilities[$j]['image'] = $image ;
						$facilities[$j]['show_type'] = $showType ;
					
					}
					
					
				}
				$now_availables[$i]['facilities'] = $facilities ;

				
			}
		}
		

		$next_availables = [] ;
		$room_next_list = $this->room_model->findAllAvailableNextHour($userLogin->user_id , $room_ids) ;
		if(!empty($room_next_list)){
				
				
			foreach ($room_next_list as $i=>$room_available){
		
		
				$up_to = date('Y-m-d H:i:s', strtotime(date ( 'Y-m-d H:i:s' ) . "+8 hours")) ;
		
				$next_availables[$i]['id'] = intval($room_available->room_id) ;
				$next_availables[$i]['name'] = $room_available->room_name ;
				$next_availables[$i]['description'] =$room_available->description;
				$next_availables[$i]['type_id'] = intval($room_available->room_type_id) ;
				
				
				$floor_name = "" ;
				$floor_model = $this->floor_model->findByPk($room_available->floor_id) ;
				if(!empty($floor_model)){
					$floor_name = $floor_model->floor_name ;
				}
				$next_availables[$i]['floor'] = $floor_name;
				
				
				$next_event_model_list = $this->event_model->findNextEventByRoom($room_available->room_id , $up_to ) ;
				if(!empty($next_event_model)){
// 					$next_availables[$i]['from'] = $next_event_model_list[0]->end_time ;
					$next_availables[$i]['available_from'] = $next_event_model_list[0]->end_time ;
					if(!empty($next_event_model_list[1])){
						$next_availables[$i]['available_to'] = $next_event_model_list[1]->start_time;
						$next_availables[$i]['up_to'] = $this->dateDiff($next_event_model_list[0]->end_time ,$next_event_model_list[1]->start_time) ;
					}else{
						$next_availables[$i]['available_to'] = $up_to ;
						$next_availables[$i]['up_to'] = $this->dateDiff($next_event_model_list[1]->start_time,$up_to) ;
					}
				}else{
// 					$next_availables[$i]['from'] = date('Y-m-d H:i:s') ;
					$next_availables[$i]['available_from'] = date('Y-m-d H:i:s' );
					$next_availables[$i]['available_to'] = $up_to ;
					$next_availables[$i]['up_to'] = $this->dateDiff(date('Y-m-d H:i:s'),$up_to) ;
				}
		
				$contact_name = "" ;
				$contact_model = $this->contact_model->findByPk($room_available->contact_id) ;
				if(!empty($contact_model)){
					$contact_name = $contact_model->display_name ;
				}
				
				$next_availables[$i]['contact'] = $contact_name ;
				
				
				$location_name = "" ;
				$building_model = $this->building_model->findByPk($floor_model->building_id );
				if(!empty($building_model)){
					$location_model = $this->location_model->findByPk($building_model->location_id) ;
					if(!empty($location_model)){
						$location_name = $location_model->location_name ;
					}
				}
				$next_availables[$i]['location'] = $location_name ;
				
				
				$facilities = [] ;
				$facility_list = $this->room_facility_model->findAllByRoomId($room_available->room_id) ;
				if(!empty($facility_list)){
		
					foreach ($facility_list as $j=>$facility){
						// 						var_dump($facility) ;exit;
		
						$facility_id = $facility->facility_id ;
						$facility_model = $this->facility_model->findByPk($facility_id) ;
		
						$image = "" ;
		
						$facility_property_model = $this->facility_property_model->findByPk($facility_id,"image") ;
						if(!empty($facility_property_model)){
								
							$image = base_url() . "data/facility/" . $facility_property_model->property_value ;
						}
		
						$showType = "" ;
						$facility_property_model = $this->facility_property_model->findByPk($facility_id,"showtype") ;
						if(!empty($facility_property_model)){
							$showType = $facility_property_model->property_value ;
						}
		
						$facilities[$j]['facility_id'] = $facility_id ;
						$facilities[$j]['facility'] = $facility_model->facility_name ;
						$facilities[$j]['qty'] = intval($facility->quantity) ;
		
						$facilities[$j]['image'] = $image ;
						$facilities[$j]['show_type'] = $showType ;
							
					}
						
						
				}
				$next_availables[$i]['facilities'] = $facilities ;
		
		
			}
		}
		
		$data = array('now_availables' => $now_availables,'next_availables' => $next_availables);
		
		http_helper::response($this, http_helper::STATUS_SUCCESS, $data);
		
	}
	

	//Code written by purpledesign.in Jan 2014
	function dateDiff($formdate,$todat){
		$d_one = new DateTime($formdate);
		$d_two = new DateTime($todat);
		
		//get the difference object
		$d_diff = $d_one->diff($d_two);
		
		$h_diff = $d_diff->format('%h') ;
		if($h_diff < 1) {
			return $d_diff->format('%i mins') ;
		}else{
			$m_diff = $d_diff->format('%i') ;
			if($m_diff > 0){
				return $d_diff->format('%h hrs %i mins') ;
			}else{
				return $d_diff->format('%h hrs') ;
			}
		}
		
		//dump it
// 		var_dump($d_diff);
		
		//use the format string
// 		var_dump($d_diff->format('%H hour %i minute %s second %d day %m month %Y year'));
		
	}
	 
}
