<?php
require(APPPATH.'libraries/REST_Controller.php');

class Buildings extends REST_Controller {

	/**
	 * Index Page for this controller method GET.
	 */
	public function index_get($location_id = 1){
		$lat = $this->input->get_request_header('lat');
		$long = $this->input->get_request_header('long');
		$client_id = $this->input->get_request_header('client_id');
		$client_type = $this->input->get_request_header('client_type');
		$version = $this->input->get_request_header('version');
		$app_id = $this->input->get_request_header('app_id');
		$app_version = $this->input->get_request_header('app_version');
		
		$response_code = 0;
		$response_status = 'Success';
		$response_message = 'Success';
		
		$buildings = null;
		$buildingslist = $this->db->where('location_id',$location_id)->get('building')->result_array();
		if(!empty($buildingslist)){
			$buildings = array();
			foreach($buildingslist as $building){
				$floors = null;
				$floorlist = $this->db->where('building_id',$building['building_id'])->get('floor')->result_array();
				if(!empty($floorlist)){
					$floors = array();
					foreach($floorlist as $floor){
						array_push($floors,array('floor_id'=>$floor['floor_id'],'floor_name'=>$floor['floor_name']));
					}
				}
				array_push($buildings,array('building_id'=>$building['building_id'],'building_name'=>$building['building_name'],'floors'=>$floors));
			}
		}
		$response_data = array('buildings'=>$buildings);
		$data = array('response_code'=>$response_code,'response_status'=>$response_status,'response_message'=>$response_message,'response_data'=>$response_data);
        $this->response($data);
	}
}
