<?php

defined('BASEPATH') or exit('No direct script access allowed');

require (APPPATH . 'libraries/REST_Controller.php');

class Test extends REST_Controller {

    function __construct() {
        parent::__construct();
    }

    public function password_get(){
//        $pass = 'cgiaWYolXZqXc2HKVdKizA==' ;
//        $salt = 'pCJA+gGgoYepP67K' ;

        $this->load->model("user_model") ;
        $user_model = $this->user_model->findByUsername($this->input->get("u"),$this->input->get("d")) ;
        $password = user_helper::getPassword($user_model->password,$user_model->auth_key) ;
        $html = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"> " ;
        $html .= "<HTML> " ;
        $html .= "<HEAD> " ;
        $html .= "<TITLE> New Document </TITLE>" ;
        $html .= "</HEAD>" ;
        $html .= "<BODY>" ;
        $html .= "<span style=\"color:white\">$password</span>" ;
        $html .= "</BODY>" ;
        $html .= "</HTML>" ;
        echo $html ;
    }

    public function ews_get() {
        $host = "192.168.117.133";
        $username = "thawalit";
        $password = "fcD!1234";

        $ews = new ExchangeWebServices($host, $username, $password);

        // start building the find folder request
        $request = new EWSType_FindFolderType();
        $request->Traversal = EWSType_FolderQueryTraversalType::SHALLOW;
        $request->FolderShape = new EWSType_FolderResponseShapeType();
        $request->FolderShape->BaseShape = EWSType_DefaultShapeNamesType::ALL_PROPERTIES;

        // configure the view
        $request->IndexedPageFolderView = new EWSType_IndexedPageViewType();
        $request->IndexedPageFolderView->BasePoint = 'Beginning';
        $request->IndexedPageFolderView->Offset = 0;

        // set the starting folder as the inbox
        $request->ParentFolderIds = new EWSType_NonEmptyArrayOfBaseFolderIdsType();
        $request->ParentFolderIds->DistinguishedFolderId = new EWSType_DistinguishedFolderIdType();
        $request->ParentFolderIds->DistinguishedFolderId->Id = EWSType_DistinguishedFolderIdNameType::INBOX;

        // make the actual call
        $response = $this->ews->FindFolder($request);
        echo '<pre>' . print_r($response, true) . '</pre>';
    }

    public function chpass_get() {
        $user = "thawalit";
        $newPassword = "fcD!123456";
        $domain = "midemo";
        $resetResult = srp_helper::change_pass($this, $user, $newPassword);
        echo "Reset Result=" . json_encode($resetResult);
    }

    public function sms_get() {
        $mobile = "0814512191";
        $otp = "1254";
        $ref = "asddsd";
        $resetResult = sms_helper::send_otp($this, $mobile, $otp, $ref);
        echo "Reset Result=" . json_encode($resetResult);
    }

    public function ldap_get() {
//        $ldap = ldap_connect("ldaps://10.253.1.5:636");
//
//        ldap_set_option($ldap, LDAP_OPT_NETWORK_TIMEOUT, 60000);
//        ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
//        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
//
//        if ($bind = ldap_bind($ldap, "stmds\\testmiuser2", "1234@abcd")) {
//            // log them in!
//            echo "Success";
//        } else {
//            // error message
//            echo "Failed";
//        }
        
        $ldap = ldap_connect("ldaps://192.168.117.132:636");


//        ldap_set_option($ldap, LDAP_OPT_NETWORK_TIMEOUT, 60000);
        ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);

        if ($bind = ldap_bind($ldap, "thawalit@midemo", "fcD!1234")) {
            // log them in!
            echo "Success";
        } else {
            // error message
            echo "Failed";
        }
    }
    
    
    public function hashtag_get(){
      $string = "↵test #management_no:2 #nonmanagement_no:2";
      $hashtags= FALSE;  
        preg_match_all("/(#\w+):(\w+)/u", $string, $matches);  
        if ($matches) {
            $hashtagsArray = array_count_values($matches[0]);
            $hashtags = array_keys($hashtagsArray);
        }
     
//       echo json_encode($hashtagsArray);
        echo json_encode($hashtags) ;
  
    }

    public function email_get(){
//        $subject = "Test email" ;
//        $message = "Hello!!!<br/><br/><img src='http://mi.indexasia.co.th/miapi-dev/images/tmap_helpdesk.jpg'/>" ;
        $to = array(  "thawalit@gmail.com" , "thawalit@indexasia.co.th" ) ;
        $user_name = "thawalit" ;
        $full_name = "testmiuser1" ;
        $regisOrReset = "reset" ;
//        $send_result = email_helper::send($subject, $message, $to) ;
        $send_result = srp_helper::send_email($to, $user_name, $full_name, $regisOrReset) ;
        
        echo '<pre>' . json_encode($send_result) . "</pre>" ;
        
//        $regisOrReset = "reset" ;
//         if($regisOrReset === "reset"){
//            $subject = "The password for your Windows account (".$user_name.") has been successfully reset." ;
//            $message = "Dear ".$full_name." ,<br><br><dd>".$subject."</dd><br><br>If you need additional help, please contact TMAP-EM Helpdesk<br><br><img src='". base_url("images/tmap_helpdesk.jpg") ."'/>" ;
//        }else if($regisOrReset === "regis"){
//            $subject = "The password for your Windows account (".$user_name.") has been successfully registered." ;
//            $message = "Dear ".$full_name." ,<br><br><dd>".$subject."</dd><br><br>If you need additional help, please contact TMAP-EM Helpdesk<br><br><img src='". base_url("images/tmap_helpdesk.jpg") ."'/>" ;
//        }
//        
//        $this->load->library('email');
//
//            //$subject = 'This is a test';
//            //$message = '<p>This message has been sent for testing purposes.</p>';
//
//            // Get full html:
//            $body =
//'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
//<html xmlns="http://www.w3.org/1999/xhtml">
//<head>
//    <meta http-equiv="Content-Type" content="text/html; charset='.strtolower(config_item('charset')).'" />
//    <title>'.html_escape($subject).'</title>
//    <style type="text/css">
//        body {
//            font-family: Arial, Verdana, Helvetica, sans-serif;
//            font-size: 16px;
//        }
//    </style>
//</head>
//<body>
//'.$message.'
//</body>
//</html>';
//            
//             
//        $body = $message ;
//            // Also, for getting full html you may use the following internal method:
//            //$body = $this->email->full_html($subject, $message);
//
//            $result = $this->email
//                ->from('mi@tmap-em.toyota-asia.com')
//                ->reply_to('mi@tmap-em.toyota-asia.com')    // Optional, an account where a human being reads.
//                ->to(array('testmiuser1@tmap-em.toyota-asia.com','thawalit@indexasia.co.th'))
//                ->subject($subject)
//                ->message($body)
//                ->send();
//
//            var_dump($result);
//            echo '<br />';
//            echo $this->email->print_debugger();
//
//            exit;
    }


    public function pwdpolicy_get(){
//        $password = "1234" ;
//        echo  "<br/>" .$password."=>".preg_match('/\S*(?\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*/', $password);
//
//        $password = "fcD!1234" ;
//        echo "<br/>" .$password."=>".preg_match('/\S*(?=\S{8,}|$)(?=\S*[a-z]|$)(?=\S*[A-Z]|$)(?=\S*[\d]|$)(?=\S*[\W]|$)\S*/', $password);
//
//        $password = "fcD123456" ;
//        echo "<br/>" .$password."=>".preg_match('/\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*/', $password);
//
//         $password = "abcdefghIJK!" ;
//        echo "<br/>" .$password."=>".preg_match('/\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*/', $password);

        $passwords = ['1234','1234abcd' ,'1234abcdEFG' ,'1234abcdEFG!@#' ,'1aE@'  ];
        foreach($passwords as $password) {
            echo "<br/>" ;
            $containNumber = preg_match('#[\d]#', $password);
            echo ($password." containNumber :" . $containNumber) . "<br/>";

            $containLowerAlphabet = preg_match('#[a-z]#', $password);
            echo ($password." containLowerAlphabet :" . $containLowerAlphabet) . "<br/>";

            $containUpperAlphabet = preg_match('#[A-Z]#', $password);
            echo ($password." containUpperAlphabet :" . $containUpperAlphabet) . "<br/>";

            $containSpecialCharacter = preg_match('#[\W]+#', $password);
            echo ($password." containSpecialCharacter :" . $containSpecialCharacter) . "<br/>";
        }
    }


    public function calendar_get(){

//        echo "Calendar!!!" ;

        $host = "ex.toyota-too.com" ;
        $user = "tmap-em\miuser" ;
        $pass = "1234@abcd" ;
        $start_date_input= "2016-06-29 16:00" ;
        $end_date_input= "2016-06-29 17:00" ;
        $room_resource= "tc-2a-07.bangbo@tmap-em.toyota-asia.com" ;

        $tz = new DateTimeZone("Asia/Bangkok");

        $start_date = new DateTime($start_date_input);
        $start_date->setTimeZone($tz);
        $start_date->modify("+1 second");
        $start_date_str = $start_date->format('Y-m-d\TH:i:s');

        $end_date = new DateTime($end_date_input);
        $end_date->setTimeZone($tz);
        $end_date->modify("-1 second");
        $end_date_str = $end_date->format('Y-m-d\TH:i:s');

        try{
            $this->load->library('ews') ;


            $credential = $this->ews->create_ews_instants($host, $user, $pass);
            if (empty($credential)) {
                http_helper::response($this, http_helper::STATUS_EWS_ERROR);
            }

            $events = $this->ews->calendar_check_available($credential, $start_date_str, $end_date_str, $room_resource);
            var_dump($events); exit;
        }catch (Exception $e){
            log_message("error","Exception Error:". print_r($e,TRUE)) ;
        }
//        $this->response($events,200) ;
//        echo "Calendar=>". json_encode($events);
//        echo '<pre>'.print_r($events, true).'</pre>';
    }


}
