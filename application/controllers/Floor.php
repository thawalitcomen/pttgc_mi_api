<?php
require(APPPATH . 'libraries/REST_Controller.php');

class Floor extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('client_model');
        $this->load->model('floor_model');
        $this->load->model('event_model');

        $this->load->model('zone_property_model');
        $this->load->model('room_property_model');

        $this->load->model('contact_model');
    }

    /**
     * Index Page for this controller method GET.
     */
    public function index_get($floor_id)
    {
 
// 		$lat = $this->input->get_request_header('lat');
// 		$long = $this->input->get_request_header('long');
// 		$client_id = $this->input->get_request_header('client_id');
// 		$client_type = $this->input->get_request_header('client_type');
// 		$version = $this->input->get_request_header('version');
// 		$app_id = $this->input->get_request_header('app_id');
// 		$app_version = $this->input->get_request_header('app_version');

        $reqHeaders = http_helper::requestHeaders($this);
        $clientModel = http_helper::getClientModel($this, $reqHeaders);

        $page = $this->input->get('page');
// 		$response_code = 0;
// 		$response_status = 'Success';
// 		$response_message = 'Success';
        $signages = array();
        $floor = $this->db->where('floor_id', $floor_id)->get('floor')->row_array();
        $floor_name = 'No Room';
        $zones = null;
        $rooms = null;

        if (!empty($floor)) $floor_name = $floor['floor_name'];

        $zonelist = $this->db->where('zone_id in (select zone_id from room where floor_id = ' . $floor_id . ')')->get('zone')->result_array();
        if (!empty($zonelist)) {
            $zones = array();

            foreach ($zonelist as $zone) {


// 				echo "ZoneId :$zone->zone_id" ; exit;
                $property_model_list = $this->zone_property_model->findAllByZoneId($zone['zone_id']);
// 				echo json_encode($property_model_list) ; exit;
                if (!empty($property_model_list)) {

                    $position_x = 0;
                    $position_y = 0;
                    $width = 0;
                    $height = 0;

                    foreach ($property_model_list as $property) {
                        switch ($property->property_name) {
                            case 'position_x' :
                                $position_x = $property->property_value;
                                break;
                            case 'position_y' :
                                $position_y = $property->property_value;
                                break;
                            case 'position_width' :
                                $width = $property->property_value;
                                break;
                            case 'position_height' :
                                $height = $property->property_value;
                                break;
                        }

                    }
//
//					array_push($zones,
//							array(	'zone_id'=>$zone['zone_id'],
//									'zone_name'=>$zone['zone_name'],
//									'position_x'=>$position_x,
//									'position_y'=>$position_y,
//									'width'=>$width,
//									'height'=>$height)
//							);

                    $zones[] = array('zone_id' => $zone['zone_id'],
                        'zone_name' => $zone['zone_name'],
                        'position_x' => $position_x,
                        'position_y' => $position_y,
                        'width' => $width,
                        'height' => $height);

                }


            }
        }

        $roomlist = $this->db->where('floor_id', $floor_id)->get('room')->result_array();
        if (!empty($roomlist)) {
            $rooms = array();
            foreach ($roomlist as $room) {

                $currentTime = date('Y-m-d H:i:s');
                $startTime = date('Y-m-d H:i:s', strtotime($currentTime . "-1 hours"));
                $endTime = date('Y-m-d H:i:s', strtotime($currentTime . "+1 hours"));

                $status = "Available";
                $schedules = null;
                $event_list = $this->event_model->findByRoom($room['room_id'], $startTime, $endTime);
                if (!empty($event_list)) {
                    $schedules = [];
                    foreach ($event_list as $j => $event) {

                        $start = $event->start_time;
                        $end = $event->end_time;

                        if ($currentTime >= $start && $currentTime <= $end) {
                            $status = "Inuse";
                        }
                    }

                }

                $position_x = 0;
                $position_y = 0;
                $width = 0;
                $height = 0;

                $property_model_list = $this->room_property_model->findAllByRoomId($room['room_id']);
                if (!empty($property_model_list)) {
                    foreach ($property_model_list as $property) {
                        switch ($property->property_name) {
                            case 'position_x' :
                                $position_x = $property->property_value;
                                break;
                            case 'position_y' :
                                $position_y = $property->property_value;
                                break;
                            case 'position_width' :
                                $width = $property->property_value;
                                break;
                            case 'position_height' :
                                $height = $property->property_value;
                                break;
                        }

                    }

//					array_push($rooms, array(
//							'room_id'=>intval($room['room_id']),
//							'room_name'=>$room['room_name'],
//							'room_type'=>$room['room_type_id'],
//							'status'=>$status,
//							'position_x'=>$position_x,
//							'position_y'=>$position_y,
//							'width'=>$width,
//							'height'=>$height));


                }

                $rooms[] = array(
                    'room_id' => intval($room['room_id']),
                    'room_name' => $room['room_name'],
                    'room_type' => $room['room_type_id'],
                    'status' => $status,
                    'position_x' => $position_x,
                    'position_y' => $position_y,
                    'width' => $width,
                    'height' => $height

                );


            }
        }

        $response_data = array('floor_id' => $floor_id, 'floor_name' => $floor_name, 'layout' => $floor['image'], 'zones' => $zones, 'rooms' => $rooms);
// 		$data = array('response_code'=>$response_code,'response_status'=>$response_status,'response_message'=>$response_message,'response_data'=>$response_data);
//      $this->response($data);

        http_helper::response($this, http_helper::STATUS_SUCCESS, $response_data);
    }

    public function schedules_get($floor_id)
    {
        //$this->output->enable_profiler(TRUE);

// 		$lat = $this->input->get_request_header('lat');
// 		$long = $this->input->get_request_header('long');
// 		$client_id = $this->input->get_request_header('client_id');
// 		$client_type = $this->input->get_request_header('client_type');
// 		$version = $this->input->get_request_header('version');
// 		$app_id = $this->input->get_request_header('app_id');
// 		$app_version = $this->input->get_request_header('app_version');

        $reqHeaders = http_helper::requestHeaders($this);
        $clientModel = http_helper::getClientModel($this, $reqHeaders);

        $page = $this->input->get('page');
        $page_size = $this->input->get('page_size');
        if ($page < 1) $page = 1;

        $total_page = 0;

// 		$response_code = 0;
// 		$response_status = 'Success';
// 		$response_message = 'Success';
        $rooms = null;

        $total = $this->db->where('floor_id', $floor_id)->where('status_id', 1)->count_all_results('room');
        $pagesize = !empty($page_size) ? $page_size : 10;
        $total_page = ceil($total / $pagesize);
        $offset = ($page - 1) * $pagesize;

        $start_now = $this->input->get('start_now');
        if (is_null($start_now) || empty($start_now)) {
            $start_now = 0;
        }

        $roomlist = $this->db->where('floor_id', $floor_id)->where('status_id', 1)->get('room', $pagesize, $offset)->result_array();
        if (!empty($roomlist)) {
            $rooms = [];
            foreach ($roomlist as $i => $room) {

                $meeting_id = 0;
                $subject = '';
                $host = '';
                $start = null;
                $end = null;

                $room_id = $room['room_id'];
                $rooms[$i]['room_id'] = intval($room_id);
                $rooms[$i]['room_name'] = $room['room_name'];
                $rooms[$i]['room_type'] = intval($room['room_type_id']);

// 				$rooms[$i]['schedules']

// 				$currentschedule = $this->db->where('room_id',$room_id)->where("start_time < now()")->where("end_time > now() ")->limit(1)->get('event')->row_array();
                $currentTime = date('Y-m-d H:i:s');
                $startTime = $start_now ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s', strtotime($currentTime . "-2 hours"));
                $endTime = date('Y-m-d H:i:s', strtotime($currentTime . "+5 hours"));

// 				echo json_encode('[Search]curr'.$currentTime) ;
// 				echo json_encode('[Search]start'.$startTime) ;
// 				echo json_encode('[Search]end'.$endTime) ;
// 				exit;


                $schedules = null;
                $event_list = $this->event_model->findByRoom($room_id, $startTime, $endTime);
                if (!empty($event_list)) {
                    $schedules = [];
                    foreach ($event_list as $j => $event) {

                        $meeting_id = $event->event_id;

                        $create_by = $event->create_by;


                        $start = $event->start_time;
                        $end = $event->end_time;
// 						if($currentTime > $start && $currentTime < $end ){
// // 							$statusCount++ ;
// 						}elseif($currentTime < $start ){
// 							$status = 'Upcoming';
// 						}


                        $schedules[$j]['meeting_id'] = intval($meeting_id);
// 						$schedules[$j]['subject'] = $subject ;

                        /*
                        $contact_tmp = '';
                        preg_match('/##(\w+)##/', $event->topic , $matches);
                        log_message("error","Match" .json_encode($matches)) ;
                        if(!empty($matches)){
                            $subject = str_replace($matches[0],"", $event->topic );
                            $contact_id_tmp = intval(str_replace("#","",$matches[0])) ;
                            log_message("error","contact_id_tmp :".$contact_id_tmp) ;
                            if(!empty($contact_id_tmp) && $contact_id_tmp > 0){
                                $contact_tmp_model = $this->contact_model->findByPk($contact_id_tmp) ;
                                if(!empty($contact_tmp_model)){
                                    $contact_tmp = $contact_tmp_model->display_name ;
                                }
                            }
                        }else{
                            $subject = $event->topic ;
                        }
                        */

                        $subject = $event->topic;
                        //log_message("error", "Subject :" . $subject);

                        $schedules[$j]['subject'] = $subject;
                        $schedules[$j]['start'] = $start;
                        $schedules[$j]['end'] = $end;

                        $host = array();
                        if (!empty($create_by)) {
                            $contact = $this->db->where('contact_id', $create_by)->limit(1)->get('contact')->row_array();
                            $host['id'] = intval($contact['contact_id']);
//							$host['name']= !empty($contact_tmp)?$contact_tmp : $contact['display_name'];
                            $host['name'] = $contact['display_name'];
                            $host['phone'] = $contact['telephone'];
                            $host['email'] = $contact['email'];
                        }

                        $schedules[$j]['host'] = $host;

                    }
                }

                $rooms[$i]['schedules'] = $schedules;
                //ADDDATE(DATE(now()),1);
// 				array_push($schedules,array('room_id'=>$room_id,'room_name'=>$room['room_name'],
// 					'room_type'=>$room['room_type_id'],'status'=>$status,'meeting_id'=>$meeting_id,
// 					'subject'=>$subject,'host'=>$host,'start'=>$start,'end'=>$end)
// 				);
            }
        }
        $response_data = array('page' => $page, 'total_page' => $total_page, 'rooms' => $rooms);
// 		$data = array('response_code'=>$response_code,'response_status'=>$response_status,'response_message'=>$response_message,'response_data'=>$response_data);
//         $this->response($data);

        http_helper::response($this, http_helper::STATUS_SUCCESS, $response_data);
    }

    public function floors_get($building_id)
    {

        $reqHeaders = http_helper::requestHeaders($this);
        $clientModel = http_helper::getClientModel($this, $reqHeaders);


        $floors = [];
        $floorlist = $this->db->where('building_id', $building_id)->get('floor')->result_array();

        if (!empty($floorlist)) {
            foreach ($floorlist as $floor) {
                //array_push($floors, array('floor_id' => intval($floor['floor_id']), 'floor_name' => $floor['floor_name'], 'layout' => $floor['image']));
                $floors[] = array(
                    'floor_id' => intval($floor['floor_id']),
                    'floor_name' => $floor['floor_name'],
                    'layout' => $floor['image']
                );
            }
        } else {
            $response_code = 1;
            $response_status = 'No Floor Config';
            $response_message = 'No Floor Config';
        }

        //$response_data = array('floor_id'=>$floor_id,'floor_name'=>$floor_name,'zones'=>$zones,'rooms'=>$rooms);

        $response_data = array('floors' => $floors);

// 		$data = array('response_code'=>$response_code,'response_status'=>$response_status,'response_message'=>$response_message,'response_data'=>$response_data);
//         $this->response($data);

        http_helper::response($this, http_helper::STATUS_SUCCESS, $response_data);
    }
}
