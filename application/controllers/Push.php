<?php 
class Push extends CI_Controller {
	
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		
		$test = $this->input->get('test') ;
		if(!$this->input->is_cli_request() && ( $test != 'y' ))
		{
			//echo "This script can only be accessed via the command line" . PHP_EOL;
			show_error('Direct access is not allowed');
			return;
		}
		
	
		$this->load->model('push_queue_model');
		$this->load->model('client_model');

	}
	
	
	public function push(){
		
		while (true){
			
			log_message("debug", "[PUSH-NOTIFICATION] start......... ") ;
			$push = null ;
			
			$queue_list = $this->push_queue_model->fetchQueue(10) ;
			log_message("debug", "[PUSH-NOTIFICATION] queue_list:". json_encode($queue_list)) ;
			if(!empty($queue_list)){
				foreach ($queue_list as $queue){
					try{
						// Update to processiong status
						log_message("debug", "[PUSH-NOTIFICATION] Queue=".json_encode($queue)."") ;
						$queue->attemps = $queue->attemps+1 ;
						$this->push_queue_model->updateStatusQueue($queue->queue_id,'Processing',$queue->attemps ) ;
						
						$contact_id = $queue->contact_id ;
						if(empty($contact_id)){
							log_message("error", "[PUSH-NOTIFICATION] Empty contact_id") ;
							continue ;
						}
						
						$client_model_list = $this->client_model->findByContactId($contact_id) ;
						if(empty($client_model_list)){
							log_message("error", "[PUSH-NOTIFICATION] Not found client model") ;
							$this->push_queue_model->updateStatusQueue($queue->queue_id,'Failed',null,"Not found client model by contact_id:".$contact_id) ;
							continue ;
						}
						
						$success = 0 ;
						foreach ($client_model_list as $client_model) {
							
							if(empty($client_model->push_token)){
								continue ;
							}
							if("MB" == $client_model->client_type){
								switch ($client_model->os){
									case "ANDROID" :
										$pushResult = $this->android_push($client_model->push_token,$queue->message) ;
	
										log_message("debug", "[PUSH-NOTIFICATION] pushResult=".json_encode($pushResult)) ;
										if($pushResult){
											$success++ ;
										}
										
										break ;
										
									case "IOS" :
										$pushResult = $this->apple_push($client_model->push_token,$queue->message) ;
										log_message("debug", "[PUSH-NOTIFICATION] pushResult=".json_encode($pushResult)) ;
										if($pushResult){
											$success++ ;
										}
										break ;
								}
							
							}
						}
					
						
						if( $success > 0){
							$this->push_queue_model->updateStatusQueue($queue->queue_id,'Success',null,isset($pushResult)?$pushResult:"") ;
						}else{
							$this->push_queue_model->updateStatusQueue($queue->queue_id,'Failed',null,isset($pushResult)?$pushResult:"") ;
						}
					}catch (Exception $e){
						$this->push_queue_model->updateStatusQueue($queue->queue_id,'Failed',null, "Exception Error :".json_encode($e)) ;
						log_message("error",  "Exception Error :".json_encode($e)) ;
					}
				}
			}
			
			sleep(5);
		}
		
		
		
	}
	
	public function queue(){
		log_message("debug", "[PUSH-NOTIFICATION] start......... ") ;
		$queue_id = $this->input->get("queue_id");
		if(!empty($queue_id))$queue = $this->push_queue_model->findByPk($queue_id) ;
		log_message("debug", "[PUSH-NOTIFICATION] queue_list:". json_encode($queue_list)) ;
		if(!empty($queue)){
			try{
				if($queue->status!='Queue') die("[PUSH-NOTIFICATION] not allow $queue_id ");
				// Update to processiong status
				log_message("debug", "[PUSH-NOTIFICATION] Queue=".json_encode($queue)."") ;
				$queue->attemps = $queue->attemps+1 ;
				$this->push_queue_model->updateStatusQueue($queue->queue_id,'Processing',$queue->attemps ) ;
	
				$contact_id = $queue->contact_id ;
				if(empty($contact_id)){
					log_message("error", "[PUSH-NOTIFICATION] Empty contact_id") ;
					die("[PUSH-NOTIFICATION] Empty contact_id");
				}
	
				$client_model_list = $this->client_model->findByContactId($contact_id) ;
				if(empty($client_model_list)){
					log_message("error", "[PUSH-NOTIFICATION] Not found client model by contact_id:".$contact_id) ;
					$this->push_queue_model->updateStatusQueue($queue->queue_id,'Failed',null,"Not found client model by contact_id:".$contact_id) ;
					die("[PUSH-NOTIFICATION] Not found client model by contact_id:".$contact_id);
				}
	
				$success = 0 ;
				foreach ($client_model_list as $client_model) {
	
					if(empty($client_model->push_token)){
						continue ;
					}
					if("MB" == $client_model->client_type){
						switch ($client_model->os){
							case "ANDROID" :
								$pushResult = $this->android_push($client_model->push_token,$queue->message) ;
	
								log_message("debug", "[PUSH-NOTIFICATION] pushResult=".json_encode($pushResult)) ;
								if($pushResult){
									$success++ ;
								}
	
								break ;
	
							case "IOS" :
								$pushResult = $this->apple_push($client_model->push_token,$queue->message) ;
								log_message("debug", "[PUSH-NOTIFICATION] pushResult=".json_encode($pushResult)) ;
								if($pushResult){
									$success++ ;
								}
								break ;
						}
							
					}
				}
					
	
				if( $success > 0){
					$this->push_queue_model->updateStatusQueue($queue->queue_id,'Success',null,isset($pushResult)?$pushResult:"") ;
				}else{
					$this->push_queue_model->updateStatusQueue($queue->queue_id,'Failed',null,isset($pushResult)?$pushResult:"") ;
				}
			}catch (Exception $e){
				$this->push_queue_model->updateStatusQueue($queue->queue_id,'Failed',null, "Exception Error :".json_encode($e)) ;
				log_message("error",  "Exception Error :".json_encode($e)) ;
			}
		}else{
			echo "Cannot found queue id $queue_id ";
		}
	}
	
	public function android_push($devices, $msg, $ntfyType = "") {
		$device_id  = $devices;
		$message    = $msg;
		$notifyType = $ntfyType;
	
		/*
		 there are example data.
		 if un note this section , you can use some fake data for testign
		 $device_id  = array("50");
		 $message    = "test";
		 $notifyType = "packages";
		 */
		if(!is_array($device_id)) { $device_id = array($device_id); }
	
		/* if data is null , then return false */
		if( count($device_id) <= 0 or trim($message) == "") { return false; }
		$post_fields = array(
				"registration_ids"=>$device_id,
				"data" => array("dataType"=>$notifyType,"message"=>$message)
		);
	
		/* initial the curl object */
		$curl = curl_init();
		curl_setopt($curl , CURLOPT_URL , "https://android.googleapis.com/gcm/send");
		curl_setopt($curl , CURLOPT_POST , true );
		curl_setopt($curl , CURLOPT_RETURNTRANSFER , true );
		curl_setopt($curl , CURLOPT_SSL_VERIFYPEER , false );
		curl_setopt($curl , CURLOPT_HTTPHEADER  ,
				array( "Content-Type: ". "application/json" ,
						"Authorization: key=". "IzaSyChavTpTgWrLK3hno1IK0vQYrxu2gPgQrM")
				);
		curl_setopt($curl , CURLOPT_POSTFIELDS , json_encode( $post_fields) );
		
		//proxy details
// 		curl_setopt($curl, CURLOPT_PROXY, '172.17.17.31:8080');
// 		curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
		log_message("debug", "[PUSH-NOTIFICATION] curl " . json_encode($curl)) ;
		$pushResult = curl_exec( $curl );
		log_message("debug", "[PUSH-NOTIFICATION] result" . json_encode($pushResult)) ;
		if($pushResult)
		{
			$pushResultArray = json_decode($pushResult,true);
	
			/* check if notify send success */
			if($pushResultArray["success"]==0)
			{
				return false;
			}
			else
			{
				return true;
			}
	
		}
		else
		{
			return false;
		}
	
	}
	
	function apple_push($devices , $msg , $ntfyType = "") {
		
		$result = false ;
		try{
			$this->load->library('apn');
			$this->apn->payloadMethod = 'enhance'; // Ð²ÐºÐ»ÑŽÑ‡Ð¸Ñ‚Ðµ Ñ�Ñ‚Ð¾Ñ‚ Ð¼ÐµÑ‚Ð¾Ð´ Ð´Ð»Ñ� Ð¾Ñ‚Ð»Ð°Ð´ÐºÐ¸
			$this->apn->connectToPush();
			
			// adding custom variables to the notification
			// 	$this->apn->setData(array( 'someKey' => true ));
			
			$send_result = $this->apn->sendMessage( $devices, $msg , /*badge*/ 2, /*sound*/ 'default'  );
			
			if($send_result){
				log_message('debug','Sending successful');
				$result = true ;
			}else{
				log_message('error',$this->apn->error);
			}
			
			$this->apn->disconnectPush();
		}catch (Exception $e){
			log_message('error', "Exception Error:". $e->getMessage());
		}
		return $result ;
	}
	
	function test_ios(){
		$devices = $this->input->post('device');// "461919aa9e8e6fed0a228580e66e94abdaf1f89c773fa03ecd084fa947771c9e" ;
		$msg = $this->input->post('msg');
		$ntype = $this->input->post('ntype');
		$result = "No result";
		if(!empty($devices)&&!empty($msg)){
			$result = $this->apple_push($devices, $msg) ;
			log_message( "debug","Result".json_encode($result)) ;
		}
		$this->load->view('push_form',array('device'=>$devices,'msg'=>$msg,'ntype'=>$ntype,'result'=>$result));
	}
	
	function test_android(){		
		$devices = $this->input->post('device');// "461919aa9e8e6fed0a228580e66e94abdaf1f89c773fa03ecd084fa947771c9e" ;
		$msg = $this->input->post('msg');
		$ntype = $this->input->post('ntype');
		$result = "No result";
		if(!empty($devices)&&!empty($msg)){
			$result = $this->android_push($devices, $msg, $ntype);
			log_message( "debug","Result".json_encode($result)) ;
		}
		$this->load->view('push_form',array('device'=>$devices,'msg'=>$msg,'ntype'=>$ntype,'result'=>$result));
	}
	
// 	public function test_ios(){
// 		$devices = "c5161c23dc05623ade905ea9ad16e8b267633d34085de365b766c8ee25a965d3" ;
// 		$msg = "Test ...  à¸«à¸�à¸«à¸�à¸«" ;
// 		$result = $this->apple_push($devices, $msg) ;
// 		echo json_encode($result) ;
// 	}
	
// 	public function test_ios2(){
// 		$deviceid = $this->input->get('deviceid') ;
// 		switch ($deviceid){
// 			case 1 : $device_token = "c5161c23dc05623ade905ea9ad16e8b267633d34085de365b766c8ee25a965d3" ; break ;
// 			case 2 : $device_token = "1719ad8759e939e60ad9bd5e76b35f5dc7bcaefd6377e6a3372b40f9452a6541" ; break ;
// 			case 3 : $device_token = "428271fd20eda88ba7cd678f72f390398e8336075e2be1778d1cb4dd2409b909" ; break ;
// 			case 4 : $device_token = "63946e5dc2c4bdfbe876bc9f3f9313a3bf00debe771acb137de9406311f25d29" ; break ;
// 		}
		
// 		$this->load->library('apn');
// 		$this->apn->payloadMethod = 'enhance'; // Ð²ÐºÐ»ÑŽÑ‡Ð¸Ñ‚Ðµ Ñ�Ñ‚Ð¾Ñ‚ Ð¼ÐµÑ‚Ð¾Ð´ Ð´Ð»Ñ� Ð¾Ñ‚Ð»Ð°Ð´ÐºÐ¸
// 		$this->apn->connectToPush();
		
// 		// adding custom variables to the notification
// // 		$this->apn->setData(array( 'someKey' => true ));
		
// 		$send_result = $this->apn->sendMessage($device_token, 'Test notif #1 (TIME:'.date('H:i:s').')', /*badge*/ 2, /*sound*/ 'default'  );
		
// 		if($send_result){
// 			log_message('debug','Sending successful');
// 		}else{
// 			log_message('error',$this->apn->error);
// 		}
	
// 		$this->apn->disconnectPush();
// 	}
	
// 	// designed for retreiving devices, on which app not installed anymore
// 	public function apn_feedback()
// 	{
// 		$this->load->library('apn');
	
// 		$unactive = $this->apn->getFeedbackTokens();
	
// 		if (!count($unactive))
// 		{
// 			log_message('info','Feedback: No devices found. Stopping.');
// 			return false;
// 		}
	
// 		foreach($unactive as $u)
// 		{
// 			$devices_tokens[] = $u['devtoken'];
// 		}
	
// 		/*
// 		 print_r($unactive) -> Array ( [0] => Array ( [timestamp] => 1340270617 [length] => 32 [devtoken] => 002bdf9985984f0b774e78f256eb6e6c6e5c576d3a0c8f1fd8ef9eb2c4499cb4 ) )
// 		 */
// 	}
	
		
}