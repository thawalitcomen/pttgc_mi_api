<?php
class Status extends CI_Controller {

	function __construct() {
		// Construct the parent class
		parent::__construct ();

		$this->load->model ( 'api_response_model' );
	}
	
	function index(){
		$data['datas'] =  $this->api_response_model->findAll();
// 		return http_helper::response($this, http_helper::STATUS_SUCCESS , $data) ;
		return $this->load->view('status', $data);
	}

	
}