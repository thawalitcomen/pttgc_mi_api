<?php

defined('BASEPATH') or exit('No direct script access allowed');

/* * ======================== BEGIN MIDEMO CONFIGURATION ==============* */

$config['timeout'] = '3600';
$config['server'] = '192.168.117.132';
$config['port'] = '389';
$config['binduser'] = 'midemo\Administrator';
$config['bindpw'] = 'fcD!1234';
$config['basedn'] = 'local';
$config['defaultdn'] = 'midemo';

$config ['domain_default'] = 'midemo';

$config ['midemo'] = array(
    'ldapServers' => 'ldaps://192.168.117.132:636',
    'domain' => 'midemo',
    'domain_dn' => 'dc=midemo,dc=local',
    'enterprise_name' => 'midemo',
    'user' => 'Administrator',
    'pass' => 'fcD!1234'
);

/* * ======================== END MIDEMO CONFIGURATION ==============**/
