<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',
 	//'hostname' => '172.17.31.108',
	//	'hostname' => 'localhost',
	// 'hostname' => 'thawalitdb.database.windows.net',
	// 'username' => 'thawalit@thawalitdb',
	'hostname' => 'localhost\sqlexpress',
	'username' => 'sa',
	'password' => 'P@ssw0rd',
	'database' => 'mi',
	'dbdriver' => 'sqlsrv',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_unicode_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

