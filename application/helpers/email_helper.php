<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of email_helper
 *
 * @author thawalit
 */
class email_helper {

    public static function send($subject, $message, $to, $mailType = 'html', $from = NULL, $fromName = NULL) {

        try {
            $CI = & get_instance();
            $CI->load->library('email');

            // Get full html:
            $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
                <title>' . html_escape($subject) . '</title>
                <style type="text/css">
                    body {
                        font-family: Arial, Verdana, Helvetica, sans-serif;
                        font-size: 16px;
                    }
                </style>
            </head>
            <body>
            ' . $message . '
            </body>
            </html>';


            $body = $message;

            log_message("debug", "attach path:" . getcwd());

            $mailer = $CI->email;
//            $mailer->SMTPAuth = False;
//            $cid = $mailer->attachment_cid($image);

            $CI->load->config("email") ;
            $from_default = $config = $CI->config->item("email_from");

            if(!empty($fromName)){
                $mailer->from(empty($from) ? $from_default : $from, empty($fromName) ? '' : $fromName);
            }else{
                $mailer->from(empty($from) ? $from_default : $from );
            }

//          $mailer->reply_to('mi@tmap-em.toyota-asia.com')    // Optional, an account where a human being reads.
            $mailer->to($to);
            $mailer->subject($subject);
            $mailer->message($body . "<br/><img src='cid:it_helpdesk.png' alt='it_helpdesk' />");
            $mailer->attach(getcwd() . "/data/it_helpdesk.png");

//            log_message("debug","cid:".$cid) ;

            $result = $mailer->send();
            //log_message("error", $CI->email->print_debugger());
            if ($result) {
                return true;
            }
        }catch (Exception $e){
            log_message("error","Send Email Failed!") ;
        }
        return false;
    }


    public static function sendLayout($subject, $message, $to, $mailType = 'html', $from = NULL, $fromName = NULL , $layout_image) {

        try {
            $CI = & get_instance();
            $CI->load->library('email');

            // Get full html:
            $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
                <title>' . html_escape($subject) . '</title>
                <style type="text/css">
                    body {
                        font-family: Arial, Verdana, Helvetica, sans-serif;
                        font-size: 16px;
                    }
                </style>
            </head>
            <body>
            ' . $message . '
            </body>
            </html>';


            $body = $message;

            log_message("debug", "attach pathXX:" . getcwd());

            $mailer = $CI->email;
//            $cid = $mailer->attachment_cid($image);

            $CI->load->config("email") ;
            $from_default = $config = $CI->config->item("email_from");

            if(!empty($fromName)){
                $mailer->from(empty($from) ? $from_default : $from, empty($fromName) ? '' : $fromName);
            }else{
                $mailer->from(empty($from) ? $from_default : $from );
            }

//          $mailer->reply_to('mi@tmap-em.toyota-asia.com')    // Optional, an account where a human being reads.
            $mailer->to($to);
            $mailer->subject($subject);
//            $mailer->message($body . "<br/><img src='cid:$layout_image' alt='layout' />");
//            $mailer->attach(getcwd() . "/data/room_property/$layout_image");
            $mailer->message($body . "<br/><img src='$layout_image' alt='layout' />");

//            log_message("debug","cid:".$cid) ;

            $result = $mailer->send();
            //log_message("error", $CI->email->print_debugger());
            if ($result) {
                return true;
            }
        }catch (Exception $e){
            log_message("error","Send Email Failed!") ;
//            log_message("error", $CI->email->print_debugger());
        }
        return false;
    }
}
