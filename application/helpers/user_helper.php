<?php

class user_helper {
	
	
// 	public static function getPassword($db_user_name ,$db_pass , $db_salt , $db_create_at){
		
// 		$pass_bin = hex2bin($db_pass) ;
		
// 		$pass_decrypted = crypt_helper::decrypt($pass_bin, $db_salt) ;
		
// 		$passWithDate = explode(":",$pass_decrypted) ;
		
// 		if(!empty($passWithDate)){
// 			return $passWithDate[0] ;
// 		}else{
// 			return null ;
// 		}

// 	}


// 	public static function getPassword($db_user_name ,$db_pass , $db_salt , $db_create_at){
		
// 		$pass =  base64_decode(hex2bin($db_pass),true) ;
// 		log_message("debug", "GET-PASSWORD:FROM ".$db_pass. " TO " . $pass) ;
// 		return $pass ;
	
// 	}
	
// 	public static function setPassword($pass , $db_salt){
		
// 		$pass_new = bin2hex(base64_encode($pass)) ;
// 		log_message("debug", "SET-PASSWORD:FROM ".$pass." TO ".$pass_new) ;
// 		return $pass_new ;
// 	}

	public static function getPassword($pass , $salt){
		
		$pass = self::decrypt($salt, $salt, $pass);
		log_message("debug", "GET-PASSWORD:FROM ".$salt. " TO " . $pass) ;
		return $pass ;
	
	}
	
	public static function setPassword($pass , $salt){
	
		$pass_new = self::encrypt($salt, $salt, $pass) ;
		log_message("debug", "SET-PASSWORD:FROM ".$pass." TO ".$pass_new) ;
		return $pass_new ;
	}
	
	public static function generateSalt(){
		return substr ( base64_encode ( openssl_random_pseudo_bytes ( 17 ) ), 0, 16 ) ;
	}
	
	public static function encrypt($key,$salt,$str) {
		$iv = $salt ;
		$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
		mcrypt_generic_init($td, $key , $iv);
		$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$pad = $block - (strlen($str) % $block);
		$str .= str_repeat(chr($pad), $pad);
		$encrypted = mcrypt_generic($td, $str);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		return base64_encode($encrypted);
	}
	
	public static function decrypt($key,$salt,$code) {
		$iv = $salt;
		$td = mcrypt_module_open('rijndael-128', '', 'cbc', '');
		mcrypt_generic_init($td, $key , $iv);
		$str = mdecrypt_generic($td, base64_decode($code));
		$block = mcrypt_get_block_size('rijndael-128', 'cbc');
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		return $str;
		//return $this->strippadding($str);
	}
	

	
}
