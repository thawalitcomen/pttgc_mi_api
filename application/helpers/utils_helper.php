<?php

class utils_helper
{

    public static function second_diff($start, $end)
    {

        $uts['start'] = strtotime($start);
        $uts['end'] = strtotime($end);

        return $uts['end'] - $uts['start'];
    }


    public static function formatMobile($mobile)
    {
        $mobileLength = strlen($mobile);
        if ($mobileLength >= 11) {  // +66-xx-yyyyyyyy , 66-xx-yyyyyyy
            return '0' . substr($mobile, -9);
        } elseif ($mobileLength == 10) { // 0-xx-yyyyyyy
            if ($mobile[0] !== '0') {
                return FALSE;
            } else {
                return $mobile;
            }
        } elseif ($mobileLength == 9) { // xx-yyyyyyy
            return '0' . $mobile;
        } else {
            return FALSE;
        }
    }

    public static function toMsisdn($mobile, $country_code = "+66", $dash_incluse = FALSE)
    {
        $mobileLength = strlen($mobile);
        if ($mobileLength == 10) {
            if ($mobile[0] == '0') {
                return substr($country_code, 1) . substr($mobile, 1);
            }
        } else if ($mobileLength == 11) {
            return $mobile;
        } else if ($mobileLength == 12) {
            return substr($mobile, 1);
        } else {
            return FALSE;
        }
    }

    /**
     * @param $mobile
     * send mobile format +668xxxxxxxx to (+66)81-234-5678
     */
    public static function toToyotaFormat($mobile, $country_code = "+66" )
    {
        log_message("debug", "mobile:$mobile");
        $orgMobile = self::toMsisdn($mobile);
        log_message("debug", "orgMobile:$orgMobile");
        $rtnMobile = "";
        $orgLen = strlen($orgMobile);
        //$rtnLen = $orgLen+2 ;
        for ($i = 0; $i < $orgLen; $i++) {
//            log_message("debug", "$i=$orgMobile[$i] ");
//            sleep(2) ;
            if ($i == 3 || $i == 6) {
                $rtnMobile .= $orgMobile[$i] . "-";
            } else {
                $rtnMobile .= $orgMobile[$i];
            }
        }

        $mobile_toyota = "($country_code)" . substr($rtnMobile, 2);
        for($i=0;$i<strlen($mobile_toyota);$i++){
            if($i==9 || $i==10 || $i==12 || $i==13 ){
                $mobile_toyota[$i] = "X" ;
            }
        }
        return $mobile_toyota ;
    }

    public static function get_mime_type($file)
    {

        // our list of mime types
        $mime_types = array(
            "pdf" => "application/pdf"
        , "exe" => "application/octet-stream"
        , "zip" => "application/zip"
        , "docx" => "application/msword"
        , "doc" => "application/msword"
        , "xls" => "application/vnd.ms-excel"
        , "ppt" => "application/vnd.ms-powerpoint"
        , "gif" => "image/gif"
        , "png" => "image/png"
        , "jpeg" => "image/jpg"
        , "jpg" => "image/jpg"
        , "mp3" => "audio/mpeg"
        , "wav" => "audio/x-wav"
        , "mpeg" => "video/mpeg"
        , "mpg" => "video/mpeg"
        , "mpe" => "video/mpeg"
        , "mov" => "video/quicktime"
        , "avi" => "video/x-msvideo"
        , "3gp" => "video/3gpp"
        , "css" => "text/css"
        , "jsc" => "application/javascript"
        , "js" => "application/javascript"
        , "php" => "text/html"
        , "htm" => "text/html"
        , "html" => "text/html"
        );

        $extension = strtolower(end(explode('.', $file)));

        return $mime_types[$extension];
    }


    public static function getHashtags($string)
    {
        $hashtags = FALSE;
        preg_match_all("/(#\w+):(\w+)/u", $string, $matches);
        if ($matches) {
            $hashtagsArray = array_count_values($matches[0]);
            $hashtags = array_keys($hashtagsArray);
        }

        return $hashtags;
    }

    static function startsWith($haystack, $needle)
    {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    static function endsWith($haystack, $needle)
    {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }


}
