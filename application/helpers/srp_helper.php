<?php

class srp_helper {

    const STATUS_WAIT = 0;
    const STATUS_VERIFIED = 1;
    const SHUFFLE_SERVERS = FALSE ;

    public static function generateRefCode($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        // $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i ++) {
            $string .= $characters [mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    public static function generateVerifyCode($length = 4) {
        $characters = '0123456789';
        $string = '';

        for ($i = 0; $i < $length; $i ++) {
            $string .= $characters [mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    public static function generateToken() {
        return md5(uniqid(rand(), true));
    }


    public static function get_ldap_connection($ctx, $domain, $user, $pass ,$identity=NULL) {
        //log_message("debug", "CTX :" . json_encode($ctx));
        $ctx->config->load('ldap');

        log_message("debug", "Domain from Login :" . strtolower($domain));
        if (empty($domain)) {
            $domain = $ctx->config->item('domain_default');
            log_message("debug", "Domain from Config :" . strtolower($domain));
        }
        //log_message("debug", "Config:" . json_encode($ctx->config->item(strtolower($domain))));
        $config = $ctx->config->item(strtolower($domain));
//        log_message("debug", "Ldap Config:" . json_encode($config));

        $ldapServers = $config["ldapServers"];
        log_message("debug", "multi-ldap servers :" . json_encode(is_array($ldapServers)));

        if (is_array($ldapServers)) {
            shuffle($ldapServers);

            log_message("debug", "multi-ldap servers :" . json_encode($ldapServers));
            foreach ($ldapServers as $ldapServer) {

                try {
                    $ldap_connection = ldap_connect($ldapServer) or die("Failed to connect to ldap server.");

                    if ($ldap_connection) {

                        ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
                        ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);
                        $bind = ldap_bind($ldap_connection, $user . '@' . $domain, $pass);

                        if ($bind) {
//                            log_message("error", "Cann't bind " . $ldapServer . " :: " . $user . '@' . $domain . " with pass [" . $pass . "], reason: " . ldap_error($ldap_connection));
//                            self::close_ldap_connection($ldap_connection);
//
//                            return FALSE;
//                        } else {

                            if ($identity !== NULL) {
                                $ldap_connection = self::checkIdentityNumber($ldap_connection, $config["domain_dn"], $user, $identity);
                                if ($ldap_connection) {
                                    return $ldap_connection;
                                } else {
                                    log_message("debug", "next server");
                                }
                            } else {
                                return $ldap_connection;
                            }

                        }else{
                            self::close_ldap_connection($ldap_connection);
                            log_message("error", "Cann't bind " . print_r( $ldapServer ,true )." reason: " . ldap_error($ldap_connection));
                        }
                    } else {
                        log_message("error", "Cann't connect " . $ldapServers);
                    }
                }catch (Exception $e){}

            }

            return FALSE;
        } else {

            $ldap_connection = ldap_connect($ldapServers) or die("Failed to connect to ldap server.");
            log_message("error", "ldap_connection=" . $ldap_connection);
            ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);
            if ($ldap_connection) {
                $bind = ldap_bind($ldap_connection, $user . '@' . $domain, $pass);
                log_message("error", "bind=" . $bind);
                if (!$bind) {
                    log_message("error", "Cann't bind " . $ldapServers . " :: " . $user . '@' . $domain . ", reason: " . ldap_error($ldap_connection));
                    self::close_ldap_connection($ldap_connection);

                    return FALSE;
                } else {


                    if($identity !== NULL) {
                        $ldap_connection = self::checkIdentityNumber($ldap_connection,$config["domain_dn"],$user,$identity) ;
                        if($ldap_connection){
                            return $ldap_connection ;
                        }
                    }else{
                        return $ldap_connection;
                    }

                }
            } else {
                log_message("error", "Cann't connect " . $ldapServers);
            }

            return FALSE;
        }
    }

    public static function close_ldap_connection($ldap_connection) {
        @ldap_close($ldap_connection);
    }

    public static function change_pass($ctx, $user, $new_pass, $domain = NULL) {

        $changePassResult = array("status" => FALSE, "message" => "Unknown Error");
        try {

            $ctx->config->load('ldap');

            log_message("debug", "Domain from Login :" . $domain);
            if (is_null($domain)) {
                $domain = $ctx->config->item('domain_default');
                log_message("debug", "Domain from Config :" . $domain);
            }
            log_message("debug", "Config:" . json_encode($ctx->config->item($domain)));
            $config = $ctx->config->item($domain);
            log_message("debug", "Ldap Config:" . json_encode($config));

            $ldapServers = $config["ldapServers"];
            log_message("debug", "multi-ldapservers :" . json_encode(is_array($ldapServers)));
            if (is_array($ldapServers)) {

                $resetCount = 0;
                $errors = [];
                foreach ($ldapServers as $ldapServer) {
                    //log_message ( "debug", "Multi Ldap servers");
                    $admin = $config["user"];
                    $pass = $config["pass"];
                    $domain_dn = $config["domain_dn"];

                    $connect_ldap_info = "\n=== LDAP CONFIG ===";
                    $connect_ldap_info .= "\ndomain:" . $domain;
                    $connect_ldap_info .= "\nldapServer:" . $ldapServer;
                    $connect_ldap_info .= "\nAdmin:" . $admin;
                    $connect_ldap_info .= "\nPass:" . $pass;
                    $connect_ldap_info .= "\nDomainDn:" . $domain_dn;

                    // $ldapServers,$domain,$user,$pass
                    $ldap_connection = self::get_ldap_connection($ctx, $domain, $admin, $pass);

                    log_message("debug", "LDAP Connect:" . (($ldap_connection) ? "Success" : "Failed" ));
                    if ($ldap_connection) {


                        $filter = "(sAMAccountName=$user)";
                        $result = ldap_search($ldap_connection, $domain_dn, $filter);
                        ldap_sort($ldap_connection, $result, "sn");
                        $info = ldap_get_entries($ldap_connection, $result);
                        log_message ( "debug", "LDAP Bind info:" . json_encode ( $info ) );


                        $userDn = $info[0]["distinguishedname"] [0];
                        $newPassword = $new_pass;
                        $newPassword = "\"" . $newPassword . "\"";
                        $len = strlen($newPassword);
                        $newPassw = '';
                        for ($i = 0; $i < $len; $i ++) {
                            $newPassw .= "{$newPassword{$i}}\000";
                        }
                        $newPassword = $newPassw;
                        $userdata ["unicodePwd"] = $newPassword;
                        $userdata ["lockoutTime"] = 0;

                        //log_message ( "debug", "New Password->" . $newPassword . " ::: " . json_encode ( $newPassword ) );
                        //log_message ( "debug", "New Password->" . $userdata ["unicodePwd"] . " ::: " . json_encode ( $userdata ["unicodePwd"] ) );
                        //log_message ( "debug", "UserData----->" . json_encode ( $userdata ) );
                        $result = ldap_mod_replace($ldap_connection, $userDn, $userdata);
                        $connect_ldap_info .= "\nReset Result :" . json_encode($result);
                        if ($result) {
                            // return msg(ldap_error($ldap_connection));
                            $resetCount++;
                        } else {
                            $errors[] = ldap_errno($ldap_connection) . ":" . ldap_error($ldap_connection);
                        }
                    }

                    $connect_ldap_info .= "\n=== END ===\n";
                    log_message("debug", "Connect Ldap :" . $connect_ldap_info);
                    self::close_ldap_connection($ldap_connection);
                }


                if ($resetCount <= 0) {
                    // return msg(ldap_error($ldap_connection));
                    $changePassResult ['status'] = FALSE;
                    $changePassResult ['message'] = $errors[0]; //implode("\n",$errors);
                } else {
                    $changePassResult ['status'] = TRUE;
                    $changePassResult ['message'] = "Success";
                }

                self::close_ldap_connection($ldap_connection);

                return $changePassResult;
            } else {


                $admin = $config["user"];
                $pass = $config["pass"];
                $domain_dn = $config["domain_dn"];

                $connect_ldap_info = "\n=== LDAP CONFIG ===";
                $connect_ldap_info .= "\ndomain:" . $domain;
                $connect_ldap_info .= "\nldapServers:" . $ldapServers;
                $connect_ldap_info .= "\nAdmin:" . $admin;
                $connect_ldap_info .= "\nPass:" . $pass;
                $connect_ldap_info .= "\nDomainDn:" . $domain_dn;

                // $ldapServers,$domain,$user,$pass
                $ldap_connection = self::get_ldap_connection($ctx, $domain, $admin, $pass);

                log_message("debug", "LDAP Connection:" . ($ldap_connection) ? "Success" : "Failed" );
                if ($ldap_connection) {


                    $filter = "(sAMAccountName=$user)";
                    $result = ldap_search($ldap_connection, $domain_dn, $filter);
                    ldap_sort($ldap_connection, $result, "sn");
                    $info = ldap_get_entries($ldap_connection, $result);
                    log_message("debug", "LDAP Bind info:" . json_encode($info));

                    /**
                     * comment for auto unlock
                     * $isLocked = isset($info[0]["lockoutTime"])?$info[0]["lockoutTime"]:0 ;
                     * if ($isLocked > 0 ) {
                     *
                     * $changePassResult['status'] = FALSE ;
                     * $changePassResult['message'] = 'account_locked' ;
                     * return $changePassResult;
                     * }
                     */
                    $userDn = $info[0]["distinguishedname"] [0];
                    $newPassword = $new_pass;
                    $newPassword = "\"" . $newPassword . "\"";
                    $len = strlen($newPassword);
                    $newPassw = '';
                    for ($i = 0; $i < $len; $i ++) {
                        $newPassw .= "{$newPassword{$i}}\000";
                    }
                    $newPassword = $newPassw;
                    $userdata ["unicodePwd"] = $newPassword;
                    $userdata ["lockoutTime"] = 0;

                    //log_message ( "debug", "New Password->" . $newPassword . " ::: " . json_encode ( $newPassword ) );
                    //log_message ( "debug", "New Password->" . $userdata ["unicodePwd"] . " ::: " . json_encode ( $userdata ["unicodePwd"] ) );
                    //log_message ( "debug", "UserData----->" . json_encode ( $userdata ) );
                    $result = ldap_mod_replace($ldap_connection, $userDn, $userdata);
                    $connect_ldap_info .= "\nReset Result :" . json_encode($result);
                    if (!$result) {
                        // return msg(ldap_error($ldap_connection));
                        $changePassResult ['status'] = FALSE;
                        $changePassResult ['message'] = ldap_errno($ldap_connection) . ":" . ldap_error($ldap_connection);
                    } else {
                        $changePassResult ['status'] = TRUE;
                        $changePassResult ['message'] = "Success";
                    }
                } else {
                    // return msg("wrong_admin");
                    $changePassResult ['status'] = FALSE;
                    $changePassResult ['message'] = "wrong_admin";
                }
                self::close_ldap_connection($ldap_connection);
                $connect_ldap_info .= "\n=== END ===\n";
                log_message("debug", "Connect Ldap :" . $connect_ldap_info);
                return $changePassResult;
            }
        } catch (Exception $e) {

            log_message("error", "Exception Error:" . $e-- > getMessage());
        } finally {
            self::close_ldap_connection($ldap_connection);
        }

        return $changePassResult;
    }

    public static function send_email($email, $user_name, $full_name, $format = "reset") {

        $header = "This email has been sent automatically by Self-reset Windows Password system.<br>" ;
        $header .= "======================================================================<br>";

        // if ($format === "reset") {
        //     $subject = "The password for your Windows account (" . $user_name . ") has been successfully reset.";
        //     $message = $header . "Dear " . $full_name . " ,<br><br><dd>" . $subject . "</dd><br><br>If you need additional help, please contact TMAP-EM Helpdesk<br><br>";
        // } else if ($format === "regis") {
        //     $subject = "You have successfully registered to Self-reset Windows Password system.";
        //     $message = $header . "Dear " . $full_name . " ,<br><br><dd>" . $subject . "</dd><br><br>If you need additional help, please contact TMAP-EM Helpdesk<br><br>";
        // } else if ($format === "update") {
        //     $subject = "Your information has been successfully updated.";
        //     $message = $header . "Dear " . $full_name . " ,<br><br><dd>" . $subject . "</dd><br><br>If you need additional help, please contact TMAP-EM Helpdesk<br><br>";
        // }

        if ($format === "reset") {
            $subject = "The password for your Windows account (" . $user_name . ") has been successfully reset.";
            $message = $header . "Dear " . $full_name . " ,<br><br><dd>" . $subject . "</dd><br><br>If you need additional help, please contact IT Helpdesk<br><br>";
        } else if ($format === "regis") {
            $subject = "You have successfully registered to Self-reset Windows Password system.";
            $message = $header . "Dear " . $full_name . " ,<br><br><dd>" . $subject . "</dd><br><br>If you need additional help, please contact IT Helpdesk<br><br>";
        } else if ($format === "update") {
            $subject = "Your information has been successfully updated.";
            $message = $header . "Dear " . $full_name . " ,<br><br><dd>" . $subject . "</dd><br><br>If you need additional help, please contact IT Helpdesk<br><br>";
        }

        return email_helper::send($subject, $message, $email);
    }


    public static function get_anonymous_ldap_connection($ctx, $domain, $user, $identity=NULL) {
        //log_message("debug", "CTX :" . json_encode($ctx));
        $ctx->config->load('ldap');

        log_message("debug", "Domain from Login :" . strtolower($domain));
        if (empty($domain)) {
            $domain = $ctx->config->item('domain_default');
            log_message("debug", "Domain from Config :" . strtolower($domain));
        }
        //log_message("debug", "Config:" . json_encode($ctx->config->item(strtolower($domain))));
        $config = $ctx->config->item(strtolower($domain));
//        log_message("debug", "Ldap Config:" . print_r($config,true));

        $ldapServers = $config["ldapServers"];
        log_message("debug", "multi-ldap servers :" . json_encode(is_array($ldapServers)) );

        if (is_array($ldapServers)) {
            shuffle($ldapServers);
            log_message("debug", "multi-ldap servers :" . json_encode($ldapServers));
            foreach ($ldapServers as $ldapServer) {
                try {

                    log_message("debug", "Try to connect " . $ldapServer);
                    $ldap_connection = ldap_connect($ldapServer) or die("Failed to connect to ldap server.");

                    if ($ldap_connection) {

                        ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
                        ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);

                        $bind = ldap_bind($ldap_connection, $config["user"] . "@" . $config["domain"], $config["pass"]);

                        if ($bind) {
//                        log_message("error", "Cann't bind " . print_r( $ldapServer ,true )." reason: " . ldap_error($ldap_connection));
//                        self::close_ldap_connection($ldap_connection);
//
//                        return FALSE;
//                    } else {
                            log_message("debug", "identity :" . $identity);
                            if ($identity !== NULL) {
                                $ldap_connection = self::checkIdentityNumber($ldap_connection, $config["domain_dn"], $user, $identity);
                                log_message("debug", "checkIdentityNumber :" . print_r( $ldap_connection , TRUE));
                                if ($ldap_connection) {
                                    return $ldap_connection;
                                } else {
                                    log_message("debug", "next server");
                                }
                            } else {
                                return $ldap_connection;
                            }
                        }else{
                            self::close_ldap_connection($ldap_connection);
                            log_message("error", "Cann't bind " . print_r( $ldapServer ,true )." reason: " . ldap_error($ldap_connection));
                        }
                    } else {
                        log_message("error", "Cann't connect " . $ldapServer);

                    }
                }catch (Exception $e){}
            }

            return FALSE;
        } else {

            $ldap_connection = ldap_connect($ldapServers) or die("Failed to connect to ldap server.");
            log_message("error", "ldap_connection=" . $ldap_connection);
            ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);
            if ($ldap_connection) {
                $bind = ldap_bind($ldap_connection,$config["user"]."@".$config["domain"] ,$config["pass"] );
                log_message("error", "bind=" . $bind);
                if (!$bind) {
                    log_message("error", "Cann't bind " . print_r($ldapServers ,true ) . ", reason: " . ldap_error($ldap_connection));
                    self::close_ldap_connection($ldap_connection);

                    return FALSE;
                } else {

                    log_message("debug", "identity :" . $identity );
                    if($identity !== NULL) {
                        $ldap_connection = self::checkIdentityNumber($ldap_connection,$config["domain_dn"],$user,$identity) ;
                        if($ldap_connection){
                            return $ldap_connection ;
                        }
                    }else{
                        return $ldap_connection;
                    }
                }
            } else {
                log_message("error", "Can not connect " . $ldapServers);
            }

            return FALSE;
        }
    }


    private static function checkIdentityNumber($ldap_connection , $domain_dn ,$userName, $identity ){

        $filter = "(sAMAccountName=$userName)";
        $result = ldap_search($ldap_connection,$domain_dn , $filter);

        ldap_sort($ldap_connection, $result, "sn");
        $entries = ldap_get_entries($ldap_connection, $result);
        //log_message("debug","checkIdentityNumber[user=|$userName|,dn=|$domain_dn|]-->".print_r($entries,true)) ;
        if(!$entries || $entries['count'] <= 0){
            return FALSE;
        }else {

            if ($entries['count'] > 0) {
                $odd = 0;
                foreach ($entries[0] AS $key => $value) {
                    if (0 === $odd % 2) {
                        //$ldap_columns[] = $key;
                        //                                 //log_message ( "debug", $key . ":" . json_encode($value));
                        if ($key === 'pager') {
                            //log_message("debug", "Pager:|" . $value['0'] . "| -> input identity|" . $identity . "|");
                            if (substr($identity, -5) === $value['0']) {
                                return $ldap_connection;
                            }
                        }
                    }
                    $odd++;
                }
            }

            return -1;
        }

    }


}
