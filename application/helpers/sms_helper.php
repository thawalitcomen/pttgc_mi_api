<?php
class sms_helper {
// 	public static function send_otp($context, $mobile, $otp) {
// 		$context->config->load ( 'sms' );
		
// 		$sms_config = $context->config->item ( 'sms' );
// 		log_message ( "debug", "sms_config=" . json_encode ( $sms_config ) );
		
// 		$post_fields = array ();
// 		foreach ( $sms_config ['params'] as $param ) {
			
// // 			$value = str_replace('{otp}' , $otp , $param ) ;
// // 			$value = str_replace('{msisdn}' , $mobile , $param ) ;
			
// // 			$post_fields [] = array (
// // 					$key => $value
// // 			);

// 			switch ($param){
// 				case 'otp' : $post_fields[] = array($param=>$otp) ; break;
// 				case 'msisdn' : $post_fields[] = array($param=>utils_helper::toMsisdn($mobile)) ; break;
// 				case 'lang' : $post_fields[] = array($param=>'2') ; break;
// 			}
// 		}
// 		log_message ( "debug", "post_fields=" . json_encode ( $post_fields ) );
		
// 		/* initial the curl object */
// 		$curl = curl_init ();
// 		curl_setopt ( $curl, CURLOPT_URL, $sms_config ['url'] );
// 		curl_setopt ( $curl, CURLOPT_POST, true );
// 		curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
// 		curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false );
// 		// curl_setopt($curl , CURLOPT_HTTPHEADER ,
// 		// array( "Content-Type: ".$this->contentType 
// 		// );
// 		curl_setopt ( $curl, CURLOPT_POSTFIELDS, json_encode ( $post_fields ) );
		
// 		/* send request */
// 		$send_result = array();//curl_exec ( $curl );
		
// 		log_message ( "debug", "Send Resut=" . json_encode ( $send_result ) );
// 	}
	
	
	public static function send_otp($context, $mobile, $otp , $ref) {
		
		
		$context->config->load ( 'sms' );
	
		$sms_enable = $context->config->item ( 'enable' , false ) ;
		log_message ( "debug", "Sms Enable=" . json_encode($sms_enable) );
		if($sms_enable){
			$sms_config = $context->config->item ( 'sms' );
			log_message ( "debug", "sms_config=" . json_encode ( $sms_config ) );
		
			
			$message = "" ;
			$context->load->model('config_model') ;
			$config_model = $context->config_model->findByKey('otp.message.en') ;
			if(!empty($config_model)){
				$message = $config_model->value ;
				log_message ( "debug", "org msg=" .$message);
				$message = str_replace('{otp}',$otp,$message) ;
				log_message ( "debug", "replace otp=" .$message);
				$message = str_replace('{ref}',$ref,$message) ;
				log_message ( "debug", "replace ref=" .$message);
			}
			$message = trim($message) ;
			
            $msisdn = utils_helper::toMsisdn($mobile) ;
            if( empty($msisdn) || empty($message)){
            	return FALSE;
            }
				
// 			$post_fields = array ();
// 			foreach ( $sms_config ['params'] as $param ) {
// 				switch ($param){
// 					case 'msg' : $post_fields[] = array($param=>$message) ; break;
// 					case 'msisdn' : $post_fields[] = array($param=>$msisdn) ; break;
// 				}
// 			}
// 			log_message ( "debug", "post_fields=" . json_encode ( $post_fields ) );

            $url = $sms_config['url'] . "?msisdn=".$msisdn."&msg=".urlencode($message);
// 			$url = $sms_config['url']  ;
            log_message ( "debug", "URL=" . $url );
            
			/* initial the curl object */
			$curl = curl_init ();
			curl_setopt ( $curl, CURLOPT_URL, $url );
			curl_setopt ( $curl, CURLOPT_POST,$sms_config['method']==='post'? true : false );
			curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false );
	
//			curl_setopt ( $curl, CURLOPT_POSTFIELDS, json_encode ( $post_fields ) );
		
			/* send request */
			$xml = curl_exec ( $curl );
			log_message ( "debug", "XML=" . $xml );
			$obj = new SimpleXMLElement($xml);
			$send_result = json_decode(json_encode($obj), TRUE);
			log_message ( "debug", "Send Resut=" . json_encode ( $send_result ) );
			$status = $send_result['status'] ;
			$detail = $send_result['detail'] ;
			log_message ( "debug", "Status=" .$status);
			log_message ( "debug", "Detail=" .$detail );
		
			return $send_result ;
	
		}
	}
}