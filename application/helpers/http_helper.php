<?php

class http_helper {

    const LOG_REQEUST = true;
    const LOG_RESPONSE = true;
    const LOG_RESPONSE_SUCCESS = false;
    const STATUS_SUCCESS = 0;
    const STATUS_INTERNAL_SERVER_ERROR = 500;
    const STATUS_UNKNOWN_ERROR = 501;
    const STATUS_EMPLOYEE_NOT_FOUND = 20001;
    const STATUS_UPLOAD_FAILED = 20002;
    const STATUS_INVALID_USERNAME = 20003;
    const STATUS_INVALID_PASSWORD = 20004;
    const STATUS_USER_EXPIRED = 20005;
    const STATUS_USER_NOT_FOUND = 20006;
    const STATUS_NOT_AUTHORIZED = 20007;
    const STATUS_LOGIN_LDAP_FAILED = 20008;
    const STATUS_MEETING_NOT_FOUND = 20009;
    const STATUS_ROOM_NOT_FOUND = 20010;
    const STATUS_INVALID_INPUT = 20011;
    const STATUS_ROOM_NOT_AVAILABLE = 20012;
    const STATUS_DEVICE_NOT_REGISTER = 20013;
    const STATUS_DATA_NOT_FOUND = 20014;
    const STATUS_INVALID_HEADER = 20015;
    const STATUS_EWS_ERROR = 20016;
    const STATUS_INVALID_USER_OR_PASSWORD = 20017;
    const STATUS_TOKEN_EXPIRED = 20018;
    const STATUS_REQUEST_MICORE_FAILED = 20019;
    const STATUS_CANCEL_MEETING_FAILED = 20020;
    const STATUS_OVER_LIMIT_FILE_SIZE = 20021;
    const STATUS_START_LESSTHAN_NOW = 20022;
    const STATUS_START_LESSTHAN_END = 20023;
    const STATUS_RESERVE_OVER_LIMIT = 20024;
    const STATUS_NOT_REGISTER_MOBILE = 20025;
    const STATUS_INVALID_MOBILE = 20026;
    const STATUS_MOBILE_ALREADY_EXIST = 20027;
    const STATUS_WAITNG_FOR_VERIFY_CODE = 20028;
    const STATUS_RECURRENCE_MOTHOD_NOT_IMPLEMENT = 20029;
    const STATUS_INVALID_OTP = 20030;
    const STATUS_INVALID_IDENTITY_NUMBER = 20031;
    const STATUS_INVALID_RESET_TOKEN = 20032;
    //const STATUS_DUPLICATE_MOBILE = 20033;
    const STATUS_RESET_PASSWORD_FAILED = 20034;
    const STATUS_INVALID_PASSWORD_POLICY_1 = 20035;
    const STATUS_INVALID_PASSWORD_POLICY_2 = 20036;
    const STATUS_INVALID_PASSWORD_POLICY_3 = 20037;
    const STATUS_USER_NOT_REGISTER = 20038;
    const STATUS_INVALID_CREDENTIAL = 20039;
    const STATUS_USER_ALREADY_REGISTER = 20040;

    public static function response($controller, $code, $data = null, $title = null, $message = null , $params = [] ) {

        $controller->load->model('api_response_model');

        $apiResponse = $controller->api_response_model->findByCode($code);
        if (empty($apiResponse)) {
            $response_code = self::STATUS_UNKNOWN_ERROR;
            $response_status = 'Unknown Error';
            $response_title = 'Result';
            $response_message = 'Unknown Error';
        } else {
            $response_code = $code;
            $response_status = $apiResponse->response_status;
            $response_title = empty($title) ?  $apiResponse->response_title : $title ;
            $response_message = empty($message) ? $apiResponse->response_message : $message;
        }

        if(!empty($params)){
            foreach ($params as $param) {
                $response_message = str_replace("{mobile}",$param["mobile"],$response_message) ;
            }
        }

        $json_response = array(
            'response_code' => intval($code),
            'response_status' => $response_status,
            'response_title' => $response_title,
            'response_message' => $response_message,
//            'response_title_th' => $response_title_th,
//            'response_message_th' => $response_message_th,
            'response_data' => $data
        );

        if (self::LOG_RESPONSE && ( intval($code) != 0 || self::LOG_RESPONSE_SUCCESS )) {
            $response_info = "\n==================== BEGIN RESPONSE =====================";
            $response_info .= "\n" . json_encode($json_response);
            $response_info .= "\n==================== END RESPONSE =====================";
            log_message("debug", $response_info);
        }

        $controller->response($json_response);
    }

    public static function requestHeaders($controller) {

// 		header("Access-Control-Allow-Methods: GET, OPTIONS");
// 		header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

        $reqHeaders = $controller->input->request_headers();


        $post_params = $controller->input->post();
//		log_message("debug", json_encode($post_params)) ;

        /*
          $lat = isset($reqHeaders['lat'])?$reqHeaders['lat']:null ;
          $long = isset($reqHeaders['long'])?$reqHeaders['long']:null ;
          $client_id = isset($reqHeaders['client_id'])?$reqHeaders['client_id']:null ;
          $client_type = isset($reqHeaders['client_type'])?$reqHeaders['client_type']:null ;
          $os = isset($reqHeaders['os'])?$reqHeaders['os']:null ;
          $os_version = isset($reqHeaders['os_version'])?$reqHeaders['os_version']:null ;
          $app_id = isset($reqHeaders['app_id'])?$reqHeaders['app_id']:null ;
          $app_version = isset($reqHeaders['app_version'])?$reqHeaders['app_version']:null ;
          $access_token = isset($reqHeaders['access_token'])?$reqHeaders['access_token']:null ;
          $verify_code = isset($reqHeaders['verify_code'])?$reqHeaders['verify_code']:null ;



          if(empty($client_id) || empty($client_type)){


          $lat = isset($reqHeaders['Lat'])?$reqHeaders['Lat']:null ;
          $long = isset($reqHeaders['Long'])?$reqHeaders['Long']:null ;
          $client_id = isset($reqHeaders['Client_id'])?$reqHeaders['Client_id']:null ;
          $client_type = isset($reqHeaders['Client_type'])?$reqHeaders['Client_type']:null ;
          $os = isset($reqHeaders['Os'])?$reqHeaders['Os']:null ;
          $os_version = isset($reqHeaders['Os_version'])?$reqHeaders['Os_version']:null ;
          $app_id = isset($reqHeaders['App_id'])?$reqHeaders['App_id']:null ;
          $app_version = isset($reqHeaders['App_version'])?$reqHeaders['App_version']:null ;
          $access_token = isset($reqHeaders['Access_token'])?$reqHeaders['Access_token']:null ;
          $verify_code = isset($reqHeaders['Verify_code'])?$reqHeaders['Verify_code']:null ;

          if(empty($client_id) || empty($client_type)){
          http_helper::response($controller,http_helper::STATUS_INVALID_HEADER) ;
          }
          }

         */
        /*
         *
          $lat = isset($reqHeaders['Lat'])?$reqHeaders['Lat']:null ;
          $long = isset($reqHeaders['Long'])?$reqHeaders['Long']:null ;
          $client_id = isset($reqHeaders['Client_id'])?$reqHeaders['Client_id']:null ;
          $client_type = isset($reqHeaders['Client_type'])?$reqHeaders['Client_type']:null ;
          $os = isset($reqHeaders['Os'])?$reqHeaders['Os']:null ;
          $os_version = isset($reqHeaders['Os_version'])?$reqHeaders['Os_version']:null ;
          $app_id = isset($reqHeaders['App_id'])?$reqHeaders['App_id']:null ;
          $app_version = isset($reqHeaders['App_version'])?$reqHeaders['App_version']:null ;
          $access_token = isset($reqHeaders['Access_token'])?$reqHeaders['Access_token']:null ;
          $verify_code = isset($reqHeaders['Verify_code'])?$reqHeaders['Verify_code']:null ;
         */

        $lat = $controller->input->get_request_header('lat', TRUE);
        $long = $controller->input->get_request_header('long', TRUE);
        $client_id = $controller->input->get_request_header('client-id', TRUE);
        $client_type = $controller->input->get_request_header('client-type', TRUE);
        $os = $controller->input->get_request_header('os', TRUE);
        $os_version = $controller->input->get_request_header('os-version', TRUE);
        $app_id = $controller->input->get_request_header('app-id', TRUE);
        $app_version = $controller->input->get_request_header('app-version', TRUE);
        $access_token = $controller->input->get_request_header('access-token', TRUE);
        $verify_code = $controller->input->get_request_header('verify-code', TRUE);
        $push_token = $controller->input->get_request_header('push-token', TRUE);
        $otp_token = $controller->input->get_request_header('otp-token', TRUE);
        $reset_token = $controller->input->get_request_header('reset-token', TRUE);


        if ("WB" != $client_type && "TB" != $client_type) {
            if (empty($client_id) || empty($client_type)) {
                http_helper::response($controller, http_helper::STATUS_INVALID_HEADER);
            }
        } else {
            if (empty($client_id)) {
                $client_id = http_helper::create_guid(( 'Y-m-d H:i:s'));
            }
        }



        $headers = array('lat' => $lat,
            'long' => $long,
            'client_id' => $client_id,
            'client_type' => $client_type,
            "os" => $os,
            'os_version' => $os_version,
            'app_id' => $app_id,
            'app_version' => $app_version,
            'access_token' => $access_token,
            'verify_code' => $verify_code,
            'push_token' => $push_token,
            'otp_token' => $otp_token,
            'reset_token' => $reset_token
        );


        if (self::LOG_REQEUST) {
            $request_info = "\n==================== BEGIN REQUEST =====================";
            $request_info .= "\nURI:" . $controller->input->server("REQUEST_URI");
            $request_info .= "\nHEADER:" . json_encode($controller->input->request_headers());
            $request_info .= "\nPOST:";
            if ($controller->input->post()) {
                $request_info .= json_encode($controller->input->post(NULL, TRUE));

// 				$inputStr = $controller->input->post(NULL, TRUE) ;
// 				if ( base64_encode(base64_decode($inputStr, true)) === $inputStr){
// 					$request_info .= "***[Base64 Image]***" ;
// 				}else{
// 					$request_info .= json_encode($inputStr) ;
// 				}
            } else {
                $request_info .= "===NONE===";
            }
            $request_info .= "\nGET:";
            if ($controller->input->get()) {
                $request_info .= json_encode($controller->input->get(NULL, TRUE));
            } else {
                $request_info .= "===NONE===";
            }
            $request_info .= "\n=================== END REQUEST =======================\n";
            log_message("debug", $request_info);
        }

        return $headers;
    }

    public static function getClientModel($controller, $reqHeaders) {

        $client_id = $reqHeaders['client_id'];
        $lat = $reqHeaders['lat'];
        $long = $reqHeaders['long'];
        $app_id = $reqHeaders['app_id'];
        $type = $reqHeaders['client_type'];
        $os = $reqHeaders['os'];
        $os_version = $reqHeaders['os_version'];
        $app_version = $reqHeaders['app_version'];
        $push_token = $reqHeaders['push_token'];

        $clientModel = $controller->client_model->findByUuid($client_id, $lat, $long, $app_id, $type, $os, $os_version, $app_version, $push_token);
        if (empty($clientModel)) {
            log_message("error", "Find Client By UUID Failed!!! $client_id  , $lat,$long,$app_id, $type ,$os, $os_version, $app_version, $push_token");
            self::response($controller, http_helper::STATUS_INVALID_INPUT);
        }
        return $clientModel;
    }

    public static function create_guid($namespace = '') {
        static $guid = '';
        $uid = uniqid("", true);
        $data = $namespace;
        $data .= $_SERVER['REQUEST_TIME'];
        $data .= $_SERVER['HTTP_USER_AGENT'];
// 		$data .= $_SERVER['LOCAL_ADDR'];
// 		$data .= $_SERVER['LOCAL_PORT'];
        $data .= $_SERVER['REMOTE_ADDR'];
        $data .= $_SERVER['REMOTE_PORT'];
        $hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
        $guid = substr($hash, 0, 8) .
                '-' .
                substr($hash, 8, 4) .
                '-' .
                substr($hash, 12, 4) .
                '-' .
                substr($hash, 16, 4) .
                '-' .
                substr($hash, 20, 12)
        ;
        return $guid;
    }

}
